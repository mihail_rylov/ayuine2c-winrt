#ifndef CAMERACONTROLLER_HPP
#define CAMERACONTROLLER_HPP

#include "Editor.hpp"

#include <QObject>
#include <QPoint>
#include <QRect>
#include <Math/Vec3.hpp>
#include <Math/Ray.hpp>
#include <Math/Camera.hpp>

class QWidget;
class QMouseEvent;
class QKeyEvent;

class Frame;

class EDITOR_EXPORT CameraController : public QObject
{
  Q_OBJECT

public:
  explicit CameraController(QWidget *parent = 0);
  ~CameraController();

public:
  const vec3& origin() const;
  void setOrigin(const vec3& origin);

  const vec3& offset() const;
  void setOffset(const vec3& offset);

  float zoom() const;
  void setZoom(float zoom);

  virtual void modelOnSphere(const sphere& center);

  void setMinZoom(float minZoom);
  void setMaxZoom(float maxZoom);

public:
  virtual bool loadSettings();
  virtual bool saveSettings(bool save = false);
  virtual bool updateCamera(QRect& viewport, Camera& camera) = 0;
  virtual vec3 screenToWorld(QPoint point) = 0;
  virtual QPoint worldToScreen(const vec3& point) = 0;
  virtual ray direction(QPoint point) = 0;

  virtual QStringList toStringList() const;

public:
  bool updateFrame(Frame& frame);

protected:
  virtual bool mousePress(QWidget* widget, QMouseEvent& event);
  virtual bool mouseRelease(QWidget* widget, QMouseEvent& event);
  virtual bool mouseDblClick(QWidget* widget, QMouseEvent& event);
  virtual bool mouseMove(QWidget* widget, QMouseEvent& event);
  virtual bool mouseLost(QWidget* widget, QMouseEvent& event);
  virtual bool keyPress(QWidget* widget, QKeyEvent& event);

protected:
  virtual bool event(QEvent *event);
  virtual bool eventFilter(QObject *object, QEvent *event);

protected:
  vec3 m_origin;
  vec3 m_offset;
  float m_zoom;
  QPoint m_mouse;
  float m_minZoom;
  float m_maxZoom;
};

#endif // CAMERACONTROLLER_HPP
