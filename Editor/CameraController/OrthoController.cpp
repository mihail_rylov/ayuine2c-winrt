#include "OrthoController.hpp"
#include <QWidget>
#include <QMouseEvent>
#include <QKeyEvent>

OrthoController::OrthoController(QWidget *parent) :
  CameraController(parent)
{
  m_axis = Axis::X;
  m_minZoom = 0.01f;
  m_maxZoom = 1000.0f;
  m_zoom = 80.0f;
  m_linearKeyVelocity = 0.5f;
  m_zoomFactor = 4.0f;
}

Axis::Enum OrthoController::axis() const
{
  return m_axis;
}

void OrthoController::setAxis(Axis::Enum axis)
{
  m_axis = axis;
}

float OrthoController::linearKeyVelocity() const
{
  return m_linearKeyVelocity;
}

void OrthoController::setLinearKeyVelocity(float linearKeyVelocity)
{
  m_linearKeyVelocity = linearKeyVelocity;
}

vec3 OrthoController::screenToWorld(QPoint point)
{
  QRect viewport;
  Camera camera;
  if(!updateCamera(viewport, camera))
    return vec3();

  return camera.origin
      + camera.right * ((point.x() - viewport.width() * 0.5f) / m_zoom)
      - camera.up * ((point.y() - viewport.height() * 0.5f) / m_zoom);
}

QPoint OrthoController::worldToScreen(const vec3 &point)
{
  QRect viewport;
  Camera camera;
  if(!updateCamera(viewport, camera))
    return QPoint();

  vec3 delta = point - camera.origin;
  return QPoint((delta ^ camera.right) * m_zoom + viewport.width() * 0.5f,
                (delta ^ camera.up) * -m_zoom + viewport.height() * 0.5f);
}

ray OrthoController::direction(QPoint point)
{
  QRect viewport;
  Camera camera;
  if(!updateCamera(viewport, camera))
    return ray();

  return ray(screenToWorld(point) - camera.at * 4096.0f, camera.at);
}

bool OrthoController::updateCamera(QRect &viewport, Camera &camera)
{
  if(QWidget* widget = qobject_cast<QWidget*>(parent()))
  {
    viewport = widget->geometry();
    Camera::orthoCamera(camera, m_origin, m_axis, m_zoom, viewport.width(), viewport.height());
    return true;
  }
  return false;
}

bool OrthoController::mousePress(QWidget *widget, QMouseEvent &event)
{
  if(QWidget::mouseGrabber() != widget)
    return false;

  if(event.buttons() & Qt::RightButton)
  {
    widget->grabMouse();
    widget->setFocus();
    m_mouse = event.globalPos();
    return true;
  }
  return false;
}

bool OrthoController::mouseRelease(QWidget *widget, QMouseEvent &event)
{
  if(QWidget::mouseGrabber() != widget)
    return false;
  if(event.buttons() & Qt::RightButton)
    return false;
  widget->releaseMouse();
  return true;
}

bool OrthoController::mouseMove(QWidget *widget, QMouseEvent &event)
{
  if(event.buttons() & Qt::RightButton)
  {
    QPoint delta = event.globalPos() - m_mouse;
    QCursor::setPos(m_mouse);

    float s = (float)delta.x() / widget->width();
    float t = - (float)delta.y() / widget->height();

    if(event.buttons() & Qt::LeftButton)
    {
      // Oblicz wspolczynnik
      float factor = m_zoomFactor * t;

      // Zastosuj powiekszenie/pomniejszenie
      m_zoom *= powf(2, factor);
      m_zoom = qBound(m_zoom, m_minZoom, m_maxZoom);
    }
    else
    {
      // Wyznacz wektory styczne
      QRect viewport;
      Camera camera;
      if(!updateCamera(viewport, camera))
        return false;

      // Przesu� siatk�
      m_origin -= camera.right * ((float)delta.x() / m_zoom);
      m_origin += camera.up * ((float)delta.y() / m_zoom);
    }

    widget->update();
    return true;
  }

  return false;
}

bool OrthoController::mouseLost(QWidget *widget, QMouseEvent &event)
{
  widget->releaseMouse();
  return true;
}

bool OrthoController::keyPress(QWidget *widget, QKeyEvent &event)
{
  QRect viewport;
  Camera camera;
  if(!updateCamera(viewport, camera))
    return false;

  switch(event.key())
  {
  case Qt::Key_Up:
    m_origin += camera.up * linearKeyVelocity();
    break;

  case Qt::Key_Down:
    m_origin -= camera.up * linearKeyVelocity();
    break;

  case Qt::Key_Left:
    m_origin -= camera.right * linearKeyVelocity();
    break;

  case Qt::Key_Right:
    m_origin += camera.right * linearKeyVelocity();
    break;

  default:
    return false;
  }

  widget->update();
  return true;
}

float OrthoController::zoomFactor() const
{
  return m_zoomFactor;
}

void OrthoController::setZoomFactor(float zoomFactor)
{
  m_zoomFactor = zoomFactor;
}
