HEADERS += \
    CameraController/PerspectiveController.hpp \
    CameraController/OrthoController.hpp \
    CameraController/ModelViewController.hpp \
    CameraController/FirstPersonController.hpp \
    CameraController/CameraController.hpp

SOURCES += \
    CameraController/PerspectiveController.cpp \
    CameraController/OrthoController.cpp \
    CameraController/ModelViewController.cpp \
    CameraController/FirstPersonController.cpp \
    CameraController/CameraController.cpp

INCLUDEPATH += $$PWD
