#ifndef FIRSTPERSONCONTROLLER_HPP
#define FIRSTPERSONCONTROLLER_HPP

#include "PerspectiveController.hpp"

class EDITOR_EXPORT FirstPersonController : public PerspectiveController
{
  Q_OBJECT
public:
  explicit FirstPersonController(QWidget *parent = 0);

protected:
  QString section() const;

protected:
  bool mousePress(QWidget* widget, QMouseEvent& event);
  bool mouseRelease(QWidget* widget, QMouseEvent& event);
  bool mouseMove(QWidget* widget, QMouseEvent& event);
  bool mouseLost(QWidget* widget, QMouseEvent& event);
  bool keyPress(QWidget* widget, QKeyEvent& event);

public:
  float angularKeyVelocity() const;
  void setAngularKeyVelocity(float angularKeyVelocity);

  float linearKeyVelocity() const;
  void setLinearKeyVelocity(float linearKeyVelocity);

public:
  bool updateCamera(QRect& viewport, Camera& camera);  
  void modelOnSphere(const sphere &center);

protected:
  float m_angularKeyVelocity;
  float m_linearKeyVelocity;
};

#endif // FIRSTPERSONCONTROLLER_HPP
