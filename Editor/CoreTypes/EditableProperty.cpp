#include "EditableProperty.hpp"
#include "ObjectProperty.hpp"
#include "ResourceProperty.hpp"
#include "PropertyEditor/EnumProperty.h"

#include <QMetaProperty>

#include <Core/ClassInfo.hpp>
#include <Core/Resource.hpp>

static QHash<QByteArray, const QMetaObject*> &propertyTypes()
{
  static QHash<QByteArray, const QMetaObject*> properties;
  return properties;
}

EditableProperty::EditableProperty(const QMetaObject* metaObject, const QString& name, QObject* propertyObject, QObject* parent)
  : Property(name, propertyObject, parent)
{
  for(int i = metaObject->propertyOffset(); i < metaObject->propertyCount(); ++i)
  {
    QMetaProperty metaProperty = metaObject->property(i);

    if(metaProperty.isDesignable(this))
    {
      if(Property* property = createPropertyType(metaProperty, this, this))
      {
        m_properties.append(property);
      }
    }
  }
}

QVariant EditableProperty::value(int role) const
{
  QVariant data = Property::value(role);

  if(data.isValid() && role != Qt::UserRole)
  {
    switch (role)
    {
    case Qt::DisplayRole:
      return QString("[ %1 ]").arg(valueList().join(", "));

    case Qt::EditRole:
      return QString("%1").arg(valueList().join(", "));
    };
  }

  return data;
}

void EditableProperty::setValue(const QVariant &value)
{
  if(value.type() == QVariant::String)
  {
    QStringList values;

    QString v = value.toString();
    QRegExp rx("([+-]?([0-9]*[\\.,])?[0-9]+(e[+-]?[0-9]+)?)");
    rx.setCaseSensitivity(Qt::CaseInsensitive);

    int pos = 0;
    while ((pos = rx.indexIn(v, pos)) != -1)
    {
      values.append(rx.cap(1));
      pos += rx.matchedLength();
    }

    setValueList(values);
  }
  else
  {
    Property::setValue(value);
  }
}

QStringList EditableProperty::valueList() const
{
  QStringList values;

  foreach(Property* property, m_properties)
  {
    values.append(property->value(Qt::DisplayRole).toString());
  }

  return values;
}

void EditableProperty::setValueList(const QStringList &values)
{
  unsigned index = 0;
  foreach(Property* property, m_properties)
  {
    if(index >= values.size())
      continue;
    property->setValue(values[index++]);
    // setProperty(qPrintable(property->objectName()), values[index++]);
  }
}

Property * EditableProperty::createPropertyType(const QMetaProperty& property, QObject *propertyObject, QObject *parent)
{
  if(property.isEnumType())
  {
    return new EnumProperty(property.enumerator(), property.name(), propertyObject, parent);
  }
  else if(const QMetaObject* metaObject = propertyTypes()[property.typeName()])
  {
    return (Property*)metaObject->newInstance(
          Q_ARG(QString, property.name()),
          Q_ARG(QObject*, propertyObject),
          Q_ARG(QObject*, parent));
  }
  else if(QByteArray(property.typeName()).endsWith("Ref"))
  {
    QByteArray typeName = property.typeName();
    typeName.chop(3);

    if(const QMetaObject* metaObject = ClassInfo::getMetaObject(typeName.constData(), &Resource::staticMetaObject))
    {
      return new ResourceProperty(metaObject, property.userType(), property.name(), propertyObject, parent);
    }
    else if(const QMetaObject* metaObject = ClassInfo::getMetaObject(typeName.constData()))
    {
      return new ObjectProperty(metaObject, property.userType(), property.name(), propertyObject, parent);
    }
  }

  return new Property(property.name(), propertyObject, parent);
}

Property * EditableProperty::createPropertyType(const QByteArray &typeName, const QString &name, QObject *propertyObject, QObject *parent)
{
  if(const QMetaObject* metaObject = propertyTypes()[typeName])
  {
    return (Property*)metaObject->newInstance(
          Q_ARG(QString, name),
          Q_ARG(QObject*, propertyObject),
          Q_ARG(QObject*, parent));
  }  
  else if(typeName.endsWith("Ref"))
  {
    if(const QMetaObject* metaObject = ClassInfo::getMetaObject(typeName.left(typeName.length()-3), &Resource::staticMetaObject))
    {
      return new ResourceProperty(metaObject, QMetaType::type(typeName.constData()), name, propertyObject, parent);
    }
    else if(const QMetaObject* metaObject = ClassInfo::getMetaObject(typeName.left(typeName.length()-3)))
    {
      return new ObjectProperty(metaObject, QMetaType::type(typeName.constData()), name, propertyObject, parent);
    }
  }

  return new Property(name, propertyObject, parent);
}

Property * EditableProperty::createPropertyType(const QString& name, QObject* propertyObject, Property* parent)
{
  if(!propertyObject)
    return NULL;

  QVariant value = propertyObject->property(qPrintable(name));
  if(value.isNull())
    return NULL;

  return createPropertyType(value.typeName(), name, propertyObject, parent);
}

bool EditableProperty::registerPropertyType(const QByteArray &typeName, const QMetaObject *metaObject)
{
  if(propertyTypes().contains(typeName))
    return false;
  propertyTypes()[typeName] = metaObject;
  return true;
}

void EditableProperty::unregisterPropertyType(const QByteArray &typeName, const QMetaObject *metaObject)
{
  if(!propertyTypes().contains(typeName))
    return;
  if(propertyTypes()[typeName] != metaObject)
    return;
  propertyTypes().remove(typeName);
}
