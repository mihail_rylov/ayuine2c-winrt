#ifndef RESOURCEPROPERTY_HPP
#define RESOURCEPROPERTY_HPP

#include "ObjectProperty.hpp"

class Resource;
class QMenu;

class ResourceProperty : public ObjectProperty
{
  Q_OBJECT
  Q_PROPERTY(QString resourceName READ resourceName WRITE setResourceName DESIGNABLE true)
  Q_PROPERTY(bool isLocal READ isLocal WRITE setLocal DESIGNABLE true)
  Q_PROPERTY(QString fileName READ fileName DESIGNABLE true)

public:
  Q_INVOKABLE explicit ResourceProperty(const QMetaObject* resourceType, int userType, const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  void setValue(const QVariant& value);

  QString resourceName() const;
  void setResourceName(const QString& name);

  bool isLocal() const;
  void setLocal(bool isLocal);

  QString fileName() const;

  QSharedPointer<Resource> resource() const;
  QSharedPointer<Resource> resource(const QVariant& value) const;
  void setResource(const QSharedPointer<Resource>& resource);

  virtual QMenu* createMenu() const;

public slots:
  void pick();
  void edit();
  void unlink();
};

#endif // RESOURCEPROPERTY_HPP
