#ifndef VEC3PROPERTY_HPP
#define VEC3PROPERTY_HPP

#include "EditableProperty.hpp"

class Vec3Property : public EditableProperty
{
  Q_OBJECT
  Q_PROPERTY(float x READ x WRITE setX DESIGNABLE true)
  Q_PROPERTY(float y READ y WRITE setY DESIGNABLE true)
  Q_PROPERTY(float z READ z WRITE setZ DESIGNABLE true)

public:
  Q_INVOKABLE explicit Vec3Property(const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  float x() const;
  void setX(float x);
  float y() const;
  void setY(float y);
  float z() const;
  void setZ(float z);
};

#endif // VEC3PROPERTY_HPP
