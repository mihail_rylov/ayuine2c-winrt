#ifndef BOXPROPERTY_HPP
#define BOXPROPERTY_HPP

#include "EditableProperty.hpp"

#include <Math/Vec3.hpp>

class BoxProperty : public EditableProperty
{
  Q_OBJECT
  Q_PROPERTY(vec3 mins READ mins WRITE setMins DESIGNABLE true)
  Q_PROPERTY(vec3 maxs READ maxs WRITE setMaxs DESIGNABLE true)

public:
  Q_INVOKABLE explicit BoxProperty(const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  vec3 mins() const;
  void setMins(vec3 mins);
  vec3 maxs() const;
  void setMaxs(vec3 maxs);
};

#endif // BOXPROPERTY_HPP
