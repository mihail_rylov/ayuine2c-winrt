#include "SphereProperty.hpp"

#include <Math/Sphere.hpp>

Q_IMPLEMENT_PROPERTYTYPE(sphere, SphereProperty)

SphereProperty::SphereProperty(const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(&SphereProperty::staticMetaObject, name, propertyObject, parent)
{
}

vec3 SphereProperty::center() const
{
  return value().value<sphere>().Center;
}

void SphereProperty::setCenter(vec3 center)
{
  setValue(QVariant::fromValue(sphere(center, radius())));
}

float SphereProperty::radius() const
{
  return value().value<sphere>().Radius;
}

void SphereProperty::setRadius(float radius)
{
  setValue(QVariant::fromValue(sphere(center(), radius)));
}
