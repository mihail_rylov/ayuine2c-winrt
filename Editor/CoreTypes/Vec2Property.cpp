#include "Vec2Property.hpp"

#include <Math/Vec2.hpp>

Q_IMPLEMENT_PROPERTYTYPE(vec2, Vec2Property)

Vec2Property::Vec2Property(const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(&Vec2Property::staticMetaObject, name, propertyObject, parent)
{
}

float Vec2Property::x() const
{
  return value().value<vec2>().X;
}

void Vec2Property::setX(float x)
{
  setValue(QVariant::fromValue(vec2(x, y())));
}

float Vec2Property::y() const
{
  return value().value<vec2>().Y;
}

void Vec2Property::setY(float y)
{
  setValue(QVariant::fromValue(vec2(x(), y)));
}
