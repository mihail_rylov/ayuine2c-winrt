#ifndef EDITABLEPROPERTY_HPP
#define EDITABLEPROPERTY_HPP

#include "Editor.hpp"

#include <PropertyEditor/Property.h>

class EDITOR_EXPORT EditableProperty : public Property
{
  Q_OBJECT

public:
  explicit EditableProperty(const QMetaObject* metaObject, const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  QVariant value(int role = Qt::UserRole) const;
  void setValue(const QVariant& value);

protected:
  virtual QStringList valueList() const;
  virtual void setValueList(const QStringList& values);

public:
  static Property* createPropertyType(const QMetaProperty& property, QObject* propertyObject = 0, QObject* parent = 0);
  static Property* createPropertyType(const QByteArray& typeName, const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);
  static Property* createPropertyType(const QString& name, QObject* propertyObject, Property* parent);
  static bool registerPropertyType(const QByteArray& typeName, const QMetaObject* metaObject);
  static void unregisterPropertyType(const QByteArray& typeName, const QMetaObject* metaObject);

private:
  QVector<Property*> m_properties;
};

#define Q_IMPLEMENT_PROPERTYTYPE(Type, PropertyType)  \
  static int register##Type##PropertyType() { EditableProperty::registerPropertyType(QMetaType::typeName(qMetaTypeId<Type>()), &PropertyType::staticMetaObject); return 0; } \
  static int unregister##Type##PropertyType() { EditableProperty::unregisterPropertyType(QMetaType::typeName(qMetaTypeId<Type>()), &PropertyType::staticMetaObject); return 0; } \
  Q_CONSTRUCTOR_FUNCTION(register##Type##PropertyType)  Q_DESTRUCTOR_FUNCTION(unregister##Type##PropertyType)

#endif // EDITABLEPROPERTY_HPP
