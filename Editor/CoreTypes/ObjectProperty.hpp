#ifndef OBJECTPROPERTY_HPP
#define OBJECTPROPERTY_HPP

#include "EditableProperty.hpp"

class QMenu;

class ObjectProperty : public EditableProperty
{
  Q_OBJECT

public:
  Q_INVOKABLE explicit ObjectProperty(const QMetaObject* metaObject, const QMetaObject* objectType, int userType, const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);
  Q_INVOKABLE explicit ObjectProperty(const QMetaObject* objectType, int userType, const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);
  ~ObjectProperty();

public:
  void setValue(const QVariant& value);

  QSharedPointer<QObject> object() const;
  QSharedPointer<QObject> object(const QVariant& value) const;
  void setObject(const QSharedPointer<QObject>& object);

  QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem& option);
  bool setEditorData(QWidget *editor, const QVariant &data);

  void update();

  bool hasChildren() const;
  void populateChildren();

  virtual QMenu* createMenu() const;

  bool mimeData(QMimeData *mimeData);
  bool dropMimeData(const QMimeData *mimeData, Qt::DropAction dropAction);
  bool supportsDrop() const;
  bool supportsDrag() const;

protected:
  QStringList valueList() const;

public slots:
  virtual void unlink();

protected:
  const QMetaObject* m_objectType;
  int m_userType;
  QScopedPointer<QMenu> m_menu;
  bool m_childrenPopulated;
  QVector<Property*> m_properties;
};

#endif // OBJECTPROPERTY_HPP
