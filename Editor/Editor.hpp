#ifndef EDITOR_HPP
#define EDITOR_HPP

#include <QtCore>

#ifdef EDITOR_EXPORTS
#define EDITOR_EXPORT Q_DECL_EXPORT
#else
#define EDITOR_EXPORT Q_DECL_IMPORT
#endif

#endif // EDITOR_HPP
