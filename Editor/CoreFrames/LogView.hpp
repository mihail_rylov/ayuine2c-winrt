#ifndef LOGVIEW_HPP
#define LOGVIEW_HPP

#include <QWidget>

#include "Editor.hpp"

namespace Ui {
class LogView;
}

class EDITOR_EXPORT LogView : public QWidget
{
  Q_OBJECT
  
public:
  explicit LogView(QWidget *parent = 0);
  ~LogView();

private:
  static void messageOutput(QtMsgType msgType, const char* message);
  
private:
  Ui::LogView *ui;

private:
  LogView* otherView;
};

#endif // LOGVIEW_HPP
