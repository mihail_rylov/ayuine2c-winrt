#ifndef METAOBJECTITEMMODEL_HPP
#define METAOBJECTITEMMODEL_HPP

#include <QAbstractItemModel>
#include <QSet>

#include "Editor.hpp"

class EDITOR_EXPORT MetaObjectItemModel : public QAbstractItemModel
{
  Q_OBJECT
public:
  explicit MetaObjectItemModel(const QMetaObject* rootItem, QObject *parent = 0);
  ~MetaObjectItemModel();

public:
  QModelIndex index(int row, int column,
                            const QModelIndex &parent = QModelIndex()) const;
  QModelIndex parent(const QModelIndex &child) const;
  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex &index, const QVariant &value, int role);
  Qt::ItemFlags flags(const QModelIndex &index) const;
  QMimeData *mimeData(const QModelIndexList &indexes) const;

  bool checkable() const;
  void setCheckable(bool checkable);

  QSet<const QMetaObject*> checked() const;
  bool isChecked(const QMetaObject* metaObject);

signals:
  void checkedChanged(const QMetaObject* metaObject, bool checked);

public:
  const QMetaObject* rootItem() const;

private:
  typedef QVector<const QMetaObject*> MetaObjectList;
  const QMetaObject* m_rootItem;
  QHash<const QMetaObject*, MetaObjectList> m_metaObjects;
  QSet<const QMetaObject*> m_checked;
  bool m_checkable;
};

#endif // METAOBJECTITEMMODEL_HPP
