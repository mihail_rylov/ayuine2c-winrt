#include "MetaObjectItemModel.hpp"

#include <QIcon>
#include <Core/ClassInfo.hpp>

MetaObjectItemModel::MetaObjectItemModel(const QMetaObject* rootItem, QObject *parent) :
  QAbstractItemModel(parent), m_rootItem(rootItem)
{
  foreach(const QMetaObject* metaObject, ClassInfo::metaObjects(rootItem))
  {
    do
    {
      m_checked.insert(metaObject);
      if(metaObject == rootItem)
        break;
      m_metaObjects[metaObject->superClass()].append(metaObject);
    }
    while(metaObject == metaObject->superClass());
  }

  setSupportedDragActions(Qt::CopyAction);
}

MetaObjectItemModel::~MetaObjectItemModel()
{
}

QModelIndex MetaObjectItemModel::index(int row, int column, const QModelIndex &parent) const
{
  if(row < 0 || column < 0)
    return QModelIndex();

  if(parent.isValid())
  {
    MetaObjectList metaObjectList = m_metaObjects[(const QMetaObject*)parent.internalPointer()];

    if(row < metaObjectList.size())
    {
      return createIndex(row, column, (void*)metaObjectList[row]);
    }
  }
  else if(row == 0)
  {
    return createIndex(0, 0, (void*)rootItem());
  }

  return QModelIndex();
}

QModelIndex MetaObjectItemModel::parent(const QModelIndex &child) const
{
  if(child.isValid())
  {
    if(const QMetaObject* metaObject = (const QMetaObject*)child.internalPointer())
    {
      if(metaObject == rootItem())
        return QModelIndex();

      const QMetaObject* superObject = metaObject->superClass();

      return createIndex(m_metaObjects[superObject].indexOf(metaObject), child.column(), (void*)superObject);
    }
  }
  return QModelIndex();
}

int MetaObjectItemModel::rowCount(const QModelIndex &parent) const
{
  if(parent.isValid())
  {
    if(const QMetaObject* metaObject = (const QMetaObject*)parent.internalPointer())
    {
      return m_metaObjects[metaObject].size();
    }
    return 0;
  }
  return 1;
}

int MetaObjectItemModel::columnCount(const QModelIndex &parent) const
{
  return 1;
}

QVariant MetaObjectItemModel::data(const QModelIndex &index, int role) const
{
  if(!index.isValid())
    return QVariant();

  if(const QMetaObject* metaObject = (const QMetaObject*)index.internalPointer())
  {
    switch(role)
    {
    case Qt::DisplayRole:
      return metaObject->className();

    case Qt::DecorationRole:
      return QVariant::fromValue(ClassInfo::getIcon(metaObject));

    case Qt::CheckStateRole:
      return m_checked.contains(metaObject) ? Qt::Checked : Qt::Unchecked;

    default:
      return QVariant();
    }
  }

  return QVariant();
}

const QMetaObject * MetaObjectItemModel::rootItem() const
{
  return m_rootItem;
}

QMimeData * MetaObjectItemModel::mimeData(const QModelIndexList &indexes) const
{
  if(indexes.isEmpty())
    return NULL;

  if(indexes.size() > 0 && indexes[0].isValid())
  {
    if(const QMetaObject* metaObject = (const QMetaObject*)indexes[0].internalPointer())
    {
      if(!metaObject->constructorCount())
        return NULL;

      QMimeData* mimeData = new QMimeData();
      mimeData->setData("MetaObjectItemModel", metaObject->className());
      return mimeData;
    }
  }
  return NULL;
}

Qt::ItemFlags MetaObjectItemModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index);

  if(index.isValid())
  {
    if(const QMetaObject* metaObject = (const QMetaObject*)index.internalPointer())
    {
      if(metaObject->constructorCount())
      {
        defaultFlags |= Qt::ItemIsDragEnabled;
      }

      if(m_checkable && metaObject != m_rootItem)
      {
        defaultFlags |= Qt::ItemIsUserCheckable;
      }
    }
  }

  return defaultFlags;
}

bool MetaObjectItemModel::checkable() const
{
  return m_checkable;
}

void MetaObjectItemModel::setCheckable(bool checkable)
{
  m_checkable = checkable;
}

bool MetaObjectItemModel::isChecked(const QMetaObject *metaObject)
{
  return m_checked.contains(metaObject);
}

bool MetaObjectItemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if(!index.isValid())
    return false;

  if(const QMetaObject* metaObject = (const QMetaObject*)index.internalPointer())
  {
    switch(role)
    {
    case Qt::CheckStateRole:
    {
      QVector<const QMetaObject*> metaObjects;
      metaObjects.append(metaObject);
      for(int i = 0; i < metaObjects.count(); ++i)
        metaObjects += m_metaObjects[metaObjects[i]];

      if(value.toBool())
      {
        for(int i = 0; i < metaObjects.count(); ++i)
          m_checked.insert(metaObjects[i]);
        emit checkedChanged(metaObject, true);
      }
      else
      {
        for(int i = 0; i < metaObjects.count(); ++i)
          m_checked.remove(metaObjects[i]);
        emit checkedChanged(metaObject, false);
      }

      beginRemoveRows(index, 0, m_metaObjects[metaObject].size());
      endRemoveRows();
    }
      return true;

    default:
      return false;
    }
  }

  return false;
}

QSet<const QMetaObject *> MetaObjectItemModel::checked() const
{
  return m_checked;
}
