#ifndef RESOURCEPREVIEWGENERATOR_HPP
#define RESOURCEPREVIEWGENERATOR_HPP

#include <QObject>
#include <QPixmap>

#include "Editor.hpp"

class EDITOR_EXPORT ResourcePreviewGenerator : public QObject
{
  Q_OBJECT

public:
  static QList<ResourcePreviewGenerator*> &previewGenerators();

public:
  explicit ResourcePreviewGenerator(QObject *parent = 0);
  ~ResourcePreviewGenerator();

public:
  virtual bool generate(const QString& fileName, const QMetaObject* metaObject, QPixmap& preview) = 0;

public:
  static QPixmap generate(const QString& fileName);
};

#endif // RESOURCEPREVIEWGENERATOR_HPP
