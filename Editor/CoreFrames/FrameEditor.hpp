#ifndef FRAMEEDITOR_HPP
#define FRAMEEDITOR_HPP

#include <QMainWindow>

#include "Editor.hpp"
#include "ResourceEditor.hpp"

class Frame;
class BloomComponent;
class CameraController;
class RenderComponentWidget;

namespace Ui {
class FrameEditor;
}

class EDITOR_EXPORT FrameEditor : public ResourceEditor
{
  Q_OBJECT

public:
  explicit FrameEditor(QWidget *parent = 0);
  ~FrameEditor();

public slots:
  virtual void prepareComponents();
  virtual void finalizeComponents();
  virtual void updateUi();

protected:
  RenderComponentWidget *renderComponents();

protected slots:
  void on_actionWire_triggered();
  void on_actionSolid_triggered();
  void on_actionShaded_triggered();
  void on_actionLit_triggered();
  void on_actionDeferred_triggered();

protected:
  QScopedPointer<Frame> m_frame;
  QScopedPointer<CameraController> m_cameraController;
private:
  Ui::FrameEditor* ui;
};

#endif // FRAMEEDITOR_HPP
