#include "LogView.hpp"
#include "ui_LogView.h"

static LogView* logViews;
static QtMsgHandler otherMsgHandler;

#ifdef _MSC_VER
#include <windows.h>
#include <Dbghelp.h>
#pragma comment(lib, "dbghelp.lib")

static const int m_callStackDepth = 5;
static QString m_fileNameFilter;

//------------------------------------//
// GetCurrentContext macros

#define GetCurrentContext(c, contextFlags) \
    do { \
    memset(&c, 0, sizeof(CONTEXT)); \
    c.ContextFlags = contextFlags; \
    RtlCaptureContext(&c); \
    } while(0)

class ExceptionSymbolDesc : public IMAGEHLP_SYMBOL64
{
    char NameAlign[256];

public:
    ExceptionSymbolDesc()
    {
        ZeroMemory(this, sizeof(*this));
        SizeOfStruct = sizeof(IMAGEHLP_SYMBOL64);
        MaxNameLength = sizeof(NameAlign) + 1;
    }
};


class ExceptionLineDesc : public IMAGEHLP_LINE64
{
public:
    ExceptionLineDesc()
    {
        ZeroMemory(this, sizeof(*this));
        SizeOfStruct = sizeof(IMAGEHLP_LINE64);
    }
};

static void refreshSymbols()
{
    SymSetOptions(SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS | SYMOPT_LOAD_LINES);
    SymInitialize(GetCurrentProcess(), NULL, true);
}

static QString walkCallStack(int discard)
{
    QStringList callStack;

    // Wydrukuj stos
    if(m_callStackDepth)
    {
        static bool initialized = false;

        if(!initialized)
        {
            refreshSymbols();
            initialized = true;
        }

        // Pobierz kontekst
        CONTEXT context = {0};
        GetCurrentContext(context, CONTEXT_FULL);
        bool filtered = false;

        // Wype�nij ramk�
        STACKFRAME64 frame;
        ZeroMemory(&frame, sizeof(STACKFRAME));
        frame.AddrPC.Offset = context.Eip;
        frame.AddrStack.Offset = context.Esp;
        frame.AddrFrame.Offset = context.Ebp;
        frame.AddrFrame.Mode = frame.AddrStack.Mode = frame.AddrPC.Mode = AddrModeFlat;

        // Przechod� stos do samego ko�ca
        for(int level = 0; level <= m_callStackDepth &&
            StackWalk64(IMAGE_FILE_MACHINE_I386, GetCurrentProcess(), GetCurrentThread(),	&frame, &context, NULL, SymFunctionTableAccess64, SymGetModuleBase64, NULL) &&
            frame.AddrFrame.Offset; ++level)
        {
            // Omi� aktualn� funkcj�
            if(level < discard)
                continue;

            // Nie sko�czony stos?
            if(frame.AddrPC.Offset == frame.AddrReturn.Offset)
            {
                callStack.append("... endless");
                break;
            }

            ExceptionSymbolDesc symbolDesc;
            DWORD64 offsetFromSymbol = 0;

            // Pobierz nazw� funkcji
            if(SymGetSymFromAddr64(GetCurrentProcess(), frame.AddrPC.Offset, &offsetFromSymbol, &symbolDesc))
            {
                ExceptionLineDesc lineDesc;
                DWORD offsetFromLine = 0;

                // Wypisz nazw� funkcji z lini�
                if(SymGetLineFromAddr64(GetCurrentProcess(), frame.AddrPC.Offset, &offsetFromLine, &lineDesc))
                {
                    // Przefiltruj nazw�
                    // if(!_strnicmp(lineDesc.FileName, *m_fileNameFilter, m_fileNameFilter.length()))
                    {
                        if(filtered)
                            callStack.append("...");
                        callStack.append(QString("%1 at %2:%3").arg(symbolDesc.Name).arg(lineDesc.FileName).arg(lineDesc.LineNumber));
                        filtered = false;
                        continue;
                    }
                }

                // Wypisz nazw� funkcji bez linii
                else if(m_fileNameFilter.isEmpty())
                {
                    if(filtered)
                        callStack.append("...");
                    callStack.append(symbolDesc.Name);
                    filtered = false;
                    continue;
                }
            }

            // Wypisz adres funkcji
            else if(m_fileNameFilter.isEmpty())
            {
                if(filtered)
                    callStack.append("...");
                callStack.append(QString::number((unsigned)frame.AddrPC.Offset, 16));
                filtered = false;
                continue;
            }
            filtered = true;
        }
    }

    return callStack.join("\r\n");
}
#endif

LogView::LogView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogView)
{
    ui->setupUi(this);

    if(!logViews)
        otherMsgHandler = qInstallMsgHandler(messageOutput);
    otherView = logViews;
    logViews = this;
}

LogView::~LogView()
{
    delete ui;

    LogView** logView = &logViews;

    while(*logView)
    {
        if(*logView == this)
        {
            *logView = otherView;
            break;
        }
        logView = &(*logView)->otherView;
    }

    if(!logViews)
        qInstallMsgHandler(otherMsgHandler);
}

void LogView::messageOutput(QtMsgType msgType, const char *message)
{
    QString messageText = message;
    QString categoryText;
    QString callStackText;
    QString statusText = QDateTime::currentDateTime().toString();

    int index = messageText.indexOf(' ');
    if(index > 0 && messageText[0] == '[' && messageText[index-1] == ']')
    {
        categoryText = messageText.left(index);
        messageText = messageText.mid(index).trimmed();
    }
    else
    {
        categoryText = "[QT]";
    }

    QString itemText = categoryText + " " + messageText;

#ifdef _MSC_VER
    callStackText = walkCallStack(2);
#endif

    for(LogView* logView = logViews; logView; logView = logView->otherView)
    {
        QScopedPointer<QListWidgetItem> widgetItem(new QListWidgetItem(itemText));
        widgetItem->setToolTip(callStackText);
        widgetItem->setStatusTip(statusText);

        switch(msgType)
        {
        case QtDebugMsg:
            widgetItem->setBackgroundColor(Qt::lightGray);
            widgetItem->setTextColor(Qt::black);
            break;

        case QtWarningMsg:
            widgetItem->setBackgroundColor(Qt::yellow);
            widgetItem->setTextColor(Qt::black);
            break;

        case QtCriticalMsg:
            widgetItem->setBackgroundColor(Qt::green);
            widgetItem->setTextColor(Qt::black);
            break;

        case QtFatalMsg:
            widgetItem->setBackgroundColor(Qt::red);
            widgetItem->setTextColor(Qt::black);
            break;

        default:
            break;
        }

        logView->ui->listWidget->addItem(widgetItem.take());
    }

    if(otherMsgHandler)
    {
        otherMsgHandler(msgType, message);
    }
}
