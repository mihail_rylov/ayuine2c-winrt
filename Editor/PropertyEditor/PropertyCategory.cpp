#include "PropertyCategory.hpp"

PropertyCategory::PropertyCategory(const QString& name, QObject* propertyObject, QObject* parent)
  : Property(name, propertyObject, parent)
{
}

bool PropertyCategory::isRoot()
{
  return true;
}
