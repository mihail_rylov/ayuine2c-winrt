#include "MeshSubsetFragment_p.hpp"
#include "MeshSubset.hpp"
#include "Mesh.hpp"
#include "MeshVertex.hpp"

MeshSubsetFragment::MeshSubsetFragment(MeshSubset* meshSubset)
{
  m_meshSubset = NULL;
  m_material = meshSubset->material();
  m_vertexBuffer = meshSubset->mesh()->renderVertexBuffer();
  m_indexBuffer = meshSubset->mesh()->renderIndexBuffer();
  m_drawer = meshSubset->drawer();
  m_drawer.BaseIndex += meshSubset->mesh()->renderVertexOffset();
  m_drawer.StartIndex += meshSubset->mesh()->renderIndexOffset();
  m_materialQuality = MaterialSystem::High;
}

MeshSubsetFragment::~MeshSubsetFragment()
{
  if(m_meshSubset && m_meshSubset->m_meshSubsetFragment[m_materialQuality] == this)
  {
    m_meshSubset->m_meshSubsetFragment[m_materialQuality] = NULL;
  }
}

ShaderState* MeshSubsetFragment::materialShaderState() const
{
  return m_material.data();
}

VertexBuffer::Types MeshSubsetFragment::instanceType() const
{
  return MeshVertex::vertexType;
}

bool MeshSubsetFragment::bind(const Light* lights)
{
  getRenderSystem().setVertexBuffer(vertexType(), m_vertexBuffer.data());
  getRenderSystem().setIndexBuffer(m_indexBuffer.data());
  return InstancedFragment::bind(lights);
}

void MeshSubsetFragment::draw()
{
  if(!m_drawer.PrimitiveCount)
    return;
  getRenderSystem().draw(m_drawer);
}

SelectedMeshSubsetFragment::SelectedMeshSubsetFragment(const MeshSubset* meshSubset)
{
  m_vertexBuffer = meshSubset->mesh()->renderVertexBuffer();
  m_indexBuffer = meshSubset->mesh()->renderIndexBuffer();
  m_drawer = meshSubset->drawer();
  m_drawer.BaseIndex += meshSubset->mesh()->renderVertexOffset();
  m_drawer.StartIndex += meshSubset->mesh()->renderIndexOffset();
}

SelectedMeshSubsetFragment::~SelectedMeshSubsetFragment()
{
}

bool SelectedMeshSubsetFragment::prepare()
{
  return true;
}

VertexBuffer::Types SelectedMeshSubsetFragment::vertexType() const
{
  return MeshVertex::vertexType;
}

bool SelectedMeshSubsetFragment::bind(const Light* lights)
{
  getRenderSystem().setVertexBuffer(vertexType(), m_vertexBuffer.data());
  getRenderSystem().setIndexBuffer(m_indexBuffer.data());
  return TexturedFragment::bind(lights);
}

void SelectedMeshSubsetFragment::draw()
{
  if(!m_drawer.PrimitiveCount)
    return;
  getRenderSystem().draw(m_drawer);
}

float MeshSubsetFragment::distance(const vec3& cameraOrigin, const vec4& cameraPlane) const
{
  if(instances().size())
  {
#if 0
    const box& bounds = instances()[0].boxBounds;

    vec3 best(0,0,0);
    best.X = qBound(bounds.Mins.X, cameraOrigin.X, bounds.Maxs.X);
    best.Y = qBound(bounds.Mins.Y, cameraOrigin.Y, bounds.Maxs.Y);
    best.Z = qBound(bounds.Mins.Z, cameraOrigin.Z, bounds.Maxs.Z);

    return (best - cameraOrigin).length();
#else
    return (instances()[0].sphereBounds.Center - cameraOrigin).length() + instances()[0].sphereBounds.Radius;
#endif
  }
  return FLT_MAX;
}

Material::OpaqueMode MeshSubsetFragment::opaque() const
{
  if(m_material)
    return m_material->opaque();
  return Material::Opaque;
}

MaterialSystem::Quality MeshSubsetFragment::materialQuality() const
{
  return m_materialQuality;
}

void MeshSubsetFragment::setMaterialQuality(MaterialSystem::Quality quality)
{
  m_materialQuality = quality;
}
