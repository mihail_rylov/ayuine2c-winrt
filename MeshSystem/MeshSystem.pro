TEMPLATE = lib
TARGET = MeshSystem

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lMaterialSystem -lPipeline -lPhysics

# DEFINES += NO_PHYSICS

HEADERS += \
    MeshSystem.hpp \
    MeshSubsetFragment_p.hpp \
    MeshSubset.hpp \
    Mesh.hpp \
    MeshVertex.hpp

SOURCES += \
    MeshSystem.cpp \
    MeshSubsetFragment.cpp \
    MeshSubset.cpp \
    Mesh.cpp \
    MeshVertex.cpp
