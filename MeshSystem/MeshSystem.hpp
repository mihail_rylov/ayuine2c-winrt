#ifndef MESHSYSTEM_HPP
#define MESHSYSTEM_HPP

#include <QtGlobal>

#ifdef MESHSYSTEM_EXPORTS
#define MESHSYSTEM_EXPORT Q_DECL_EXPORT
#else
#define MESHSYSTEM_EXPORT Q_DECL_IMPORT
#endif

#endif // MESHSYSTEM_HPP
