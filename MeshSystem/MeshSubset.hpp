#ifndef MESHSUBSET_HPP
#define MESHSUBSET_HPP

#include "MeshSystem.hpp"

#include <QObject>
#include <QSharedPointer>
#include <Render/PrimitiveDrawer.hpp>
#include <Math/Box.hpp>
#include <Math/Sphere.hpp>
#include <MaterialSystem/Material.hpp>

class Frame;
class ColorPickFrame;
class Mesh;
class MeshSubsetFragment;

class MESHSYSTEM_EXPORT MeshSubset : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString objectName READ objectName WRITE setObjectName)
  Q_PROPERTY(IndexedPrimitiveDrawer drawer READ drawer WRITE setDrawer DESIGNABLE false)
  Q_PROPERTY(MaterialRef material READ material WRITE setMaterial)
  Q_PROPERTY(box boxBounds READ boxBounds WRITE setBoxBounds)
  Q_PROPERTY(sphere sphereBounds READ sphereBounds WRITE setSphereBounds)

public:
  Q_INVOKABLE MeshSubset();
  ~MeshSubset();

public:
  QSharedPointer<Mesh> mesh() const;

  const IndexedPrimitiveDrawer& drawer() const;
  void setDrawer(const IndexedPrimitiveDrawer& drawer);

  const QSharedPointer<Material> &material() const;
  void setMaterial(const QSharedPointer<Material>& material);

  const box& boxBounds() const;
  void setBoxBounds(const box& bounds);

  const sphere& sphereBounds() const;
  void setSphereBounds(const sphere& bounds);

public:
  virtual void setObjectName(const QString &name);

signals:
  void objectNameChanged(QObject* object = NULL, const QString& oldName = QString());
  void updated(QObject* object = NULL);

public:
  void generateFrame(Frame& frame, const matrix& objectTransform);
  void generateDebugFrame(Frame& frame, const matrix& objectTransform, QObject* owner = NULL);
  void generateColorPickFrame(ColorPickFrame& frame, const matrix& objectTransform, QObject* owner = NULL);

private:
  IndexedPrimitiveDrawer m_drawer;
  QSharedPointer<Material> m_material;
  box m_boxBounds;
  sphere m_sphereBounds;
  QWeakPointer<Mesh> m_mesh;

private:
  MeshSubsetFragment* m_meshSubsetFragment[MaterialSystem::QualityCount];

  friend class Mesh;
  friend class MeshSubsetFragment;
};

Q_DECLARE_OBJECTREF(MeshSubset)

#endif // MESHSUBSET_HPP
