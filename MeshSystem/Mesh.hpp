#ifndef MESH_HPP
#define MESH_HPP

#include "MeshSystem.hpp"
#include "MeshVertex.hpp"
#include "MeshSubset.hpp"

#include <Core/Resource.hpp>

#include <Math/Box.hpp>
#include <Math/Sphere.hpp>
#include <Math/Vec3.hpp>

class MeshVertex;
class VertexBuffer;
class MeshSubset;
class IndexBuffer;
class Collider;
class CollideInfo;

class Frame;

class MESHSYSTEM_EXPORT Mesh : public Resource
{
  Q_OBJECT
  Q_ENUMS(ColliderType)
  Q_PROPERTY(MeshVertexList vertices READ vertices WRITE setVertices DESIGNABLE false)
  Q_PROPERTY(MeshIndicesList indices READ indices WRITE setIndices DESIGNABLE false)
  Q_PROPERTY(MeshSubsetRefList subsets READ subsets WRITE setSubsets DESIGNABLE false)
  Q_PROPERTY(box boxBounds READ boxBounds WRITE setBoxBounds)
  Q_PROPERTY(sphere sphereBounds READ sphereBounds WRITE setSphereBounds)
  Q_PROPERTY(ColliderType colliderType READ colliderType WRITE setColliderType)
  Q_PROPERTY(float colliderMass READ colliderMass WRITE setColliderMass)
  Q_PROPERTY(Vec3List colliderVertices READ colliderVertices WRITE setColliderVertices DESIGNABLE false)
  Q_PROPERTY(MeshIndicesList colliderIndices READ colliderIndices WRITE setColliderIndices DESIGNABLE false)
  Q_CLASSINFO("IconFileName", ":/Resources/IconMesh.png")

public:
  enum ColliderType
  {
    ColliderDisabled,
    ColliderBox,
    ColliderSphere,
    ColliderConvex,
    ColliderExact
  };

public:
  Q_INVOKABLE Mesh();
  ~Mesh();

public:
  const QVector<MeshVertex>& vertices() const;
  void setVertices(const QVector<MeshVertex> &vertices);

  const QVector<unsigned>& indices() const;
  void setIndices(const QVector<unsigned>& indices);

  const QVector<QSharedPointer<MeshSubset> >& subsets() const;
  void setSubsets(const QVector<QSharedPointer<MeshSubset> >& subsets);
  void addSubset(const QSharedPointer<MeshSubset>& subset);

  const box& boxBounds() const;
  void setBoxBounds(const box& bounds);

  const sphere& sphereBounds() const;
  void setSphereBounds(const sphere& bounds);

public:
  ColliderType colliderType() const;
  void setColliderType(ColliderType colliderType);

  void buildColliderObject();

  float colliderMass() const;
  void setColliderMass(float colliderMass);

  const QVector<vec3> &colliderVertices() const;
  void setColliderVertices(const QVector<vec3>& colliderVertices);

  const QVector<unsigned>& colliderIndices() const;
  void setColliderIndices(const QVector<unsigned>& colliderIndices);

  const QSharedPointer<Collider>& colliderObject() const;
  void setColliderObject(const QSharedPointer<Collider>& colliderObject);

public:
  const QSharedPointer<VertexBuffer> &renderVertexBuffer() const;
  unsigned renderVertexOffset() const;

  const QSharedPointer<IndexBuffer> &renderIndexBuffer() const;
  unsigned renderIndexOffset() const;

  static void optimizeRenderVertexBuffers();
  static void optimizeRenderIndexBuffers();
  static void optimizeRender();

public:
  void generateFrame(Frame& frame, const matrix& objectTransform);
  void generateDebugFrame(Frame& frame, const matrix& objectTransform, QObject* owner = NULL);
  void generateColorPickFrame(ColorPickFrame& frame, const matrix& objectTransform, QObject* owner = NULL);

public:
  bool collide(const ray& dir, CollideInfo& collide);

private:
  void generateColliderVertices();

private:
  QVector<MeshVertex> m_vertices;
  QVector<unsigned> m_indices;
  QVector<QSharedPointer<MeshSubset> > m_subsets;
  box m_boxBounds;
  sphere m_sphereBounds;

private:
  ColliderType m_colliderType;
  float m_colliderMass;
  QVector<vec3> m_colliderVertices;
  QVector<unsigned> m_colliderIndices;
  QSharedPointer<Collider> m_colliderObject;

private:
  QSharedPointer<VertexBuffer> m_renderVertexBuffer;
  unsigned m_renderVertexOffset;
  QSharedPointer<IndexBuffer> m_renderIndexBuffer;
  unsigned m_renderIndexOffset;
};

Q_DECLARE_OBJECTREF(Mesh)

#endif // MESH_HPP
