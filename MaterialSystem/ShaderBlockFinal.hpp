#ifndef SHADERBLOCKFINAL_HPP
#define SHADERBLOCKFINAL_HPP

#include "ShaderBlock.hpp"

class MATERIALSYSTEM_EXPORT ShaderBlockFinal : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Final")

public:
  virtual const QMetaObject* blockType() const;
  virtual unsigned blockMaxCount() const;
};

#endif // SHADERBLOCKFINAL_HPP
