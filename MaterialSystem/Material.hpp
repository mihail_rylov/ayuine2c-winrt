#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "Shader.hpp"
#include "MaterialSystem.hpp"
#include "ShaderState.hpp"

#include <Core/Resource.hpp>
#include <Render/RenderSystem.hpp>
#include <Render/Texture.hpp>

class MaterialShader;

class MATERIALSYSTEM_EXPORT Material : public Resource, public ShaderState
{
  Q_OBJECT
  Q_PROPERTY(TextureRef mainTexture READ mainTexture WRITE setMainTexture)
  Q_PROPERTY(ShaderRef materialShader READ materialShader WRITE setMaterialShader)
  Q_PROPERTY(ShaderValues materialValues READ values WRITE setValues)
  Q_PROPERTY(ShaderValues __refmaterialValues READ refValues STORED false DESIGNABLE false)
  Q_PROPERTY(OpaqueMode opaque READ opaque WRITE setOpaque)
  Q_CLASSINFO("IconFileName", ":/Resources/IconMaterial.png")
  Q_ENUMS(OpaqueMode)

public:
  enum OpaqueMode
  {
    Opaque,
    AlphaTest,
    Sort
  };

public:
  Q_INVOKABLE explicit Material();

public:
  const QSharedPointer<Texture> &mainTexture() const;
  void setMainTexture(const QSharedPointer<Texture> &mainTexture);

  OpaqueMode opaque() const;
  void setOpaque(OpaqueMode mode);

  bool isOpaque() const;

public:
  bool shadows() const;
  void setShadows(bool shadows);

  bool collidable() const;
  void setCollidable(bool collidable);

  bool lighting() const;
  void setLighting(bool lighting);

  float staticFriction() const;
  void setStaticFriction(float friction);

  float dynamicFriction() const;
  void setDynamicFriction(float friction);

  float elasticity() const;
  void setElasticity(float elasticity);

  float softness() const;
  void setSoftness(float softness);

private:
  QSharedPointer<Texture> m_mainTexture;
  OpaqueMode m_transparent;
  bool m_shadows, m_lighting;
  bool m_collidable;
  float m_staticFriction, m_dynamicFriction;
  float m_elasticity, m_softness;
};

Q_DECLARE_OBJECTREF(Material)

#endif // MATERIAL_HPP
