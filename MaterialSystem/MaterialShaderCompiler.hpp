#ifndef MATERIALSHADERCOMPILER_HPP
#define MATERIALSHADERCOMPILER_HPP

#include <Render/VertexBuffer.hpp>

#include "MaterialSystem.hpp"
#include "ShaderBlock.hpp"
#include "ShaderBlockOutput.hpp"
#include "ShaderBlockFinal.hpp"

class RenderShader;
class ShaderBlock;

class MATERIALSYSTEM_EXPORT MaterialShaderCompiler : public QObject
{
  Q_OBJECT
  Q_ENUMS(PixelShaderValue)

public:
  struct ShaderLink : public QString
  {
  public:
    ShaderLink()
    {
    }

    ShaderLink(const QString& text) : QString(text)
    {
    }

  public:
    operator bool () const
    {
      return !isEmpty();
    }
    bool operator ! () const
    {
      return isEmpty();
    }
  };

public:
  enum PixelShaderValue
  {
    // Globalne
    pObjectColor,
    pColor,
    pTime,
    pFrame,
    pPixelSize,

    // �wiat
    pScreen,
    pScreenDepth,
    pDepth,
    pDepth2,

    // Model
    pOrigin,    
    pLocalOrigin,
    pNormal,
    pTangent,
    pBinormal,
    pTexCoords,
    pMapCoords,

    // Kamera
    pCameraDir,
    pCameraTanDir,

    pMaxValues
  };

  enum { pMaxLinks = 16 };

protected:
  QStringList m_vertexShaderIn, m_vertexShaderOut, m_vertexShaderUniform, m_vertexShaderConst;
  QStringList m_vertexShaderBody;
  QStringList m_pixelShaderIn, m_pixelShaderOut, m_pixelShaderUniform, m_pixelShaderConst;
  QStringList m_pixelShaderBody;

protected:
  QStringList m_pixelVariables;
  unsigned m_usedLinks;

public:
  QSharedPointer<ShaderBlock> materialLink;
  MaterialSystem::Quality quality;
  bool lit;

public:
  MaterialShaderCompiler();
  virtual ~MaterialShaderCompiler();

protected:
  virtual QString outScreen() = 0;

public:
  virtual QString getp(PixelShaderValue value);
  virtual ShaderLink getLink(const QString& name);

  bool hasVariable(const QString& variable) const;
  bool addVariable(const QString& variable);

  void psin(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void psout(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void psuniform(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void psconst(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void ps(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());

  void vsin(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void vsout(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void vsuniform(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void vsconst(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());
  void vs(const char* fmt, QString a1 = QString(), QString a2 = QString(), QString a3 = QString(), QString a4 = QString(), QString a5 = QString(), QString a6 = QString(), QString a7 = QString(), QString a8 = QString(), QString a9 = QString());

  QString code() const;
  QSharedPointer<RenderShader> shader() const;
  bool linkAsTexCoord(const QString &name, ShaderBlock::LinkType type);
};

#endif // MATERIALSHADERCOMPILER_HPP
