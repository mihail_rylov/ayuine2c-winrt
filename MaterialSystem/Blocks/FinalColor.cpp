#include "ShaderBlockFinal.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockFinalColor : public ShaderBlockFinal
{
  Q_OBJECT
  Q_CLASSINFO("BlockName", "Color")

  enum Inputs
  {
    iColor,
    iAlpha,
    iMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockFinalColor()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iColor:	return Link(index, Float3, "Color");
    case iAlpha:    return Link(index, Float, "Alpha");
    default:        return Link();
    }
  }
};

#include "FinalColor.moc"

