#include "ShaderBlockOutput.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockOutputSolid : public ShaderBlockOutput
{
  Q_OBJECT
  Q_CLASSINFO("BlockName", "Solid")

  enum Inputs
  {
      iColor,
      iAlpha,
      iMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockOutputSolid()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iColor:    return Link(index, Float3, "Color");
    case iAlpha:    return Link(index, Float, "Alpha");
    default:        return Link();
    }
  }
};

#include "OutputSolid.moc"
