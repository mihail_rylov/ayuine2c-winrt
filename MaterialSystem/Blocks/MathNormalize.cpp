#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathNormalize : public ShaderBlock
{
  Q_OBJECT
  Q_ENUMS(Size Other)
  Q_PROPERTY(Size size READ size WRITE setSize)
  Q_PROPERTY(Other other READ other WRITE setOther)
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Normalize")

public:
  enum Size
  {
    Vec2,
    Vec3,
    Vec4
  };

  enum Other
  {
    Leave,
    Set1,
    Set0
  };

  enum Inputs
  {
    iIn,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathNormalize()
  {
    m_size = Vec4;
    m_other = Leave;
  }

public:
  Size size() const
  {
    return m_size;
  }
  void setSize(Size size)
  {
    m_size = size;
    changed();
  }
  Other other() const
  {
    return m_other;
  }
  void setOther(Other other)
  {
    m_other = other;
    changed();
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:			return Link(index, Float4, "In");
    default:            return Link();
    }
    return Link();
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float4, "Out");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
    {
      QString mask;

      compiler.ps("float4 %1 = normalize(%2);", varName, inputVarName(compiler, iIn));

      switch(m_size)
      {
      case Vec2:
        mask = "ba";
        break;

      case Vec3:
        mask = "a";
        break;

      case Vec4:
        break;
      }

      if(mask.length() != 0)
      {
        switch(m_other)
        {
        case Leave:
          break;

        case Set0:
          compiler.ps("%1.%2 = 0;", varName, mask);
          break;

        case Set1:
          compiler.ps("%1.%2 = 1;", varName, mask);
          break;
        }
      }
    }
      break;
    }
  }

private:
  Size m_size;
  Other m_other;
};

#include "MathNormalize.moc"
