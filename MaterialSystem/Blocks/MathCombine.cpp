#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathCombine : public ShaderBlock
{
  Q_OBJECT
  Q_ENUMS(Op)
  Q_PROPERTY(Op colorOp READ colorOp WRITE setColorOp)
  Q_PROPERTY(Op alphaOp READ alphaOp WRITE setAlphaOp)
  Q_PROPERTY(int inputCount READ inputCount WRITE setInputCount STORED false)
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Combine")

public:
  enum Op
  {
    SelectA,
    SelectB,
    Add,
    Subtract,
    Multiply,
    Divide,
    Max,
    Min,
    Dot3,
    Dot4,
    Average,
    Zero,
    One
  };

  enum { iMin = 2, iMax = 16 };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathCombine()
  {
    m_colorOp = Add;
    m_alphaOp = Add;
    m_inputCount = iMin;
  }

public:
  void setInputCount(unsigned inputCount)
  {
    QVector<OutputLink> values = links();
    values.resize(qBound<unsigned>(iMin, inputCount, iMax));
    setLinks(values);
  }
  Op colorOp() const
  {
    return m_colorOp;
  }
  void setColorOp(Op colorOp)
  {
    m_colorOp = colorOp;
    changed();
  }
  Op alphaOp() const
  {
    return m_alphaOp;
  }
  void setAlphaOp(Op alphaOp)
  {
    m_alphaOp = alphaOp;
    changed();
  }

public:
  unsigned inputCount() const
  {
    return qBound<unsigned>(iMin, links().size(), iMax);
  }
  Link inputLink(unsigned index) const
  {
    if(index < inputCount())
    {
        static const char* inputNames[iMax] = {"A", "B", "C", "D", "E", "F", "G", "H",
            "I", "J", "K", "L", "M", "N", "O", "P"};
        return Link(index, Float4, inputNames[index]);
    }
    return Link();
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float4, "Out");
    default:             return Link();
    }
  }
  QString combineOps(MaterialShaderCompiler& compiler, const QString &fmt, const QString &rgba, bool average = false) const
  {
      QString vars[iMax];
      unsigned varCount = 0;

      for(unsigned i = 0; i < inputCount(); ++i)
      {
          if(!inputHasLink(i))
              continue;
          vars[varCount++] = inputVarName(compiler, i) + rgba;
      }

      if(varCount)
      {
          for(unsigned count = varCount; count > 1; )
          {
              for(unsigned i = 0; i+1 < count; i += 2)
                  vars[i/2] = fmt.arg(vars[i], vars[i+1]);

              if(count & 1)
                  vars[count/2] = vars[count-1];
              count = (count + 1) / 2;
          }
          if(average && varCount > 1)
              return QString("(%1)/%2.0f").arg(vars[0], QString::number(varCount));
          return vars[0];
      }

      return "float(0)";
  }
  QString compileCombineOp(MaterialShaderCompiler& compiler, Op combineOp, const QString &rgba) const
  {
      switch(combineOp)
      {
      case SelectA:
          return QString("%1%2").arg(inputVarName(compiler, 0), rgba);

      case SelectB:
          return QString("%1%2").arg(inputVarName(compiler, 1), rgba);

      case Add:
          return combineOps(compiler, "%1 + %2", rgba);

      case Subtract:
          return combineOps(compiler, "%1 - %2", rgba);

      case Multiply:
          return combineOps(compiler, "%1 * %2", rgba);

      case Divide:
          return combineOps(compiler, "%1 / %2", rgba);

      case Max:
          return combineOps(compiler, "max(%1, %2)", rgba);

      case Min:
          return combineOps(compiler, "min(%1, %2)", rgba);

      case Dot3:
          return combineOps(compiler, "dot(%1, %2)", rgba);

      case Dot4:
          return combineOps(compiler, "dot(%1, %2)", ".rgba");

      case Average:
          return combineOps(compiler, "%1 + %2", rgba, true);

      default:
      case Zero:
          return "float(0.0f)" + rgba;

      case One:
          return "float(1.0f)" + rgba;
      }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
      compiler.ps("float4 %1 = float4(%2, %3);", varName, compileCombineOp(compiler, m_colorOp, ".rgb"), compileCombineOp(compiler, m_alphaOp, ".a"));
      break;
    }
  }

private:
  unsigned m_inputCount;
  Op m_colorOp;
  Op m_alphaOp;
};

#include "MathCombine.moc"
