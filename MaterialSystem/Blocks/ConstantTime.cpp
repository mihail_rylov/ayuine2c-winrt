#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockConstantTime : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Constant")
  Q_CLASSINFO("BlockName", "Time")
public:
  Q_INVOKABLE MaterialShaderBlockConstantTime()
  {
  }

public:
  unsigned outputCount() const
  {
    return 1;
  }
  Link outputLink(unsigned index) const
  {
    return Link(index, Float, "Out");
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    return compiler.getp(MaterialShaderCompiler::pTime);
  }
};

#include "ConstantTime.moc"

