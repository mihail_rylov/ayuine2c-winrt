#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockStreamVertexOrigin : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Stream")
  Q_CLASSINFO("BlockName", "VertexOrigin")
public:
  Q_INVOKABLE MaterialShaderBlockStreamVertexOrigin()
  {
  }

public:
  unsigned outputCount() const
  {
    return 1;
  }
  Link outputLink(unsigned index) const
  {
    return Link(index, Float3, "Out");
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    return compiler.getp(MaterialShaderCompiler::pOrigin);
  }
};

#include "StreamVertexOrigin.moc"
