#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Render/RenderSystem.hpp>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockVariableFloat : public MaterialShaderBlockOutputVariable
{
  Q_OBJECT
  Q_PROPERTY(float value READ value WRITE setValue)
  Q_CLASSINFO("BlockName", "Float")

public:
  Q_INVOKABLE MaterialShaderBlockVariableFloat()
  {
  }

public:
  LinkType constType() const
  {
    return Float;
  }
  float value() const
  {
    return m_value;
  }
  void setValue(float value)
  {
    if(m_value != value)
    {
      m_value = value;
      changed();
    }
  }
  QVariant constValue() const
  {
    return m_value;
  }
  void setConstValue(const QVariant& value)
  {
    setValue(value.value<float>());
  }
  void setRenderVariable(unsigned offset, const QVariant &value, bool onlyUsed)
  {
    if(!rawBaseMaterialShader())
      return;

    unsigned constOffset = offset + rawBaseMaterialShader()->constOffset();

    if(!onlyUsed || getRenderSystem().isConstUsed(constOffset))
    {
      float constValue[4];
      constValue[0] = constValue[1] = constValue[2] = constValue[3] = value.value<float>();

      getRenderSystem().setConst(constOffset, constValue, 1);
    }
  }

private:
  float m_value;
};

#include "VariableFloat.moc"
