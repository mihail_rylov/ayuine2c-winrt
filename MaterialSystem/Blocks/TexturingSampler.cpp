#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockTexturingSampler : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Texturing")
  Q_CLASSINFO("BlockName", "Sampler")

  enum Inputs
  {
    iSampler,
    iCoords,
    iMax
  };

  enum Outputs
  {
    oColor,
    oAlpha,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockTexturingSampler()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iSampler:      return Link(index, Sampler, "Sampler");
    case iCoords:		return Link(index, Float2, "Coords");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oColor:          return Link(index, Float4, "Color");
    case oAlpha:          return Link(index, Float, "Alpha");
    default:              return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oColor:
        compiler.ps("float4 %1 = tex2D(%2, %3);", varName, inputVarName(compiler, iSampler), inputHasLink(iCoords) ? inputVarName(compiler, iCoords) : compiler.getp(MaterialShaderCompiler::pTexCoords));
        break;

    case oAlpha:
        compiler.ps("float %1 = %2.a;", varName, outputVarName(compiler, oColor));
        break;
    }
  }
};

#include "TexturingSampler.moc"
