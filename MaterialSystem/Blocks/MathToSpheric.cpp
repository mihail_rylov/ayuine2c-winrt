#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathToSpheric : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "ToSpheric")

  enum Inputs
  {
    iIn,
    iMax
  };

  enum Outputs
  {
    oPhi,
    oTheta,
    oRadius,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathToSpheric()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float3, "In");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oRadius:        return Link(index, Float, "Radius");
    case oPhi:           return Link(index, Float, "Phi");
    case oTheta:         return Link(index, Float, "Theta");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oRadius:
      compiler.ps("float %1 = length(%2);", varName, inputVarName(compiler, iIn));
      break;

    case oPhi:
      compiler.ps("float %1 = atan2(%2.y, %2.x);", varName, inputVarName(compiler, iIn));
      break;

    case oTheta:
      compiler.ps("float %1 = asin(%2.z / %3);", varName, inputVarName(compiler, iIn), outputVarName(compiler, oRadius));
      break;
    }
  }
};

#include "MathToSpheric.moc"
