#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathJoin : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Join")

  enum Inputs
  {
      iR,
      iG,
      iB,
      iA,
      iMax
  };

  enum Outputs
  {
      oOut,
      oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathJoin()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iR:				return Link(index, Float, "R");
    case iG:				return Link(index, Float, "G");
    case iB:				return Link(index, Float, "B");
    case iA:				return Link(index, Float, "A");
    default:                return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:          return Link(index, Float4, "Out");
    default:            return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    ShaderBlock::compileVarName(compiler, index, varName);
    compiler.ps("%1 = float4(%2, %3, %4, %5);",
                varName,
                inputVarName(compiler, iR),
                inputVarName(compiler, iG),
                inputVarName(compiler, iB),
                inputVarName(compiler, iA));
  }
};

#include "MathJoin.moc"
