#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockTexturingScreen : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Texturing")
  Q_CLASSINFO("BlockName", "Screen")

  enum Inputs
  {
    iSampler,
    iCoords,
    iOffset,
    iMax
  };

  enum Outputs
  {
    oColor,
    oAlpha,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockTexturingScreen()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iSampler:      return Link(index, Sampler, "Sampler");
    case iCoords:		return Link(index, Float2, "Coords");
    case iOffset:		return Link(index, Float2, "Offset");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oColor:          return Link(index, Float4, "Color");
    case oAlpha:          return Link(index, Float, "Alpha");
    default:              return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oColor:
    {
      QString coords;
      if(inputHasLink(iCoords))
        coords = QString("(%1).xy * float2(0.5f, -0.5f) + 0.5f").arg(inputVarName(compiler, iCoords));
      else
        coords = compiler.getp(MaterialShaderCompiler::pScreen);
      compiler.ps("float4 %1 = tex2D(%2, %3 + %4);", varName, inputVarName(compiler, iSampler), coords, inputVarName(compiler, iOffset));
    }
    break;

    case oAlpha:
      compiler.ps("float %1 = %2.a;", varName, outputVarName(compiler, oColor));
      break;
    }
  }
};

#include "TexturingScreen.moc"
