#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectVarianceShadowing : public ShaderBlock
{
  Q_OBJECT
  Q_ENUMS(ShadowMode)
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "VarianceShadowing")
  Q_PROPERTY(int samples READ samples WRITE setSamples)
  Q_PROPERTY(float delta READ delta WRITE setDelta)
  Q_PROPERTY(ShadowMode shadowMode READ shadowMode WRITE setShadowMode)
  Q_PROPERTY(float shadowed READ shadowed WRITE setShadowed)

  enum Inputs
  {
    iLightDir,
    iLightDist,
    iShadows,
    iMax
  };

  enum Outputs
  {
    oShadows,
    oDepth,
    oMax
  };

public:
  enum ShadowMode
  {
    Plain,
    Cube
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectVarianceShadowing()
  {
    m_samples = 13;
    m_shadowMode = Cube;
    m_delta = 0.006f;
    m_shadowed = 0.3f;
  }

public:
  int samples() const
  {
    return m_samples;
  }
  void setSamples(int samples)
  {
    if(m_samples != samples)
    {
      m_samples = qBound(1, samples, 13);
      changed();
    }
  }
  float delta() const
  {
    return m_delta;
  }
  void setDelta(float delta)
  {
    if(m_delta != delta)
    {
      m_delta = qBound(0.0f, delta, 0.02f);
      changed();
    }
  }
  ShadowMode shadowMode() const
  {
    return m_shadowMode;
  }
  void setShadowMode(ShadowMode shadowMode)
  {
    if(m_shadowMode != shadowMode)
    {
      m_shadowMode = shadowMode;
      changed();
    }
  }
  float shadowed() const
  {
    return m_shadowed;
  }
  void setShadowed(float shadowed)
  {
    if(m_shadowed != shadowed)
    {
      m_shadowed = qBound(0.0f, shadowed, 1.0f);
      changed();
    }
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iLightDir:			return Link(index, Float3, "LightDir");
    case iLightDist:		return Link(index, Float, "LightDist");
    case iShadows:			return Link(index, Sampler, "Shadows");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oShadows:		return Link(index, Float, "Out");
    case oDepth:		  return Link(index, Float, "Depth");
    default:          return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oDepth:
      if(inputHasLink(iLightDir) && inputHasLink(iShadows))
      {
        QString lightDir = inputVarName(compiler, iLightDir);
        QString shadows = inputVarName(compiler, iShadows);

        if(m_shadowMode == Plain)
        {
          QString texCoords = buildTempVarName(compiler, "texCoords");
          compiler.ps("float2 %1 = %2.xy * float2(0.5f, -0.5f) + 0.5f;", texCoords, lightDir);
          compiler.ps("float %1 = tex2D(%2, %3).r;",
                    varName, shadows, texCoords);
        }
        else
        {
          compiler.ps("float %1 = texCUBE(%2, %3).r;",
                    varName, shadows, lightDir);
        }
      }
      else
      {
        compiler.ps("float %1 = 1.0f;", varName);
      }
      break;

    case oShadows:
      if(inputHasLink(iLightDir) && inputHasLink(iLightDist) && inputHasLink(iShadows) && !qFuzzyIsNull(m_shadowed - 1.0f))
      {
        QString lightDir = inputVarName(compiler, iLightDir);        
        QString lightDist = inputVarName(compiler, iLightDist);
        QString shadows = inputVarName(compiler, iShadows);
        QString normal = compiler.getp(MaterialShaderCompiler::pNormal);

        QString moments = buildTempVarName(compiler, "moments");
        QString dist = buildTempVarName(compiler, "dist");
        QString e2 = buildTempVarName(compiler, "e2");
        QString ex2 = buildTempVarName(compiler, "ex2");
        QString variance = buildTempVarName(compiler, "variance");
        QString md = buildTempVarName(compiler, "md");

        compiler.ps("float %1;", varName);

        if(m_shadowMode == Plain)
        {
          compiler.ps("if(normalize(%1).z > 0.707f) {", lightDir);
          QString texCoords = buildTempVarName(compiler, "texCoords");
          compiler.ps("float2 %1 = %2.xy * float2(0.5f, -0.5f) + 0.5f;", texCoords, lightDir);
          lightDir = texCoords;

          compiler.ps("float2 %1 = tex2D(%2, %3.xy).rg;",
                    moments, shadows, lightDir);
        }
        else
        {
          compiler.ps("float2 %1 = texCUBE(%2, %3).rg;",
                    moments, shadows, lightDir);
        }

        if(compiler.addVariable("poisson"))
        {
          compiler.ps("float2 poisson[12] = {float2(-0.326212f, -0.40581f), float2(-0.840144f, -0.07358f), float2(-0.695914f, 0.457137f),");
          compiler.ps("float2(-0.203345f, 0.620716f), float2(0.96234f, -0.194983f), float2(0.473434f, -0.480026f),");
          compiler.ps("float2(0.519456f, 0.767022f), float2(0.185461f, -0.893124f), float2(0.507431f, 0.064425f),");
          compiler.ps("float2(0.89642f, 0.412458f), float2(-0.32194f, -0.932615f), float2(-0.791559f, -0.59771f)};");
        }

        compiler.ps("float %1 = %2 - 0.0005 - 0.01 * (1 - dot(%2, %3));",
                    dist, lightDist, normal);

        if(m_samples > 1)
        {
          compiler.ps("for(int i = 0; i < %1; i++)", QString::number(m_samples-1));
          if(m_shadowMode == Plain)
          {
            compiler.ps("%1 += tex2D(%2, %3.xy + poisson[i].xy * %4).rg;",
              moments, shadows, lightDir, QString::number(m_delta));
          }
          else
          {
            compiler.ps("%1 += texCUBE(%2, %3 + float3(poisson[i].xy, poisson[11-i].x) * %4).rg;",
                    moments, shadows, lightDir, QString::number(m_delta));
          }
        }

        compiler.ps("%1 /= %2.0;", moments, QString::number(m_samples));

        compiler.ps("float %1 = %2.y;", e2, moments);
        compiler.ps("float %1 = %2.x * %2.x;", ex2, moments);
        compiler.ps("float %1 = min(max(%2 - %3, 0) + 0.00003, 1.0);", variance, e2, ex2);
        compiler.ps("float %1 = %2.x - %3;", md, moments, dist);
        compiler.ps("%1 = max(%2 <= %3.x, %4 / (%4 + %5 * %5));",
                    varName, dist, moments, variance, md);

        if(!qFuzzyIsNull(m_shadowed))
        {
          compiler.ps("%1 = lerp(%2, 1.0f, %1);", varName, QString::number(m_shadowed));
        }

        if(m_shadowMode == Plain)
        {
          compiler.ps("}");
          compiler.ps("else");
          compiler.ps("{");
          compiler.ps("%1 = 1.0f;", varName);
          compiler.ps("}");
        }
      }
      else
      {
        compiler.ps("float %1 = 1.0f;", varName);
      }
      break;
    }
  }

private:
  int m_samples;
  float m_delta;
  float m_shadowed;
  ShadowMode m_shadowMode;
};

#include "EffectVarianceShadowing.moc"
