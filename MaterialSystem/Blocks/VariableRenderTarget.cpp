#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Render/Texture.hpp>
#include <Render/RenderSystem.hpp>
#include <QPixmap>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockVariableRenderTarget : public MaterialShaderBlockOutputVariable
{
  Q_OBJECT
  Q_CLASSINFO("BlockName", "RenderTarget")
  Q_PROPERTY(RenderSystem::SamplerState sampler READ sampler WRITE setSampler)

public:
  Q_INVOKABLE MaterialShaderBlockVariableRenderTarget()
  {
    m_sampler = RenderSystem::LinearWrap;
  }

public:
  RenderSystem::SamplerState sampler() const
  {
    return m_sampler;
  }
  void setSampler(RenderSystem::SamplerState sampler)
  {
    if(m_sampler != sampler)
    {
      m_sampler = sampler;
      changed();
    }
  }

public:
  LinkType constType() const
  {
    return Sampler;
  }
  QVariant constValue() const
  {
    return QVariant::fromValue(RenderTextureRef());
  }
  const QMetaObject *blockType() const
  {
    return &staticMetaObject;
  }
  unsigned blockMaxCount() const
  {
    return 4;
  }
  bool isDesignable() const
  {
    return false;
  }
  void setRenderVariable(unsigned offset, const QVariant &value, bool onlyUsed)
  {
    if(!rawBaseMaterialShader())
      return;

    unsigned samplerOffset = offset + rawBaseMaterialShader()->samplerOffset();

    if(!onlyUsed || getRenderSystem().isSamplerUser(samplerOffset))
    {
      RenderTextureRef texture = value.value<RenderTextureRef>();
      getRenderSystem().setTexture(texture.data(), samplerOffset);
      getRenderSystem().setSamplerState(m_sampler, samplerOffset);
    }
  }

private:
  RenderSystem::SamplerState m_sampler;
};

#include "VariableRenderTarget.moc"
