#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Render/RenderSystem.hpp>
#include <Math/Vec3.hpp>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockVariableVector : public MaterialShaderBlockOutputVariable
{
  Q_OBJECT
  Q_PROPERTY(vec4 value READ value WRITE setValue)
  Q_CLASSINFO("BlockName", "Vector")

public:
  Q_INVOKABLE MaterialShaderBlockVariableVector()
  {
  }

public:
  LinkType constType() const
  {
    return Float4;
  }
  vec4 value() const
  {
    return m_value;
  }
  void setValue(const vec4 &value)
  {
    if(m_value != value)
    {
      m_value = value;
      changed();
    }
  }
  QVariant constValue() const
  {
    return QVariant::fromValue(m_value);
  }
  void setConstValue(const QVariant& value)
  {
    setValue(value.value<vec4>());
  }
  void setRenderVariable(unsigned offset, const QVariant &value, bool onlyUsed)
  {
    if(!rawBaseMaterialShader())
      return;

    unsigned constOffset = offset + rawBaseMaterialShader()->constOffset();

    if(!onlyUsed || getRenderSystem().isConstUsed(constOffset))
    {
      vec4 constValue(value.value<vec4>());

      getRenderSystem().setConst(constOffset, &constValue.X, 1);
    }
  }

private:
  vec4 m_value;
};

#include "VariableVector.moc"
