#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockStreamVertexDepth : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Stream")
  Q_CLASSINFO("BlockName", "VertexDepth")
public:
  Q_INVOKABLE MaterialShaderBlockStreamVertexDepth()
  {
  }

public:
  unsigned outputCount() const
  {
    return 1;
  }
  Link outputLink(unsigned index) const
  {
    return Link(index, Float, "Out");
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    return compiler.getp(MaterialShaderCompiler::pDepth);
  }
};

#include "StreamVertexDepth.moc"
