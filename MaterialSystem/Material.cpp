#include "Material.hpp"
#include <MaterialSystem/MaterialShader.hpp>

Q_IMPLEMENT_OBJECTREF(Material)

Material::Material()
{
  m_transparent = Opaque;
}

bool Material::shadows() const
{
  return m_shadows;
}

void Material::setShadows(bool shadows)
{
  m_shadows = shadows;
}

bool Material::lighting() const
{
  return m_lighting;
}

void Material::setLighting(bool lighting)
{
  m_lighting = lighting;
}

float Material::staticFriction() const
{
  return m_staticFriction;
}

void Material::setStaticFriction(float friction)
{
  m_staticFriction = friction;
}

float Material::dynamicFriction() const
{
  return m_dynamicFriction;
}

void Material::setDynamicFriction(float friction)
{
  m_dynamicFriction = friction;
}

float Material::elasticity() const
{
  return m_elasticity;
}

void Material::setElasticity(float elasticity)
{
  m_elasticity = elasticity;
}

float Material::softness() const
{
  return m_softness;
}

void Material::setSoftness(float softness)
{
  m_softness = softness;
}

bool Material::collidable() const
{
  return m_collidable;
}

void Material::setCollidable(bool collidable)
{
  m_collidable = collidable;
}

const QSharedPointer<Texture> & Material::mainTexture() const
{
  return m_mainTexture;
}

void Material::setMainTexture(const QSharedPointer<Texture> &mainTexture)
{
  m_mainTexture = mainTexture;
}

bool Material::isOpaque() const
{
  switch(m_transparent)
  {
  case Opaque:
  case AlphaTest:
    return true;

  default:
    return false;
  }
}

Material::OpaqueMode Material::opaque() const
{
  return m_transparent;
}

void Material::setOpaque(Material::OpaqueMode mode)
{
  m_transparent = mode;
}
