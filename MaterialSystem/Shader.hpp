#ifndef SHADER_HPP
#define SHADER_HPP

#include <Core/Resource.hpp>

#include "MaterialSystem.hpp"

#include "ShaderBlock.hpp"
#include "ShaderBlockVariable.hpp"
#include "ShaderState.hpp"

#include <Render/VertexBuffer.hpp>

class RenderShader;
typedef QSharedPointer<RenderShader> RenderShaderRef;

struct ShaderConst
{
  QByteArray name;
  unsigned index;
  ShaderBlock::LinkType type;
  QVariant value;
  QSharedPointer<ShaderBlockVariable> variable;

  ShaderConst()
  {
    index = ~0;
    type = ShaderBlock::Undefined;
  }

  bool operator < (const ShaderConst& other) const
  {
    return name < other.name;
  }
  bool operator == (const ShaderConst& other) const
  {
    return name == other.name;
  }
};

class RenderShader;

class MATERIALSYSTEM_EXPORT Shader : public Resource
{
  Q_OBJECT
  Q_PROPERTY(ShaderBlockRefList blocks READ blocks WRITE setBlocks DESIGNABLE false)
  Q_PROPERTY(ShaderValues materialValues READ values WRITE setValues STORED false)
  Q_PROPERTY(ShaderValues __refmaterialValues READ values STORED false DESIGNABLE false)
  Q_CLASSINFO("IconFileName", ":/Resources/IconShader.png")

public:
  explicit Shader();
  ~Shader();

signals:
  void blockAdded(const QSharedPointer<ShaderBlock>& newBlock = QSharedPointer<ShaderBlock>());
  void blockRemoved(const QSharedPointer<ShaderBlock>& oldBlock = QSharedPointer<ShaderBlock>());

public:
  QVector<QSharedPointer<ShaderBlock> > blocks() const;
  void setBlocks(const QVector<QSharedPointer<ShaderBlock> >& blocks);

public slots:
  virtual bool addBlock(const QSharedPointer<ShaderBlock>& block);
  virtual bool removeBlock(unsigned index);
  virtual bool removeBlock(const QSharedPointer<ShaderBlock>& block);
  virtual QSharedPointer<ShaderBlock> findBlockType(const QMetaObject* metaObject) const;
  virtual QSharedPointer<ShaderBlock> findBlock(const QByteArray& name) const;

  unsigned blockCount() const
  {
    return m_blocks.size();
  }

  QSharedPointer<ShaderBlock> block(unsigned index) const
  {
    if((unsigned)m_blocks.size() >= index)
      return QSharedPointer<ShaderBlock>();
    return m_blocks[index];
  }

public:
  virtual void changed();
  virtual void updateConsts();
  void setConsts(const ShaderValues& values, bool onlyUsed = true) const;
  const ShaderConst* findConst(const QByteArray& name, ShaderBlock::LinkType type = ShaderBlock::Undefined) const;
  QString unique() const;

signals:
  void constsUpdated(Shader* object = NULL);

public:
  bool contains(const QByteArray& name) const;
  QVariant get(const QByteArray& name) const;
  ShaderValues values() const;
  void setValues(const ShaderValues& values);

public:
  virtual unsigned constOffset() const = 0;
  virtual unsigned constMaxCount() const = 0;
  virtual unsigned samplerOffset() const = 0;
  virtual unsigned samplerMaxCount() const = 0;
  virtual VertexBuffer::Types vertexType() const = 0;

public:
  RenderShaderRef shader(unsigned index, Shader* link) const;
  void setShader(unsigned index, Shader* link, const RenderShaderRef& shader);

private slots:
  void linkedShaderConstsUpdated(Shader* link);

private:
  QVector<QHash<Shader*, RenderShaderRef> > m_shaders;

private:
  QVector<QSharedPointer<ShaderBlock> > m_blocks;
  QVector<ShaderConst> m_consts;
  unsigned m_maxConsts, m_maxSamplers;
  bool m_flushConsts;

public:
  unsigned internalIndex;
  Shader* internalNext;
  ShaderState* internalStates;
};

Q_DECLARE_OBJECTREF(Shader)

#endif // SHADER_HPP
