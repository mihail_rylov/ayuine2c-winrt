#include "ShaderState.hpp"
#include "ShaderState_p.hpp"
#include "Shader.hpp"

#include <Core/Resource.hpp>
#include <Math/Matrix.hpp>
#include <Render/RenderShader.hpp>

Q_IMPLEMENT_METATYPE(ShaderValues)

ShaderState::ShaderState()
  : data(new ShaderStateData)
{
  internalIndex = 0;
}

ShaderState::~ShaderState()
{
}

ShaderStateData::ShaderStateData()
{
}

ShaderStateData::~ShaderStateData()
{
}

QDataStream& operator << (QDataStream& ds, const ShaderState& state)
{
  ds << state.data->materialShader << state.data->values;
  return ds;
}

QDataStream& operator >> (QDataStream& ds, ShaderState& state)
{
  ds >> state.data->materialShader >> state.data->values;
  return ds;
}

bool ShaderState::isValid() const
{
  return !data->materialShader.isNull();
}

bool ShaderState::contains(const QByteArray &name) const
{
  if(!data->materialShader)
    return false;
  if(data->values.contains(name))
    return true;
  if(data->materialShader->contains(name))
    return true;
  return false;
}

QVariant ShaderState::get(const QByteArray &name) const
{
  if(!data->materialShader)
    return false;
  if(data->values.contains(name))
    return data->values[name];
  if(data->materialShader->contains(name))
    return data->materialShader->get(name);
  return QVariant();
}

void ShaderState::set(const QByteArray &name, const QVariant &value)
{
  if(value.isNull())
    data->values.remove(name);
  else
    data->values.insert(name, value);
}

void ShaderState::set(const QByteArray& name, float value)
{
  return set(name, QVariant::fromValue(value));
}

void ShaderState::set(const QByteArray& name, const vec2& value)
{
  return set(name, QVariant::fromValue(value));
}

void ShaderState::set(const QByteArray& name, const vec3& value)
{
  return set(name, QVariant::fromValue(value));
}

void ShaderState::set(const QByteArray& name, const vec4& value)
{
  return set(name, QVariant::fromValue(value));
}

void ShaderState::set(const QByteArray& name, const matrix& value)
{
  return set(name, QVariant::fromValue(value));
}

void ShaderState::set(const QByteArray& name, const TextureRef& value)
{
  return set(name, QVariant::fromValue(value));
}

void ShaderState::set(const QByteArray& name, const RenderTextureRef& value)
{
  return set(name, QVariant::fromValue(value));
}

void ShaderState::set(const QByteArray& name, const QString& value)
{
  return set(name, QVariant::fromValue(Resource::load<Texture>(value)));
}

ShaderValues ShaderState::values() const
{
  return data->values;
}

const QSharedPointer<Shader> & ShaderState::materialShader() const
{
  return data->materialShader;
}

Shader* ShaderState::rawMaterialShader() const
{
	return data->materialShader.data();
}

void ShaderState::setMaterialShader(const QSharedPointer<Shader> &materialShader)
{
  if(data->materialShader == materialShader)
    return;
  data->materialShader = materialShader;
}

void ShaderState::setMaterialShader(const QString& materialShader)
{
  setMaterialShader(Resource::load<Shader>(materialShader));

  if(data->materialShader)
    data->materialShader->setInternal(true);
}

bool ShaderState::setConsts(bool onlyUsed) const
{
  if(!data->materialShader)
    return false;
  data->materialShader->setConsts(data->values, onlyUsed);
  return true;
}

void ShaderState::setValues(const ShaderValues &values)
{
  data->values = values;
}

ShaderValues ShaderState::allValues() const
{
  if(data->materialShader)
  {
    QHash<QByteArray, QVariant> values;
    values = data->materialShader->values();
    values.unite(data->values);
    return values;
  }
  else
  {
    return data->values;
  }
}

ShaderValues ShaderState::refValues() const
{
  if(data->materialShader)
  {
    return data->materialShader->values();
  }
  else
  {
    return ShaderValues();
  }
}
