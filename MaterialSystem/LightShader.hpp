#ifndef LIGHTSHADER_HPP
#define LIGHTSHADER_HPP

#include "Shader.hpp"
#include "ShaderBlockFinal.hpp"

class MATERIALSYSTEM_EXPORT LightShader : public Shader
{
    Q_OBJECT
    Q_CLASSINFO("UniquePrefix", "ls")

public:
  Q_INVOKABLE explicit LightShader();

public:
  bool addBlock(const QSharedPointer<ShaderBlock>& block);
  QSharedPointer<ShaderBlockFinal> findFinal() const;

public:
  unsigned constOffset() const;
  unsigned samplerOffset() const;  
  unsigned constMaxCount() const;
  unsigned samplerMaxCount() const;
  VertexBuffer::Types vertexType() const;
};

Q_DECLARE_OBJECTREF(LightShader)

#endif // LIGHTSHADER_HPP
