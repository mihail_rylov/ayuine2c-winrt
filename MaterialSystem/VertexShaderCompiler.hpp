#ifndef VERTEXSHADERCOMPILER_HPP
#define VERTEXSHADERCOMPILER_HPP

#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT VertexShaderCompiler : public MaterialShaderCompiler
{
  Q_OBJECT
  Q_ENUMS(VertexShaderValue)

public:
  enum VertexShaderValue
  {
    //! Parametry
    vFrame, //!
    vTime, //!
    vPixelSize, //!

    //! Macierze
    vProjViewMatrix,
    vObjectMatrix,

    //! Pozycja ekranowa
    vOutScreen,
    vScreen,

    //! �wiat
    vVertexOrigin,
    vColor,
    vOrigin,
    vLocalOrigin,
    vNormal,
    vTangent,
    vBinormal,
    vTexCoords,
    vMapCoords,

    //! Kamera
    vCameraOrigin,
    vCameraDistance,
    vCameraInvDistance,
    vCameraAt,
    vCameraUp,
    vCameraRight,
    vCameraDir,
    vCameraTanDir,

    vMaxValues
  };

public:
  VertexShaderCompiler(VertexBuffer::Types vertexType);

public:
  VertexBuffer::Types vertexType;

protected:
  QString getv(VertexShaderValue value);
  QString getp(VertexShaderValue value);
  QString getp(PixelShaderValue value);

public:
  QString outScreen();

protected:
  bool m_vertexValues[vMaxValues];
  bool m_pixelValues[pMaxValues];
  bool m_pixelLinks[vMaxValues];
};

#endif // VERTEXSHADERCOMPILER_HPP
