#include "LightShader.hpp"
#include "ShaderBlockInput.hpp"
#include "ShaderBlockOutput.hpp"
#include "ShaderBlockFinal.hpp"

#include <Render/RenderShaderConst.hpp>

Q_IMPLEMENT_OBJECTREF(LightShader)

LightShader::LightShader()
{
}

bool LightShader::addBlock(const QSharedPointer<ShaderBlock> &block)
{
  if(block.objectCast<ShaderBlockOutput>())
    return false;
  return Shader::addBlock(block);
}

QSharedPointer<ShaderBlockFinal> LightShader::findFinal() const
{
  return findBlockType(&ShaderBlockFinal::staticMetaObject).objectCast<ShaderBlockFinal>();
}

unsigned LightShader::constOffset() const
{
  return 40;
}

unsigned LightShader::samplerOffset() const
{
  return 6;
}

unsigned LightShader::constMaxCount() const
{
  return 20;
}

unsigned LightShader::samplerMaxCount() const
{
  return 4;
}

VertexBuffer::Types LightShader::vertexType() const
{
  return VertexBuffer::Vertex;
}
