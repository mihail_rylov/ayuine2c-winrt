#ifndef SHADERSTATE_HPP
#define SHADERSTATE_HPP

#include <QDataStream>
#include <QSharedDataPointer>
#include <QHash>

#include <Render/RenderTexture.hpp>
#include <Render/Texture.hpp>

#include "MaterialSystem.hpp"

#include "ShaderState_p.hpp"

class ShaderStateData;
class Material;
class Shader;

typedef QHash<QByteArray, QVariant> ShaderValues;

Q_DECLARE_METATYPE(ShaderValues)

struct vec2;
struct vec3;
struct vec4;
struct matrix;

class Fragment;

class MATERIALSYSTEM_EXPORT ShaderState
{
public:
  ShaderState();
  ~ShaderState();

public:
  bool contains(const QByteArray& name) const;
  QVariant get(const QByteArray& name) const;
  void set(const QByteArray& name, const QVariant& value = QVariant());
  void set(const QByteArray& name, float value);
  void set(const QByteArray& name, const vec2& value);
  void set(const QByteArray& name, const vec3& value);
  void set(const QByteArray& name, const vec4& value);
  void set(const QByteArray& name, const matrix& value);
  void set(const QByteArray& name, const TextureRef& value);
  void set(const QByteArray& name, const RenderTextureRef& value);
  void set(const QByteArray& name, const QString& value);
  ShaderValues values() const;
  ShaderValues refValues() const;
  ShaderValues allValues() const;
  void setValues(const ShaderValues& values);
  bool setConsts(bool onlyUsed = true) const;

public:
  Shader* rawMaterialShader() const;
  const QSharedPointer<Shader>& materialShader() const;
  void setMaterialShader(const QSharedPointer<Shader>& materialShader);
  void setMaterialShader(const QString& materialShader);

public:
  bool isValid() const;

public:
  operator bool () const
  {
    return isValid();
  }

public:
  class StateSetter
  {
  public:
    StateSetter(ShaderState& state_, const QByteArray &name_)
      : state(state_), name(name_)
    {
    }

  public:
    void clear()
    {
      state.set(name);
    }

  public:
    operator QVariant () const
    {
      return state.get(name);
    }
    StateSetter& operator = (const QVariant& value)
    {
      state.set(name, value);
      return *this;
    }

  private:
    ShaderState& state;
    QByteArray name;
  };

  QVariant operator [] (const QByteArray& name) const
  {
    return get(name);
  }

  StateSetter operator [] (const QByteArray& name)
  {
    return StateSetter(*this, name);
  }

private:
  QSharedDataPointer<ShaderStateData> data;

public:
  mutable unsigned internalIndex;
  mutable ShaderState* internalNext;
  mutable Fragment* internalFragments;

  friend QDataStream& operator << (QDataStream& ds, const ShaderState& state);
  friend QDataStream& operator >> (QDataStream& ds, ShaderState& state);
};

Q_DECLARE_METATYPE(ShaderState)

#endif // SHADERSTATE_HPP
