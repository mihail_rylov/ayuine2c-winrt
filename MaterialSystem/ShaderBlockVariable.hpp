#ifndef SHADERBLOCKVARIABLE_HPP
#define SHADERBLOCKVARIABLE_HPP

#include "ShaderBlock.hpp"

#include <Math/Vec4.hpp>

class MaterialShaderCompiler;
class QPixmap;
class Texture;

class MATERIALSYSTEM_EXPORT ShaderBlockVariable : public ShaderBlock
{
  Q_OBJECT
  Q_PROPERTY(QString objectName READ objectName WRITE setObjectName)
  Q_CLASSINFO("BlockGroup", "Variable")

public:
  void setObjectName(const QString &name);

public:
  virtual const QMetaObject* blockType() const;
  virtual unsigned blockMaxCount() const;
  virtual bool isDesignable() const;

public:
  virtual LinkType constType() const = 0;
  virtual QString constName() const;
  virtual QVariant constValue() const;
  virtual void setConstValue(const QVariant& value);

public:
  virtual void setRenderVariable(unsigned offset, const QVariant& value, bool onlyUsed = true) = 0;

signals:
  void constValueChanged(const QVariant& newValue);

protected:
  virtual QString constVarName(MaterialShaderCompiler& compiler) const;
  virtual void compileConstVar(MaterialShaderCompiler& compiler, const QString& varName) const;
};

class MaterialShaderBlockOutputVariable : public ShaderBlockVariable
{
  Q_OBJECT

public:
  virtual const char* constLabel() const;
  virtual unsigned outputCount() const;
  virtual Link outputLink(unsigned index) const;
  virtual QString buildOutputVarName(MaterialShaderCompiler& compiler, unsigned output) const;
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned output, const QString& varName) const;
};

#endif // SHADERBLOCKVARIABLE_HPP
