#include "ShaderBlockOutput.hpp"

const QMetaObject * ShaderBlockOutput::blockType() const
{
  return &staticMetaObject;
}

unsigned ShaderBlockOutput::blockMaxCount() const
{
  return 1;
}
