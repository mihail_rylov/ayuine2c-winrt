#include "MaterialShader.hpp"
#include "ShaderBlockInput.hpp"
#include "ShaderBlockOutput.hpp"
#include "ShaderBlockFinal.hpp"

#include <Render/RenderShaderConst.hpp>

Q_IMPLEMENT_OBJECTREF(MaterialShader)
Q_IMPLEMENT_OBJECTREF(GeneralShader)

MaterialShader::MaterialShader()
{
}

bool GeneralShader::addBlock(const QSharedPointer<ShaderBlock> &block)
{
  if(block.objectCast<ShaderBlockInput>())
    return false;
  if(block.objectCast<ShaderBlockFinal>())
    return false;
  return Shader::addBlock(block);
}

QSharedPointer<ShaderBlockOutput> GeneralShader::findOutput() const
{
  return findBlockType(&ShaderBlockOutput::staticMetaObject).objectCast<ShaderBlockOutput>();
}

unsigned GeneralShader::constOffset() const
{
  return 20;
}

unsigned GeneralShader::samplerOffset() const
{
  return 0;
}

unsigned GeneralShader::constMaxCount() const
{
  return 20;
}

unsigned GeneralShader::samplerMaxCount() const
{
  return 6;
}

VertexBuffer::Types MaterialShader::vertexType() const
{
  return VertexBuffer::Types(VertexBuffer::Vertex | VertexBuffer::Coords | VertexBuffer::Coords2 | VertexBuffer::Normal | VertexBuffer::Tangent);
}

GeneralShader::GeneralShader()
{
}
