//------------------------------------//
//
// Input.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Engine
// Date: 2006-7-31
//
//------------------------------------//

#include "StdAfx.hpp"

//------------------------------------//
// DInput libraries

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

aIMPLEMENT_CLASS(Input, CLASS_None)

//------------------------------------//
// Input: Constructor

Input& getInput() {
	static Input self;
	return self;
}

//------------------------------------//
// Input: Constructor

Input::Input() {
	// Wyzeruj stan
	m_mouseX = m_mouseY = m_mouseDeltaX = m_mouseDeltaY = 0;

	// Wyczy�� stan klawiszy
	for(unsigned i = 0; i < kCount; i++) {
		m_keyState[i] = 0;
		m_keyPrevState[i] = 0;
	}

	// Wiadomo�� do konsoli
	logf("Input: Creating...");
}

//------------------------------------//
// Input: Destructor

Input::~Input() {
	// Wiadomo�� do konsoli
	logf("Input: Releasing...");
	release();
}

//------------------------------------//
// Input: Methods

void Input::init() {
	CHECKARG2(m_input, m_input == nullptr);
	CHECKARG2(m_keyboard, m_keyboard == nullptr);
	CHECKARG2(m_mouse, m_mouse == nullptr);

	// Utw�rz urz�dzenia do obs�ugi wej�cia
	if(FAILED(DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_input, nullptr))) {
		errorf("Input: Can't initialize input");
		return;
	}

	//// Utw�rz urz�dzenia do obs�ugi klawiatury
	//if(SUCCEEDED(m_input->CreateDevice(GUID_SysKeyboard, &m_keyboard, nullptr))) {
	//	// Ustaw format klawiatury
	//	if(SUCCEEDED(m_keyboard->SetDataFormat(&c_dfDIKeyboard))) {
	//		// Zacznij �ledzenie klawiatury
	//		if(FAILED(m_keyboard->Acquire())) {
	//			errorf("Input: Can't acquire keyboard");
	//			m_keyboard->Release();
	//		}
	//	}
	//	else {
	//		errorf("Input: Can't set keyboard format");
	//		m_keyboard->Release();
	//	}
	//}
	//else {
	//	errorf("Input: Can't initialize keyboard");
	//}

	// Utw�rz urz�dzenia do obs�ugi myszki
	if(SUCCEEDED(m_input->CreateDevice(GUID_SysMouse, &m_mouse, nullptr))) {
		// Ustaw format klawiatury
		if(SUCCEEDED(m_mouse->SetDataFormat(&c_dfDIMouse2))) {
			// Zacznij �ledzenie klawiatury
			if(FAILED(m_mouse->Acquire())) {
				errorf("Input: Can't acquire mouse");
				m_mouse->Release();
			}
		}
		else {
			errorf("Input: Can't set mouse format");
			m_mouse->Release();
		}
	}
	else {
		errorf("Input: Can't initialize mouse");
	}
}

void Input::release() {
	// Zako�cz �ledzenie klawiatury
	if(m_keyboard) {
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = nullptr;
	}

	// Zako�cz �ledzenie myszki
	if(m_mouse) {
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = nullptr;
	}

	// Usu� urz�dzenie wej�cia
	if(m_input) {
		m_input->Release();
		m_input = nullptr;
	}
}

int Input::mouseX(bool delta) {
	dGUARD {
		// Pobierz przesuni�cie myszki
		return delta ? m_mouseDeltaX : m_mouseX;
	}
	dUNGUARD;
}

int Input::mouseY(bool delta) {
	dGUARD {
		// Pobierz przesuni�cie myszki
		return delta ? m_mouseDeltaY : m_mouseX;
	}
	dUNGUARD;
}

bool Input::state(unsigned key, bool once) {
	dGUARD {
		// Sprawd� argumenty
		ASSERT(0 <= key && key < COUNT_OF(m_keyState));

		// Brak klawisza
		if(key == 0)
			return false;

		// Pobierz stan klawisza
		return (m_keyState[key] & 0x80) != 0 && (once == false || (m_keyPrevState[key] & 0x80) == 0);
	}
	dUNGUARD;
}

void Input::update() {
	// Skopiuj stary stan klawiatury
	memcpy(m_keyPrevState, m_keyState, sizeof(m_keyPrevState));
	memset(m_keyState, 0, sizeof(m_keyState));

	// Pobierz stan klawiatury
	if(m_keyboard) {
		// Pobierz stan klawiatury u�ywaj�c DInput8
		if(FAILED(m_keyboard->GetDeviceState(kCount, m_keyState)))
			errorf("Input: Can't get keyboard state");
	}
	else {
		// Pobierz stan klawiatury u�ywaj�c WinAPI
		if(!GetKeyboardState(m_keyState))
			errorf("Input: Can't get keyboard state");
	}

	// Pobierz stan myszki
	if(m_mouse) {
		DIMOUSESTATE2	mouseState;

		// Pobierz stan klawiatury u�ywaj�c DInput8
		if(SUCCEEDED(m_mouse->GetDeviceState(sizeof(mouseState), &mouseState))) {
			// Zapisz nowy stan myszy
			m_mouseDeltaX = mouseState.lX;
			m_mouseDeltaY = mouseState.lY;

			// TODO: Zapisz stan przycisk�w myszy
			for(unsigned i = 0; i < COUNT_OF(mouseState.rgbButtons); i++)
				if((mouseState.rgbButtons[i] & 0x80) != 0)
					m_keyState[mLeft + i] = true;

			// Zapisz stan rolki
			if(mouseState.lZ > 0)
				m_keyState[mUp] = true;
			else if(mouseState.lZ < 0)
				m_keyState[mDown] = true;
		}
		else {
			errorf("Input: Can't get mouse state");
		}
	}
	else {
		POINT mouse;

		// Pobierz pozycj� kursora
		if(GetCursorPos(&mouse)) {
			// Zapisz stary stan myszy
			m_mouseDeltaX = mouse.x - m_mouseX;
			m_mouseDeltaY = mouse.y - m_mouseY;
			//m_mousePrevState = m_mouseState;

			// Zapisz nowy stan myszy
			// TODO : Obs�uga przycisk�w myszy
			m_mouseX = mouse.x;
			m_mouseY = mouse.y;
		}
		else {
			errorf("Input: Can't get mouse state");
		}
	}
}
