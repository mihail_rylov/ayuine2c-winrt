#ifndef QRENDERWIDGET_HPP
#define QRENDERWIDGET_HPP

#include <QWidget>
#include "Render.hpp"

class RenderWidgetPrivate;
class RenderSystem;

class RENDER_EXPORT QRenderWidget : public QWidget
{
  Q_OBJECT
public:
  explicit QRenderWidget(QWidget *parent = 0);
  ~QRenderWidget();

public:
  RenderSystem& renderSystem();

public:
  virtual bool setRenderTarget();
  virtual bool setDepthTarget();
  virtual bool render();
  void swapBuffers();

public:
  bool swapBefore() const;
  void setSwapBefore(bool swapBefore);

protected:
  void showEvent(QShowEvent *);
  void paintEvent(QPaintEvent *);
  void resizeEvent(QResizeEvent *);

private slots:
  void deviceRestore();
  void deviceLost();

public:
  QPaintEngine *paintEngine() const { return 0; }

public:
  QScopedPointer<RenderWidgetPrivate> d_ptr;
};

#endif // QRENDERWIDGET_HPP
