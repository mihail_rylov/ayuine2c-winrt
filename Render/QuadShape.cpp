#include "RenderSystem.hpp"
#include "VertexBuffer.hpp"
#include "PrimitiveDrawer.hpp"

bool RenderSystem::drawScreenQuad()
{
  static const float v[4][5] =
  {
    {-1, -1, 0, 0, 1},
    { 1, -1, 0, 1, 1},
    { 1,  1, 0, 1, 0},
    {-1,  1, 0, 0, 0}
  };

  if(setVertexData(VertexBuffer::Types(VertexBuffer::Vertex | VertexBuffer::Coords), v, sizeof(v)))
  {
    draw(PrimitiveDrawer(PrimitiveDrawer::TriFan, 0, 2));
    return true;
  }
  return false;
}
