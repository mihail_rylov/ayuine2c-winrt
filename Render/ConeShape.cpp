#include "RenderSystem.hpp"
#include "RenderSystem_p.hpp"
#include "PrimitiveDrawer.hpp"

#include <Math/Box.hpp>
#include <Math/Matrix.hpp>

const int ConeVertices = 16;

void RenderSystem::lostCone()
{
  d_ptr->m_coneVertexBuffer.reset();
  d_ptr->m_coneIndexBuffer.reset();
  d_ptr->m_coneLineIndexBuffer.reset();
}

void RenderSystem::restoreCone()
{
  QVector<vec3> vertices;
  vertices << vec3(0,0,0);
  vertices << vec3(0,0,1);

  QVector<ushort> indices, lineIndices;

  for(int i = 0; i < ConeVertices; ++i)
  {
    vec3 v(qSin(2 * i * M_PI / ConeVertices), qCos(2 * i * M_PI / ConeVertices), 1.0f);
    vertices << v;

    indices << 0 << 2+(i+1)%ConeVertices << 2+i;
    indices << 1 << 2+i << 2+(i+1)%ConeVertices;

    lineIndices << 0 << 2+i;
    // lineIndices << 1 << 2+i;
    lineIndices << 2+i << 2+(i+1)%ConeVertices;
  }

  d_ptr->m_coneVertexBuffer.reset(new StaticVertexBuffer<vec3>(vertices));
  d_ptr->m_coneIndexBuffer.reset(new ShortStaticIndexBuffer(indices));
  d_ptr->m_coneLineIndexBuffer.reset(new ShortStaticIndexBuffer(lineIndices));
}

bool RenderSystem::drawCone(const matrix& transform)
{
  setTransform(transform);

  if(setVertexBuffer(VertexBuffer::Vertex, d_ptr->m_coneVertexBuffer.data()) &&
     setIndexBuffer(d_ptr->m_coneIndexBuffer.data()))
  {
    draw(IndexedPrimitiveDrawer(PrimitiveDrawer::TriList, 0, d_ptr->m_coneVertexBuffer->count(),
                                0, d_ptr->m_coneIndexBuffer->count() / 3));
    return true;
  }

  return false;
}

bool RenderSystem::drawLineCone(const matrix& transform)
{
  setTransform(transform);

  if(setVertexBuffer(VertexBuffer::Vertex, d_ptr->m_coneVertexBuffer.data()) &&
     setIndexBuffer(d_ptr->m_coneLineIndexBuffer.data()))
  {
    draw(IndexedPrimitiveDrawer(PrimitiveDrawer::LineList, 0, d_ptr->m_coneVertexBuffer->count(),
                                0, d_ptr->m_coneLineIndexBuffer->count() / 3));
    return true;
  }

  return false;
}
