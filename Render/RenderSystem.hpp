#ifndef RENDERSYSTEM_HPP
#define RENDERSYSTEM_HPP

#include <QObject>
#include <QColor>
#include <QRect>
#include <Math/MathLib.hpp>
#include "Render.hpp"
#include "IndexBuffer.hpp"
#include "VertexBuffer.hpp"
#include "GraphicsFont.hpp"

struct IDirect3DDevice9;

class RenderSystemPrivate;
class BaseRenderTarget;
class RenderTarget;
class CubeRenderTarget;
class VertexBuffer;
class IndexBuffer;
class RenderTexture;
class RenderShader;
class BasePrimitiveDrawer;
struct Instance;
struct Camera;
struct matrix;
struct vec2;
struct box;
struct sphere;

#pragma pack(4)
class RenderConsts
{
  Q_GADGET
public:
  float m_transform[4][4];
  float m_up[4];
  float m_down[4];
};
#pragma pack()

struct RenderTargetBindings
{
  BaseRenderTarget* renderTarget;
  CubeFace::Enum cubeFace;
};

struct RENDER_EXPORT RenderStats
{
  unsigned drawCount;
  unsigned primCount;
  unsigned textureChanges;
  unsigned shaderChanges;
  unsigned blendChanges;
  unsigned alphaTestChanges;
  unsigned depthStencilChanges;
  unsigned rasterizerChanges;
  unsigned samplerChanges;
  unsigned fillModeChanges;
  unsigned indexBufferChanges;
  unsigned vertexBufferChanges;
  unsigned vertexTypeChanges;
  unsigned dynamicVertexCount;
  unsigned dynamicVertexSize;
  unsigned dynamicIndexCount;
  unsigned dynamicIndexSize;
  unsigned vertexConstChanges;
  unsigned vertexConstSize;
  unsigned pixelConstChanges;
  unsigned pixelConstSize;
  unsigned instanceBinds;
  unsigned instanceCount;
  unsigned ignoredStateChanges;
  unsigned ignoredChanges;

  RenderStats();

  QStringList toStringList() const;
};

class RENDER_EXPORT RenderSystem : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(RenderSystem)
  Q_ENUMS(BlendState DepthStencilState RasterizerState SamplerState FixedState FillMode AlphaTest)
  Q_FLAGS(ClearOption ClearOptions)

public:
  enum BlendState
  {
    Additive,
    AlphaBlend,
    Overlay,
    NonPremultipled,
    Opaque,
    DisableColorWrite
  };

  enum AlphaTest
  {
    AlphaTestEnabled,
    AlphaTestInverted,
    AlphaTestDisabled,
    AlphaTestEnabledLow
  };

  enum DepthStencilState
  {
    DepthDefault,
    DepthRead,
    DepthNone,
    DepthClearStencil,
    DepthFillEarlyCull,
    DepthRenderEarlyCull
  };

  enum RasterizerState
  {
    CullClockwise,
    CullCounterClockwise,
    CullNone
  };

  enum FillMode
  {
    Wire,
    Solid
  };

  enum FixedState
  {
    FixedConst,
    FixedDiffuse,
    FixedTexture,
    FixedTextureConst,
    FixedTextureDiffuse,
    FixedMaskedColor,
    FixedMaskedConst,
    FixedCount
  };

  enum SamplerState
  {
    AnisotropicClamp,
    AnisotropicWrap,
    LinearClamp,
    LinearWrap,
    PointClamp,
    PointWrap,
    AnisotropicBorderBlack,
    AnisotropicBorderWhite,
    LinearBorderBlack,
    LinearBorderWhite,
    PointBorderBlack,
    PointBorderWhite,
    AnisotropicMirror,
    LinearMirror,
    PointMirror
  };

  enum ClearOption
  {
    Depth = 1,
    Target = 2,
    Stencil = 4
  };
  Q_DECLARE_FLAGS(ClearOptions, ClearOption)

public:
  RenderSystem();

public:
  ~RenderSystem();

private:
  void restoreDeviceObjects();
  void lostDeviceObjects();

public:
  bool resetDevice();

signals:
  void deviceLost();
  void deviceRestore();

public:
  unsigned maxInstances();

public:
  bool beginScene();
  void endScene(bool force = false);

  void clear(const QColor& color);
  void clear(ClearOptions options, const QColor& color, float depth = 1.0f, int stencil = 0);

  void setViewport(const QRect& viewport);
  void setScissor(const QRect* scissor);

  void setProjView(const matrix &proj, const matrix &view);
  void setTransform(const matrix& transform);
  void setCamera(const Camera& camera);
  void setFrame(float deltaTime, float frameTime);

  void setColor(const QColor& color);
  void setBlendState(BlendState state);
  void setAlphaTest(AlphaTest alphaTest);
  void setDepthStencilState(DepthStencilState state, bool bias = false);
  void setRasterizerState(RasterizerState state);
  void setSamplerState(SamplerState state, unsigned sampler);
  void setFillMode(FillMode mode);

  void setRenderTargets(const RenderTargetBindings* renderTargets, unsigned count);
  void setRenderTarget(RenderTarget* renderTarget);
  void setRenderTarget(CubeRenderTarget* renderTarget, CubeFace::Enum face);
  void setDepthTarget(BaseRenderTarget* renderTarget);

  void setTexture(RenderTexture* texture, unsigned sampler);
  bool setTexture(const char* name, RenderTexture *texture);

  bool setConst(int vertexIndex, int pixelIndex, const float* values, unsigned count4);
  bool setConst(int index, const float* values, unsigned count4);
  bool setConst(const char* name, const float* values, unsigned count4);

  void setShader(RenderShader* shader);
  bool setShader(FixedState state);

  bool setVertexBuffer(VertexBuffer::Types type, VertexBuffer* vertexBuffer, unsigned offset = 0);
  bool setVertexData(VertexBuffer::Types type, const void* data, unsigned size);
  bool setIndexBuffer(IndexBuffer* indexBuffer);
  int setIndexData(const unsigned short* data, unsigned count);
  bool bindInstanceData(VertexBuffer::Types types, const void* instances, unsigned count, unsigned stride, bool indexed = false);
  void unbindInstanceData();

  bool usesInstancing() const;

  void draw(const BasePrimitiveDrawer* drawers, unsigned count);
  void draw(const BasePrimitiveDrawer& drawer);

  bool drawBox(const matrix& transform);
  bool drawBox(const box& bounds);
  bool drawBox(const box& bounds, const matrix& transform);

  bool drawLineBox(const matrix& transform);
  bool drawLineBox(const box& bounds);
  bool drawLineBox(const box& bounds, const matrix& transform);

  bool drawSphere(const matrix& transform, unsigned level = 3);
  bool drawSphere(const sphere& bounds, unsigned level = 3);
  bool drawSphere(const sphere& bounds, const matrix& transform, unsigned level = 3);

  bool drawCone(const matrix& transform);
  bool drawLineCone(const matrix& transform);

  bool drawScreenQuad();

  bool drawText(const vec2 &pos, const QString &text, GraphicsFont::Flags flags = 0, const QColor &color = QColor(255,255,255));

public:
  bool isConstUsed(unsigned index) const;
  bool isSamplerUser(unsigned index) const;

public:
  const RenderStats& stats() const;
  void resetStats();

private:
  void lostBox();
  void restoreBox();
  void lostSphere();
  void restoreSphere();
  void lostCone();
  void restoreCone();

public:
  IDirect3DDevice9* operator -> () const;
  operator IDirect3DDevice9* () const;

private:
  QScopedPointer<RenderSystemPrivate> d_ptr;

  friend class QRenderWidget;
};

RENDER_EXPORT RenderSystem& getRenderSystem();

#endif // RENDERSYSTEM_HPP
