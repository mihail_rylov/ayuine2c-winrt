#include "RenderComponentWidget.hpp"
#include "RenderComponent.hpp"
#include "RenderSystem.hpp"
#include "RenderQuery.hpp"

#include <QElapsedTimer>

#include <Windows.h>

RenderComponentWidget::RenderComponentWidget(QWidget *parent) :
    QRenderWidget(parent)
{
  m_event.reset(new RenderQuery(RenderQuery::Event));
}

RenderComponentWidget::~RenderComponentWidget()
{
  foreach(RenderComponent* renderComponent, m_components)
  {
    renderComponent->m_renderWidget = NULL;
    delete renderComponent;
  }
}

const QVector<RenderComponent *> & RenderComponentWidget::components() const
{
  return m_components;
}

void RenderComponentWidget::setComponents(const QVector<RenderComponent *> &components)
{
  foreach(RenderComponent* component, m_components)
    delete component;

  m_components = components;

  if(isVisible())
  {
    foreach(RenderComponent* component, m_components)
      component->resizeEvent(size());
  }
}

bool RenderComponentWidget::addComponent(QScopedPointer<RenderComponent> &renderComponent)
{
  if(addComponent(renderComponent.data()))
  {
    renderComponent.take();
    return true;
  }
  return false;
}

bool RenderComponentWidget::addComponent(RenderComponent *renderComponent)
{
  if(!renderComponent)
    return false;
  if(renderComponent->renderWidget())
    return false;
  if(m_components.contains(renderComponent))
    return false;
  m_components.append(renderComponent);
  renderComponent->m_renderWidget = this;

  if(isVisible())
  {
    renderComponent->resizeEvent(size());
    update();
  }
  return true;
}

bool RenderComponentWidget::removeComponent(RenderComponent *renderComponent)
{
  if(!renderComponent)
    return false;
  if(renderComponent->renderWidget() != this)
    return false;
  int index = m_components.indexOf(renderComponent);
  if(index < 0)
    return false;
  m_components.remove(index);
  renderComponent->m_renderWidget = NULL;
  return true;
}

bool RenderComponentWidget::removeComponent(unsigned index)
{
  if(index >= (unsigned)m_components.size())
    return false;
  RenderComponent* renderComponent = m_components[index];
  m_components.remove(index);
  renderComponent->m_renderWidget = NULL;
  delete renderComponent;
  return true;
}

void RenderComponentWidget::resizeEvent(QResizeEvent *event)
{
  QRenderWidget::resizeEvent(event);

  foreach(RenderComponent* renderComponent, m_components)
  {
    renderComponent->resizeEvent(size());
  }
}


void RenderComponentWidget::showEvent(QShowEvent *event)
{
  QRenderWidget::showEvent(event);

  if(isVisible())
  {
    foreach(RenderComponent* renderComponent, m_components)
    {
      renderComponent->resizeEvent(size());
    }
  }
}

bool isBeginLess(RenderComponent* a, RenderComponent* b)
{
  return a->beginOrder() < b->beginOrder();
}

bool isDrawLess(RenderComponent* a, RenderComponent* b)
{
  return a->drawOrder() < b->drawOrder();
}

bool RenderComponentWidget::render()
{
  if(m_components.isEmpty())
  {
    return QRenderWidget::render();
  }

  QElapsedTimer drawTimer;
  drawTimer.start();

  emit prepareComponents();

  if(m_event)
  {
    m_event->result();
  }

  QVector<RenderComponent *> begin = m_components;
  QVector<RenderComponent *> draw = m_components;

  qSort(begin.begin(), begin.end(), isBeginLess);
  qSort(draw.begin(), draw.end(), isDrawLess);

  int beginIndex = 0;
  int prevDrawOrder = -INT_MAX;

  for(int drawIndex = 0; drawIndex < draw.size(); ++drawIndex)
  {
    int drawOrder = draw[drawIndex]->drawOrder();

    bool needsRenderTarget = false;

    if(prevDrawOrder < 0 && drawOrder >= 0)
      needsRenderTarget = true;

    for( ; beginIndex < begin.size(); ++beginIndex)
    {
      if(begin[beginIndex]->beginOrder() > drawOrder)
        break;

      if(begin[beginIndex]->beginOrder() >= 0)
      {
        if(needsRenderTarget)
        {
          setRenderTarget();
          needsRenderTarget = false;
        }
      }

      begin[beginIndex]->begin();
    }

    if(needsRenderTarget)
    {
      setRenderTarget();
    }

    draw[drawIndex]->draw();
    prevDrawOrder = drawOrder;
  }

  if(m_event)
  {
    m_event->end();
  }

  emit finalizeComponents();
  return true;
}

float RenderComponentWidget::drawTime() const
{
  return m_drawTime;
}

RenderTarget * RenderComponentWidget::depthTarget() const
{
  foreach(RenderComponent* renderComponent, m_components)
  {
    if(RenderTarget* depthTarget = renderComponent->depthTarget())
      return depthTarget;
  }
  return NULL;
}

RenderTarget * RenderComponentWidget::normalTarget() const
{
  foreach(RenderComponent* renderComponent, m_components)
  {
    if(RenderTarget* normalTarget = renderComponent->normalTarget())
      return normalTarget;
  }
  return NULL;
}

QStringList RenderComponentWidget::toStringList() const
{
  QStringList list;

  foreach(RenderComponent* renderComponent, m_components)
  {
    QStringList list2 = renderComponent->toStringList();

    if(list2.size())
    {
      if(list.size())
        list << "";
      list << renderComponent->metaObject()->className();
      list << list2;
    }
  }

  return list;
}
