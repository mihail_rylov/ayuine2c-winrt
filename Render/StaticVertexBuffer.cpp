#include "StaticVertexBuffer.hpp"
#include "VertexBuffer_p.hpp"

StaticVertexBuffer::StaticVertexBuffer(const void* vertices, unsigned count, unsigned stride)
{
  Q_ASSERT(vertices);
  Q_ASSERT(count);
  Q_ASSERT(stride);
  d_ptr->create(vertices, count, stride);
}

