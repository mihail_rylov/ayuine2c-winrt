#include "RenderQuery.hpp"
#include "RenderQuery_p.hpp"
#include "RenderSystem.hpp"
#include <d3d9.h>

RenderQuery::RenderQuery(Type type) :
  QObject(&getRenderSystem()), d_ptr(new RenderQueryPrivate)
{
  d_ptr->create(type);
  connect(&getRenderSystem(), SIGNAL(deviceLost()), SLOT(deviceLost()));
  connect(&getRenderSystem(), SIGNAL(deviceRestore()), SLOT(deviceReset()));
}

RenderQuery::~RenderQuery()
{
}

RenderQuery::Type RenderQuery::type()
{
  return d_ptr->m_type;
}

RenderQueryPrivate::RenderQueryPrivate()
{
  m_type = RenderQuery::Undefined;
  m_running = false;
  m_result = 0;
}

bool RenderQueryPrivate::create(RenderQuery::Type type)
{
  if(m_query)
    return false;

  HRESULT hr;

  switch(type)
  {
  case RenderQuery::Occlussion:
    hr = getRenderSystem()->CreateQuery(D3DQUERYTYPE_OCCLUSION, (LPDIRECT3DQUERY9*)&m_query);
    break;

  case RenderQuery::Event:
    hr = getRenderSystem()->CreateQuery(D3DQUERYTYPE_EVENT, (LPDIRECT3DQUERY9*)&m_query);
    break;

  default:
    hr = E_INVALIDARG;
    break;
  }

  if(dxe(hr))
  {
    m_type = type;
    m_result = 0;
    m_running = false;
    return true;
  }
  return false;
}

bool RenderQuery::isRunning() const
{
  return d_ptr->m_running;
}

bool RenderQuery::begin()
{
  return d_ptr->begin();
}

bool RenderQueryPrivate::begin()
{
  if(!m_query)
    return false;
  if(m_running)
    return true;

  switch(m_type)
  {
  case RenderQuery::Occlussion:
    if(dxe(m_query->Issue(D3DISSUE_BEGIN)))
    {
      m_running = true;
      return true;
    }
    return false;

  case RenderQuery::Event:
    m_running = true;
    return true;

  default:
    return false;
  }
}

void RenderQueryPrivate::end()
{
  if(!m_query)
    return;

  switch(m_type)
  {
  case RenderQuery::Occlussion:
    if(!m_running)
      break;
    m_query->Issue(D3DISSUE_END);
    m_running = false;
    m_result = ~0;
    break;

  case RenderQuery::Event:
    m_query->Issue(D3DISSUE_END);
    m_running = false;
    break;

  case RenderQuery::Undefined:
    break;
  }
}

bool RenderQueryPrivate::flush()
{
  if(!m_query)
    return false;

  void* data = 0;
  unsigned size = 0;

  switch(m_type)
  {
  case RenderQuery::Occlussion:
    if(m_result != ~0U)
      return true;
    if(m_running)
      end();
    data = &m_result;
    size = sizeof(m_result);
    break;

  case RenderQuery::Event:
    // end();
    break;

  default:
    return false;
  }

  while(m_query->GetData(&m_result, sizeof(m_result), D3DGETDATA_FLUSH) == S_FALSE);
  return true;
}

void RenderQuery::end()
{
  d_ptr->end();
}

unsigned RenderQuery::result()
{
  if(d_ptr->flush())
    return d_ptr->m_result;
  return 0;
}

void RenderQuery::deviceLost()
{
  d_ptr->m_query.reset();
  d_ptr->m_running = false;
  d_ptr->m_result = 0;
}

void RenderQuery::deviceReset()
{
  d_ptr->create(d_ptr->m_type);
}
