#ifndef RENDERSYSTEM_P_HPP
#define RENDERSYSTEM_P_HPP

#include "Render_p.hpp"
#include <Math/MathLib.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Math/Matrix.hpp>
#include <QRect>
#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"
#include "RenderShader_p.hpp"
#include "RenderSystem.hpp"

#include <d3d9.h>

typedef float float2[2];
typedef float float3[3];
typedef float float4[4];
typedef float float3x4[3][4];
typedef float float4x4[4][4];

#pragma pack(4)
struct FrameConsts
{
  float2 invViewport;
  float deltaTime, frameTime;
};

struct VSRenderConsts
{
  FrameConsts frame;
  matrix projView;
  matrix obj;
  float4 viewOrigin;
  float4 viewAt;
  float4 viewUp;
  float4 viewRight;
};

struct PSRenderConsts
{
  FrameConsts frame;
  float4 color;
};
#pragma pack()

struct IDirect3D9;
struct IDirect3DDevice9;
struct IDirect3DVertexDeclaration9;
struct ID3DXFont;

class BaseDynamicVertexBuffer;
class RenderShader;

template<typename T>
class DynamicIndexBuffer;
typedef DynamicIndexBuffer<unsigned short> ShortDynamicIndexBuffer;

class IndexedPrimitiveDrawer;

class RenderSystemPrivate
{
public:
  enum { MaxShaderConsts = 256 };

  boost::intrusive_ptr<IDirect3D9> m_d3d9;
  boost::intrusive_ptr<IDirect3DDevice9> m_device;
  D3DCAPS9 m_caps;
  D3DPRESENT_PARAMETERS m_d3dpp;
  QWidget* m_widget;

  QRect m_viewport;
  VSRenderConsts m_vsConsts;
  PSRenderConsts m_psConsts;
  QScopedPointer<BaseDynamicVertexBuffer> m_dynamicVertexBuffer;
  QScopedPointer<ShortDynamicIndexBuffer> m_dynamicIndexBuffer;
  boost::intrusive_ptr<IDirect3DVertexDeclaration9> m_vertexDeclarations[VertexBuffer::Max];
  QVector<ShaderConstDesc> m_shaderConsts;
  QVector<ShaderSamplerDesc> m_shaderSamplers;
  QBitArray m_shaderUsedConsts;
  QBitArray m_shaderUsedSamplers;
  QScopedPointer<RenderShader> m_fixedShaders[RenderSystem::FixedCount];

  QScopedPointer<VertexBuffer> m_boxVertexBuffer;
  QScopedPointer<ShortStaticIndexBuffer> m_boxIndexBuffer, m_boxLineIndexBuffer;

  QScopedPointer<VertexBuffer> m_coneVertexBuffer;
  QScopedPointer<ShortStaticIndexBuffer> m_coneIndexBuffer, m_coneLineIndexBuffer;

  QScopedPointer<VertexBuffer> m_sphereVertexBuffer;
  QScopedPointer<ShortStaticIndexBuffer> m_sphereIndexBuffer;
  QVector<IndexedPrimitiveDrawer> m_sphereLevels;

  boost::intrusive_ptr<ID3DXFont> m_font;
  TEXTMETRIC m_fontMetric;
  int m_fontWidths[256];
  unsigned m_sceneLevel;

public:
  enum { MaxTextures = 8, MaxRenderTargets = 4 };
  RenderTexture* m_textures[MaxTextures];
  RenderShader* m_shader;
  RenderSystem::BlendState m_blendState;
  RenderSystem::AlphaTest m_alphaTest;
  RenderSystem::DepthStencilState m_depthStencilState;
  bool m_depthBias;
  RenderSystem::RasterizerState m_rasterizerState;
  RenderSystem::SamplerState m_samplerState[MaxTextures];
  RenderSystem::FillMode m_fillMode;
  IndexBuffer* m_indexBuffer;
  VertexBuffer::Types m_vertexType;
  VertexBuffer* m_vertexBuffer;
  int m_vertexOffset;
  bool m_usesInstancing;

public:
  RenderStats m_stats;

public:
  RenderSystemPrivate();
};

#endif // RENDERSYSTEM_P_HPP
