#include "VertexBuffer.hpp"
#include "VertexBuffer_p.hpp"
#include "RenderSystem.hpp"
#include <d3d9.h>
#include <Math/Vec2.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Math/Matrix.hpp>

VertexBuffer::VertexBuffer() :
  QObject(&getRenderSystem()), d_ptr(new VertexBufferPrivate)
{
}

VertexBuffer::VertexBuffer(const void* vertices, unsigned count, unsigned stride) :
  QObject(&getRenderSystem()), d_ptr(new VertexBufferPrivate)
{
  Q_ASSERT(vertices);
  Q_ASSERT(count);
  Q_ASSERT(stride);
  d_ptr->create(vertices, count, stride);
}

VertexBuffer::VertexBuffer(unsigned count, unsigned stride) :
  QObject(&getRenderSystem()), d_ptr(new VertexBufferPrivate)
{
  Q_ASSERT(count);
  Q_ASSERT(stride);
  d_ptr->create(count, stride, false);
}

VertexBuffer::~VertexBuffer()
{
}

unsigned VertexBuffer::stride() const
{
  return d_ptr->m_stride;
}

unsigned VertexBuffer::count() const
{
  return d_ptr->m_count;
}

unsigned VertexBuffer::size() const
{
  return d_ptr->m_stride * d_ptr->m_count;
}

VertexBufferPrivate::VertexBufferPrivate()
{
  m_count = 0;
  m_stride = 0;
  m_dynamic = false;
  m_locked = false;
  m_offset = 0;
}

VertexBufferPrivate::~VertexBufferPrivate()
{
  unlock();
}

bool VertexBufferPrivate::create(unsigned count, unsigned stride, bool dynamic)
{
  if(m_handle)
    return false;

  D3DPOOL pool;
  unsigned usage;

  if(dynamic)
  {
    pool = D3DPOOL_DEFAULT;
    usage = D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY;
  }
  else
  {
    pool = D3DPOOL_MANAGED;
    usage = D3DUSAGE_WRITEONLY;
  }

  if(dxe(getRenderSystem()->CreateVertexBuffer(
           count * stride, usage, 0,
           pool,
           (LPDIRECT3DVERTEXBUFFER9*)&m_handle,
           NULL)))
  {
    m_count = count;
    m_stride = stride;
    m_dynamic = dynamic;
    m_offset = dynamic ? count : 0;
    m_locked = false;
    return true;
  }
  return false;
}

void* VertexBufferPrivate::lock(unsigned offset, unsigned count, bool nooverwrite)
{
  if(!m_handle)
    return NULL;

  if(m_locked)
    unlock();

  unsigned flags;

  if(m_dynamic)
    flags = nooverwrite ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD;
  else
    flags = nooverwrite ? D3DLOCK_NOOVERWRITE : 0;

  void* dest;
  if(dxe(m_handle->Lock(offset * m_stride, count * m_stride, &dest, flags)))
  {
    m_locked = true;
    return dest;
  }
  return NULL;
}

void VertexBufferPrivate::unlock()
{
  if(!m_handle)
    return;

  if(m_locked)
  {
    m_handle->Unlock();
    m_locked = false;
  }
}

bool VertexBufferPrivate::create(const void* data, unsigned count, unsigned stride)
{
  Q_ASSERT(data);
  Q_ASSERT(count);

  if(create(count, stride, false))
  {
    if(void* locked = lock(0, count))
    {
      memcpy(locked, data, count * m_stride);
      unlock();
      return true;
    }
  }
  return false;
}

BaseDynamicVertexBuffer::BaseDynamicVertexBuffer(unsigned count, unsigned stride)
{
  Q_ASSERT(count);
  Q_ASSERT(stride);

  if(d_ptr->create(count, stride, true))
  {
    connect(&getRenderSystem(), SIGNAL(deviceLost()), SLOT(deviceLost()));
    connect(&getRenderSystem(), SIGNAL(deviceRestore()), SLOT(deviceReset()));
  }
}

void VertexBuffer::set(const void *data, unsigned count, unsigned offset)
{
  if(void * locked = d_ptr->lock(offset, count))
  {
    memcpy(locked, data, count * d_ptr->m_stride);
    d_ptr->unlock();
  }
}

int BaseDynamicVertexBuffer::append(const void *data, unsigned count)
{
  int offset = 0;
  if(void * locked = lockAppend(count, offset))
  {
    memcpy(locked, data, count * d_ptr->m_stride);
    d_ptr->unlock();
    return offset;
  }
  return -1;
}

unsigned VertexBuffer::stride(Types type)
{
  unsigned stride = sizeof(vec3); // vtVertex
  if(type & Coords)
    stride += sizeof(vec2);
  if(type & Coords2)
    stride += sizeof(vec2);
  if(type & Color)
    stride += sizeof(unsigned);
  if(type & Normal)
    stride += sizeof(vec3);
  if(type & Tangent)
    stride += sizeof(vec4);
  if(type & Size)
    stride += sizeof(float);
  return stride;
}

unsigned VertexBuffer::decl(Types type, D3DVERTEXELEMENT9 *declList)
{
  unsigned offset = 0;

  // Dodaj wierzchołek
  if((type & Vertex) == Vertex)
  {
    D3DVERTEXELEMENT9 decl = {0, offset, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0};
    *declList++ = decl;
    offset += sizeof(vec3);
  }

  // Dodaj koordynanty tekstur
  if((type & Coords) == Coords)
  {
    D3DVERTEXELEMENT9 decl = {0, offset, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0};
    *declList++ = decl;
    offset += sizeof(vec2);
  }

  // Dodaj drugie koordynanty tekstur
  if((type & Coords2) == Coords2)
  {
    D3DVERTEXELEMENT9 decl = {0, offset, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1};
    *declList++ = decl;
    offset += sizeof(vec2);
  }

  // Dodaj kolor wierzchołka
  if((type & Color) == Color)
  {
    D3DVERTEXELEMENT9 decl = {0, offset, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0};
    *declList++ = decl;
    offset += sizeof(unsigned);
  }

  // Dodaj wektor normalny
  if((type & Normal) == Normal)
  {
    D3DVERTEXELEMENT9 decl = {0, offset, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0};
    *declList++ = decl;
    offset += sizeof(vec3);
  }

  // Dodaj wektor styczny
  if((type & Tangent) == Tangent)
  {
    D3DVERTEXELEMENT9 decl = {0, offset, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0};
    *declList++ = decl;
    offset += sizeof(vec4);
  }

  // Dodaj wielkość wierzchołka
  if((type & Size) == Size)
  {
    D3DVERTEXELEMENT9 decl = {0, offset, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_PSIZE, 0};
    *declList++ = decl;
    offset += sizeof(float);
  }

  // Dodaj dane do instancingu
  if((type & Instanced) == Instanced)
  {
    D3DVERTEXELEMENT9 decl0 = {1, 0,  D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2};
    D3DVERTEXELEMENT9 decl1 = {1, 16,  D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3};
    D3DVERTEXELEMENT9 decl2 = {1, 32,  D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4};
    //D3DVERTEXELEMENT9 decl3 = {1, 48,  D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 5};
    *declList++ = decl0;
    *declList++ = decl1;
    *declList++ = decl2;
    //*declList++ = decl3;

    if((type & InstanceColor) == InstanceColor)
    {
      D3DVERTEXELEMENT9 decl3 = {1, 48,  D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 1};
      *declList++ = decl3;
    }
  }

  // Zakończ opis deklaracji
  D3DVERTEXELEMENT9 declEnd = D3DDECL_END();
  *declList++ = declEnd;
  return offset;
}

VertexBuffer::Types VertexBuffer::encodeStride(Types type, unsigned stride)
{
  type &= StrideOffset-1;
  type |= Type(stride << StrideOffset);
  return type;
}

unsigned VertexBuffer::decodeStride(Types type)
{
  return type >> StrideOffset;
}

VertexBuffer::Types VertexBuffer::decodeType(Types type)
{
  return Types(type & (Max-1));
}

void BaseDynamicVertexBuffer::deviceLost()
{
  d_ptr->m_handle.reset();
}

void BaseDynamicVertexBuffer::deviceReset()
{
  if(d_ptr->m_stride)
  {
    d_ptr->m_handle.reset();
    d_ptr->create(d_ptr->m_count, d_ptr->m_stride, d_ptr->m_dynamic);
  }
}

unsigned VertexBuffer::instanceStride(Types type)
{
  unsigned stride = 0;
  if(type & Instanced)
  {
    stride += sizeof(vec4) * 3;
    if(type & InstanceColor)
      stride += sizeof(vec4);
  }
  return stride;
}

bool VertexBuffer::isLocked() const
{
  return d_ptr->m_locked;
}

void VertexBuffer::unlock()
{
  d_ptr->unlock();
}

void * BaseDynamicVertexBuffer::lockAppend(unsigned count, int &offset)
{
  if(count > d_ptr->m_count)
    return NULL;

  bool nooverwrite = false;

  if(d_ptr->m_offset + count > d_ptr->m_count)
  {
    offset = 0;
  }
  else
  {
    offset = d_ptr->m_offset;
    nooverwrite = true;
  }

  if(void * locked = d_ptr->lock(offset, count, nooverwrite))
  {
    d_ptr->m_offset += count;
    return locked;
  }
  offset = -1;
  return NULL;
}

void * VertexBuffer::lock(unsigned count, unsigned offset)
{
  return d_ptr->lock(offset, count, false);
}
