#ifndef INDEXBUFFER_HPP
#define INDEXBUFFER_HPP

#include <QObject>
#include <QScopedPointer>
#include "Render.hpp"

class IndexBufferPrivate;

class IndexedPrimitiveDrawer;

class RENDER_EXPORT IndexBuffer : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(IndexBuffer)
  Q_ENUMS(Format)

public:
  enum Format
  {
    None,
    Short = sizeof(unsigned short),
    Long = sizeof(unsigned long)
  };

protected:
  explicit IndexBuffer();
  explicit IndexBuffer(const void* indicies, unsigned count, Format format);
  explicit IndexBuffer(unsigned count, Format format);
public:
  ~IndexBuffer();

public slots:
  Format format() const;
  unsigned count() const;
  unsigned size() const;

public:
  void set(const void* data, unsigned count, unsigned offset = 0);

public:
  bool isLocked() const;
  void* lock(unsigned count, unsigned offset = 0);
  void unlock();

public:
  static QVector<quint32> stripify(const QVector<quint32>& indices);
  static QVector<quint32> stripify(const quint32* indices, unsigned indexCount);
  static QVector<quint16> stripify(const QVector<quint16>& indices);
  static QVector<quint16> stripify(const quint16* indices, unsigned indexCount);
  static void stripify(QVector<quint32>& indices, QVector<IndexedPrimitiveDrawer>& drawers);
  static void stripify(QVector<quint16>& indices, QVector<IndexedPrimitiveDrawer>& drawers);

protected:
  QScopedPointer<IndexBufferPrivate> d_ptr;

  friend class RenderSystem;
};

template<typename T>
class StaticIndexBuffer : public IndexBuffer
{
public:
  explicit StaticIndexBuffer(const T* indices, unsigned count)
    : IndexBuffer(indices, count, (Format)sizeof(T))
  {
  }
  explicit StaticIndexBuffer(const QVector<T>& indices)
    : IndexBuffer(indices.constData(), indices.size(), (Format)sizeof(T))
  {
  }
  explicit StaticIndexBuffer(unsigned count)
    : IndexBuffer(count, (Format)sizeof(T))
  {
  }

public:
  T* lock(unsigned count, unsigned offset = 0)
  {
    return (T*)IndexBuffer::lock(count, offset);
  }
  void set(const T* data, unsigned count, unsigned offset = 0)
  {
    IndexBuffer::set(data, count, offset);
  }
  void set(const QVector<T>& data, unsigned offset = 0)
  {
    set(data.constData(), data.size(), offset);
  }
};

typedef StaticIndexBuffer<quint16> ShortStaticIndexBuffer;
typedef StaticIndexBuffer<quint32> LongStaticIndexBuffer;

class BaseDynamicIndexBuffer : public IndexBuffer
{
  Q_OBJECT

public:
  explicit BaseDynamicIndexBuffer(unsigned indexCount, Format format);

public:
  int append(const void* data, unsigned count);

public:
  void* lockAppend(unsigned count, int& offset);

private slots:
  void deviceLost();
  void deviceReset();
};

template<typename T>
class DynamicIndexBuffer : public BaseDynamicIndexBuffer
{
public:
  DynamicIndexBuffer(unsigned indexCount)
    : BaseDynamicIndexBuffer(indexCount, (Format)sizeof(T))
  {}

public:
  void set(const T* data, unsigned count, unsigned offset = 0)
  {
    BaseDynamicIndexBuffer::set(data, count, offset);
  }

  void set(const QVector<T>& data, unsigned offset = 0)
  {
    set(data.constData(), data.size(), offset);
  }

  int append(const T* data, unsigned count)
  {
    return BaseDynamicIndexBuffer::append(data, count);
  }

  int append(const QVector<T>& data)
  {
    return append(data.constData(), data.size());
  }

  T* lock(unsigned count, unsigned offset = 0)
  {
    return (T*)BaseDynamicIndexBuffer::lock(count, offset);
  }

  T* lockAppend(unsigned count, int& offset)
  {
    return (T*)BaseDynamicIndexBuffer::lockAppend(count, offset);
  }
};

typedef DynamicIndexBuffer<unsigned short> ShortDynamicIndexBuffer;
typedef DynamicIndexBuffer<unsigned long> LongDynamicIndexBuffer;

#endif // INDEXBUFFER_HPP
