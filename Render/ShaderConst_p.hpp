#ifndef SHADERCONSTS_P_HPP
#define SHADERCONSTS_P_HPP

#include "Render_p.hpp"
#include <QByteArray>

class ShaderConstPrivate
{
public:
  QByteArray name;
  QByteArray data;
  unsigned updated;

public:
  ShaderConstPrivate()
  {
    updated = 0;
  }
};

#endif // SHADERCONSTS_P_HPP
