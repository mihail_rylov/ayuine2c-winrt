#ifndef RENDERCOMPONENT_HPP
#define RENDERCOMPONENT_HPP

#include "Render.hpp"
#include <QSize>
#include <QObject>

class RenderComponentWidget;
class RenderTarget;

class RENDER_EXPORT RenderComponent : public QObject
{
  Q_OBJECT

public:
  RenderComponent();
  virtual ~RenderComponent();

public:
  RenderComponentWidget* renderWidget() const;
  bool setWidgetRenderTarget();
  bool setWidgetDepthTarget();

  virtual QStringList toStringList() const;

public:
  virtual RenderTarget* depthTarget() const;
  virtual RenderTarget* normalTarget() const;

public:
  const QSize& size() const;

protected:
  virtual void resizeEvent(const QSize& newSize);

public:
  virtual int beginOrder();
  virtual int drawOrder();

public:
  virtual bool begin();
  virtual void draw() = 0;

private:
  RenderComponentWidget* m_renderWidget;
  QSize m_size;
  friend class RenderComponentWidget;
};

#endif // RENDERCOMPONENT_HPP
