#include "RenderSystem.hpp"
#include "RenderSystem_p.hpp"
#include "RenderTexture.hpp"
#include "RenderTexture_p.hpp"
#include "RenderShader.hpp"
#include "RenderShader_p.hpp"
#include "IndexBuffer.hpp"
#include "IndexBuffer_p.hpp"
#include "VertexBuffer.hpp"
#include "VertexBuffer_p.hpp"
#include "PrimitiveDrawer.hpp"
#include "RenderTarget.hpp"
#include "RenderTarget_p.hpp"
#include "RenderShaderConst.hpp"
#include <Math/Camera.hpp>
#include <d3d9.h>
#include <d3dx9.h>
#include <QWidget>
#include <QMetaEnum>

#define NVPERFHUD

static RenderSystem* renderSystem = NULL;

bool dxe(long hr)
{
  if(SUCCEEDED(hr))
    return true;
  qWarning() << "[D3D9] Error: " << QString::number((unsigned)hr, 16);
  return false;
}

RenderSystem& getRenderSystem()
{
  if(!renderSystem)
    renderSystem = new RenderSystem();
  return *renderSystem;
}

void deleteRenderSystem()
{
  if(renderSystem)
  {
    delete renderSystem;
    renderSystem = NULL;
  }
}

Q_DESTRUCTOR_FUNCTION(deleteRenderSystem)

RenderSystem::RenderSystem()
  : d_ptr(new RenderSystemPrivate)
{
  qDebug() << "[D3D9] Creating RenderSystem";

  renderSystem = this;

  d_ptr->m_d3d9.reset(Direct3DCreate9(D3D_SDK_VERSION));

  if(!d_ptr->m_d3d9)
  {
    qFatal("[D3D9] Failed to Create SDK");
    return;
  }

  qMemSet(&d_ptr->m_d3dpp, 0, sizeof(d_ptr->m_d3dpp));
  d_ptr->m_d3dpp.Windowed = TRUE;
  d_ptr->m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
  d_ptr->m_d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;
  d_ptr->m_d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
  d_ptr->m_d3dpp.MultiSampleQuality = 0;
  d_ptr->m_d3dpp.EnableAutoDepthStencil = TRUE;
  d_ptr->m_d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;

  // Ustaw domy�lne ustawienia
  unsigned adapterToUse = D3DADAPTER_DEFAULT;
  D3DDEVTYPE deviceType = D3DDEVTYPE_HAL;

#ifdef NVPERFHUD
  // Szukaj 'NVIDIA PerfHUD'
  for(unsigned adapter = 0; adapter < d_ptr->m_d3d9->GetAdapterCount(); ++adapter)
  {
    D3DADAPTER_IDENTIFIER9 id;

    if(!dxe(d_ptr->m_d3d9->GetAdapterIdentifier(adapter, 0, &id)))
      continue;

    if(strstr(id.Description, "PerfHUD") != 0)
    {
      qWarning() << "[D3D9] Found NVPerfHUD";
      adapterToUse = adapter;
      deviceType = D3DDEVTYPE_REF;
      break;
    }
  }
#endif // NVPERFHUD

  // Check Device Caps
  d_ptr->m_d3d9->GetDeviceCaps(adapterToUse, deviceType, &d_ptr->m_caps);

  DWORD behaviorFlags = D3DCREATE_HARDWARE_VERTEXPROCESSING;
  if( ( d_ptr->m_caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT ) == 0 ||
      d_ptr->m_caps.VertexShaderVersion < D3DVS_VERSION( 2, 0 ) )
  {
    behaviorFlags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
  }

  // Hardware Device
  HRESULT hr = d_ptr->m_d3d9->CreateDevice(adapterToUse, deviceType, GetDesktopWindow(),
                                           behaviorFlags, &d_ptr->m_d3dpp, (LPDIRECT3DDEVICE9*)&d_ptr->m_device);

  if(FAILED(hr))
  {
    qFatal("[D3D9] Failed to Create Device");
    return;
  }

  if(1)
  {
    D3DXFONT_DESC desc;

    // Wype�nij opis
    ZeroMemory(&desc, sizeof(desc));
    desc.Height = 12;
    desc.Width = 6;
    wcscpy(desc.FaceName, L"Verdana");

    dxe(D3DXCreateFontIndirect(d_ptr->m_device.get(), &desc, (LPD3DXFONT*)&d_ptr->m_font));
    dxe(d_ptr->m_font->GetTextMetrics(&d_ptr->m_fontMetric));
    GetCharWidth32(d_ptr->m_font->GetDC(), 0, 255, d_ptr->m_fontWidths);
  }

  restoreDeviceObjects();
}

RenderSystem::~RenderSystem()
{
  qDebug() << "[D3D9] Deleting RenderSystem";

  lostDeviceObjects();

  d_ptr->m_font.reset();
  d_ptr->m_device.reset();
  d_ptr->m_d3d9.reset();

  renderSystem = NULL;
}

IDirect3DDevice9* RenderSystem::operator -> () const
{
  return d_ptr->m_device.get();
}

RenderSystem::operator IDirect3DDevice9* () const
{
  return d_ptr->m_device.get();
}

void RenderSystem::clear(const QColor& color)
{
  clear(ClearOptions(Target | Depth | Stencil), color);
}

void RenderSystem::clear(ClearOptions options, const QColor& color, float depth, int stencil)
{
  if(!d_ptr->m_device)
    return;
  unsigned flags = 0;
  if(options & Target)
    flags |= D3DCLEAR_TARGET;
  if(options & Depth)
    flags |= D3DCLEAR_ZBUFFER;
  if(options & Stencil)
    flags |= D3DCLEAR_STENCIL;
  dxe(d_ptr->m_device->Clear(0, NULL, flags, color.rgba(), depth, stencil));
}

void RenderSystem::setViewport(const QRect& viewport)
{
  if(!d_ptr->m_device)
    return;

  D3DVIEWPORT9 vp =
  {
    viewport.x(), viewport.y(),
    viewport.width(), viewport.height(),
    0.0f, 1.0f
  };

  dxe(d_ptr->m_device->SetViewport(&vp));

  d_ptr->m_viewport = viewport;

  d_ptr->m_vsConsts.frame.invViewport[0] =
      d_ptr->m_psConsts.frame.invViewport[0] =
      1.0f / viewport.width();

  d_ptr->m_vsConsts.frame.invViewport[1] =
      d_ptr->m_psConsts.frame.invViewport[1] =
      1.0f / viewport.height();

  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::Frame, d_ptr->m_vsConsts.frame.invViewport, 1);
  d_ptr->m_device->SetPixelShaderConstantF(PixelShaderConst::Frame, d_ptr->m_psConsts.frame.invViewport, 1);
}

void RenderSystem::setScissor(const QRect* scissor)
{
  if(!d_ptr->m_device)
    return;

  if(scissor)
  {
    QRect viewport = d_ptr->m_viewport.intersect(*scissor);

    RECT rectangle =
    {
      viewport.left(), viewport.top(),
      viewport.right(), viewport.bottom()
    };

    dxe(d_ptr->m_device->SetScissorRect(&rectangle));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SCISSORTESTENABLE, true));
  }
  else
  {
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SCISSORTESTENABLE, false));
  }
}

void RenderSystem::setProjView(const matrix &proj, const matrix &view)
{
  if(!d_ptr->m_device)
    return;

  matrix::multiply(d_ptr->m_vsConsts.projView, proj, view);
  d_ptr->m_vsConsts.obj = mat4Identity;

  matrix invProjView;
  matrix::invert(invProjView, d_ptr->m_vsConsts.projView);

  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::ProjView, &d_ptr->m_vsConsts.projView.m11, 4);
  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::Object, &d_ptr->m_vsConsts.obj.m11, 3);
  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::ProjViewObject, &d_ptr->m_vsConsts.projView.m11, 4);
  d_ptr->m_device->SetPixelShaderConstantF(PixelShaderConst::InvProjView, &invProjView.m11, 4);
}

void RenderSystem::setTransform(const matrix& transform)
{
  if(!d_ptr->m_device)
    return;

  d_ptr->m_vsConsts.obj = transform;

  matrix projViewObject;
  matrix::multiply(projViewObject, d_ptr->m_vsConsts.projView, transform);

  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::Object, &d_ptr->m_vsConsts.obj.m11, 3);
  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::ProjViewObject, &projViewObject.m11, 4);
}

void RenderSystem::setCamera(const Camera& camera)
{
  if(!d_ptr->m_device)
    return;

  float dist = 1.0f;
  if(!qFuzzyIsNull(camera.distance))
    dist = 1.0f / camera.distance;

  vec4(camera.origin, dist).copy(d_ptr->m_vsConsts.viewOrigin);
  vec4(camera.at, 1.0f).copy(d_ptr->m_vsConsts.viewAt);
  vec4(camera.up, 1.0f).copy(d_ptr->m_vsConsts.viewUp);
  vec4(camera.right, 1.0f).copy(d_ptr->m_vsConsts.viewRight);  

  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::ViewOrigin, d_ptr->m_vsConsts.viewOrigin, 1);
  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::ViewAt, d_ptr->m_vsConsts.viewAt, 1);
  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::ViewUp, d_ptr->m_vsConsts.viewUp, 1);
  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::ViewRight, d_ptr->m_vsConsts.viewRight, 1);

  setProjView(camera.projection, camera.view);
}

void RenderSystem::setFrame(float deltaTime, float frameTime)
{
  if(!d_ptr->m_device)
    return;

  d_ptr->m_vsConsts.frame.deltaTime =
      d_ptr->m_psConsts.frame.deltaTime =
      deltaTime;

  d_ptr->m_vsConsts.frame.frameTime =
      d_ptr->m_psConsts.frame.frameTime =
      frameTime;

  d_ptr->m_device->SetVertexShaderConstantF(VertexShaderConst::Frame, d_ptr->m_vsConsts.frame.invViewport, 1);
  d_ptr->m_device->SetPixelShaderConstantF(PixelShaderConst::Frame, d_ptr->m_psConsts.frame.invViewport, 1);
}

void RenderSystem::setColor(const QColor &color)
{
  if(!d_ptr->m_device)
    return;

  vec4(color.redF(), color.greenF(), color.blueF(), color.alphaF()).copy(d_ptr->m_psConsts.color);
  d_ptr->m_device->SetPixelShaderConstantF(PixelShaderConst::Color, d_ptr->m_psConsts.color, 1);
}

void RenderSystem::setBlendState(BlendState state)
{
  if(!d_ptr->m_device)
    return;

  if(d_ptr->m_blendState == state)
  {
    d_ptr->m_stats.ignoredStateChanges++;
    return;
  }

  switch(state)
  {
  case Additive:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_ALPHA));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHABLENDENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE));
    break;

  case AlphaBlend:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_ALPHA));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHABLENDENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA));
    break;

  case Overlay:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_ALPHA));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHABLENDENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE));
    break;

  case NonPremultipled:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_ALPHA));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHABLENDENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA));
    break;

  case Opaque:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_ALPHA));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHABLENDENABLE, false));
    break;

  case DisableColorWrite:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_COLORWRITEENABLE, 0));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHABLENDENABLE, false));
    break;
  }

  d_ptr->m_stats.blendChanges++;
  d_ptr->m_blendState = state;
}

void RenderSystem::setAlphaTest(RenderSystem::AlphaTest alphaTest)
{
  if(!d_ptr->m_device)
    return;

  if(d_ptr->m_alphaTest == alphaTest)
  {
    d_ptr->m_stats.ignoredStateChanges++;
    return;
  }

  switch(alphaTest)
  {
  case AlphaTestEnabled:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHATESTENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHAREF, 0x80));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER));
    break;

  case AlphaTestEnabledLow:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHATESTENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHAREF, 0x40));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER));
    break;

  case AlphaTestInverted:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHATESTENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHAREF, 0x80));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_LESS));
    break;

  case AlphaTestDisabled:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ALPHATESTENABLE, false));
    break;
  }

  d_ptr->m_stats.alphaTestChanges++;
  d_ptr->m_alphaTest = alphaTest;
}

template<typename To, typename From>
inline To absolute_cast(From v)
{
  return (To&)v;
}

void RenderSystem::setDepthStencilState(DepthStencilState state, bool bias)
{
  if(!d_ptr->m_device)
    return;

  if(d_ptr->m_depthStencilState == state && d_ptr->m_depthBias == bias)
  {
    d_ptr->m_stats.ignoredStateChanges++;
    return;
  }

  switch(state)
  {
  case DepthDefault:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZWRITEENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILENABLE, false));
    break;

  case DepthRead:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZWRITEENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILENABLE, false));
    break;

  case DepthNone:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZWRITEENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILENABLE, false));
    break;

  case DepthClearStencil:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZWRITEENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZENABLE, false));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_ZERO));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_ZERO));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_ZERO));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, false));
    break;

  case DepthFillEarlyCull:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZWRITEENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZENABLE, true));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILREF, 0));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_DECR));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_INCR));
    break;

  case DepthRenderEarlyCull:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZWRITEENABLE, false));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_ZENABLE, false));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILREF, 0));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILMASK, 0xFF));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILENABLE, true));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_NOTEQUAL));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP));

    dxe(d_ptr->m_device->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, false));
    break;
  }

  if(bias)
  {
    dxe(d_ptr->m_device->SetRenderState(D3DRS_DEPTHBIAS, absolute_cast<unsigned>(-0.0001f)));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, absolute_cast<unsigned>(-0.2f)));
  }
  else
  {
    dxe(d_ptr->m_device->SetRenderState(D3DRS_DEPTHBIAS, absolute_cast<unsigned>(0)));
    dxe(d_ptr->m_device->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, absolute_cast<unsigned>(0)));
  }

  d_ptr->m_stats.depthStencilChanges++;
  d_ptr->m_depthStencilState = state;
  d_ptr->m_depthBias = bias;
}

void RenderSystem::setRasterizerState(RasterizerState state)
{
  if(!d_ptr->m_device)
    return;

  if(d_ptr->m_rasterizerState == state)
  {
    d_ptr->m_stats.ignoredStateChanges++;
    return;
  }

  switch(state)
  {
  case CullClockwise:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW));
    break;

  case CullCounterClockwise:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW));
    break;

  case CullNone:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE));
    break;
  }

  d_ptr->m_stats.rasterizerChanges++;
  d_ptr->m_rasterizerState = state;
}

void RenderSystem::setFillMode(RenderSystem::FillMode mode)
{
  if(!d_ptr->m_device)
    return;

  if(d_ptr->m_fillMode == mode)
  {
    d_ptr->m_stats.ignoredStateChanges++;
    return;
  }

  switch(mode)
  {
  case Wire:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME));
    break;

  case Solid:
    dxe(d_ptr->m_device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID));
    break;
  }

  d_ptr->m_stats.fillModeChanges++;
  d_ptr->m_fillMode = mode;
}

void RenderSystem::setSamplerState(SamplerState state, unsigned sampler)
{
  if(!d_ptr->m_device)
    return;

  if(sampler >= RenderSystemPrivate::MaxTextures)
    return;

  if(d_ptr->m_samplerState[sampler] == state)
  {
    d_ptr->m_stats.ignoredStateChanges++;
    return;
  }

  switch(state)
  {
  case PointClamp:
  case LinearClamp:
  case AnisotropicClamp:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSW, D3DTADDRESS_CLAMP));
    break;

  case PointWrap:
  case LinearWrap:
  case AnisotropicWrap:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP));
    break;

  case PointMirror:
  case LinearMirror:
  case AnisotropicMirror:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSW, D3DTADDRESS_MIRROR));
    break;

  case AnisotropicBorderBlack:
  case LinearBorderBlack:
  case PointBorderBlack:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSW, D3DTADDRESS_BORDER));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_BORDERCOLOR, D3DCOLOR_XRGB(0x0, 0x0, 0x0)));
    break;

  case AnisotropicBorderWhite:
  case LinearBorderWhite:
  case PointBorderWhite:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_ADDRESSW, D3DTADDRESS_BORDER));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_BORDERCOLOR, D3DCOLOR_XRGB(0xFF, 0xFF, 0xFF)));
    break;
  }

  switch(state)
  {
  case PointClamp:
  case PointWrap:
  case PointMirror:
  case PointBorderWhite:
  case PointBorderBlack:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MIPFILTER, D3DTEXF_NONE));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MINFILTER, D3DTEXF_POINT));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MAGFILTER, D3DTEXF_POINT));
    break;

  case LinearClamp:
  case LinearWrap:
  case LinearMirror:
  case LinearBorderWhite:
  case LinearBorderBlack:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MINFILTER, D3DTEXF_LINEAR));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR));
    break;

  case AnisotropicClamp:
  case AnisotropicWrap:
  case AnisotropicMirror:
  case AnisotropicBorderWhite:
  case AnisotropicBorderBlack:
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC));
    dxe(d_ptr->m_device->SetSamplerState(sampler, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC));
    break;
  }

  d_ptr->m_stats.samplerChanges++;
  d_ptr->m_samplerState[sampler] = state;
}

void RenderSystem::setRenderTargets(const RenderTargetBindings* renderTargets, unsigned count)
{
  if(!d_ptr->m_device)
    return;

  for(int i = 0; i < RenderSystemPrivate::MaxTextures; ++i)
  {
    if(d_ptr->m_textures[i])
    {
      d_ptr->m_device->SetTexture(i, NULL);
      d_ptr->m_textures[i] = NULL;
      d_ptr->m_stats.textureChanges++;
    }
  }

  if(!count)
  {
    for(int i = 1; i < RenderSystemPrivate::MaxRenderTargets; ++i)
      d_ptr->m_device->SetRenderTarget(i, NULL);
    d_ptr->m_device->SetDepthStencilSurface(NULL);
    return;
  }

  for(unsigned i = 0; i < RenderSystemPrivate::MaxRenderTargets; ++i)
  {
    if(i < count && renderTargets[i].renderTarget)
    {
      const RenderTargetBindings& binding = renderTargets[i];
      dxe(d_ptr->m_device->SetRenderTarget(i, binding.renderTarget->d_ptr->surface(binding.cubeFace).get()));
    }
    else
    {
      dxe(d_ptr->m_device->SetRenderTarget(i, NULL));
    }
  }

  if(count > 0 && renderTargets[0].renderTarget)
  {
    if(renderTargets[0].renderTarget->e_ptr->m_depthTarget)
    {
      dxe(d_ptr->m_device->SetDepthStencilSurface(renderTargets[0].renderTarget->e_ptr->m_depthTarget.get()));
    }

    QSize size(renderTargets[0].renderTarget->width(), renderTargets[0].renderTarget->height());
    setViewport(QRect(QPoint(), size));
  }
}

void RenderSystem::setRenderTarget(RenderTarget* renderTarget)
{
  RenderTargetBindings binding = {renderTarget, CubeFace::PX};
  setRenderTargets(&binding, 1);
}

void RenderSystem::setRenderTarget(CubeRenderTarget* renderTarget, CubeFace::Enum face)
{
  RenderTargetBindings binding = {renderTarget, face};
  setRenderTargets(&binding, 1);
}

void RenderSystem::setTexture(RenderTexture* texture, unsigned sampler)
{
  if(!d_ptr->m_device)
    return;

  if(sampler >= RenderSystemPrivate::MaxTextures)
    return;

  if(d_ptr->m_textures[sampler] == texture)
  {
    d_ptr->m_stats.ignoredStateChanges++;
    return;
  }

  d_ptr->m_stats.textureChanges++;
  d_ptr->m_textures[sampler] = texture;

  if(texture && texture->d_ptr->m_handle)
    dxe(d_ptr->m_device->SetTexture(sampler, texture->d_ptr->m_handle.get()));
  else
    dxe(d_ptr->m_device->SetTexture(sampler, NULL));
}

bool RenderSystem::setTexture(const char* name, RenderTexture *texture)
{
  if(d_ptr->m_shaderSamplers.empty())
    return false;
  if(!d_ptr->m_device)
    return false;

  ShaderSamplerDesc desc;
  desc.name = name;

  QVector<ShaderSamplerDesc>::const_iterator itor = qBinaryFind(d_ptr->m_shaderSamplers, desc);
  if(itor == d_ptr->m_shaderSamplers.end())
    return false;

  if(itor->pixelIndex >= 0)
  {
    setTexture(texture, itor->pixelIndex);
    return true;
  }
  return false;
}

bool RenderSystem::setConst(int vertexIndex, int pixelIndex, const float* values, unsigned count4)
{
  if(!d_ptr->m_device)
    return false;

  bool result = false;

  if(vertexIndex >= 0)
  {
    d_ptr->m_device->SetVertexShaderConstantF(vertexIndex, values, count4);
    d_ptr->m_stats.vertexConstChanges++;
    d_ptr->m_stats.vertexConstSize += count4;
    result = true;
  }

  if(pixelIndex >= 0)
  {
    d_ptr->m_device->SetPixelShaderConstantF(pixelIndex, values, count4);
    d_ptr->m_stats.pixelConstChanges++;
    d_ptr->m_stats.pixelConstSize += count4;
    result = true;
  }

  return result;
}

bool RenderSystem::setConst(int index, const float* values, unsigned count4)
{
  return setConst(index, index, values, count4);
}

bool RenderSystem::setConst(const char *name, const float *values, unsigned count4)
{
  if(d_ptr->m_shaderConsts.empty())
    return false;
  if(!d_ptr->m_device)
    return false;

  ShaderConstDesc desc;
  desc.name = name;

  QVector<ShaderConstDesc>::const_iterator itor = qBinaryFind(d_ptr->m_shaderConsts, desc);
  if(itor == d_ptr->m_shaderConsts.end())
    return false;

  return setConst(itor->vertexOffset, itor->pixelOffset, values, count4);
}

void RenderSystem::setShader(RenderShader* shader)
{
  if(!d_ptr->m_device)
    return;

  if(d_ptr->m_shader == shader)
  {
    d_ptr->m_stats.ignoredChanges++;
    return;
  }

  d_ptr->m_stats.shaderChanges++;
  d_ptr->m_shader = shader;

  if(!shader)
  {
    d_ptr->m_shaderUsedConsts.clear();
    d_ptr->m_shaderUsedSamplers.clear();
    d_ptr->m_shaderConsts.clear();
    d_ptr->m_shaderSamplers.clear();
    dxe(d_ptr->m_device->SetVertexShader(NULL));
    dxe(d_ptr->m_device->SetPixelShader(NULL));
    return;
  }

  dxe(d_ptr->m_device->SetVertexShader(shader->d_ptr->m_vertexShader.get()));
  dxe(d_ptr->m_device->SetPixelShader(shader->d_ptr->m_pixelShader.get()));

  d_ptr->m_shaderUsedConsts = shader->d_ptr->m_shaderUsedConsts;
  d_ptr->m_shaderUsedSamplers = shader->d_ptr->m_shaderUsedSamplers;
  d_ptr->m_shaderConsts = shader->d_ptr->m_shaderConsts;
  d_ptr->m_shaderSamplers = shader->d_ptr->m_shaderSamplers;
}

bool RenderSystem::setShader(FixedState state)
{
  if(state >= FixedCount)
    return false;
  setShader(d_ptr->m_fixedShaders[state].data());
  return true;
}

bool RenderSystem::setVertexBuffer(VertexBuffer::Types type, VertexBuffer* vertexBuffer, unsigned offset)
{
  if(!d_ptr->m_device)
    return false;

  unsigned stride = VertexBuffer::decodeStride(type);

  VertexBuffer::Types vertexType = VertexBuffer::decodeType(type);

  if(usesInstancing())
    vertexType |= VertexBuffer::Instanced;
  else
    vertexType &= ~VertexBuffer::Instanced;

  if(d_ptr->m_vertexType != vertexType)
  {
    dxe(d_ptr->m_device->SetVertexDeclaration(d_ptr->m_vertexDeclarations[vertexType].get()));
    d_ptr->m_vertexType = vertexType;
    d_ptr->m_stats.vertexTypeChanges++;
  }

  if(vertexBuffer)
  {
    if(d_ptr->m_vertexBuffer == vertexBuffer && d_ptr->m_vertexOffset == offset)
    {
      d_ptr->m_stats.ignoredChanges++;
      return true;
    }

    dxe(d_ptr->m_device->SetStreamSource(0, vertexBuffer->d_ptr->m_handle.get(), offset, stride ? stride : vertexBuffer->d_ptr->m_stride));
    d_ptr->m_vertexBuffer = vertexBuffer;
    d_ptr->m_vertexOffset = offset;
    d_ptr->m_stats.vertexBufferChanges++;
  }
  else
  {
    if(!d_ptr->m_vertexBuffer)
    {
      d_ptr->m_stats.ignoredChanges++;
      return true;
    }

    dxe(d_ptr->m_device->SetStreamSource(0, NULL, 0, 0));
    d_ptr->m_vertexBuffer = NULL;
    d_ptr->m_vertexOffset = -1;
    d_ptr->m_stats.vertexBufferChanges++;
  }
  return true;
}

bool RenderSystem::setVertexData(VertexBuffer::Types type, const void* data, unsigned size)
{
  if(!d_ptr->m_device)
    return false;

  if(!d_ptr->m_dynamicVertexBuffer)
    return false;

  int offset = d_ptr->m_dynamicVertexBuffer->append(data, size);
  if(offset < 0)
    return false;

  d_ptr->m_stats.dynamicVertexCount++;
  d_ptr->m_stats.dynamicVertexSize += size;
  d_ptr->m_vertexBuffer = NULL;

  if(!VertexBuffer::decodeStride(type))
    type = VertexBuffer::encodeStride(type, VertexBuffer::stride(type));

  return setVertexBuffer(type, d_ptr->m_dynamicVertexBuffer.data(), offset);
}

bool RenderSystem::setIndexBuffer(IndexBuffer* indexBuffer)
{
  if(!d_ptr->m_device)
    return false;

  if(d_ptr->m_indexBuffer == indexBuffer)
  {
    d_ptr->m_stats.ignoredChanges++;
    return true;
  }

  d_ptr->m_indexBuffer = indexBuffer;
  d_ptr->m_stats.indexBufferChanges++;

  if(indexBuffer)
    dxe(d_ptr->m_device->SetIndices(indexBuffer->d_ptr->m_handle.get()));
  else
    dxe(d_ptr->m_device->SetIndices(NULL));
  return true;
}

int RenderSystem::setIndexData(const unsigned short* data, unsigned count)
{
  if(!d_ptr->m_device)
    return -1;

  if(!d_ptr->m_dynamicIndexBuffer)
    return -1;

  int offset = d_ptr->m_dynamicIndexBuffer->append(data, count);
  if(offset < 0)
    return -1;

  d_ptr->m_stats.dynamicIndexCount++;
  d_ptr->m_stats.dynamicIndexSize += count * sizeof(unsigned short);
  d_ptr->m_indexBuffer = NULL;

  setIndexBuffer(d_ptr->m_dynamicIndexBuffer.data());
  return offset;
}

bool RenderSystem::bindInstanceData(VertexBuffer::Types types, const void* instances, unsigned count, unsigned stride, bool indexed)
{
  if(!d_ptr->m_device)
    return false;

  if(d_ptr->m_caps.VertexShaderVersion < D3DVS_VERSION(3,0))
    return false;

  if(count < 1)
    return false;

  unsigned instanceStride = VertexBuffer::instanceStride(types);
  unsigned dataSize = instanceStride * count;

  if(instanceStride > stride)
    return false;

  int offset = 0;

  if(void* locked = d_ptr->m_dynamicVertexBuffer->lockAppend(dataSize, offset))
  {
    // copy data into instance buffer
    const char* src = (const char*)instances;
    char* dest = (char*)locked;

    for(unsigned i = count; i-- > 0; )
    {
      qMemCopy(dest, src, instanceStride);
      src += stride;
      dest += instanceStride;
    }

    d_ptr->m_dynamicVertexBuffer->unlock();

    // bind instancing
    dxe(d_ptr->m_device->SetStreamSourceFreq(1, (D3DSTREAMSOURCE_INSTANCEDATA | 1)));
    dxe(d_ptr->m_device->SetStreamSourceFreq(0, (D3DSTREAMSOURCE_INDEXEDDATA | count)));

    // bind stream source
    dxe(d_ptr->m_device->SetStreamSource(1, d_ptr->m_dynamicVertexBuffer->d_ptr->m_handle.get(), offset, instanceStride));

    d_ptr->m_stats.instanceBinds++;
    d_ptr->m_stats.instanceCount += count;
    d_ptr->m_usesInstancing = true;
    return true;
  }
  return false;
}

void RenderSystem::unbindInstanceData()
{
  if(!d_ptr->m_device)
    return;
  dxe(d_ptr->m_device->SetStreamSourceFreq(0,1));
  dxe(d_ptr->m_device->SetStreamSourceFreq(1,1));
  d_ptr->m_usesInstancing = false;
}

void RenderSystem::draw(const BasePrimitiveDrawer& drawer)
{
  draw(&drawer, 1);
}

void RenderSystem::draw(const BasePrimitiveDrawer* drawers, unsigned count)
{
  if(!d_ptr->m_device)
    return;

  for(unsigned i = 0; i < count; ++i)
  {
    try
    {
      switch(drawers[i].Type)
      {
      case BasePrimitiveDrawer::Vertices:
      {
        const PrimitiveDrawer* drawer = (const PrimitiveDrawer*)&drawers[i];
        d_ptr->m_device->DrawPrimitive((D3DPRIMITIVETYPE)drawer->Topology, drawer->StartVertex, drawer->PrimitiveCount);
        d_ptr->m_stats.primCount += drawer->PrimitiveCount;
        d_ptr->m_stats.drawCount++;
      }
      break;

      case BasePrimitiveDrawer::Indices:
      {
        const IndexedPrimitiveDrawer* drawer = (const IndexedPrimitiveDrawer*)&drawers[i];
        d_ptr->m_device->DrawIndexedPrimitive((D3DPRIMITIVETYPE)drawer->Topology, drawer->BaseIndex, drawer->MinIndex,
                                              drawer->NumVertices, drawer->StartIndex,
                                              drawer->PrimitiveCount);
        d_ptr->m_stats.primCount += drawer->PrimitiveCount;
        d_ptr->m_stats.drawCount++;
      }
      break;

      default:
        break;
      }
    }
    catch(...)
    {
      qDebug() << "[RENDER] Draw Exception";
    }
  }
}

RenderSystemPrivate::RenderSystemPrivate()
{
  m_widget = NULL;
  m_sceneLevel = 0;

  m_shader = NULL;
  m_indexBuffer = NULL;
  qMemSet(m_textures, 0, sizeof(m_textures));
  m_blendState = (RenderSystem::BlendState)~0;
  m_alphaTest = (RenderSystem::AlphaTest)~0;
  m_depthStencilState = (RenderSystem::DepthStencilState)~0;
  m_rasterizerState = (RenderSystem::RasterizerState)~0;
  qMemSet(m_samplerState, ~0, sizeof(m_samplerState));
  m_fillMode = (RenderSystem::FillMode)~0;
  m_vertexType = (VertexBuffer::Types)~0;
  m_vertexBuffer = NULL;
  m_vertexOffset = -1;
  m_usesInstancing = false;
}

bool RenderSystem::resetDevice()
{
  if(!d_ptr->m_device)
    return false;

  qDebug() << "[D3D9] Resetting Device";

  lostDeviceObjects();

  if(d_ptr->m_widget)
  {
    if(d_ptr->m_widget->isFullScreen())
      qDebug() << "[D3D9] Reset in Fullscreen Mode";
    else
      qDebug() << "[D3D9] Reset in MainWindow Mode " << d_ptr->m_widget->width() << "x" << d_ptr->m_widget->height();
    d_ptr->m_d3dpp.BackBufferWidth = d_ptr->m_widget->width();
    d_ptr->m_d3dpp.BackBufferHeight = d_ptr->m_widget->height();
    d_ptr->m_d3dpp.hDeviceWindow = d_ptr->m_widget->winId();
    d_ptr->m_d3dpp.Windowed = !d_ptr->m_widget->isFullScreen();
  }
  else
  {
    qDebug() << "[D3D9] Reset in Windowed Mode";
    d_ptr->m_d3dpp.BackBufferWidth = 0;
    d_ptr->m_d3dpp.BackBufferHeight = 0;
    d_ptr->m_d3dpp.hDeviceWindow = GetDesktopWindow();
    d_ptr->m_d3dpp.Windowed = TRUE;
  }

  HRESULT hr = d_ptr->m_device->Reset(&d_ptr->m_d3dpp);

  if(!dxe(hr))
  {
    qFatal("[D3D9] Failed to Reset Device");
    d_ptr->m_device.reset();
    return false;
  }

  restoreDeviceObjects();
  return true;
}

void RenderSystem::lostDeviceObjects()
{
  emit deviceLost();

  if(d_ptr->m_font)
  {
    d_ptr->m_font->OnLostDevice();
  }

  d_ptr->m_dynamicIndexBuffer.reset();
  d_ptr->m_dynamicVertexBuffer.reset();

  for(int i = 0; i < VertexBuffer::Max; ++i)
    d_ptr->m_vertexDeclarations[i].reset();

  for(int i = 0; i < FixedCount; ++i)
    d_ptr->m_fixedShaders[i].reset();

  d_ptr->m_shaderConsts.clear();
  d_ptr->m_shaderSamplers.clear();

  d_ptr->m_sceneLevel = 0;

  d_ptr->m_shader = NULL;
  d_ptr->m_indexBuffer = NULL;
  qMemSet(d_ptr->m_textures, 0, sizeof(d_ptr->m_textures));
  d_ptr->m_blendState = (RenderSystem::BlendState)~0;
  d_ptr->m_alphaTest = (RenderSystem::AlphaTest)~0;
  d_ptr->m_depthStencilState = (RenderSystem::DepthStencilState)~0;
  d_ptr->m_rasterizerState = (RenderSystem::RasterizerState)~0;
  qMemSet(d_ptr->m_samplerState, ~0, sizeof(d_ptr->m_samplerState));
  d_ptr->m_fillMode = (RenderSystem::FillMode)~0;
  d_ptr->m_vertexType = (VertexBuffer::Types)~0;
  d_ptr->m_vertexBuffer = NULL;
  d_ptr->m_vertexOffset = -1;
  d_ptr->m_usesInstancing = false;

  d_ptr->m_shaderUsedConsts.clear();
  d_ptr->m_shaderUsedSamplers.clear();

  lostBox();
  lostSphere();
  lostCone();
}

void RenderSystem::restoreDeviceObjects()
{
  d_ptr->m_dynamicVertexBuffer.reset(new BaseDynamicVertexBuffer(1024 * 1024, 1));
  d_ptr->m_dynamicIndexBuffer.reset(new ShortDynamicIndexBuffer(128 * 1024));

  for(int i = 0; i < VertexBuffer::Max; ++i)
  {
    static D3DVERTEXELEMENT9 elements[128];
    VertexBuffer::decl((VertexBuffer::Types)i, elements);
    d_ptr->m_vertexDeclarations[i].reset();
    d_ptr->m_device->CreateVertexDeclaration(elements, (LPDIRECT3DVERTEXDECLARATION9*)&d_ptr->m_vertexDeclarations[i]);
  }

  QMetaEnum metaEnum;
  metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("FixedState"));

  for(int i = 0; i < FixedCount; ++i)
  {
    d_ptr->m_fixedShaders[i].reset(new RenderShader(RenderShader::File, ":/Render/SimpleShader.fx", metaEnum.valueToKey(i)));
  }

  d_ptr->m_shaderConsts.clear();
  d_ptr->m_shaderSamplers.clear();

  if(d_ptr->m_font)
  {
    d_ptr->m_font->OnResetDevice();
  }

  restoreBox();
  restoreSphere();
  restoreCone();

  emit deviceRestore();
}

unsigned RenderSystem::maxInstances()
{
  static bool instancing = getSettings().value("Render/EnableInstancing", "true").toBool();
  if(!instancing || d_ptr->m_caps.VertexShaderVersion < D3DVS_VERSION(3,0))
    return 0;
  return qMin<unsigned>(d_ptr->m_dynamicVertexBuffer->size() / (4 * sizeof(vec4)), 256);
}

static QSize measureText(const QString &text, int widths[256], const TEXTMETRIC& metric)
{
  QSize size(0, 0);
  int lineSize = 0;

  // Oblicz szeroko�� tekstu
  foreach(QChar i, text)
  {
    if(i.toLatin1() == '\n')
    {
      size.setWidth(qMax(size.width(), lineSize));
      size.setHeight(size.height() + metric.tmHeight);
      lineSize = 0;
      continue;
    }
    lineSize += i.toLatin1() < 256 ? widths[i.toLatin1()] : metric.tmAveCharWidth;
  }

  // Dodaj ostatni� lini�
  if(lineSize > 0)
  {
    size.setWidth(qMax(size.width(), lineSize));
    size.setHeight(size.height() + metric.tmHeight);
  }
  return size;
}

static QRect measureText(const vec2 &at, const QString &text, GraphicsFont::Flags flags, int widths[256], const TEXTMETRIC& metric)
{
  QRect r;

  // Pobierz wielko�� tekstu
  QSize size = measureText(text, widths, metric);
  if(size.isEmpty())
    return r;

  r.setSize(size);

  // Oblicz po�o�enie poziomie
  switch(flags & (GraphicsFont::Left | GraphicsFont::Center | GraphicsFont::Right))
  {
  case GraphicsFont::Left:
    r.setX(at.X);
    break;

  case GraphicsFont::Center:
    r.setX(at.X - size.width() / 2);
    break;

  case GraphicsFont::Right:
    r.setX(at.X - size.width());
    break;
  }

  // Oblicz po�o�enie pionowe
  switch(flags & (GraphicsFont::Top | GraphicsFont::Middle | GraphicsFont::Bottom))
  {
  case GraphicsFont::Top:
    r.setY(at.Y);
    break;

  case GraphicsFont::Middle:
    r.setY(at.Y - size.height() / 2);

    break;

  case GraphicsFont::Bottom:
    r.setY(at.Y - size.height());
    break;
  }
  return r;
}

bool RenderSystem::drawText(const vec2 &pos, const QString &text, GraphicsFont::Flags flags, const QColor &color)
{
  if(!d_ptr->m_device)
    return false;
  if(!d_ptr->m_font)
    return false;

  QRect measuredRect = measureText(pos, text, flags, d_ptr->m_fontWidths, d_ptr->m_fontMetric);

  RECT windowsRect = {measuredRect.left(), measuredRect.top(), measuredRect.right(), measuredRect.bottom()};

  d_ptr->m_font->DrawTextW(NULL, (const WCHAR*)text.utf16(), text.length(),
                           &windowsRect,
                           flags & (DT_EXPANDTABS | DT_RTLREADING | DT_NOCLIP | DT_SINGLELINE | DT_WORDBREAK),
                           D3DCOLOR_COLORVALUE(color.redF(), color.greenF(), color.blueF(), color.alphaF()));
  return true;
}

bool RenderSystem::beginScene()
{
  if(!d_ptr->m_device)
    return false;

  if(d_ptr->m_sceneLevel)
  {
    ++d_ptr->m_sceneLevel;
    return true;
  }

  if(SUCCEEDED(d_ptr->m_device->BeginScene()))
  {
    resetStats();
    ++d_ptr->m_sceneLevel;
    return true;
  }
  return false;
}

void RenderSystem::endScene(bool force)
{
  if(!d_ptr->m_device)
    return;

  if(force)
  {
    d_ptr->m_device->EndScene();
    d_ptr->m_sceneLevel = 0;
  }
  else if(d_ptr->m_sceneLevel > 0)
  {
    --d_ptr->m_sceneLevel;

    if(d_ptr->m_sceneLevel == 0)
    {
      d_ptr->m_device->EndScene();
    }
  }
}

void RenderSystem::setDepthTarget(BaseRenderTarget *renderTarget)
{
  if(!d_ptr->m_device)
    return;

  if(renderTarget)
    dxe(d_ptr->m_device->SetDepthStencilSurface(renderTarget->e_ptr->m_depthTarget.get()));
  else
    dxe(d_ptr->m_device->SetDepthStencilSurface(NULL));
}

const RenderStats & RenderSystem::stats() const
{
  return d_ptr->m_stats;
}

void RenderSystem::resetStats()
{
  d_ptr->m_stats = RenderStats();
}

RenderStats::RenderStats()
{
  drawCount = 0;
  primCount = 0;
  textureChanges = 0;
  shaderChanges = 0;
  blendChanges = 0;
  alphaTestChanges = 0;
  depthStencilChanges = 0;
  rasterizerChanges = 0;
  samplerChanges = 0;
  fillModeChanges = 0;
  indexBufferChanges = 0;
  vertexBufferChanges = 0;
  vertexTypeChanges = 0;
  dynamicVertexSize = 0;
  dynamicVertexCount = 0;
  dynamicIndexSize = 0;
  dynamicIndexCount = 0;
  vertexConstChanges = 0;
  vertexConstSize = 0;
  pixelConstChanges = 0;
  pixelConstSize = 0;
  instanceBinds = 0;
  instanceCount = 0;
  ignoredStateChanges = 0;
  ignoredChanges = 0;
}

QStringList RenderStats::toStringList() const
{
  QStringList list;
  list << "RenderStats:" <<
          QString().sprintf("Draw/PrimCount: %i/%i", drawCount, primCount) <<
          QString().sprintf("TextureChanges: %i", textureChanges) <<
          QString().sprintf("ShaderChanges: %i", shaderChanges) <<
          QString().sprintf("BlendChanges: %i", blendChanges) <<
          QString().sprintf("AlphaTestChanges: %i", alphaTestChanges) <<
          QString().sprintf("DepthStencilChanges: %i", depthStencilChanges) <<
          QString().sprintf("RasterizerChanges: %i", rasterizerChanges) <<
          QString().sprintf("SamplerChanges: %i", samplerChanges) <<
          QString().sprintf("FillModeChanges: %i", fillModeChanges) <<
          QString().sprintf("IndexBufferChanges: %i", indexBufferChanges) <<
          QString().sprintf("VertexBufferChanges: %i", vertexBufferChanges) <<
          QString().sprintf("VertexTypeChanges: %i", vertexTypeChanges) <<
          QString().sprintf("DynamicVertexBuffer: %i in %i", dynamicVertexSize, dynamicVertexCount) <<
          QString().sprintf("DynamicIndexBuffer: %i in %i", dynamicIndexSize, dynamicIndexCount) <<
          QString().sprintf("VertexConst: %i in %i", vertexConstSize, vertexConstChanges) <<
          QString().sprintf("PixelConst: %i in %i", pixelConstSize, pixelConstChanges) <<
          QString().sprintf("Instances: %i in %i", instanceCount, instanceBinds) <<
          QString().sprintf("IgnoredStateChanges: %i", ignoredStateChanges) <<
          QString().sprintf("IgnoredChanges: %i", ignoredChanges);
  return list;
}

bool RenderSystem::isConstUsed(unsigned index) const
{
  if(d_ptr->m_shaderUsedConsts.size() <= index)
    return false;
  return d_ptr->m_shaderUsedConsts.testBit(index);
}

bool RenderSystem::isSamplerUser(unsigned index) const
{
  if(d_ptr->m_shaderUsedSamplers.size() <= index)
    return false;
  return d_ptr->m_shaderUsedSamplers.testBit(index);
}

bool RenderSystem::usesInstancing() const
{
  return d_ptr->m_usesInstancing;
}
