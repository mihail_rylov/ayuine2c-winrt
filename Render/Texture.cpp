#include "Texture.hpp"
#include "Texture_p.hpp"
#include "RenderTexture.hpp"
#include "RenderTexture_p.hpp"
#include <Core/ResourceImporter.hpp>

Q_IMPLEMENT_OBJECTREF(Texture)

Texture::Texture() : d_ptr(new TexturePrivate)
{
}

Texture::~Texture()
{
}

unsigned Texture::width() const
{
  if(RenderTexture* texture = *this)
    return texture->width();
  return 0;
}

unsigned Texture::height() const
{
  if(RenderTexture* texture = *this)
    return texture->height();
  return 0;
}

unsigned Texture::depth() const
{
  if(RenderTexture* texture = *this)
    return texture->depth();
  return 0;
}

RenderTexture::Format Texture::format() const
{
  if(RenderTexture* texture = *this)
    return texture->format();
  return RenderTexture::Undefined;
}

Texture::Type Texture::type() const
{
  if(RenderTexture* texture = *this)
  {
    if(texture->isPlain())
      return Plain;
    if(texture->isCube())
      return Cube;
    if(texture->isVolume())
      return Volume;
  }
  return Undefined;
}

Texture::operator RenderTexture *() const
{
  if(d_ptr->m_texture == NULL)
  {
    if(d_ptr->m_lazyFileName.size())
    {
      d_ptr->m_texture = RenderTexture::fromFile(d_ptr->m_lazyFileName);
    }
    else if(d_ptr->m_data.size())
    {
      d_ptr->m_texture = RenderTexture::fromMemory(d_ptr->m_data, "(memory)");
    }
  }
  return d_ptr->m_texture;
}

class TextureImporter : public ResourceImporter
{
public:
  TextureImporter()
  {
    registerNativeImporter("jpg", &Texture::staticMetaObject);
    registerNativeImporter("jpeg", &Texture::staticMetaObject);
    registerNativeImporter("png", &Texture::staticMetaObject);
    registerNativeImporter("dds", &Texture::staticMetaObject);
    registerNativeImporter("hdr", &Texture::staticMetaObject);
    registerNativeImporter("tga", &Texture::staticMetaObject);
  }

public:
  QSharedPointer<Resource> load(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    if(!device)
      return QSharedPointer<Resource>();

    if(device->objectName().length())
    {
      QSharedPointer<Texture> newTexture(new Texture());
      newTexture->d_ptr->m_lazyFileName = device->objectName();
      return newTexture;
    }

    QByteArray data = device->readAll();

    if(data.length() != 0)
    {
      QSharedPointer<Texture> newTexture(new Texture());
      newTexture->d_ptr->m_data = data;
      return newTexture;
    }

    return QSharedPointer<Resource>();
  }

  const QMetaObject* metaObject(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    return &Texture::staticMetaObject;
  }
};

static TextureImporter textureImporter;

TexturePrivate::TexturePrivate()
{
}

TexturePrivate::~TexturePrivate()
{
}

QByteArray Texture::data() const
{
  if(!d_ptr->m_data.isEmpty())
    return d_ptr->m_data;

  if(d_ptr->m_lazyFileName.size())
  {
    QFile file(d_ptr->m_lazyFileName);
    if(file.open(QFile::ReadOnly))
      return file.readAll();
  }
  return QByteArray();
}

void Texture::setData(const QByteArray &data)
{
  d_ptr->m_image = QImage();
  d_ptr->m_lazyFileName.clear();
  d_ptr->m_data = data;
  delete d_ptr->m_texture;
#if 0
  d_ptr->m_texture = new FileTexture(d_ptr->m_data, QString("(memory)"));
  d_ptr->m_image = QImage();
#endif
}

QImage Texture::toImage() const
{
  if(d_ptr->m_image.isNull())
  {
    if(RenderTexture* texture = *this)
    {
      d_ptr->m_image = texture->toImage();
    }
    return d_ptr->m_image;
  }
  return QImage();
}

bool Texture::isVolume() const
{
  if(RenderTexture* texture = *this)
  {
    if(texture->isVolume())
      return true;
  }
  return false;
}

bool Texture::isPlainOrVolume() const
{
  if(RenderTexture* texture = *this)
  {
    if(texture->isPlain())
      return true;
    if(texture->isVolume())
      return true;
  }
  return Undefined;
}
