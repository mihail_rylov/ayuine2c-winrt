#include "DynamicIndexBuffer.hpp"
#include "IndexBuffer_p.hpp"

DynamicIndexBuffer::DynamicIndexBuffer(unsigned indexCount, Format format)
{
  Q_ASSERT(indexCount);
  d_ptr->create(indexCount, format, true);
}

void DynamicIndexBuffer::set(const unsigned void *data, unsigned count, unsigned offset)
{
  if(void * locked = d_ptr->lock(offset, count))
  {
    memcpy(locked, data, count * d_ptr->m_stride);
    d_ptr->unlock();
  }
}

int DynamicIndexBuffer::append(const void *data, unsigned count)
{
  if(count > m_count)
    return -1;

  int offset = 0;

  if(d_ptr->m_offset + count > d_ptr->m_count)
    offset = 0;
  else
    offset = d_ptr->m_offset;

  if(void * locked = d_ptr->lock(offset, count, true))
  {
    memcpy(locked, data, count * d_ptr->m_stride);
    d_ptr->unlock();
    d_ptr->m_offset += count;
    return offset;
  }
  return -1;
}
