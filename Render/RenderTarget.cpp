#include "RenderTexture.hpp"
#include "RenderTexture_p.hpp"
#include "RenderTarget.hpp"
#include "RenderTarget_p.hpp"
#include "RenderSystem.hpp"
#include <d3d9.h>
#include <d3dx9.h>
#include <QDebug>

D3DFORMAT toFormat(RenderTexture::Format format);

D3DFORMAT toFormat(BaseRenderTarget::DepthFormat format)
{
  switch(format)
  {
  case BaseRenderTarget::Depth16:         return D3DFMT_D16;
  case BaseRenderTarget::Depth24:         return D3DFMT_D24X8;
  case BaseRenderTarget::Depth24Stencil8: return D3DFMT_D24S8;
  case BaseRenderTarget::DepthNone:       return D3DFMT_D32;
  default:                                return D3DFMT_D24S8;
  }
}

BaseRenderTarget::BaseRenderTarget()
  : e_ptr(new RenderTargetPrivate)
{
}

BaseRenderTarget::~BaseRenderTarget()
{
}

RenderTargetPrivate::RenderTargetPrivate()
{
  m_depthFormat = BaseRenderTarget::DepthNone;
}

RenderTargetPrivate::~RenderTargetPrivate()
{
}

RenderTarget::RenderTarget(unsigned width, unsigned height, RenderTexture::Format format)
{
  if(dxe(getRenderSystem()->CreateTexture(width, height, 1, D3DUSAGE_RENDERTARGET, toFormat(format), D3DPOOL_DEFAULT, (LPDIRECT3DTEXTURE9*)&d_ptr->m_handle, NULL)))
  {
    d_ptr->m_width = width;
    d_ptr->m_height = height;
    d_ptr->m_format = format;
    d_ptr->m_type = D3DRTYPE_TEXTURE;
    d_ptr->m_usage = D3DUSAGE_RENDERTARGET;

    connect(&getRenderSystem(), SIGNAL(deviceLost()), SLOT(deviceLost()));
    connect(&getRenderSystem(), SIGNAL(deviceRestore()), SLOT(deviceReset()));
  }
}

CubeRenderTarget::CubeRenderTarget(unsigned size, RenderTexture::Format format)
{
  if(dxe(getRenderSystem()->CreateCubeTexture(size, 1, D3DUSAGE_RENDERTARGET, toFormat(format), D3DPOOL_DEFAULT, (LPDIRECT3DCUBETEXTURE9*)&d_ptr->m_handle, NULL)))
  {
    d_ptr->m_width = size;
    d_ptr->m_height = size;
    d_ptr->m_format = format;
    d_ptr->m_type = D3DRTYPE_CUBETEXTURE;
    d_ptr->m_usage = D3DUSAGE_RENDERTARGET;

    connect(&getRenderSystem(), SIGNAL(deviceLost()), SLOT(deviceLost()));
    connect(&getRenderSystem(), SIGNAL(deviceRestore()), SLOT(deviceReset()));
  }
}

BaseRenderTarget::DepthFormat BaseRenderTarget::depthFormat() const
{
  return e_ptr->m_depthFormat;
}

void BaseRenderTarget::setDepthFormat(BaseRenderTarget::DepthFormat depthFormat)
{
  if(e_ptr->m_depthFormat == depthFormat)
    return;

  if(d_ptr->m_width == 0 || d_ptr->m_height == 0)
    return;

  e_ptr->m_depthTarget.reset();

  if(depthFormat != DepthNone)
  {
    if(!dxe(getRenderSystem()->CreateDepthStencilSurface(d_ptr->m_width, d_ptr->m_height, toFormat(depthFormat),
                                                         D3DMULTISAMPLE_NONE, 0, FALSE, (LPDIRECT3DSURFACE9*)&e_ptr->m_depthTarget,
                                                         NULL)))
    {
      e_ptr->m_depthFormat = DepthNone;
      return;
    }
  }
  e_ptr->m_depthFormat = depthFormat;
}

void BaseRenderTarget::setDepthFormat(BaseRenderTarget* otherRenderTarget)
{
  if(otherRenderTarget)
  {
    e_ptr->m_depthTarget = otherRenderTarget->e_ptr->m_depthTarget;
    e_ptr->m_depthFormat = otherRenderTarget->e_ptr->m_depthFormat;
  }
  else
  {
    e_ptr->m_depthTarget.reset();
    e_ptr->m_depthFormat = DepthNone;
  }
}

void BaseRenderTarget::deviceLost()
{
  d_ptr->m_handle.reset();
  e_ptr->m_depthTarget.reset();
}

void BaseRenderTarget::deviceReset()
{
  qDebug() << "[RENDER] Restore DepthStencil " << e_ptr->m_depthFormat;

  if(e_ptr->m_depthFormat != DepthNone)
  {
    e_ptr->m_depthTarget.reset();

    dxe(getRenderSystem()->CreateDepthStencilSurface(d_ptr->m_width, d_ptr->m_height, toFormat(e_ptr->m_depthFormat),
                                                         D3DMULTISAMPLE_NONE, 0, FALSE, (LPDIRECT3DSURFACE9*)&e_ptr->m_depthTarget,
                                                         NULL));
  }
}

void RenderTarget::deviceReset()
{
  BaseRenderTarget::deviceReset();

  if(d_ptr->m_type == D3DRTYPE_TEXTURE)
  {
    d_ptr->m_handle.reset();
    dxe(getRenderSystem()->CreateTexture(d_ptr->m_width, d_ptr->m_height, 1, D3DUSAGE_RENDERTARGET, toFormat(d_ptr->m_format), D3DPOOL_DEFAULT, (LPDIRECT3DTEXTURE9*)&d_ptr->m_handle, NULL));
  }
}

void CubeRenderTarget::deviceReset()
{
  BaseRenderTarget::deviceReset();

  if(d_ptr->m_type == D3DRTYPE_CUBETEXTURE)
  {
    d_ptr->m_handle.reset();
    dxe(getRenderSystem()->CreateCubeTexture(d_ptr->m_width, 1, D3DUSAGE_RENDERTARGET, toFormat(d_ptr->m_format), D3DPOOL_DEFAULT, (LPDIRECT3DCUBETEXTURE9*)&d_ptr->m_handle, NULL));
  }
}
