#include "IndexBuffer.hpp"
#include "IndexBuffer_p.hpp"
#include "PrimitiveDrawer.hpp"
#include "RenderSystem.hpp"
#include <d3d9.h>

#include <deque>
#include <algorithm>
#include "TriStripper/tri_stripper.h"

IndexBuffer::IndexBuffer() :
  QObject(&getRenderSystem()), d_ptr(new IndexBufferPrivate)
{
}

IndexBuffer::IndexBuffer(const void* indices, unsigned count, Format format) :
  QObject(&getRenderSystem()), d_ptr(new IndexBufferPrivate)
{
  Q_ASSERT(indices);
  Q_ASSERT(count);
  d_ptr->create(indices, count, format);
}

IndexBuffer::IndexBuffer(unsigned count, Format format) :
  QObject(&getRenderSystem()), d_ptr(new IndexBufferPrivate)
{
  Q_ASSERT(count);
  d_ptr->create(count, format, false);
}

IndexBuffer::~IndexBuffer()
{
}

IndexBuffer::Format IndexBuffer::format() const
{
  return d_ptr->m_format;
}

unsigned IndexBuffer::count() const
{
  return d_ptr->m_count;
}

unsigned IndexBuffer::size() const
{
  return d_ptr->m_format * d_ptr->m_count;
}

IndexBufferPrivate::IndexBufferPrivate()
{
  m_count = 0;
  m_stride = 0;
  m_format = IndexBuffer::None;
  m_dynamic = false;
  m_locked = false;
  m_offset = 0;
}

IndexBufferPrivate::~IndexBufferPrivate()
{
  unlock();
}

bool IndexBufferPrivate::create(unsigned count, IndexBuffer::Format indexFormat, bool dynamic)
{
  if(m_handle)
    return false;

  D3DFORMAT format;
  D3DPOOL pool;
  unsigned usage;
  unsigned stride;

  switch(indexFormat)
  {
  case IndexBuffer::Short:
    stride = sizeof(unsigned short);
    format = D3DFMT_INDEX16;
    break;

  case IndexBuffer::Long:
    stride = sizeof(unsigned);
    format = D3DFMT_INDEX32;
    break;

  default:
    // TODO: Invalid format
    return false;
  }

  if(dynamic)
  {
    pool = D3DPOOL_DEFAULT;
    usage = D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY;
  }
  else
  {
    pool = D3DPOOL_MANAGED;
    usage = D3DUSAGE_WRITEONLY;
  }

  if(dxe(getRenderSystem()->CreateIndexBuffer(
          count * stride, usage, format,
          pool,
          (LPDIRECT3DINDEXBUFFER9*)&m_handle,
          NULL)))
  {
    m_count = count;
    m_stride = stride;
    m_format = indexFormat;
    m_offset = dynamic ? count : 0;
    m_dynamic = dynamic;
    return true;
  }
  return false;
}

void* IndexBufferPrivate::lock(unsigned offset, unsigned count, bool nooverwrite)
{
  if(!m_handle)
    return NULL;

  if(m_locked)
    unlock();

  unsigned flags;

  if(m_dynamic)
    flags = nooverwrite ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD;
  else
    flags = nooverwrite ? D3DLOCK_NOOVERWRITE : 0;

  void* dest;
  if(dxe(m_handle->Lock(offset * m_stride, count * m_stride, &dest, flags)))
  {
    m_locked = true;
    return dest;
  }
  return NULL;
}

void IndexBufferPrivate::unlock()
{
  if(!m_handle)
    return;

  if(m_locked)
  {
    m_handle->Unlock();
    m_locked = false;
  }
}

bool IndexBufferPrivate::create(const void* data, unsigned count, IndexBuffer::Format format)
{
  Q_ASSERT(data);
  Q_ASSERT(count);

  if(create(count, format, false))
  {
    if(void* locked = lock(0, count))
    {
      memcpy(locked, data, count * m_stride);
      unlock();
      return true;
    }
  }
  return false;
}

BaseDynamicIndexBuffer::BaseDynamicIndexBuffer(unsigned indexCount, Format format)
{
  Q_ASSERT(indexCount);
  if(d_ptr->create(indexCount, format, true))
  {
    connect(&getRenderSystem(), SIGNAL(deviceLost()), SLOT(deviceLost()));
    connect(&getRenderSystem(), SIGNAL(deviceRestore()), SLOT(deviceReset()));
  }
}

void IndexBuffer::set(const void *data, unsigned count, unsigned offset)
{
  if(void * locked = d_ptr->lock(offset, count))
  {
    memcpy(locked, data, count * d_ptr->m_stride);
    d_ptr->unlock();
  }
}

void* IndexBuffer::lock(unsigned count, unsigned offset)
{
  return d_ptr->lock(offset, count, false);
}

void IndexBuffer::unlock()
{
  d_ptr->unlock();
}

bool IndexBuffer::isLocked() const
{
  return d_ptr->m_locked;
}

void * BaseDynamicIndexBuffer::lockAppend(unsigned count, int &offset)
{
  if(count > d_ptr->m_count)
    return NULL;

  bool nooverwrite = false;

  if(d_ptr->m_offset + count > d_ptr->m_count)
  {
    offset = 0;
  }
  else
  {
    offset = d_ptr->m_offset;
    nooverwrite = true;
  }

  if(void * locked = d_ptr->lock(offset, count, nooverwrite))
  {
    d_ptr->m_offset += count;
    return locked;
  }
  offset = -1;
  return NULL;
}

int BaseDynamicIndexBuffer::append(const void *data, unsigned count)
{
  int offset = 0;
  if(void * locked = lockAppend(count, offset))
  {
    memcpy(locked, data, count * d_ptr->m_stride);
    d_ptr->unlock();
    return offset;
  }
  return -1;
}

void BaseDynamicIndexBuffer::deviceLost()
{
  d_ptr->m_handle.reset();
}

void BaseDynamicIndexBuffer::deviceReset()
{
  if(d_ptr->m_stride)
  {
    d_ptr->m_handle.reset();
    d_ptr->create(d_ptr->m_count, d_ptr->m_format, d_ptr->m_dynamic);
  }
}

template<typename Type>
static QVector<Type> stripify(const Type* indices, unsigned indexCount)
{
  if(indices == NULL || indexCount < 6)
    return QVector<Type>();

  try
  {
    triangle_stripper::tri_stripper::indices stripIndices(indices, indices + indexCount);
    triangle_stripper::tri_stripper stripper(stripIndices);
    triangle_stripper::tri_stripper::primitives_vector primitives;
    stripper.Strip(&primitives);

    unsigned newIndexCount = 0;

    foreach(const triangle_stripper::tri_stripper::primitives& prim, primitives)
    {
      if(prim.m_Indices.empty())
        continue;

      switch(prim.m_Type)
      {
      case triangle_stripper::tri_stripper::PT_Triangles:
        newIndexCount += prim.m_Indices.size() + 2 * prim.m_Indices.size() / 3;
        break;

      case triangle_stripper::tri_stripper::PT_Triangle_Strip:
        if(newIndexCount > 0)
          newIndexCount += 2;
        newIndexCount += prim.m_Indices.size();
        break;
      }
    }

    if(newIndexCount > indexCount)
      return QVector<Type>();

    QVector<Type> newIndices(newIndexCount, 0);
    Type* outp = newIndices.data();

    foreach(const triangle_stripper::tri_stripper::primitives& prim, primitives)
    {
      if(prim.m_Indices.empty())
        continue;

      switch(prim.m_Type)
      {
      case triangle_stripper::tri_stripper::PT_Triangles:
      {
        const quint32* inp = &*prim.m_Indices.begin();
        const quint32* ine = &*prim.m_Indices.end();
        Type last = outp == newIndices.data() ? *inp : *outp;

        while(inp < ine)
        {
          *outp++ = last;
          *outp++ = *inp;
          *outp++ = *inp;
          *outp++ = *inp++;
          *outp++ = last = *inp++;
        }
      }
        break;

      case triangle_stripper::tri_stripper::PT_Triangle_Strip:
        if(outp != newIndices.data())
        {
          outp[0] = outp[-1];
          outp[1] = prim.m_Indices[0];
          outp += 2;
        }
        outp = qCopy(prim.m_Indices.begin(), prim.m_Indices.end(), outp);
        break;
      }
    }

    Q_ASSERT(outp == newIndices.data()+newIndexCount);

    return newIndices;
  }
  catch(std::exception&)
  {
    return QVector<Type>();
  }
}

QVector<quint16> IndexBuffer::stripify(const QVector<quint16>& indices)
{
  if(indices.isEmpty())
    return QVector<quint16>();
  return stripify(indices.constData(), indices.size());
}

QVector<quint16> IndexBuffer::stripify(const quint16* indices, unsigned indexCount)
{
  return ::stripify(indices, indexCount);
}

QVector<quint32> IndexBuffer::stripify(const QVector<quint32> &indices)
{
  if(indices.isEmpty())
    return QVector<quint32>();
  return stripify(indices.constData(), indices.size());
}

QVector<quint32> IndexBuffer::stripify(const quint32* indices, unsigned indexCount)
{
  return ::stripify(indices, indexCount);
}

template<typename Type>
void stripify(QVector<Type>& indices, QVector<IndexedPrimitiveDrawer>& drawers)
{
  QVector<Type> newIndices(indices.size() * 3);
  Type* out = newIndices.data();

  for(int i = 0; i < drawers.size(); ++i)
  {
    IndexedPrimitiveDrawer& drawer = drawers[i];
    if(drawer.PrimitiveCount <= 0)
      continue;

    Type* cur = out;
    const Type* in = indices.constData() + drawer.StartIndex;

    switch(drawer.Topology)
    {
    case PrimitiveDrawer::PointList:
      out = qCopy(in, in + drawer.PrimitiveCount, out);
      break;

    case PrimitiveDrawer::LineList:
      out = qCopy(in, in + drawer.PrimitiveCount * 2, out);
      break;

    case PrimitiveDrawer::LineStrip:
      out = qCopy(in, in + drawer.PrimitiveCount + 1, out);
      break;

    case PrimitiveDrawer::TriList:
    {
#if 0
      QVector<Type> stripped = ::stripify(in, drawer.PrimitiveCount * 3);
      if(stripped.size())
      {
        out = qCopy(stripped.begin(), stripped.end(), out);
        drawer.PrimitiveCount = stripped.size() - 2;
        drawer.Topology = PrimitiveDrawer::TriStrip;
        break;
      }
#endif
#if 1

#endif
      out = qCopy(in, in + drawer.StartIndex + drawer.PrimitiveCount * 3, out);
    }
      break;

    case PrimitiveDrawer::TriFan:
    case PrimitiveDrawer::TriStrip:
      out = qCopy(in, in + drawer.StartIndex + drawer.PrimitiveCount + 2, out);
      break;

    default:
      drawer.StartIndex = 0;
      drawer.PrimitiveCount = 0;
      continue;
    }

    drawer.StartIndex = cur - newIndices.data();
  }

  Q_ASSERT(out <= newIndices.constData() + newIndices.size());

  newIndices.resize(out - newIndices.data());
  indices = newIndices;
}

void IndexBuffer::stripify(QVector<quint32>& indices, QVector<IndexedPrimitiveDrawer>& drawers)
{
  ::stripify(indices, drawers);
}

void IndexBuffer::stripify(QVector<quint16>& indices, QVector<IndexedPrimitiveDrawer>& drawers)
{
  ::stripify(indices, drawers);
}
