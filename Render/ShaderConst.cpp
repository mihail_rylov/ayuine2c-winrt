#include "ShaderConst.hpp"
#include "ShaderConst_p.hpp"
#include "RenderSystem.hpp"

ShaderConst::ShaderConst(const char* name, unsigned size) :
  QObject(&getRenderSystem()), d_ptr(new ShaderConstPrivate)
{
  d_ptr->name = name;
  d_ptr->data.resize(size);
}

ShaderConst::~ShaderConst()
{
}

const char* ShaderConst::name() const
{
  return d_ptr->name.constData();
}

unsigned ShaderConst::size() const
{
  return d_ptr->data.size();
}

bool ShaderConst::set(const void* data, unsigned size, unsigned offset)
{
  if(offset + size >= d_ptr->data.size())
    return false;
  if(size == 0 || data == NULL)
    return false;
  qMemCopy(&d_ptr->data[offset], data, size);
  d_ptr->updated |= 1;
  return true;
}
