#ifndef BASERENDERTARGET_HPP
#define BASERENDERTARGET_HPP

#include <QObject>
#include "Render.hpp"
#include "Texture.hpp"

class TexturePrivate;

class RenderTargetPrivate;

class RENDER_EXPORT BaseRenderTarget : public RenderTexture
{
  Q_OBJECT
  Q_ENUMS(DepthFormat)
  Q_DECLARE_PRIVATE(RenderTarget)

public:
  enum DepthFormat
  {
    Depth16,
    Depth24,
    Depth24Stencil8,
    DepthNone
  };

protected:
  BaseRenderTarget();
public:
  ~BaseRenderTarget();

public:
  DepthFormat depthFormat() const;
  void setDepthFormat(DepthFormat depthFormat);
  void setDepthFormat(BaseRenderTarget* otherRenderTarget);

protected slots:
  virtual void deviceReset() = 0;
  virtual void deviceLost();

protected:
  QScopedPointer<RenderTargetPrivate> e_ptr;

  friend class RenderSystem;
};

Q_DECLARE_OBJECTREF(BaseRenderTarget)

class RENDER_EXPORT RenderTarget : public BaseRenderTarget
{
  Q_OBJECT

public:
  RenderTarget(unsigned width, unsigned height, Format format);

private:
  void deviceReset();
};

Q_DECLARE_OBJECTREF(RenderTarget)

class RENDER_EXPORT CubeRenderTarget : public BaseRenderTarget
{
  Q_OBJECT

public:
  CubeRenderTarget(unsigned size, Format format);

private:
  void deviceReset();
};

Q_DECLARE_OBJECTREF(CubeRenderTarget)

#endif // BASERENDERTARGET_HPP
