#ifndef GRAPHICSFONT_HPP
#define GRAPHICSFONT_HPP

#include <QSharedDataPointer>
#include <QFont>

class GraphicsFontData;

class GraphicsFont
{
public:
  enum Flag
  {
    None = 0,

    Left = 0,
    Center = 1,
    Right = 2,

    Top = 0,
    Middle = 4,
    Bottom = 8,

    LineBreak = 16,
    WordBreak = 32
  };
  Q_DECLARE_FLAGS(Flags, Flag)

public:
  GraphicsFont();
};

#endif // GRAPHICSFONT_HPP
