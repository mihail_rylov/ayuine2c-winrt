#ifndef SHADERCONSTS_HPP
#define SHADERCONSTS_HPP

#include <QObject>
#include "Render.hpp"

class ShaderConstPrivate;

class RENDER_EXPORT ShaderConst : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(ShaderConst)

public:
  explicit ShaderConst(const char* name, unsigned size);
  ~ShaderConst();

public:
  const char* name() const;
  unsigned size() const;
  bool set(const void* data, unsigned size, unsigned offset = 0);

private:
  QScopedPointer<ShaderConstPrivate> d_ptr;
  friend class RenderSystem;
  friend class RenderSystemPrivate;
};

#endif // SHADERCONSTS_HPP
