#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <QObject>

#include <Core/Resource.hpp>

#include "Render.hpp"
#include "RenderTexture.hpp"

class TexturePrivate;

class RENDER_EXPORT Texture : public Resource
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(Texture)
  Q_ENUMS(Type)
  Q_PROPERTY(RenderTexture::Format format READ format STORED false)
  Q_PROPERTY(Type type READ type STORED false)
  Q_PROPERTY(unsigned width READ width STORED false)
  Q_PROPERTY(unsigned height READ height STORED false DESIGNABLE isPlainOrVolume)
  Q_PROPERTY(unsigned depth READ depth STORED false DESIGNABLE isVolume)
  Q_PROPERTY(QByteArray data READ data WRITE setData DESIGNABLE false)
  Q_CLASSINFO("IconFileName", ":/Resources/IconTexture.png")

public:
  enum Type
  {
    Undefined,
    Plain,
    Volume,
    Cube
  };

public:
  Q_INVOKABLE explicit Texture();

public:
  ~Texture();

public:
  unsigned width() const;
  unsigned height() const;
  unsigned depth() const;
  RenderTexture::Format format() const;
  Type type() const;
  bool isVolume() const;
  bool isPlainOrVolume() const;
  QByteArray data() const;
  void setData(const QByteArray& data);  
  QImage toImage() const;

public:
  operator RenderTexture* () const;

private:
  QScopedPointer<TexturePrivate> d_ptr;

  friend class TextureImporter;
};

Q_DECLARE_OBJECTREF(Texture)

#endif // TEXTURE_HPP
