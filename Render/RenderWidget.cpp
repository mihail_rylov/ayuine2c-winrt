#include "RenderWidget.hpp"
#include "RenderWidget_p.hpp"
#include "RenderSystem.hpp"
#include "RenderSystem_p.hpp"

#include <Windows.h>
#include <QMainWindow>

QRenderWidget::QRenderWidget(QWidget *parent) :
  QWidget(parent), d_ptr(new RenderWidgetPrivate)
{
  setAttribute(Qt::WA_PaintOnScreen);
  setAttribute(Qt::WA_NoSystemBackground);
  setWindowFlags(windowFlags() | Qt::Window);

  connect(&getRenderSystem(), SIGNAL(deviceLost()), SLOT(deviceLost()));
  connect(&getRenderSystem(), SIGNAL(deviceRestore()), SLOT(deviceRestore()));
}

QRenderWidget::~QRenderWidget()
{
  d_ptr->m_renderTarget.reset();
  d_ptr->m_depthTarget.reset();
  d_ptr->m_swapChain.reset();

  if(renderSystem())
  {
    if(renderSystem().d_ptr->m_widget == this)
      renderSystem().d_ptr->m_widget = NULL;
  }

  // delete &renderSystem();
}

RenderSystem& QRenderWidget::renderSystem()
{
  return getRenderSystem();
}

void QRenderWidget::paintEvent(QPaintEvent *)
{
  if(!renderSystem())
    return;

  if(!d_ptr->m_renderTarget)
    return;

  // check if device is lost
  switch(renderSystem()->TestCooperativeLevel())
  {
  case D3DERR_DEVICENOTRESET:
    // reset device
    renderSystem().resetDevice();
  default:
    break;

  case D3DERR_DEVICELOST:
  case D3DERR_DRIVERINTERNALERROR:
    // device lost, we can't do anything
    return;
  }

  if(d_ptr->m_swapBefore)
    swapBuffers();

  // start scene rendering
  if(renderSystem().beginScene())
  {
    render();

    // finish scene rendering
    renderSystem().endScene();
  }

  if(!d_ptr->m_swapBefore)
    swapBuffers();
}

void QRenderWidget::resizeEvent(QResizeEvent *)
{
  if(!renderSystem())
    return;

  if(isTopLevel())
  {
    renderSystem().resetDevice();
  }
  else
  {
    deviceLost();
    deviceRestore();
  }
}

bool QRenderWidget::render()
{
  if(!setRenderTarget())
    return false;
  return true;
}

void QRenderWidget::deviceRestore()
{
  if(!renderSystem())
    return;

  d_ptr->m_d3dpp = renderSystem().d_ptr->m_d3dpp;

  if(isTopLevel())
  {
    if(!dxe(renderSystem()->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, (LPDIRECT3DSURFACE9*)&d_ptr->m_renderTarget)))
    {
      qWarning("[D3D9] Failed to get RenderTarget from Device");
    }
  }
  else
  {
    qDebug("[D3D9] Recreating SwapChain [%ix%i]", width(), height());

    // Fill parameters
    d_ptr->m_d3dpp.BackBufferWidth = 0;
    d_ptr->m_d3dpp.BackBufferHeight = 0;
    d_ptr->m_d3dpp.hDeviceWindow = winId();

    d_ptr->m_swapChain.reset();
    d_ptr->m_renderTarget.reset();
    d_ptr->m_depthTarget.reset();

    // Create additional swapChain
    if(!dxe(renderSystem()->CreateAdditionalSwapChain(&d_ptr->m_d3dpp, (LPDIRECT3DSWAPCHAIN9*)&d_ptr->m_swapChain)))
      return;

    // Get final parameters
    if(!dxe(d_ptr->m_swapChain->GetPresentParameters(&d_ptr->m_d3dpp)))
      return;

    if(!dxe(d_ptr->m_swapChain->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, (LPDIRECT3DSURFACE9*)&d_ptr->m_renderTarget)))
    {
      qWarning("[D3D9] Failed to get RenderTarget from SwapChain");
    }
  }

  // Create depthStencil surface
  dxe(renderSystem()->CreateDepthStencilSurface(width(), height(),
                                                d_ptr->m_d3dpp.AutoDepthStencilFormat,
                                                d_ptr->m_d3dpp.MultiSampleType, d_ptr->m_d3dpp.MultiSampleQuality,
                                                FALSE,
                                                (LPDIRECT3DSURFACE9*)&d_ptr->m_depthTarget, NULL));
}

void QRenderWidget::deviceLost()
{
  d_ptr->m_swapChain.reset();
  d_ptr->m_renderTarget.reset();
  d_ptr->m_depthTarget.reset();
}

RenderWidgetPrivate::RenderWidgetPrivate()
{
  m_swapBefore = false;
}

RenderWidgetPrivate::~RenderWidgetPrivate()
{
}

void QRenderWidget::showEvent(QShowEvent *)
{
  if(isTopLevel())
  {
    qDebug() << "[RENDER] Showing MainWindow";

    if(renderSystem().d_ptr->m_widget == NULL)
    {
      renderSystem().d_ptr->m_widget = this;
      renderSystem().resetDevice();
    }
  }
  else
  {
    qDebug() << "[RENDER] Showing ChainWindow";

    if(renderSystem().d_ptr->m_widget == this)
    {
      renderSystem().d_ptr->m_widget = NULL;
      renderSystem().resetDevice();
    }
  }
}

bool QRenderWidget::setRenderTarget()
{
  // setup render targets
  if(SUCCEEDED(renderSystem()->SetRenderTarget(0, d_ptr->m_renderTarget.get())))
  {
    // render scene
    renderSystem()->SetDepthStencilSurface(d_ptr->m_depthTarget.get());
    renderSystem().setViewport(QRect(QPoint(), size()));
    renderSystem().clear(Qt::black);
    return true;
  }
  return false;
}

bool QRenderWidget::setDepthTarget()
{
  renderSystem()->SetDepthStencilSurface(d_ptr->m_depthTarget.get());
  return true;
}

void QRenderWidget::swapBuffers()
{
  // swap buffers
  if(d_ptr->m_swapChain)
    dxe(d_ptr->m_swapChain->Present(0, 0, 0, 0, 0));
  else
    dxe(renderSystem()->Present(0, 0, 0, 0));
}

bool QRenderWidget::swapBefore() const
{
  return d_ptr->m_swapBefore;
}

void QRenderWidget::setSwapBefore(bool swapBefore)
{
  d_ptr->m_swapBefore = swapBefore;
}
