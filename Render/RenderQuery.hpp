#ifndef RENDERQUERY_HPP
#define RENDERQUERY_HPP

#include <QObject>

#include "Render.hpp"

class RenderQueryPrivate;

class RENDER_EXPORT RenderQuery : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(RenderQuery)
  Q_ENUMS(Type)

public:
  enum Type
  {
    Undefined,
    Occlussion,
    Event
  };

public:
  explicit RenderQuery(Type type);
  ~RenderQuery();

public:
  Type type();
  bool begin();
  void end();
  bool isRunning() const;
  unsigned result();

private slots:
  void deviceLost();
  void deviceReset();

public:
  QScopedPointer<RenderQueryPrivate> d_ptr;
};

#endif // RENDERQUERY_HPP
