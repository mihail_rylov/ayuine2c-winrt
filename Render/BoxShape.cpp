#include "RenderSystem.hpp"
#include "RenderSystem_p.hpp"
#include "PrimitiveDrawer.hpp"

#include <Math/Box.hpp>
#include <Math/Matrix.hpp>

static const int BoxVertsCount = 8;

static vec3 BoxVerts[BoxVertsCount] =
{
  vec3(-1, -1, -1),
  vec3(-1, -1,  1),
  vec3(-1,  1, -1),
  vec3( 1, -1, -1),
  vec3(-1,  1,  1),
  vec3( 1,  1, -1),
  vec3( 1, -1,  1),
  vec3( 1,  1,  1)
};

static const int BoxIndicesCount = 6 * 3 * 2;

static ushort BoxIndices[BoxIndicesCount] =
{
  7, 6, 3,  5, 7, 3,
  4, 2, 0,  1, 4, 0,
  7, 4, 1,  6, 7, 1,
  5, 3, 0,  2, 5, 0,
  7, 5, 2,  4, 7, 2,
  6, 1, 0,  3, 6, 0,
};

static const int BoxLineIndicesCount = 6 * 4 * 2;

static ushort BoxLineIndices[BoxLineIndicesCount] =
{
  7, 6,  6, 3,  3, 5,  5, 7,
  4, 2,  2, 0,  0, 1,  1, 4,
  7, 4,  4, 1,  1, 6,  6, 7,
  5, 3,  3, 0,  0, 2,  2, 5,
  7, 5,  5, 2,  2, 4,  4, 7,
  6, 1,  1, 0,  0, 3,  3, 6
};

void RenderSystem::lostBox()
{
  d_ptr->m_boxVertexBuffer.reset();
  d_ptr->m_boxIndexBuffer.reset();
  d_ptr->m_boxLineIndexBuffer.reset();
}

void RenderSystem::restoreBox()
{
  d_ptr->m_boxVertexBuffer.reset(new StaticVertexBuffer<vec3>(BoxVerts, BoxVertsCount));
  d_ptr->m_boxIndexBuffer.reset(new ShortStaticIndexBuffer(BoxIndices, BoxIndicesCount));
  d_ptr->m_boxLineIndexBuffer.reset(new ShortStaticIndexBuffer(BoxLineIndices, BoxLineIndicesCount));
}

bool RenderSystem::drawBox(const matrix& transform)
{
  setTransform(transform);

  if(setVertexBuffer(VertexBuffer::Vertex, d_ptr->m_boxVertexBuffer.data()) &&
     setIndexBuffer(d_ptr->m_boxIndexBuffer.data()))
  {
    draw(IndexedPrimitiveDrawer(PrimitiveDrawer::TriList, 0, d_ptr->m_boxVertexBuffer->count(),
                                0, d_ptr->m_boxIndexBuffer->count() / 3));
    return true;
  }

  return false;
}

bool RenderSystem::drawBox(const box &bounds)
{
  return drawBox(bounds.toTransform());
}

bool RenderSystem::drawBox(const box& bounds, const matrix& transform)
{
  matrix newTransform;
  matrix::multiply(newTransform, transform, bounds.toTransform());
  return drawBox(newTransform);
}

bool RenderSystem::drawLineBox(const matrix& transform)
{
  setTransform(transform);

  if(setVertexBuffer(VertexBuffer::Vertex, d_ptr->m_boxVertexBuffer.data()) &&
     setIndexBuffer(d_ptr->m_boxLineIndexBuffer.data()))
  {
    draw(IndexedPrimitiveDrawer(PrimitiveDrawer::LineList, 0, d_ptr->m_boxVertexBuffer->count(),
                                0, d_ptr->m_boxLineIndexBuffer->count() / 2));
    return true;
  }

  return false;
}

bool RenderSystem::drawLineBox(const box& bounds)
{
  return drawLineBox(bounds.toTransform());
}

bool RenderSystem::drawLineBox(const box& bounds, const matrix& transform)
{
  matrix newTransform;
  matrix::multiply(newTransform, transform, bounds.toTransform());
  return drawLineBox(newTransform);
}
