#include "SceneEditor.hpp"
#include "SceneItemModel.hpp"
#include "CameraController/CameraController.hpp"

#include <Render/RenderComponentWidget.hpp>

#include <Math/Vec3.hpp>

#include <SceneGraph/Actor.hpp>
#include <Render/RenderComponentWidget.hpp>

#include <QMouseEvent>

bool SceneEditor::rotateMousePress(QMouseEvent *mouseEvent)
{
  renderComponents()->grabMouse();
  m_mouseOffset = mouseEvent->pos();
  m_rotateOffset = euler();
  m_rotateCenter = moveCenter();
  m_rotatePlane = movePlane();
  return true;
}

bool SceneEditor::rotateMouseMove(QMouseEvent *mouseEvent)
{
  ray dir1 = m_cameraController->direction(m_mouseOffset);
  ray dir2 = m_cameraController->direction(mouseEvent->pos());
  vec3 p1 = dir1.hit(m_movePlane.collide(dir1));
  vec3 p2 = dir2.hit(m_movePlane.collide(dir2));

  vec3 center = m_rotateCenter;

  if(actionPivotMode->isChecked())
  {
    center = m_pivotOffset;
  }

  euler p1a(p1 - m_rotateCenter, 0.0f);
  euler p2a(p2 - m_rotateCenter, 0.0f);

  float yaw = p2a.Yaw - p1a.Yaw;
  float pitch = p2a.Pitch - p1a.Pitch;

  if(!actionPivotMode->isChecked())
  {
    yaw = (mouseEvent->pos() - m_mouseOffset).x() * Deg2Rad;
    pitch = (mouseEvent->pos() - m_mouseOffset).y() * -Deg2Rad;
  }

  if(actionSnapGrid->isChecked())
  {
    const float roundAngle = 5.0f / 180.0f * M_PI;

    yaw = qRound(yaw / roundAngle) * roundAngle;
    pitch = qRound(pitch / roundAngle) * roundAngle;

    if(qFuzzyIsNull(yaw) && qFuzzyIsNull(pitch))
      return true;
  }

  yaw -= m_rotateOffset.Yaw;
  pitch -= m_rotateOffset.Pitch;

  m_rotateOffset.Yaw += yaw;
  m_rotateOffset.Pitch += pitch;

  bool changed = false;

  foreach(const QModelIndex &index, sceneTreeView->selectionModel()->selectedIndexes())
  {
    if(!index.isValid())
      continue;

    Actor* actor = m_sceneItemModel->toActor(index);
    if(!actor)
      continue;

    if(actionPivotMode->isChecked())
    {
      matrix newTransform(actor->worldTransform()), temp;

      matrix::translation(temp, -m_pivotOffset);
      matrix::multiply(newTransform, temp, newTransform);

      matrix::rotation(temp, euler(yaw, pitch, 0.0f));
      matrix::multiply(newTransform, temp, newTransform);

      matrix::translation(temp, m_pivotOffset);
      matrix::multiply(newTransform, temp, newTransform);

      actor->setWorldTransform(newTransform, true);
    }
    else
    {
      euler newAngles(actor->worldAngles());

      newAngles.Yaw += yaw;
      newAngles.Pitch += pitch;
      newAngles.Pitch = qBound<float>(-M_PI/2.0f, newAngles.Pitch, M_PI/2.0f);

      actor->setWorldAngles(newAngles, true);
    }

    changed = true;
  }

  if(changed)
  {
    renderComponents()->repaint();
  }
  return true;
}

bool SceneEditor::rotateMouseRelease(QMouseEvent *mouseEvent)
{
  renderComponents()->releaseMouse();
  return true;
}
