#include "SceneItemModel.hpp"
#include <SceneGraph/ActorManager.hpp>
#include <SceneGraph/Scene.hpp>
#include <Core/ClassInfo.hpp>
#include <QIcon>

#define qSceneDebug() qDebug()

#ifndef qSceneDebug
#define qSceneDebug() QNoDebug()
#endif

SceneItemModel::SceneItemModel(QObject *parent) :
  QAbstractItemModel(parent)
{
  setSupportedDragActions(Qt::CopyAction | Qt::MoveAction | Qt::LinkAction);
}

SceneItemModel::~SceneItemModel()
{
}

QModelIndex SceneItemModel::index(int row, int column, const QModelIndex &parent) const
{
  if(row < 0 || column < 0)
    return QModelIndex();

  // qSceneDebug() << "[SCENEITEM] GetIndex " << row << " Parent: " << parent.internalPointer();

  if(parent.isValid())
  {
    Actor* actor = (Actor*)parent.internalPointer();
    if(!actor)
      return QModelIndex();

    ActorManager* actorManager = qobject_cast<ActorManager*>(actor);
    if(!actorManager)
      return QModelIndex();

    if(row >= actorManager->actors().size())
      return QModelIndex();

    Actor* child = actorManager->actors()[row].data();
    return createIndex(row, column, child);
  }
  else if(row == 0 && m_scene)
  {
    return createIndex(0, 0, m_scene->rootActor().data());
  }

  return QModelIndex();
}

QModelIndex SceneItemModel::index(Actor* actor) const
{
  if(!actor)
    return QModelIndex();
  if(!actor->scene())
    return QModelIndex();

  int index = 0;

  if(actor->parentActor())
    index = actor->parentActor()->indexOf(actor);

  // qSceneDebug() << "[SCENEITEM] GetIndexOf " << actor << " at " << index;

  return createIndex(index, 0, actor);
}

QModelIndex SceneItemModel::parent(const QModelIndex &child) const
{
  if(!child.isValid())
    return QModelIndex();

  // qSceneDebug() << "[SCENEITEM] GetParent " << child.internalPointer() << " at " << child.row();

  Actor* actor = (Actor*)child.internalPointer();
  if(!actor)
    return QModelIndex();

  return index(actor->parentActor().data());
}

int SceneItemModel::rowCount(const QModelIndex &parent) const
{
  if(!parent.isValid())
    return 1;

  Actor* actor = (Actor*)parent.internalPointer();
  if(!actor)
    return 0;

  ActorManager* actorManager = qobject_cast<ActorManager*>(actor);
  if(!actorManager)
    return 0;

  if(actorManager->grouped())
    return 0;
  else
    return actorManager->actors().size();
}

int SceneItemModel::columnCount(const QModelIndex &parent) const
{
  return 1;
}

QVariant SceneItemModel::data(const QModelIndex &index, int role) const
{
  if(!index.isValid())
    return QVariant();

  Actor* actor = (Actor*)index.internalPointer();
  if(!actor)
    return QVariant();

  switch(role)
  {
  case Qt::DisplayRole:
    if(actor->objectName().length())
      return actor->objectName();
    else
      return actor->metaObject()->className();

  case Qt::EditRole:
    return actor->objectName();

  case Qt::ToolTipRole:
    return actor->metaObject()->className();

  case Qt::DecorationRole:
    return QVariant::fromValue(ClassInfo::getIcon(actor->metaObject()));

  default:
    return QVariant();
  }
}

bool SceneItemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if(!index.isValid())
    return false;

  Actor* actor = (Actor*)index.internalPointer();
  if(!actor)
    return false;

  switch(role)
  {
  case Qt::EditRole:
    actor->setObjectName(value.value<QString>());
    return true;

  default:
    return false;
  }
}

const QSharedPointer<Scene> & SceneItemModel::scene() const
{
  return m_scene;
}

void SceneItemModel::setScene(const QSharedPointer<Scene> &scene)
{
  if(m_scene)
  {
    m_scene->disconnect(this);
  }

  beginResetModel();

  m_scene = scene;

  if(m_scene)
  {
    connect(m_scene.data(), SIGNAL(rootActorAboutToBeChanged(Scene*,ActorManager*)), SLOT(rootActorAboutToChanged(Scene*,ActorManager*)));
    connect(m_scene.data(), SIGNAL(rootActorChanged(Scene*,ActorManager*)), SLOT(rootActorChanged(Scene*,ActorManager*)));

    connect(m_scene.data(), SIGNAL(objectNameChanged(Actor*)), SLOT(objectNameChanged(Actor*)));
    connect(m_scene.data(), SIGNAL(graphicsChanged(Actor*)), SLOT(graphicsChanged(Actor*)));

    connect(m_scene.data(), SIGNAL(groupedChanged(ActorManager*)), SLOT(groupedChanged(ActorManager*)));
    connect(m_scene.data(), SIGNAL(actorAdded(ActorManager*,Actor*)), SLOT(actorAdded(ActorManager*,Actor*)));
    connect(m_scene.data(), SIGNAL(actorAboutToBeRemoved(ActorManager*,Actor*)), SLOT(actorAboutToBeRemoved(ActorManager*,Actor*)));
    connect(m_scene.data(), SIGNAL(actorRemoved(ActorManager*,Actor*)), SLOT(actorRemoved(ActorManager*,Actor*)));
    connect(m_scene.data(), SIGNAL(actorsAboutToReset(ActorManager*)), SLOT(actorsAboutToReset(ActorManager*)));
    connect(m_scene.data(), SIGNAL(actorsReset(ActorManager*)), SLOT(actorsReset(ActorManager*)));
  }

  endResetModel();
}

Qt::ItemFlags SceneItemModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags flags = QAbstractItemModel::flags(index);

  if(index.isValid())
  {
    Actor* actor = (Actor*)index.internalPointer();

    if(actor)
    {
      flags |= Qt::ItemIsDragEnabled;

      if(actor->isSelectable())
        flags |= Qt::ItemIsSelectable;
      else
        flags &= ~Qt::ItemIsSelectable;

      if(qobject_cast<ActorManager*>(actor))
        flags |= Qt::ItemIsDropEnabled;
    }
  }

  return flags;
}

void SceneItemModel::objectNameChanged(Actor *actor)
{
  QModelIndex model = index(actor);

  if(model.isValid())
    emit dataChanged(model, model);
}

void SceneItemModel::actorAdded(ActorManager *manager, Actor *actor)
{
  int actorIndex = manager->indexOf(actor);
  if(actorIndex >= 0)
  {
    beginInsertRows(index(manager), actorIndex, actorIndex);
    endInsertRows();
  }
  qSceneDebug() << "[SCENEITEM] Added to " << manager << " an " << actor << " at " << actorIndex;
}

void SceneItemModel::actorAboutToBeRemoved(ActorManager *manager, Actor *actor)
{
  int actorIndex = manager->indexOf(actor);
  beginRemoveRows(index(manager), actorIndex, actorIndex);
  qSceneDebug() << "[SCENEITEM] Will remove from " << manager << " an " << actor << " at " << actorIndex;
}

void SceneItemModel::actorRemoved(ActorManager *manager, Actor *actor)
{
  endRemoveRows();
  qSceneDebug() << "[SCENEITEM] Removed from " << manager << " an " << actor;
}

void SceneItemModel::actorsAboutToReset(ActorManager *manager)
{
  beginRemoveRows(index(manager), 0, manager->actors().size());
  qSceneDebug() << "[SCENEITEM] Will reset an " << manager;
}

void SceneItemModel::actorsReset(ActorManager *manager)
{
  endRemoveRows();
  qSceneDebug() << "[SCENEITEM] Did reset an " << manager;
}

void SceneItemModel::rootActorAboutToChanged(Scene *scene, ActorManager *manager)
{
  beginResetModel();
}

void SceneItemModel::rootActorChanged(Scene *scene, ActorManager *manager)
{
  endResetModel();
}

void SceneItemModel::graphicsChanged(Actor *actor)
{
  QModelIndex model = index(actor);

  if(model.isValid())
  {
    emit graphicsChanged(model);
  }
}

Qt::DropActions SceneItemModel::supportedDropActions() const
{
  return Qt::MoveAction | Qt::CopyAction;
}

QDataStream& operator << (QDataStream& ds, const QModelIndex& index)
{
  ds.writeRawData((const char*)&index, sizeof(index));
  return ds;
}

QDataStream& operator >> (QDataStream& ds, QModelIndex& index)
{
  ds.readRawData((char*)&index, sizeof(index));
  return ds;
}

QMimeData *SceneItemModel::mimeData(const QModelIndexList &indexes) const
{
  if(indexes.isEmpty())
    return NULL;

  QMimeData* mimeData = new QMimeData();
  QByteArray encodedData;

  if(indexes.size() == 1)
  {
    QDataStream ds(&encodedData, QIODevice::WriteOnly);
    ds << indexes[0].internalId();
    mimeData->setData("ObjectRef", encodedData);
  }
  else if(indexes.size() > 1)
  {
    QDataStream ds(&encodedData, QIODevice::WriteOnly);
    ds << indexes;
    mimeData->setData("SceneItemModel", encodedData);
  }
  return mimeData;
}

bool SceneItemModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
  if (action == Qt::IgnoreAction)
    return true;

  if (column > 0)
    return false;

  if(!parent.isValid())
    return false;

  Actor* actor = (Actor*)parent.internalPointer();
  if(!actor)
    return false;

  ActorManager* manager = qobject_cast<ActorManager*>(actor);
  if(!manager)
    return false;

  ActorManagerRef managerRef = QWeakPointer<ActorManager>(manager).toStrongRef();

  if (data->hasFormat("SceneItemModel") || data->hasFormat("ObjectRef"))
  {
    QModelIndexList indexes;

    if(data->hasFormat("ObjectRef"))
    {
      qint64 index = 0;
      QDataStream ds(data->data("ObjectRef"));
      ds >> index;
      if(!index)
        return false;

      QObject* object = (QObject*)index;
      if(!object || !qobject_cast<Actor*>(object))
        return false;

      indexes.append(createIndex(0, 0, object));
    }
    else
    {
      QDataStream ds(data->data("SceneItemModel"));
      ds >> indexes;
    }

    foreach(const QModelIndex& index, indexes)
    {
      Actor* actor = (Actor*)index.internalPointer();
      if(!actor)
        continue;

      bool valid = true;

      for(Actor* parent = manager; parent; parent = parent->parentActor().data())
      {
        if(actor == parent)
        {
          valid = false;
          break;
        }
      }

      if(!valid)
        continue;

      switch(action)
      {
      case Qt::CopyAction:
        manager->addActor(actor->clone());
        break;

      case Qt::MoveAction:
        actor->setParentActor(managerRef);
        break;
      }
    }
    return true;
  }
  else if(data->hasFormat("MetaObjectItemModel"))
  {
    const QMetaObject* metaObject = ClassInfo::getMetaObject(data->data("MetaObjectItemModel"), &Actor::staticMetaObject);
    if(!metaObject)
      return true;

    QSharedPointer<Actor> newActor((Actor*)metaObject->newInstance());
    if(!newActor)
      return true;

    switch(action)
    {
    case Qt::CopyAction:
      manager->addActor(newActor);
      break;
    }
    return true;
  }

  return false;
}

QStringList SceneItemModel::mimeTypes() const
{
  QStringList mimeTypes;
  mimeTypes << "SceneItemModel";
  mimeTypes << "ObjectRef";
  mimeTypes << "MetaObjectItemModel";
  return mimeTypes;
}

QVector<Actor *> SceneItemModel::all() const
{
  QVector<Actor *> actors;

  QStack<ActorManager*> visit;
  visit.push(m_scene->rootActor().data());

  while(visit.size())
  {
    ActorManager* manager = visit.pop();

    foreach(const QSharedPointer<Actor> actor, manager->actors())
    {
      actors.append(actor.data());

      if(ActorManager* childManager = qobject_cast<ActorManager*>(actor.data()))
        visit.append(childManager);
    }
  }

  return actors;
}

void SceneItemModel::groupedChanged(ActorManager *manager)
{
  beginRemoveRows(index(manager), 0, manager->actors().size());
  endRemoveRows();
}

Actor * SceneItemModel::toActor(const QModelIndex &index)
{
  if(index.isValid())
    return (Actor*)index.internalPointer();
  return NULL;
}

QSharedPointer<Actor> SceneItemModel::toActorRef(const QModelIndex &index)
{
  return QWeakPointer<Actor>(toActor(index)).toStrongRef();
}
