#ifndef MESHVIEWER_HPP
#define MESHVIEWER_HPP

#include <QMainWindow>
#include <QModelIndex>
#include <CoreFrames/FrameEditor.hpp>

#include "ui_MeshViewer.h"

class Mesh;
class MeshItemModel;

class MeshViewer : public FrameEditor, protected Ui::MeshViewer
{
  Q_OBJECT

public:
  Q_INVOKABLE explicit MeshViewer(QWidget *parent = 0);
  ~MeshViewer();

public:
  const QMetaObject* resourceType() const;
  QSharedPointer<Resource> resource() const;
  bool setResource(const QSharedPointer<Resource> &newResource);

public:
  void prepareComponents();
  void finalizeComponents();

public slots:
  void updateSelected(const QModelIndex &modelIndex);

private:
  void addMeshBox(class Mesh& mesh, Frame& frame);
  void addMeshSubsetBox(class MeshSubset& meshSubset, Frame& frame);

private:
  QScopedPointer<MeshItemModel> m_meshItemModel;
  QSharedPointer<Mesh> m_mesh;
};

#endif // MESHVIEWER_HPP
