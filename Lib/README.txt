Use Visual C++ 9.0 (2008) to compile code!
=================================================
Needed libraries:

1) wxPack v2.8.8.04 - http://wxpack.sourceforge.net
* Replace wxPack with files from Lib/wxPack-fix
* Execute wxBuild_wxWidgets.bat VC90 NULL DLL_RELEASE_MONO
* Execute wxBuild_wxWidgets.bat VC90 NULL DLL_DEBUG_MONO
* Execute in propgrid wxBuild_wxWidgets.bat VC90 NULL DLL_RELEASE_MONO (Install bakefile_gen from bakefile.org)
* Execute in propgrid wxBuild_wxWidgets.bat VC90 NULL DLL_DEBUG_MONO

2) PhysX v2.8.1 - http://developer.nvidia.com/object/physx_downloads.html

3) DirectX SDK - http://www.microsoft.com/downloads/details.aspx?familyid=519AAE99-B701-4CA1-8495-39DDDE9D7030&displaylang=en

4) boost 1.35.0 (minimal) - http://www.boostpro.com/products/free

Add include & library paths to Visual C++.
