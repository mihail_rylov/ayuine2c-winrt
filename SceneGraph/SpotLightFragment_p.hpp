#ifndef SPOTLIGHTFRAGMENT_P_HPP
#define SPOTLIGHTFRAGMENT_P_HPP

#include <Pipeline/TexturedFragment.hpp>

class SpotLightFragment : public TexturedFragment
{
public:
  SpotLightFragment();
  ~SpotLightFragment();

public:
  VertexBuffer::Types vertexType() const;

public:
  void draw();
};

#endif // SPOTLIGHTFRAGMENT_P_HPP
