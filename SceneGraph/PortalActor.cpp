#include "PortalActor.hpp"

#include <Math/Poly.hpp>
#include <Physics/CollideInfo.hpp>
#include <Pipeline/Frame.hpp>
#include <Pipeline/SheetFragment.hpp>
#include <QColor>
#include <QIcon>

PortalActor::PortalActor()
{
  m_axis = Axis::X;
  m_width = 1.0f;
  m_height = 1.0f;
}

Axis::Enum PortalActor::axis() const
{
  return m_axis;
}

void PortalActor::setAxis(Axis::Enum axis)
{
  if(axis == Axis::NonAxial)
    return;

  if(m_axis == axis)
  {
    m_axis = axis;
    updateBounds();
  }
}

float PortalActor::width() const
{
  return m_width;
}

void PortalActor::setWidth(float width)
{
  m_width = width;
  updateBounds();
}

float PortalActor::height() const
{
  return m_height;
}

void PortalActor::setHeight(float height)
{
  m_height = height;
  updateBounds();
}

void PortalActor::generateDebugFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(frame.isHidden(this, true))
    return;

  QScopedPointer<SheetFragment> outlineFragment(new SheetFragment());
  outlineFragment->setTransform(localCamera.object);
  outlineFragment->setOrigin(worldOrigin());
  outlineFragment->setAxis(axis());
  outlineFragment->setSize(vec2(width(), height()));
  outlineFragment->setRasterizerState(RenderSystem::CullNone);
  if(frame.isSelected(this))
    outlineFragment->setColor(QColor::fromRgbF(0.43f, 0.25f, 0.3f, 0.6f));
  else
    outlineFragment->setColor(QColor::fromRgbF(0.53f, 0.3f, 0.4f, 0.4f));
  outlineFragment->setFixedState(RenderSystem::FixedConst);
  outlineFragment->setFillMode(RenderSystem::Solid);
  outlineFragment->setDepthStencilState(RenderSystem::DepthRead);
  outlineFragment->setBlendState(RenderSystem::Additive);
  outlineFragment->setOrder(999);
  frame.addFragment(outlineFragment.take());

  QScopedPointer<SheetFragment> wireFragment(new SheetFragment());
  wireFragment->setTransform(localCamera.object);
  outlineFragment->setOrigin(worldOrigin());
  outlineFragment->setAxis(axis());
  outlineFragment->setSize(vec2(width(), height()));
  wireFragment->setFixedState(RenderSystem::FixedConst);
  if(frame.isSelected(this))
    outlineFragment->setColor(QColor::fromRgbF(0.55f, 0.55f, 0.4f));
  else
    outlineFragment->setColor(QColor::fromRgbF(0.95f, 0.75f, 0.4f));
  wireFragment->setFillMode(RenderSystem::Wire);
  wireFragment->setOrder(1000);
  frame.addFragment(wireFragment.take());
}

bool PortalActor::collide(const ray &dir, CollideInfo &collide, DebugView* debugView) const
{
  if(debugView && debugView->isHidden(this))
    return false;

  if(~collide.flags & CollideInfo::Picking)
      return false;

  float time = m_boxBounds.collide(dir);

  if(time < Epsf || time > 0.0f)
      return false;

  vec3 hit = dir.hit(time);

  if(collide.flags & CollideInfo::Edges && !m_boxBounds.isOnEdge(hit))
      return false;

  collide.hitTime = time;
  collide.hit = hit;
  return true;
}

bool PortalActor::supportsZones() const
{
  return false;
}

void PortalActor::updateBounds()
{
  PolyVerts verts = Poly::portal(m_axis, vec2(m_width, m_height), origin());

  m_boxBounds = box(verts);
  m_boxBounds.Mins[m_axis] -= 0.01f;
  m_boxBounds.Maxs[m_axis] += 0.01f;

  m_sphereBounds = sphere(verts);
}
