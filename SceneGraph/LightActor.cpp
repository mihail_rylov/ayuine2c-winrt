#include "LightActor.hpp"
#include "Scene.hpp"

#include <Pipeline/ShadowFrame.hpp>
#include <Pipeline/CubeShadowFrame.hpp>

#include <QIcon>

LightActor::LightActor()
{
  m_lightColor = QColor(Qt::white);
  m_lightState.setMaterialShader("SimpleLightingModel.grcz");
}

LightActor::~LightActor()
{
}

bool LightActor::extendBounds() const
{
  return false;
}

bool LightActor::supportsZones() const
{
  return true;
}

bool LightActor::zonesFromPoint() const
{
  return true;
}

bool LightActor::castShadows() const
{
  return false;
}

QColor LightActor::lightColor() const
{
  return m_lightColor;
}

void LightActor::setLightColor(QColor lightColor)
{
  if(m_lightColor != lightColor)
  {
    m_lightColor = lightColor;
    emit graphicsChanged(this);
  }
}

void LightActor::removeFromScene()
{
  if(scene())
    scene()->cubeShadowFrame()->free(this);
  if(scene())
    scene()->shadowFrame()->free(this);
  Actor::removeFromScene();
}

LightShaderRef LightActor::lightShader() const
{
  return m_lightState.materialShader().objectCast<LightShader>();
}

void LightActor::setLightShader(const LightShaderRef &lightShader)
{
  m_lightState.setMaterialShader(lightShader);
}

void LightActor::setLightShader(const QString& lightShader)
{
  m_lightState.setMaterialShader(Resource::load<LightShader>(lightShader));
}

ShaderValues LightActor::lightShaderValues() const
{
  return m_lightState.values();
}

void LightActor::setLightShaderValues(const ShaderValues& values)
{
  m_lightState.setValues(values);
}

ShaderValues LightActor::refLightShaderValues() const
{
  return m_lightState.refValues();
}

const QMetaObject * LightActor::type() const
{
  return &staticMetaObject;
}
