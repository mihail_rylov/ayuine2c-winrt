#include "TerrainFragment_p.hpp"

const VertexBuffer::Types TerrainVertex::vertexType = VertexBuffer::Types(VertexBuffer::Vertex | VertexBuffer::Coords | VertexBuffer::Color | VertexBuffer::Normal | VertexBuffer::Tangent);

TerrainFragment::TerrainFragment()
{
  m_terrainQuality = MaterialSystem::High;
  m_color = Qt::white;
}

VertexBuffer::Types TerrainFragment::vertexType() const
{
  return TerrainVertex::vertexType;
}

bool TerrainFragment::bind(const Light *lights)
{
  getRenderSystem().setVertexBuffer(vertexType(), m_vertexBuffer.data());
  getRenderSystem().setIndexBuffer(m_indexBuffer.data());
  getRenderSystem().setTransform(m_transform);
  getRenderSystem().setColor(m_color);
  return Fragment::bind(lights);
}

void TerrainFragment::draw()
{
  if(!m_drawer.PrimitiveCount)
    return;
  getRenderSystem().draw(m_drawer);
}

ShaderState * TerrainFragment::materialShaderState() const
{
  return &m_terrainState;
}

SelectedTerrainFragment::SelectedTerrainFragment()
{
}

bool SelectedTerrainFragment::prepare()
{
  return true;
}

bool SelectedTerrainFragment::bind(const Light *lights)
{
  getRenderSystem().setVertexBuffer(vertexType(), m_vertexBuffer.data());
  getRenderSystem().setIndexBuffer(m_indexBuffer.data());
  return TexturedFragment::bind(lights);
}

void SelectedTerrainFragment::draw()
{
  if(!m_drawer.PrimitiveCount)
    return;
  getRenderSystem().draw(m_drawer);
}

VertexBuffer::Types SelectedTerrainFragment::vertexType() const
{
  return TerrainVertex::vertexType;
}

MaterialSystem::Quality TerrainFragment::materialQuality() const
{
  return m_terrainQuality;
}
