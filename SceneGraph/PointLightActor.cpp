#include "PointLightActor.hpp"
#include "StaticMeshActor.hpp"
#include "PointLightFragment_p.hpp"
#include "Scene.hpp"

#include <Pipeline/PointLight.hpp>
#include <Pipeline/TexturedFragment.hpp>
#include <Pipeline/SheetFragment.hpp>

#include <MaterialSystem/ShaderState.hpp>

PointLightActor::PointLightActor()
{
  m_lightRadius = 16.0f;
  m_shadowSides = 0;
}

float PointLightActor::lightRadius() const
{
  return m_lightRadius;
}

void PointLightActor::setLightRadius(float lightRadius)
{
  if(m_lightRadius != lightRadius)
  {
    m_lightRadius = lightRadius;
    m_shadowSides = 0;
    updateBounds();
    updateZone();
    setBoundsDirty();
    emit graphicsChanged(this);
  }
}

void PointLightActor::updateBounds()
{
  m_sphereBounds = sphere(origin(), m_lightRadius);
  m_boxBounds = box(origin(), m_lightRadius);
}

void PointLightActor::invalidate(const box &boxBounds, Actor *owner)
{
  static bool init = true;
  static vec4 clipPlanes[6];

  if(!qobject_cast<StaticMeshActor*>(owner))
    return;

  if (init)
  {
    clipPlanes[0] = vec4(+1,-1,0,0); clipPlanes[0].normalizeNormal();
    clipPlanes[1] = vec4(+1,+1,0,0); clipPlanes[1].normalizeNormal();
    clipPlanes[2] = vec4(0,+1,-1,0); clipPlanes[2].normalizeNormal();
    clipPlanes[3] = vec4(0,+1,+1,0); clipPlanes[3].normalizeNormal();
    clipPlanes[4] = vec4(+1,0,-1,0); clipPlanes[4].normalizeNormal();
    clipPlanes[5] = vec4(+1,0,+1,0); clipPlanes[5].normalizeNormal();
    init = false;
  }

  if(boxBounds.test(origin()))
  {
    m_shadowSides = 0;
    return;
  }

  if(castShadows())
  {
    static const int frustum[6][4][2] =
    {
      {{0,1},{1,1},{4,1},{5,1}}, // X-
      {{0,2},{1,2},{4,2},{5,2}}, // X+
      {{0,2},{1,1},{2,1},{3,1}}, // Y-
      {{0,1},{1,2},{2,2},{3,2}}, // Y+
      {{4,2},{5,1},{2,2},{3,1}}, // Z-
      {{4,1},{5,2},{2,1},{3,2}} // Z+
    };

    vec3 worigin = worldOrigin();
    Side::Enum sides[6];
    box worldTestBox = boxBounds.transform(parentTransform());
    box testBox(worldTestBox.Mins - worigin, worldTestBox.Maxs - worigin);
    for(int i = 0; i < 6; i++)
      sides[i] = clipPlanes[i].test(testBox);

    for(int i = 0; i < 6; i++)
    {
      int j;

      for(j = 0; j < 4; j++)
      {
        int side = sides[frustum[i][j][0]];
        int test = 3 - frustum[i][j][1];
        if(!(side & test))
          break;
      }

      if(j == 4)
      {
        m_shadowSides &= ~(1 << i);
      }
    }
  }
}

void PointLightActor::renderShadowMap(CubeShadowFrame* shadowFrame, const QSharedPointer<BaseRenderTarget> &shadowTexture, CubeFace::Enum cubeFace)
{
  if(!shadowFrame->begin(shadowTexture, cubeFace))
    return;

  Camera camera;
  Camera::cubeCamera(camera, worldOrigin(), cubeFace, lightRadius());
  shadowFrame->setCamera(camera);

  if(scene())
    scene()->generateFrame(*shadowFrame);

  shadowFrame->draw();
}

bool PointLightActor::zonesFromPoint() const
{
  return true;
}

void PointLightActor::updateTransform()
{
  LightActor::updateTransform();

  if(scene())
  {
    if(scene()->cubeShadowFrame())
      scene()->cubeShadowFrame()->free(this);
  }
}

void PointLightActor::generateFrame(Frame &frame, const LocalCamera& localCamera)
{
  if(!enabled())
    return;

  if(frame.hasLighting())
  {
    m_lightState.set("lightColor", lightColor());
    m_lightState.set("lightPosition", vec4(worldOrigin(), 1.0f / qMax(lightRadius(), 0.0001f)));

    QScopedPointer<PointLight> light(new PointLight());
    light->setOrigin(worldOrigin());
    light->setRadius(lightRadius());
    light->setLightState(m_lightState);
    light->setLightQuality(MaterialSystem::qualityForSphere(frame.camera().origin, sphere(light->origin(), light->radius())));

    if(scene())
    {
      if(scene()->cubeShadowFrame() && castShadows())
      {
#if 0
        light->state().set("cubeShadowTexture", QVariant::fromValue<BaseTextureRef>(scene()->cubeShadowFrame()->cleanRenderTarget()));
#else
        ShadowTextureRef shadowTexture;

        switch(scene()->cubeShadowFrame()->update(this, shadowTexture))
        {
        case CubeShadowFrame::Ignore:
          break;
        case CubeShadowFrame::Full:
          m_shadowSides = 0;
        case CubeShadowFrame::Partial:
          for(unsigned i = 0; i < 6; ++i)
          {
            if(m_shadowSides & (1 << i))
              continue;
            m_shadowSides |= 1 << i;
            renderShadowMap(scene()->cubeShadowFrame(), shadowTexture, (CubeFace::Enum)i);
          }
          break;
        }

        light->lightState().set("cubeShadowTexture", QVariant::fromValue<RenderTextureRef>(shadowTexture));
#endif
      }
    }

    frame.addLight(light.take());
  }
}

void PointLightActor::generateDebugFrame(Frame &frame, const LocalCamera& localCamera)
{
  LightActor::generateDebugFrame(frame, localCamera);

  if(frame.isSelected(this))
  {
    vec3 origin = worldOrigin();
    vec3 size = lightRadius();

    matrix transform(
          size.X, 0, 0, 0,
          0, size.Y, 0, 0,
          0, 0, size.Z, 0,
          origin.X, origin.Y, origin.Z, 1.0f);

    QScopedPointer<PointLightFragment> wireFragment(new PointLightFragment());
    wireFragment->setTransform(transform);
    wireFragment->setFixedState(RenderSystem::FixedConst);
    wireFragment->setColor(QColor::fromRgbF(0.35f, 0.46f, 0.13f));
    wireFragment->setFillMode(RenderSystem::Wire);
    wireFragment->setOrder(1001);
    frame.addFragment(wireFragment.take());
  }
}

bool PointLightActor::castShadows() const
{
  return m_lightState.contains("cubeShadowTexture");
}

void PointLightActor::compile()
{
  m_shadowSides = 0;
}

void PointLightActor::prepare()
{
  m_shadowSides = 0;
}
