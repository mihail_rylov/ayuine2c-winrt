#ifndef SPOTLIGHTACTOR_HPP
#define SPOTLIGHTACTOR_HPP

#include "LightActor.hpp"

class ShadowFrame;
class BaseRenderTarget;

typedef QSharedPointer<BaseRenderTarget> ShadowTextureRef;

class SCENEGRAPH_EXPORT SpotLightActor : public LightActor
{
  Q_OBJECT
  Q_PROPERTY(float lightRadius READ lightRadius WRITE setLightRadius)
  Q_PROPERTY(float shadowFovy READ shadowFovy WRITE setShadowFovy)

public:
  Q_INVOKABLE explicit SpotLightActor();

public:
  bool castShadows() const;

  float shadowFovy() const;
  void setShadowFovy(float shadowFovy);

  float lightRadius() const;
  void setLightRadius(float lightRadius);

protected:
  void updateBounds();
  void updateTransform();

public:
  void invalidate(const box &boxBounds, Actor *owner);
  void generateFrame(Frame &frame, const LocalCamera& localCamera);
  void generateDebugFrame(Frame &frame, const LocalCamera& localCamera);
  bool zonesFromPoint() const;
  void compile();
  void prepare();

private:
  const Camera& lightCamera();

private:
  void renderShadowMap(ShadowFrame* shadowFrame, const QSharedPointer<BaseRenderTarget> &shadowTexture);

private:
  float m_lightRadius;
  unsigned m_shadowSides;
  float m_shadowFovy;
  Camera m_lightCamera;
  bool m_lightCameraIsValid;
};

#endif // SPOTLIGHTACTOR_HPP
