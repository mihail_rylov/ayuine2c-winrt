#ifndef POINTLIGHTFRAGMENT_P_HPP
#define POINTLIGHTFRAGMENT_P_HPP

#include <Pipeline/TexturedFragment.hpp>

class PointLightFragment : public TexturedFragment
{
public:
  PointLightFragment();
  ~PointLightFragment();

public:
  VertexBuffer::Types vertexType() const;

public:
  void draw();
};

#endif // POINTLIGHTFRAGMENT_P_HPP
