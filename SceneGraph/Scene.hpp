#ifndef SCENE_HPP
#define SCENE_HPP

#include <Core/Resource.hpp>

#include <Pipeline/ShadowFrame.hpp>
#include <Pipeline/CubeShadowFrame.hpp>

#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/LightShader.hpp>

#include "Actor.hpp"
#include "ActorManager.hpp"

class PhysicsScene;

struct GameInputData
{
  bool Up, Down;
  bool Left, Right;
  int X, Y;
  float dX, dY;
  bool Fire;
  bool Jump;
  float Delta;

  GameInputData()
  {
    Up = Down = Left = Right = Fire = Jump = false;
    X = Y = 0;
    dX = dY = 0.0f;
    Delta = 0.0f;
  }
};

class SCENEGRAPH_EXPORT Scene : public Resource
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/Resources/IconWorld.png")

  Q_PROPERTY(vec3 gravity READ gravity WRITE setGravity)
  Q_PROPERTY(ActorManagerRef rootActor READ rootActor WRITE setRootActor DESIGNABLE false)
  Q_PROPERTY(ActorRef cameraActor READ cameraActor WRITE setCameraActor)
  Q_PROPERTY(ActorRef inputActor READ inputActor WRITE setInputActor)

  Q_PROPERTY(LightShaderRef ambientShader READ ambientShader WRITE setAmbientShader)
  Q_PROPERTY(ShaderValues ambientShaderValues READ ambientShaderValues WRITE setAmbientShaderValues)
  Q_PROPERTY(ShaderValues __refambientShaderValues READ refAmbientShaderValues DESIGNABLE false)

  Q_PROPERTY(MaterialShaderRef skyShader READ skyShader WRITE setSkyShader)
  Q_PROPERTY(ShaderValues skyShaderValues READ skyShaderValues WRITE setSkyShaderValues)
  Q_PROPERTY(ShaderValues __refskyShaderValues READ refSkyShaderValues DESIGNABLE false)

public:
  Q_INVOKABLE explicit Scene();
  ~Scene();

public:
  bool isGame() const;
  void setIsGame(bool isGame);

  float gameTime() const;
  void setGameTime(float gameTime);

  const box& boxBounds() const;

  const vec3& gravity() const;
  void setGravity(const vec3& gravity);

  QSharedPointer<Actor> cameraActor() const;
  void setCameraActor(const QSharedPointer<Actor>& cameraActor);

  QSharedPointer<Actor> inputActor() const;
  void setInputActor(const QSharedPointer<Actor>& inputActor);

  const QSharedPointer<ActorManager>& rootActor() const;
  void setRootActor(const QSharedPointer<ActorManager>& rootActor);

public:
  LightShaderRef ambientShader() const;
  void setAmbientShader(const LightShaderRef& lightShader);
  void setAmbientShader(const QString& lightShader);

  ShaderValues ambientShaderValues() const;
  void setAmbientShaderValues(const ShaderValues& values);

  ShaderValues refAmbientShaderValues() const;

public:
  MaterialShaderRef skyShader() const;
  void setSkyShader(const MaterialShaderRef& lightShader);
  void setSkyShader(const QString& lightShader);

  ShaderValues skyShaderValues() const;
  void setSkyShaderValues(const ShaderValues& values);

  ShaderValues refSkyShaderValues() const;

signals:
  void gravityChanged(Scene* scene);
  void cameraActorChanged(Scene* scene);
  void inputActorChanged(Scene* scene);
  void rootActorAboutToBeChanged(Scene* scene, ActorManager* manager);
  void rootActorChanged(Scene* scene, ActorManager* manager);

signals:
  void groupedChanged(ActorManager* manager);
  void actorAdded(ActorManager* manager, Actor* actor);
  void actorAboutToBeRemoved(ActorManager* manager, Actor* actor);
  void actorRemoved(ActorManager* manager, Actor* actor);
  void actorsAboutToReset(ActorManager* manager);
  void actorsReset(ActorManager* manager);
  void transformChanged(Actor* actor);
  void enabledChanged(Actor* actor);
  void parentChanged(Actor* actor);
  void objectNameAboutToBeChanged(Actor* actor);
  void objectNameChanged(Actor* actor);
  void graphicsChanged(Actor* actor);

public:
  void update(float dt);

  bool collide(const ray& dir, CollideInfo& collide, DebugView* debugView = NULL);

  bool beginGame();
  void endGame();

  void generateFrame(Frame& frame);
  void generateDebugFrame(Frame& frame);
  void generateColorPickFrame(ColorPickFrame& frame);

  void invalidate(const box& boxBounds, Actor* owner);

  bool input(const GameInputData& input);

  bool camera(Camera& camera, float aspect);

  void compile();
  void prepare();

public:
  PhysicsScene* physicsScene() const;
  CubeShadowFrame* cubeShadowFrame() const;
  ShadowFrame* shadowFrame() const;

protected:
  bool m_isGame;
  float m_gameTime;
  box m_boxBounds;
  vec3 m_gravity;
  QWeakPointer<Actor> m_cameraActor;
  QWeakPointer<Actor> m_inputActor;
  QSharedPointer<ActorManager> m_rootActor;
  QScopedPointer<PhysicsScene> m_physicsScene;
  QScopedPointer<CubeShadowFrame> m_cubeShadowFrame;
  QScopedPointer<ShadowFrame> m_shadowFrame;
  ShaderState m_ambientState;
  ShaderState m_skyState;
};

#endif // SCENE_HPP
