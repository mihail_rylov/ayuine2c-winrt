#ifndef POINTLIGHTACTOR_HPP
#define POINTLIGHTACTOR_HPP

#include "LightActor.hpp"

class CubeShadowFrame;
class BaseRenderTarget;

typedef QSharedPointer<BaseRenderTarget> ShadowTextureRef;

class SCENEGRAPH_EXPORT PointLightActor : public LightActor
{
  Q_OBJECT
  Q_PROPERTY(float lightRadius READ lightRadius WRITE setLightRadius)

public:
  Q_INVOKABLE explicit PointLightActor();

public:
  bool castShadows() const;

  float lightRadius() const;
  void setLightRadius(float lightRadius);

protected:
  void updateBounds();
  void updateTransform();

public:
  void invalidate(const box &boxBounds, Actor *owner);
  void generateFrame(Frame &frame, const LocalCamera& localCamera);
  void generateDebugFrame(Frame &frame, const LocalCamera& localCamera);
  bool zonesFromPoint() const;
  void compile();
  void prepare();

private:
  void renderShadowMap(CubeShadowFrame* shadowFrame, const QSharedPointer<BaseRenderTarget> &shadowTexture, CubeFace::Enum cubeFace);

private:
  float m_lightRadius;
  unsigned m_shadowSides;
};

#endif // POINTLIGHTACTOR_HPP
