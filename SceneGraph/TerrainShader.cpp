#include "TerrainShader.hpp"
#include "TerrainFragment_p.hpp"

#include <Render/RenderShaderConst.hpp>

Q_IMPLEMENT_OBJECTREF(TerrainShader)

TerrainShader::TerrainShader()
{
}

TerrainShader::~TerrainShader()
{
}

VertexBuffer::Types TerrainShader::vertexType() const
{
  return TerrainVertex::vertexType;
}
