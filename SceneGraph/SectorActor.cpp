#include "SectorActor.hpp"

#include <Physics/CollideInfo.hpp>
#include <Pipeline/Frame.hpp>
#include <Pipeline/ColorPickFrame.hpp>
#include <Pipeline/BoxFragment.hpp>
#include <QColor>
#include <QIcon>

SectorActor::SectorActor()
{
  m_autoSector = true;
}

void SectorActor::generateDebugFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(frame.isHidden(this, true))
      return;

  QScopedPointer<BoxFragment> outlineFragment(new BoxFragment());
  outlineFragment->setTransform(localCamera.object);
  outlineFragment->setBounds(boxBounds());
  outlineFragment->setRasterizerState(RenderSystem::CullNone);
  if(frame.isSelected(this))
    outlineFragment->setColor(QColor::fromRgbF(0.33f, 0.15f, 0.25f, 0.6f));
  else
    outlineFragment->setColor(QColor::fromRgbF(0.43f, 0.2f, 0.3f, 0.4f));
  outlineFragment->setFixedState(RenderSystem::FixedConst);
  outlineFragment->setFillMode(RenderSystem::Solid);
  outlineFragment->setDepthStencilState(RenderSystem::DepthRead);
  outlineFragment->setBlendState(RenderSystem::Additive);
  outlineFragment->setOrder(999);
  frame.addFragment(outlineFragment.take());

  QScopedPointer<BoxFragment> wireFragment(new BoxFragment());
  wireFragment->setTransform(localCamera.object);
  wireFragment->setBounds(boxBounds());
  wireFragment->setFixedState(RenderSystem::FixedConst);
  if(frame.isSelected(this))
    wireFragment->setColor(QColor::fromRgbF(0.55f, 0.25f, 0.4f));
  else
    wireFragment->setColor(QColor::fromRgbF(0.75f, 0.45f, 0.4f));
  wireFragment->setFillMode(RenderSystem::Wire);
  wireFragment->setOrder(1000);
  frame.addFragment(wireFragment.take());
}

void SectorActor::generateColorPickFrame(ColorPickFrame &frame, const LocalCamera &localCamera)
{
  if(frame.isHidden(this, true))
    return;

  QScopedPointer<BoxFragment> outlineFragment(new BoxFragment());
  outlineFragment->setTransform(localCamera.object);
  outlineFragment->setBounds(boxBounds());
  outlineFragment->setRasterizerState(RenderSystem::CullNone);
  outlineFragment->setColor(frame.objectToColor(this));
  outlineFragment->setFixedState(RenderSystem::FixedConst);
  outlineFragment->setFillMode(RenderSystem::Solid);
  outlineFragment->setDepthStencilState(RenderSystem::DepthRead);
  outlineFragment->setOrder(999);
  frame.addFragment(outlineFragment.take());
}

bool SectorActor::collide(const ray &dir, CollideInfo &collide, DebugView* debugView) const
{
  if(debugView && debugView->isHidden(this))
    return false;

  if(~collide.flags & CollideInfo::Picking)
      return false;

  float time = m_boxBounds.collide(dir);

  if(time < 0.0f || time > collide.hitTime)
      return false;

  vec3 hit = dir.hit(time);

  if(collide.flags & CollideInfo::Edges && !m_boxBounds.isOnEdge(hit))
      return false;

  collide.hitTime = time;
  collide.hit = hit;
  return true;
}

bool SectorActor::supportsZones() const
{
  return false;
}

void SectorActor::updateBounds()
{
  m_boxBounds = box(vec3(0,0,0), 1.0f).transform(m_transform);
  m_boxBounds.inflate(0.01f);
  m_sphereBounds = m_boxBounds.range();
}

bool SectorActor::autoSector() const
{
  return m_autoSector;
}

void SectorActor::setAutoSector(bool autoSector)
{
  m_autoSector = autoSector;
}
