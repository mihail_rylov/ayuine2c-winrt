#include "TerrainActor.hpp"
#include "TerrainFragment_p.hpp"
#include "Scene.hpp"

#include <QImage>

#include <Pipeline/ColorPickFrame.hpp>

#include <Physics/HeightFieldCollider.hpp>
#include <Physics/TriCollider.hpp>
#include <Physics/StaticRigidBody.hpp>

Q_IMPLEMENT_METATYPE(TerrainHeightsList)

TerrainActor::TerrainActor()
{
  m_maxLodLevels = 3;
}

TerrainActor::~TerrainActor()
{
}

TerrainShaderRef TerrainActor::terrainShader() const
{
  return m_terrainState.materialShader().objectCast<TerrainShader>();
}

void TerrainActor::setTerrainShader(const TerrainShaderRef &lightShader)
{
  m_terrainState.setMaterialShader(lightShader);
}

void TerrainActor::setTerrainShader(const QString& lightShader)
{
  m_terrainState.setMaterialShader(Resource::load<TerrainShader>(lightShader));
}

ShaderValues TerrainActor::terrainShaderValues() const
{
  return m_terrainState.values();
}

void TerrainActor::setTerrainShaderValues(const ShaderValues& values)
{
  m_terrainState.setValues(values);
}

ShaderValues TerrainActor::refTerrainShaderValues() const
{
  return m_terrainState.refValues();
}

QVector<qint16> TerrainActor::heights() const
{
  return m_heights;
}

void TerrainActor::setHeights(const QVector<qint16> &heights)
{
  m_heights = heights;

  clearTerrainData();

  if(m_heights.isEmpty())
  {
    m_minHeight = 0;
    m_maxHeight = 0;
  }

  m_minHeight = SHRT_MAX;
  m_maxHeight = SHRT_MIN;

  for(int i = 0; i < heights.size(); ++i)
  {
    m_minHeight = qMin(heights[i], m_minHeight);
    m_maxHeight = qMax(heights[i], m_maxHeight);
  }
}

QVector<vec3> TerrainActor::normals() const
{
  return m_normals;
}

void TerrainActor::setNormals(const QVector<vec3> &normals)
{
  m_normals = normals;
  clearTerrainData();
}

inline int toLodLevel(const vec3& cameraOrigin, const matrix& objectTransform, QSize size, int maxLevels)
{
#if 1
  return 0;
#else
  box bounds = box(vec3(-1, -1, -1), vec3(1, 1, 1)).transform(objectTransform);
  vec3 shortest(0,0,0);

  for(int i = 0; i < 3; ++i)
  {
    if(cameraOrigin[i] < bounds.Mins[i])
      shortest[i] = bounds.Mins[i] - cameraOrigin[i];
    else if(cameraOrigin[i] > bounds.Maxs[i])
      shortest[i] = bounds.Maxs[i] - cameraOrigin[i];
  }

  float distance = shortest.length();
  int level = 0;

  while(level < maxLevels)
  {
    if(distance < 40)
      return level;
    ++level;
    distance /= 10;
  }

  return maxLevels-1;
#endif
}

void TerrainActor::generateFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(!updateTerrainData())
    return;

  matrix objectTransform = localCamera.object * transform();
  sphere objectSphere = sphereBounds().transform(objectTransform);

  MaterialSystem::Quality quality = MaterialSystem::qualityForSphere(frame.camera().origin, objectSphere);

  QScopedPointer<TerrainFragment> terrainFragment(new TerrainFragment());
  terrainFragment->m_vertexBuffer = m_terrainVertexBuffer;
  terrainFragment->m_indexBuffer = m_terrainIndexBuffer;
  terrainFragment->m_drawer = m_terrainLodLevels[toLodLevel(frame.camera().origin, objectTransform, m_size, m_terrainLodLevels.size())];
  terrainFragment->m_transform = objectTransform;
  terrainFragment->m_terrainQuality = quality;
  terrainFragment->m_terrainState = m_terrainState;
  terrainFragment->m_terrainState.set("normalTexture", m_terrainNormalTexture);
  terrainFragment->m_terrainState.set("layerTexture", m_terrainLayerTexture);

  frame.addFragment(terrainFragment.take());
}

void TerrainActor::generateDebugFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(!updateTerrainData())
  {
    Actor::generateDebugFrame(frame, localCamera);
    return;
  }

  if(frame.isSelected(this))
  {
    matrix objectTransform = localCamera.object * transform();
    sphere objectSphere = sphereBounds().transform(objectTransform);

    if(!frame.isWireframe())
    {
      generateFrame(frame, localCamera);

      QScopedPointer<SelectedTerrainFragment> outlineFragment(new SelectedTerrainFragment());
      outlineFragment->m_vertexBuffer = m_terrainVertexBuffer;
      outlineFragment->m_indexBuffer = m_terrainIndexBuffer;
      outlineFragment->m_drawer = m_terrainLodLevels[toLodLevel(frame.camera().origin, objectTransform, m_size, m_terrainLodLevels.size())];
      outlineFragment->setTransform(objectTransform);
      outlineFragment->setColor(QColor::fromRgbF(0.35f, 0.45f, 0.23f, 0.20f));
      outlineFragment->setFixedState(RenderSystem::FixedConst);
      outlineFragment->setFillMode(RenderSystem::Solid);
      outlineFragment->setDepthStencilState(RenderSystem::DepthRead);
      outlineFragment->setBlendState(RenderSystem::Additive);
      outlineFragment->setOrder(999);
      frame.addFragment(outlineFragment.take());
    }

    QScopedPointer<SelectedTerrainFragment> wireFragment(new SelectedTerrainFragment());
    wireFragment->m_vertexBuffer = m_terrainVertexBuffer;
    wireFragment->m_indexBuffer = m_terrainIndexBuffer;
    wireFragment->m_drawer = m_terrainLodLevels[toLodLevel(frame.camera().origin, objectTransform, m_size, m_terrainLodLevels.size())];
    wireFragment->setTransform(objectTransform);
    wireFragment->setFixedState(RenderSystem::FixedConst);
    wireFragment->setColor(QColor::fromRgbF(0.55f, 0.66f, 0.33f));
    wireFragment->setFillMode(RenderSystem::Wire);
    wireFragment->setOrder(1000);
    frame.addFragment(wireFragment.take());
  }
  else
  {
    generateFrame(frame, localCamera);
  }
}

void TerrainActor::generateColorPickFrame(ColorPickFrame &frame, const LocalCamera &localCamera)
{
  if(!updateTerrainData())
  {
    Actor::generateColorPickFrame(frame, localCamera);
    return;
  }

  matrix objectTransform = localCamera.object * transform();
  sphere objectSphere = sphereBounds().transform(objectTransform);

  QScopedPointer<TerrainFragment> terrainFragment(new TerrainFragment());
  terrainFragment->m_vertexBuffer = m_terrainVertexBuffer;
  terrainFragment->m_indexBuffer = m_terrainIndexBuffer;
  terrainFragment->m_drawer = m_terrainLodLevels[toLodLevel(frame.camera().origin, objectTransform, m_size, m_terrainLodLevels.size())];
  terrainFragment->m_transform = objectTransform;
  terrainFragment->m_color = frame.objectToColor(this);
  terrainFragment->m_terrainState = m_terrainState;
  frame.addFragment(terrainFragment.take());
}

bool TerrainActor::extendBounds() const
{
  return true;
}

void TerrainActor::updateBounds()
{
  box bounds(vec3(-m_size.width() / 2, -m_size.height() / 2, m_minHeight - 0.5f), vec3(m_size.width() / 2, m_size.height() / 2, m_maxHeight + 0.5f));
  m_boxBounds = bounds.transform(m_transform);
  m_sphereBounds = m_boxBounds.range();
}

unsigned TerrainActor::maxLodLevels() const
{
  return m_maxLodLevels;
}

void TerrainActor::setMaxLodLevels(unsigned lodLevels)
{
  m_maxLodLevels = qBound<int>(0, lodLevels, 16);
}

bool TerrainActor::updateTerrainData()
{
  if(m_size.isEmpty())
    return false;

  if(!m_terrainState.rawMaterialShader())
    return false;

  if(!m_terrainVertexBuffer)
  {
    QSharedPointer<StaticVertexBuffer<TerrainVertex> > vertexBuffer(new StaticVertexBuffer<TerrainVertex>(m_size.width() * m_size.height()));

    if(TerrainVertex* vertices = vertexBuffer->lock(vertexBuffer->count()))
    {
      for(int y = 0; y < m_size.height(); ++y)
      {
        float t = (y + 0.5f) / m_size.height();

        for(int x = 0; x < m_size.width(); ++x)
        {
          int i = x + y * m_size.width();
          float s = (x + 0.5f) / m_size.width();
          float h = i < m_heights.size() ? m_heights[i] : 0;
          vec3 normal = i < m_normals.size() ? m_normals[i] : vec3(0, 0, 1);
          vec4 blends = i < m_blends.size() ? m_blends[i] : vec4(1,1,1,1);

          vertices->origin = vec3(x - m_size.width() / 2, y - m_size.height() / 2, h);
          vertices->normal = normal;
          vertices->color = QColor::fromRgbF(blends.X, blends.Y, blends.Z, blends.W).rgba();
          vertices->coords = vec2(s, t);
          vertices->tangent = vec4(1, 0, 0, 1);
          ++vertices;
        }
      }
      vertexBuffer->unlock();
    }

    m_terrainVertexBuffer = vertexBuffer;
  }

  if(!m_terrainIndexBuffer || m_terrainLodLevels.isEmpty())
  {
    QVector<ushort> indices;
    QVector<IndexedPrimitiveDrawer> drawers;

    QSize size(m_size.width() & ~1, m_size.height() & ~1);
    int step = 1;

    for(int i = 0; i <= m_maxLodLevels; ++i)
    {
      unsigned indexStart = indices.size();

      for(int y = 0; y < size.height(); y += step)
      {
        for(int x = 0; x < size.width(); x += step)
        {
          ushort a11 = x + y * m_size.width();
          ushort a21 = (x + step) + y * m_size.width();
          ushort a12 = x + (y + step) * m_size.width();
          ushort a22 = (x + step) + (y + step) * m_size.width();

          indices.append(a11);
          indices.append(a22);
          indices.append(a12);

          indices.append(a11);
          indices.append(a21);
          indices.append(a22);
        }
      }

      unsigned indexCount = indices.size() - indexStart;

      if(!indexCount)
        break;

      IndexedPrimitiveDrawer drawer;
      drawer.Topology = IndexedPrimitiveDrawer::TriList;
      drawer.MinIndex = 0;
      drawer.NumVertices = m_size.width() * m_size.height();
      drawer.StartIndex = indexStart;
      drawer.PrimitiveCount = indexCount / 3;
      drawers.append(drawer);

      step *= 4;
    }

    m_terrainIndexBuffer = QSharedPointer<IndexBuffer>(new ShortStaticIndexBuffer(indices));
    m_terrainLodLevels = drawers;
  }

  if(m_terrainLodLevels.isEmpty())
    return false;

  if(!m_terrainNormalTexture)
  {
    QImage normalImage(m_size, QImage::Format_ARGB32);

    for(int y = 0; y < m_size.height(); ++y)
    {
      for(int x = 0; x < m_size.width(); ++x)
      {
        int i = x + y * m_size.width();
        vec3 normal = i < m_normals.size() ? m_normals[i] : vec3(0, 0, 1);

        normal = normal * 0.5f + 0.5f;

        normalImage.setPixel(x, y, QColor::fromRgbF(normal.X, normal.Y, normal.Z).rgb());
      }
    }

    m_terrainNormalTexture = RenderTextureRef(RenderTexture::fromImage(normalImage));
  }

  if(!m_terrainLayerTexture)
  {
    QImage layerImage(m_size, QImage::Format_ARGB32);

    for(int y = 0; y < m_size.height(); ++y)
    {
      for(int x = 0; x < m_size.width(); ++x)
      {
        int i = x + y * m_size.width();
        vec4 blend = i < m_blends.size() ? m_blends[i] : vec4(0, 0, 0, 0);
        layerImage.setPixel(x, y, QColor::fromRgbF(blend.X, blend.Y, blend.Z, blend.W).rgba());
      }
    }

    m_terrainLayerTexture = RenderTextureRef(RenderTexture::fromImage(layerImage));
  }

  return true;
}

void TerrainActor::clearTerrainData()
{
  m_terrainVertexBuffer.clear();
  m_terrainIndexBuffer.clear();
  m_terrainLodLevels.clear();
  m_terrainNormalTexture.clear();
  m_terrainLayerTexture.clear();
  setBoundsDirty();
}

QSize TerrainActor::size() const
{
  return m_size;
}

int quantize(int x)
{
  for(int y = 1<<30; y > 0; y >>= 1)
  {
    if(x > y)
      return y;
  }
  return 0;
}

void TerrainActor::setSize(QSize size)
{
  m_size.setWidth(quantize(size.width()) | 1);
  m_size.setHeight(quantize(size.height()) | 1);
  clearTerrainData();
}

unsigned TerrainActor::width() const
{
  return m_size.width();
}

void TerrainActor::setWidth(unsigned width)
{
  setSize(QSize(width, m_size.height()));
}

unsigned TerrainActor::height() const
{
  return m_size.height();
}

void TerrainActor::setHeight(unsigned height)
{
  setSize(QSize(m_size.width(), height));
}

ShaderState TerrainActor::terrainState() const
{
  return m_terrainState;
}

ShaderState &TerrainActor::terrainState()
{
  return m_terrainState;
}

Vec4List TerrainActor::blends() const
{
  return m_blends;
}

void TerrainActor::setBlends(Vec4List blends)
{
  m_blends = blends;
  m_terrainVertexBuffer.clear();
}

bool TerrainActor::beginGame()
{
  if(!Actor::beginGame())
    return false;

  if(m_size.isEmpty())
    return false;

  if(!scene() || !scene()->physicsScene())
    return false;

  QVector<vec3> vertices(m_size.width() * m_size.height());
  QVector<ushort> indices;

  matrix transform = worldTransform();
  
  for(int y = 0; y < m_size.height(); ++y)
  { 
    for(int x = 0; x < m_size.width(); ++x)
    {
      int i = x + y * m_size.width();
      float h = i < m_heights.size() ? m_heights[i] : 0;
      vertices[i] = transform.transformCoord(vec3(x - m_size.width() / 2, y - m_size.height() / 2, h));
    }
  }

  for(int y = 1; y < m_size.height(); ++y)
  {
    for(int x = 1; x < m_size.width(); ++x)
    {
      ushort a11 = (x - 1) + (y - 1) * m_size.width();
      ushort a21 = x + (y - 1) * m_size.width();
      ushort a12 = (x - 1) + y * m_size.width();
      ushort a22 = x + y * m_size.width();

      indices.append(a11);
      indices.append(a22);
      indices.append(a12);

      indices.append(a11);
      indices.append(a21);
      indices.append(a22);
    }
  }

  QByteArray heightCollider = TriCollider::cookTriangleMesh(vertices, indices);

  m_heightFieldCollider.reset(new TriCollider(heightCollider));
  //m_heightFieldCollider->setRowScale(heightScale.X);
  //m_heightFieldCollider->setColumnScale(heightScale.Y);
  //m_heightFieldCollider->setHeightScale(heightScale.Z);

  m_heightFieldBody.reset(new StaticRigidBody(m_heightFieldCollider.data(), mat4Identity, scene()->physicsScene()));
  return true;
}

void TerrainActor::endGame()
{
  Actor::endGame();
  m_heightFieldBody.reset();
  m_heightFieldCollider.reset();
}
