#ifndef TERRAINFRAGMENT_P_HPP
#define TERRAINFRAGMENT_P_HPP

#include <Pipeline/Fragment.hpp>
#include <Pipeline/TexturedFragment.hpp>
#include <Render/VertexBuffer.hpp>
#include <Render/PrimitiveDrawer.hpp>

struct TerrainVertex
{
public:
  static const VertexBuffer::Types vertexType;

public:
  vec3 origin;
  vec2 coords;
  QRgb color;
  vec3 normal;
  vec4 tangent;
};

class TerrainFragment : public Fragment
{
public:
  TerrainFragment();

public:
  VertexBuffer::Types vertexType() const;

public:
  bool bind(const Light* lights);
  void draw();

public:
  ShaderState *materialShaderState() const;
  MaterialSystem::Quality materialQuality() const;

private:
  QSharedPointer<VertexBuffer> m_vertexBuffer;
  QSharedPointer<IndexBuffer> m_indexBuffer;
  IndexedPrimitiveDrawer m_drawer;
  matrix m_transform;
  QColor m_color;
  MaterialSystem::Quality m_terrainQuality;
  mutable ShaderState m_terrainState;

  friend class TerrainActor;
};

class SelectedTerrainFragment : public TexturedFragment
{
public:
  SelectedTerrainFragment();

public:
  bool prepare();
  bool bind(const Light* lights);
  void draw();
  VertexBuffer::Types vertexType() const;

private:
  QSharedPointer<VertexBuffer> m_vertexBuffer;
  QSharedPointer<IndexBuffer> m_indexBuffer;
  IndexedPrimitiveDrawer m_drawer;

  friend class TerrainActor;
};

#endif // TERRAINFRAGMENT_P_HPP
