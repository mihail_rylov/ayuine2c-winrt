#ifndef TERRAINACTOR_HPP
#define TERRAINACTOR_HPP

#include "Actor.hpp"

#include "TerrainShader.hpp"

#include <Render/VertexBuffer.hpp>
#include <Render/IndexBuffer.hpp>
#include <Render/PrimitiveDrawer.hpp>

typedef QVector<qint16> TerrainHeightsList;

Q_DECLARE_RAW_VECTOR_SERIALIZER(qint16)
Q_DECLARE_METATYPE(TerrainHeightsList)

class Collider;
class StaticRigidBody;

class SCENEGRAPH_EXPORT TerrainActor : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconTerrain.png")

  Q_PROPERTY(TerrainHeightsList heights READ heights WRITE setHeights DESIGNABLE false)
  Q_PROPERTY(Vec3List normals READ normals WRITE setNormals DESIGNABLE false)
  Q_PROPERTY(Vec4List blends READ blends WRITE setBlends DESIGNABLE false)
  Q_PROPERTY(QSize size READ size WRITE setSize DESIGNABLE false)
  Q_PROPERTY(int width READ width WRITE setWidth STORED false)
  Q_PROPERTY(int height READ height WRITE setHeight STORED false)
  Q_PROPERTY(int maxLodLevels READ maxLodLevels WRITE setMaxLodLevels)

  Q_PROPERTY(TerrainShaderRef terrainShader READ terrainShader WRITE setTerrainShader)
  Q_PROPERTY(ShaderValues terrainShaderValues READ terrainShaderValues WRITE setTerrainShaderValues)
  Q_PROPERTY(ShaderValues __refterrainShaderValues READ refTerrainShaderValues DESIGNABLE false)

public:
  Q_INVOKABLE explicit TerrainActor();
  ~TerrainActor();

signals:

public slots:
  QVector<qint16> heights() const;
  void setHeights(const QVector<qint16>& heights);

  Vec4List blends() const;
  void setBlends(Vec4List blends);

  QVector<vec3> normals() const;
  void setNormals(const QVector<vec3>& normals);

  QSize size() const;
  void setSize(QSize size);

  unsigned width() const;
  void setWidth(unsigned width);

  unsigned height() const;
  void setHeight(unsigned height);

  unsigned maxLodLevels() const;
  void setMaxLodLevels(unsigned lodLevels);

public:
  TerrainShaderRef terrainShader() const;
  void setTerrainShader(const TerrainShaderRef &lightShader);
  void setTerrainShader(const QString& lightShader);
  ShaderValues terrainShaderValues() const;
  void setTerrainShaderValues(const ShaderValues& values);
  ShaderValues refTerrainShaderValues() const;
  ShaderState terrainState() const;
  ShaderState &terrainState();

public:
  void generateFrame(Frame& frame, const LocalCamera& localCamera);
  void generateDebugFrame(Frame& frame, const LocalCamera& localCamera);
  void generateColorPickFrame(ColorPickFrame& frame, const LocalCamera& localCamera);

public:
  bool beginGame();
  void endGame();

protected:
  void updateBounds();
  void clearTerrainData();
  bool updateTerrainData();

public:
  bool extendBounds() const;

private:
  QVector<qint16> m_heights;
  QVector<vec3> m_normals;
  Vec4List m_blends;
  QSize m_size;
  qint16 m_minHeight, m_maxHeight;
  unsigned m_maxLodLevels;

private:
  ShaderState m_terrainState;

private:
  QSharedPointer<VertexBuffer> m_terrainVertexBuffer;
  QSharedPointer<IndexBuffer> m_terrainIndexBuffer;
  QVector<IndexedPrimitiveDrawer> m_terrainLodLevels;
  QSharedPointer<RenderTexture> m_terrainNormalTexture;
  QSharedPointer<RenderTexture> m_terrainLayerTexture;

private:
  QScopedPointer<Collider> m_heightFieldCollider;
  QScopedPointer<StaticRigidBody> m_heightFieldBody;
};

#endif // TERRAINACTOR_HPP
