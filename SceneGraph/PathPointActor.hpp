#ifndef PLAYERACTOR_HPP
#define PLAYERACTOR_HPP

#include "Actor.hpp"

struct ControllerBodyHit;

class PhysicsController;

class SCENEGRAPH_EXPORT PathPointActor : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconPlayer.png")
  Q_PROPERTY(ActorRef nextPoint READ nextPoint WRITE setNextPoint)

public:
  Q_INVOKABLE PathPointActor();
  ~PathPointActor();

public:
  bool beginGame();
  void endGame();
  bool camera(Camera &camera, float aspect);
  void update(float dt);

public:
  void generateDebugFrame(Frame& frame, const LocalCamera& localCamera);

public:
  QSharedPointer<Actor> nextPoint() const;
  void setNextPoint(const QSharedPointer<Actor>& nextPoint);

public:
  float nextTime() const;
  float totalTime() const;
  QSharedPointer<PathPointActor> pointAtTime(float time, bool loop = false, float* outTime = NULL) const;
  vec3 originAtTime(float time, bool loop = false) const;

private:
  vec3 origin2(float time);
  vec3 origin3(float time);
  vec3 origin4(float time);

private:
  QSharedPointer<PathPointActor> m_nextPoint;
  float m_pathTime;
  vec3 m_cameraPosition;
};

#endif // PLAYERACTOR_HPP
