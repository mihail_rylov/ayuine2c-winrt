#ifndef TERRAINSHADER_HPP
#define TERRAINSHADER_HPP

#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/ShaderBlockOutput.hpp>

#include "SceneGraph.hpp"

class SCENEGRAPH_EXPORT TerrainShader : public GeneralShader
{
  Q_OBJECT
  Q_CLASSINFO("UniquePrefix", "ts")

public:
  Q_INVOKABLE explicit TerrainShader();
  ~TerrainShader();

public:
  VertexBuffer::Types vertexType() const;
};

Q_DECLARE_OBJECTREF(TerrainShader)

#endif // TERRAINSHADER_HPP
