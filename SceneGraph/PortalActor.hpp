#ifndef PORTALACTOR_HPP
#define PORTALACTOR_HPP

#include "Actor.hpp"

#include <Math/mathlib.hpp>

class SCENEGRAPH_EXPORT PortalActor : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconPortal.png")
  Q_PROPERTY(Axis::Enum axis READ axis WRITE setAxis)
  Q_PROPERTY(float width READ width WRITE setWidth)
  Q_PROPERTY(float height READ height WRITE setHeight)

public:
  Q_INVOKABLE PortalActor();

public:
  Axis::Enum axis() const;
  void setAxis(Axis::Enum axis);

  float width() const;
  void setWidth(float width);

  float height() const;
  void setHeight(float height);

public:
  void generateDebugFrame(Frame &frame, const LocalCamera& localCamera);

  bool collide(const ray &dir, CollideInfo &collide, DebugView* debugView = NULL) const;

  bool supportsZones() const;

protected:
  void updateBounds();

private:
  Axis::Enum m_axis;
  float m_width;
  float m_height;
};

#endif // PORTALACTOR_HPP
