#ifndef TIMEDEMOGAME_HPP
#define TIMEDEMOGAME_HPP

#include "Game.hpp"

#include <Render/RenderSystem.hpp>

struct TimeDemoStats : public RenderStats
{
  TimeDemoStats(const RenderStats& stats) : RenderStats(stats)
  {
    frameTime = 0.0f;
    fragmentCount = 0;
    instancedRatio = 0;
    lightCount = 0;
    visibleLights = 0;
  }

  float frameTime;
  int fragmentCount;
  float instancedRatio;
  int lightCount;
  int visibleLights;

  operator float () const
  {
    return frameTime;
  }
};

class GAMESYSTEM_EXPORT TimeDemoGame : public Game
{
  Q_OBJECT
public:
  explicit TimeDemoGame(QWidget *parent = 0);
  ~TimeDemoGame();

public:
  float frameTime() const;
  void setFrameTime(float frameTime);

  unsigned maxFrames() const;
  void setMaxFrames(unsigned maxFrames);

  QList<TimeDemoStats> frameTimes() const;
  void resetFrameTimes();

  QVector<float> bestFrameTimes() const;
  float meanFrameTime() const;
  float minFrameTime() const;
  float maxFrameTime() const;
  float avgFrameTime() const;
  float stdDevFrameTime() const;

public:
  QStringList toStringList() const;

protected:
  void updateInput();
  void updateTime(float dt);

private:
  float m_frameTime;
  unsigned m_maxFrames;
  QList<TimeDemoStats> m_frameTimes;
  mutable QVector<float> m_bestFrameTimes;
};

#endif // TIMEDEMOGAME_HPP
