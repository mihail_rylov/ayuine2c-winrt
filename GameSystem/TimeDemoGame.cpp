#include "TimeDemoGame.hpp"

#include <Windows.h>

TimeDemoGame::TimeDemoGame(QWidget *parent) :
  Game(parent)
{
  m_frameTime = 20.0f / 1000.0f; // 20ms
  m_maxFrames = 10000; // test for 10000 frames
}

TimeDemoGame::~TimeDemoGame()
{
}

float TimeDemoGame::frameTime() const
{
  return m_frameTime;
}

void TimeDemoGame::setFrameTime(float fixedTime)
{
  m_frameTime = fixedTime;
}

unsigned TimeDemoGame::maxFrames() const
{
  return m_maxFrames;
}

void TimeDemoGame::setMaxFrames(unsigned fixedFrames)
{
  m_maxFrames = fixedFrames;
}

QList<TimeDemoStats> TimeDemoGame::frameTimes() const
{
    return m_frameTimes;
}

void TimeDemoGame::resetFrameTimes()
{
    m_frameTimes.clear();
    m_bestFrameTimes.clear();
}

inline bool operator < (const TimeDemoStats& a, const TimeDemoStats& b)
{
  return a.frameTime < b.frameTime;
}

QVector<float> TimeDemoGame::bestFrameTimes() const
{
    if(m_bestFrameTimes.count())
        return m_bestFrameTimes;

    m_bestFrameTimes = QVector<float>::fromStdVector(std::vector<float>(m_frameTimes.begin(), m_frameTimes.end()));
    qSort(m_bestFrameTimes);
    unsigned frameCount = m_bestFrameTimes.size() / 20;
    m_bestFrameTimes.remove(0, frameCount);
    m_bestFrameTimes.remove(m_bestFrameTimes.size() - frameCount, frameCount);
    return m_bestFrameTimes;
}

float TimeDemoGame::meanFrameTime() const
{
    return m_frameTimes.size() ? m_frameTimes[m_frameTimes.size()/2] : 0.0f;
}

float TimeDemoGame::minFrameTime() const
{
    if(bestFrameTimes().size())
    {
        float minTime = bestFrameTimes()[0];
        foreach(float time, bestFrameTimes())
            minTime = qMin(time, minTime);
        return minTime;
    }
    return 0.0f;
}

float TimeDemoGame::maxFrameTime() const
{
    if(bestFrameTimes().size())
    {
        float maxTime = bestFrameTimes()[0];
        foreach(float time, bestFrameTimes())
            maxTime = qMax(time, maxTime);
        return maxTime;
    }
    return 0.0f;
}

float TimeDemoGame::avgFrameTime() const
{
    float avgTime = 0.0f;
    foreach(float time, bestFrameTimes())
      avgTime += time;
    avgTime /= bestFrameTimes().count();
    return avgTime;
}

float TimeDemoGame::stdDevFrameTime() const
{
    float avgTime = avgFrameTime();
    float stdDevTime = 0.0f;
    foreach(float time, bestFrameTimes())
       stdDevTime += (time - avgTime) * (time - avgTime);
    stdDevTime /= bestFrameTimes().count();
    stdDevTime = sqrtf(stdDevTime);
    return stdDevTime;
}

QStringList TimeDemoGame::toStringList() const
{
    QStringList lines;

    lines.append(QString("TimeDemo: %1 of %2").arg(m_frameTimes.length()).arg(m_maxFrames));
    lines.append(QString("MinTime:%1ms").arg(minFrameTime() * 1000.0f));
    lines.append(QString("MaxTime:%1ms").arg(maxFrameTime() * 1000.0f));
    lines.append(QString("AvgTime:%1ms").arg(avgFrameTime() * 1000.0f));
    lines.append(QString("StdDevTime:%1ms").arg(stdDevFrameTime() * 1000.0f));
    lines.append("");
    lines += Game::toStringList();
    return lines;
}

void TimeDemoGame::updateInput()
{
  if(GetAsyncKeyState(VK_ESCAPE) & 0x8000)
  {
    hide();
  }
}

void TimeDemoGame::updateTime(float dt)
{
  if(dt > 0)
  {
    TimeDemoStats stats(getRenderSystem().stats());

    stats.frameTime = dt;

    if(m_frame)
    {
      stats.fragmentCount = m_frame->fragmentsCount(&stats.instancedRatio);
      stats.lightCount = m_frame->lightsCount(&stats.visibleLights);
    }

    m_frameTimes.append(stats);
    m_bestFrameTimes.clear();
  }

  Game::updateTime(m_frameTime);

  if(m_frameTimes.size() >= m_maxFrames)
    hide();
}
