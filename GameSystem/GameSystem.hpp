#ifndef GAMESYSTEM_HPP
#define GAMESYSTEM_HPP

#include <QtGlobal>

#ifdef GAMESYSTEM_EXPORTS
#define GAMESYSTEM_EXPORT Q_DECL_EXPORT
#else
#define GAMESYSTEM_EXPORT Q_DECL_IMPORT
#endif

#endif // GAMESYSTEM_HPP
