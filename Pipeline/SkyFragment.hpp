#ifndef SKYFRAGMENT_HPP
#define SKYFRAGMENT_HPP

#include "Fragment.hpp"

class PIPELINE_EXPORT SkyFragment : public Fragment
{
public:
  explicit SkyFragment();

public:
  VertexBuffer::Types vertexType() const;
  ShaderState *materialShaderState() const;

public:
  bool bind(const Light* lights);
  void draw();
  void unbind();

public:
  ShaderState& state();
  const ShaderState& state() const;
  void setState(const ShaderState& lightState);

private:
  mutable ShaderState m_state;
};

#endif // SKYFRAGMENT_HPP
