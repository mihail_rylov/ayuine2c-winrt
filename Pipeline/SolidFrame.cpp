#include "SolidFrame.hpp"
#include <MaterialSystem/MaterialShaderCompiler.hpp>
#include <Render/RenderSystem.hpp>
#include <Render/RenderShader.hpp>

void compileSolidColor(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output)
{
  if(output.isNull())
  {
    compiler.ps("outColor = %1;", compiler.getp(MaterialShaderCompiler::pColor));
  }
  else if(ShaderBlock::InputLink colorLink = output->input("Color"))
  {
    compiler.ps("outColor.rgb = %1;", colorLink.varName(compiler));
    if(ShaderBlock::InputLink alphaLink = output->input("Alpha"))
      compiler.ps("outColor.a = %1;", alphaLink.varName(compiler));
    else
      compiler.ps("outColor.a = 1.0f;");
  }
  else if(ShaderBlock::InputLink colorLink = output->input("Diffuse"))
  {
    compiler.ps("outColor.rgb = %1;", colorLink.varName(compiler));
    if(ShaderBlock::InputLink alphaLink = output->input("Opacity"))
      compiler.ps("outColor.a = %1;", alphaLink.varName(compiler));
    else
      compiler.ps("outColor.a = 1.0f;");
  }
  else
  {
    compiler.ps("outColor = %1;", compiler.getp(MaterialShaderCompiler::pColor));
  }
}

SolidFrame::SolidFrame()
{
}

void SolidFrame::compile(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output)
{
  compileSolidColor(compiler, output);
}

Frame::FrameType SolidFrame::type() const
{
  return Solid;
}
