#include "GridFragment.hpp"
#include <Render/RenderSystem.hpp>
#include <Render/PrimitiveDrawer.hpp>
#include <Math/Matrix.hpp>

inline QString offsetToText(float x)
{
  if(qFuzzyIsNull(x))
  {
    return "0";
  }
  else if(x > 1000 || x < -1000)
  {
    float y = x / 1000;
    if(qFuzzyIsNull(x))
      return QString::number((int)y) + "km";
    else
      return QString::number(y, 'g', 3) + "km";
  }
  else
  {
    if(qFuzzyIsNull(x * 100))
      return QString::number((int)x) + "m";
    else
      return QString::number(x, 'g', 2) + "m";
  }
}

GridFragment::GridFragment()
{
  setOrder(10);
  m_showText = true;
  m_size = 1.0f;
  m_blockSize = 8;
  m_color = QColor::fromRgbF(0.21f, 0.21f, 0.21f, 1.0f);
  m_blockColor = QColor::fromRgbF(0.43f, 0.33f, 0.33f, 1.0f);
  m_axisColor = QColor::fromRgbF(0.53f, 0.53f, 0.33f, 1.0f);
  m_textColor = QColor::fromRgbF(0.75f, 0.75f, 0.75f, 0.75f);
}

GridFragment::~GridFragment()
{
}

bool GridFragment::showText() const
{
  return m_showText;
}

void GridFragment::setShowText(bool showText)
{
  m_showText = showText;
}

Axis::Enum GridFragment::axis() const
{
  return m_axis;
}

void GridFragment::setAxis(Axis::Enum axis)
{
  m_axis = axis;
}

float GridFragment::size() const
{
  return m_size;
}

void GridFragment::setSize(float size)
{
  m_size = size;
}

unsigned GridFragment::blockSize() const
{
  return m_blockSize;
}

void GridFragment::setBlockSize(unsigned blockSize)
{
  m_blockSize = blockSize;
}

const vec3 & GridFragment::offset() const
{
  return m_offset;
}

void GridFragment::setOffset(const vec3 &offset)
{
  m_offset = offset;
}

QColor GridFragment::color() const
{
  return m_color;
}

void GridFragment::setColor(const QColor &color)
{
  m_color = color;
}

QColor GridFragment::blockColor() const
{
  return m_blockColor;
}

void GridFragment::setBlockColor(const QColor &blockColor)
{
  m_blockColor = blockColor;
}

QColor GridFragment::axisColor() const
{
  return m_axisColor;
}

void GridFragment::setAxisColor(const QColor &axisColor)
{
  m_axisColor = axisColor;
}

QColor GridFragment::textColor() const
{
  return m_textColor;
}

void GridFragment::setTextColor(const QColor &textColor)
{
  m_textColor = textColor;
}

QRectF GridFragment::bounds() const
{
  return m_bounds;
}

void GridFragment::setBounds(const QRectF &bounds)
{
  m_bounds = bounds;
}

ShaderState * GridFragment::materialShaderState() const
{
  return NULL;
}

bool GridFragment::bind(const Light* lights)
{
  getRenderSystem().setShader(RenderSystem::FixedConst);
  getRenderSystem().setRasterizerState(RenderSystem::CullClockwise);
  getRenderSystem().setBlendState(RenderSystem::AlphaBlend);
  getRenderSystem().setDepthStencilState(RenderSystem::DepthDefault);
  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setTransform(mat4Identity);
  return true;
}

void GridFragment::draw()
{
  const unsigned MaxVertices = 4096;
  static vec3 vertices[MaxVertices];
  unsigned count = 0;
  vec3 right, up, at;
  cubeFaceVectors(m_axis, at, up, right);

  // Oblicz wsp�rz�dne siatki
  float igridSize = 1.0f / m_size;
  float igridBlockSize = 1.0f / m_blockSize;

  vec2 min(qCeil(m_bounds.left() * igridSize) * m_size,
           qCeil(m_bounds.top() * igridSize) * m_size);

  vec2 max(qFloor(m_bounds.right() * igridSize) * m_size + Epsf,
           qFloor(m_bounds.bottom() * igridSize) * m_size + Epsf);

  vec2 minBlock(qCeil(m_bounds.left() * igridBlockSize) * m_blockSize,
                qCeil(m_bounds.top() * igridBlockSize) * m_blockSize);

  vec2 maxBlock(qFloor(m_bounds.right() * igridBlockSize) * m_blockSize + Epsf,
                qFloor(m_bounds.bottom() * igridBlockSize) * m_blockSize + Epsf);

  vec3 minX3(right * m_bounds.left() + m_offset),
      maxX3(right * m_bounds.right() + m_offset);

  vec3 minY3(up * m_bounds.top() + m_offset),
      maxY3(up * m_bounds.bottom() + m_offset);

  // ------ OSIE ------
  // Wygeneruj osie
  if(m_bounds.left() < 0 && 0 < m_bounds.right())
  {
    vertices[count++] = minY3;
    vertices[count++] = maxY3;
  }
  if(m_bounds.top() < 0 && 0 < m_bounds.bottom())
  {
    vertices[count++] = minX3;
    vertices[count++] = maxX3;
  }

  // Wy�wietl osie
  if(count > 0)
  {
    getRenderSystem().setColor(m_axisColor);
    getRenderSystem().setVertexData(VertexBuffer::Vertex, vertices, count * sizeof(vec3));
    getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::LineList, 0, count / 2));
  }
  count = 0;

  // ------ DU�E BLOCZKI ------
  // Wygeneruj linie pomocnicze
  for(float x = minBlock.X; x <= maxBlock.X && count < MaxVertices; x += m_blockSize)
  {
    // Osie?
    if(qFuzzyIsNull(x * 512.0f))
      continue;

    vec3 curr = right * x;

    // Dodaj wierzcho�ki
    vertices[count++] = minY3 + curr;
    vertices[count++] = maxY3 + curr;
  }
  for(float y = minBlock.Y; y <= maxBlock.Y && count < MaxVertices; y += m_blockSize)
  {
    // Osie?
    if(qFuzzyIsNull(y * 512.0f))
      continue;

    vec3 curr = up * y;

    // Dodaj wierzcho�ki
    vertices[count++] = minX3 + curr;
    vertices[count++] = maxX3 + curr;
  }

  // Wy�wietl linie pomocnicze
  if(count > 0)
  {
    getRenderSystem().setColor(m_blockColor);
    getRenderSystem().setVertexData(VertexBuffer::Vertex, vertices, count * sizeof(vec3));
    getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::LineList, 0, count / 2));
  }
  count = 0;

  // ------ MA�E BLOCZKI ------
  // Wygeneruj linie g��wne
  for(float x = min.X; x <= max.X && count < MaxVertices; x += m_size)
  {
    // Bloki?
    if(qFuzzyIsNull(truncf(x * igridBlockSize) * 512.0f))
      continue;

    vec3 curr = right * x;

    // Dodaj wierzcho�ki
    vertices[count++] = minY3 + curr;
    vertices[count++] = maxY3 + curr;
  }
  for(float y = min.Y; y <= max.Y && count < MaxVertices; y += m_size)
  {
    // Bloki?
    if(qFuzzyIsNull(truncf(y * igridBlockSize) * 512.0f))
      continue;

    vec3 curr = up * y;

    // Dodaj wierzcho�ki
    vertices[count++] = minX3 + curr;
    vertices[count++] = maxX3 + curr;
  }

  // Wy�wietl linie g��wne
  if(count > 0)
  {
    getRenderSystem().setColor(m_color);
    getRenderSystem().setVertexData(VertexBuffer::Vertex, vertices, count * sizeof(vec3));
    getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::LineList, 0, count / 2));
  }
  count = 0;

  // ------ TEKST ------
#if 0
  if(m_showText)
  {
    D3DVIEWPORT9 vp;
    matrix projview;

    // Pobierz rozmiar widoku
    dxe(getRenderSystem()->GetViewport(&vp));

    // Oblicz odwr�cona macierz transformacji
    //dxe(m_device->GetTransform(D3DTS_PROJECTION, (D3DMATRIX*)&projview));
    //matrix::transpose(projview);

    // Rozpocznij rysowanie tekstu
    //beginText();

    // Rysuj tekst
    for(float x = minBlock.X; x <= maxBlock.X; x += m_blockSize)
    {
      vec3 at = m_projView.transformCoord(minY3 + right * x);
      getRenderSystem().drawText(vec2(vp.X + (at.X * 0.5f + 0.5f) * vp.Width, vp.Y + 2), offsetToText(x), ffCenter | ffTop, gridTextColor());
    }
    for(float y = minBlock.Y; y >= maxBlock.Y; y -= m_blockSize)
    {
      vec3 at = m_projView.transformCoord(minX3 + up * y);
      getRenderSystem().drawText(vec2(vp.X + 2, vp.Y + (-at.Y * 0.5f + 0.5f) * vp.Height), offsetToText(y), ffLeft | ffMiddle, gridTextColor());
    }

    // Zako�cz rysowanie tekstu
    //endText();
  }
#endif
}

VertexBuffer::Types GridFragment::vertexType() const
{
  return VertexBuffer::Vertex;
}
