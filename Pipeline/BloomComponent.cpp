#include "BloomComponent.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/RenderShader.hpp>
#include <Render/RenderTarget.hpp>

#include <Math/Vec2.hpp>
#include <Math/Vec4.hpp>
#include <Math/Matrix.hpp>

BloomComponent::BloomComponent()
{
  m_shaderExtract.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Bloom.fx", "Extract"));
  m_shaderBlur.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Bloom.fx", "Blur"));
  m_shaderCombine.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Bloom.fx", "Combine"));
  m_shaderCombineEdges.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Bloom.fx", "Combine"));

  setPreset(Default);
}

BloomComponent::~BloomComponent()
{
}

void BloomComponent::resizeEvent(const QSize &newSize)
{
  m_sceneRenderTarget.reset(new RenderTarget(newSize.width(), newSize.height(), RenderTarget::Rgba));
  m_renderTarget1.reset(new RenderTarget(newSize.width() / 2, newSize.height() / 2, RenderTarget::Rgba));
  m_renderTarget2.reset(new RenderTarget(newSize.width() / 2, newSize.height() / 2, RenderTarget::Rgba));
}

int BloomComponent::beginOrder()
{
  return 5;
}

int BloomComponent::drawOrder()
{
  return 20;
}

bool BloomComponent::begin()
{
  getRenderSystem().setRenderTarget(m_sceneRenderTarget.data());
  getRenderSystem().clear(RenderSystem::Target, QColor(Qt::black));
  return true;
}

static float computeGaussian(float n, float blurAmount)
{
    return (float)((1.0f / sqrtf(2 * M_PI * blurAmount)) *
                   expf(-(n * n) / (2 * blurAmount * blurAmount)));
}

void BloomComponent::draw()
{
  prepare();
  extractBloom(m_sceneRenderTarget.data(), m_renderTarget1.data());
  blurTarget(m_renderTarget1.data(), m_renderTarget2.data(), 1.0f / (float)m_renderTarget1->width(), 0.0f);
  blurTarget(m_renderTarget2.data(), m_renderTarget1.data(), 0.0f, 1.0f / (float)m_renderTarget1->height());
  blendTargets(m_sceneRenderTarget.data(), m_renderTarget1.data());
}

void BloomComponent::setBlurParameters(float dx, float dy)
{
  const int sampleCount = 15;
  vec4 samples[sampleCount];

  samples[0] = vec4();
  samples[0].Z = computeGaussian(0, m_blurAmount);

  float totalWeights = samples[0].Z;

  for(int i = 0; i < sampleCount / 2; i++)
  {
      float weight = computeGaussian(i + 1, m_blurAmount);

      samples[i * 2 + 1].Z = weight;
      samples[i * 2 + 2].Z = weight;

      totalWeights += weight * 2;

      float sampleOffset = i * 2 + 1.5f;

      vec2 delta = vec2(dx, dy) * sampleOffset;

      samples[i * 2 + 1].X = delta.X;
      samples[i * 2 + 1].Y = delta.Y;

      samples[i * 2 + 2].X = -delta.X;
      samples[i * 2 + 2].Y = -delta.Y;
  }

  for (int i = 0; i < sampleCount; i++)
  {
      samples[i].Z /= totalWeights;
  }

  getRenderSystem().setConst(10, &samples[0].X, sampleCount);
}

void BloomComponent::setPreset(BloomComponent::Preset preset)
{
  switch(preset)
  {
  case Default:
    m_bloomThreshold = 0.25f;
    m_blurAmount = 4;
    m_bloomIntensity = 1.25f;
    m_baseIntensity = 1.0f;
    m_bloomSaturation = 1.0f;
    m_baseSaturation = 1.0f;
    break;

  case Soft:
    m_bloomThreshold = 0;
    m_blurAmount = 3;
    m_bloomIntensity = 1.25f;
    m_baseIntensity = 1.0f;
    m_bloomSaturation = 1.0f;
    m_baseSaturation = 1.0f;
    break;

  case Desaturated:
    m_bloomThreshold = 0.5f;
    m_blurAmount = 8;
    m_bloomIntensity = 2;
    m_baseIntensity = 1;
    m_bloomSaturation = 0;
    m_baseSaturation = 1;
    break;

  case Saturated:
    m_bloomThreshold = 0.25f;
    m_blurAmount = 4;
    m_bloomIntensity = 2;
    m_baseIntensity = 1;
    m_bloomSaturation = 2;
    m_baseSaturation = 0;
    break;

  case Blurry:
    m_bloomThreshold = 0;
    m_blurAmount = 2;
    m_bloomIntensity = 1;
    m_baseIntensity = 0.1f;
    m_bloomSaturation = 1;
    m_baseSaturation = 1;
    break;

  case Subtle:
    m_bloomThreshold = 0.5f;
    m_blurAmount = 2;
    m_bloomIntensity = 1;
    m_baseIntensity = 1;
    m_bloomSaturation = 1;
    m_baseSaturation = 1;
    break;
  }
}

void BloomComponent::extractBloom(RenderTarget* in, RenderTarget* out)
{
  getRenderSystem().setShader(m_shaderExtract.data());
  getRenderSystem().setConst(10, &m_bloomThreshold, 1);
  getRenderSystem().setRenderTarget(out);
  getRenderSystem().setTexture(in, 0);
  getRenderSystem().drawScreenQuad();
}

void BloomComponent::blurTarget(RenderTarget *in, RenderTarget *out, float dx, float dy)
{
  getRenderSystem().setShader(m_shaderBlur.data());
  setBlurParameters(dx, dy);
  getRenderSystem().setRenderTarget(out);
  getRenderSystem().setTexture(in, 0);
  getRenderSystem().drawScreenQuad();
}

void BloomComponent::blendTargets(RenderTarget *base, RenderTarget *blur)
{
  if(!setWidgetRenderTarget())
    return;

  getRenderSystem().setShader(m_shaderCombine.data());
  vec4 bloomParams(m_bloomIntensity, m_baseIntensity, m_bloomSaturation, m_baseSaturation);
  getRenderSystem().setConst(10, &bloomParams.X, 1.0f);
  getRenderSystem().setTexture(base, 1);
  getRenderSystem().setTexture(blur, 0);
  getRenderSystem().drawScreenQuad();
}

void BloomComponent::prepare()
{
  getRenderSystem().setSamplerState(RenderSystem::LinearClamp, 0);
  getRenderSystem().setRasterizerState(RenderSystem::CullNone);
  getRenderSystem().setBlendState(RenderSystem::Opaque);
  getRenderSystem().setAlphaTest(RenderSystem::AlphaTestDisabled);
  getRenderSystem().setDepthStencilState(RenderSystem::DepthNone);
  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setSamplerState(RenderSystem::LinearClamp, 0);
  getRenderSystem().setSamplerState(RenderSystem::LinearClamp, 1);
  getRenderSystem().setSamplerState(RenderSystem::LinearClamp, 2);
}
