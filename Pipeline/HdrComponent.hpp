#ifndef HDRCOMPONENT_HPP
#define HDRCOMPONENT_HPP

#include <QSharedPointer>
#include <QVector>

#include <Render/RenderComponent.hpp>
#include <Render/RenderShader.hpp>
#include <Render/RenderTarget.hpp>

#include "BloomComponent.hpp"

class PIPELINE_EXPORT HdrComponent : public BloomComponent
{
  Q_OBJECT

public:
  explicit HdrComponent(bool antiEdges = true);
  ~HdrComponent();

protected:
  void resizeEvent(const QSize& newSize);

public:
  void draw();

protected:
  virtual void lumScene(RenderTarget* scene, RenderTarget* out);
  virtual void lumDownsample(RenderTarget* lum, RenderTarget* out);
  virtual void lumAdapt(RenderTarget* lum, RenderTarget* oldLum, RenderTarget* out);
  virtual void extractBloom(RenderTarget* in, RenderTarget* lum, RenderTarget* out);
  virtual void blendTargets(RenderTarget* base, RenderTarget* blur, RenderTarget* lum);

private:
  QVector<QSharedPointer<RenderTarget> > m_renderLuminance;
  QScopedPointer<RenderTarget> m_renderAdaptLuminance[2];
  QScopedPointer<RenderShader> m_shaderLum, m_shaderLumDownsample, m_shaderLumAdapt;
  bool m_antiEdges;
};

#endif // HDRCOMPONENT_HPP
