#ifndef SSAOCOMPONENT_HPP
#define SSAOCOMPONENT_HPP

#include <Render/RenderComponent.hpp>

class RenderTarget;
class RenderShader;

class SSAOComponent : public RenderComponent
{
  Q_OBJECT

public:
  explicit SSAOComponent();
  ~SSAOComponent();

protected:
  void resizeEvent(const QSize &newSize);

public:
  void draw();

private:
  QScopedPointer<RenderTarget> m_mask;
  QScopedPointer<RenderShader> m_shader;
};

#endif // SSAOCOMPONENT_HPP
