#include "WireFrame.hpp"
#include <MaterialSystem/MaterialShaderCompiler.hpp>
#include <Render/RenderSystem.hpp>
#include <Render/RenderShader.hpp>

void compileSolidColor(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output);

WireFrame::WireFrame()
{
}

void WireFrame::compile(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output)
{
#if 0
  compiler.ps("outColor = %1;", compiler.getp(MaterialShaderCompiler::pColor));
#else
  compileSolidColor(compiler, output);
#endif
}

bool WireFrame::isWireframe() const
{
  return true;
}

Frame::FrameType WireFrame::type() const
{
  return Wire;
}

