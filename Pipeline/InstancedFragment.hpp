#ifndef INSTANCEDFRAGMENT_HPP
#define INSTANCEDFRAGMENT_HPP

#include "Fragment.hpp"

#include <Math/Matrix.hpp>
#include <Math/Sphere.hpp>
#include <Math/Box.hpp>
#include <QColor>

struct Camera;

struct InstanceInfo
{
public:
  matrix transform;
  QColor color;
  sphere sphereBounds;
  box boxBounds;

public:
  InstanceInfo() {
  }
};

class PIPELINE_EXPORT InstancedFragment : public Fragment
{
  enum BindState
  {
    NoBind,
    BindNonInstanced,
    BindInstanced
  };

public:
  InstancedFragment();
  ~InstancedFragment();

public:
  const box& boxBounds() const;
  void setBoxBounds(const box& bounds);

  const sphere& sphereBounds() const;
  void setSphereBounds(const sphere& bounds);

  const QVector<InstanceInfo>& instances() const;
  void setInstances(const QVector<InstanceInfo>& instances);

  bool addInstance(const InstanceInfo& instance);
  bool addInstance(const matrix& objectTransform, const QColor& objectColor, Camera* info = NULL);
  bool addInstance(const matrix& objectTransform, Camera* info = NULL);

public:
  virtual VertexBuffer::Types vertexType() const;
  virtual VertexBuffer::Types instanceType() const = 0;

	bool usesInstancing() const;

public:
  void render(const Light* lights);
private:
  void renderInstances(const Light* lights, const InstanceInfo* instances, unsigned instanceCount, BindState& bindState);

private:
  QVector<InstanceInfo> m_instances;
  box m_boxBounds;
  sphere m_sphereBounds;
};

#endif // INSTANCEDFRAGMENT_HPP
