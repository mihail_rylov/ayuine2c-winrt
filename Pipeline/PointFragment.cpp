#include "PointFragment.hpp"
#include <Render/PrimitiveDrawer.hpp>

PointFragment::PointFragment()
{
}

PointFragment::~PointFragment()
{
}

const QVector<PointVertex> & PointFragment::verts() const
{
  return m_verts;
}

void PointFragment::setVerts(const QVector<PointVertex> &verts)
{
  m_verts = verts;
}

void PointFragment::addVerts(const PointVertex &vert)
{
  m_verts.append(vert);
}

bool PointFragment::bind(const Light* lights)
{
  if(m_verts.isEmpty())
    return false;
  if(!TexturedFragment::bind(lights))
    return false;
  getRenderSystem().setVertexData(vertexType(), &m_verts[0], m_verts.size() * sizeof(PointVertex));
  return true;
}

void PointFragment::draw()
{
  getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::PointList, 0, m_verts.size()));
}

VertexBuffer::Types PointFragment::vertexType() const
{
    return VertexBuffer::Types(VertexBuffer::Vertex | VertexBuffer::Size | VertexBuffer::Color);
}

LineFragment::LineFragment() : m_drawMode(List)
{
}

LineFragment::~LineFragment()
{
}

LineFragment::DrawMode LineFragment::drawMode() const
{
    return m_drawMode;
}

void LineFragment::setDrawMode(LineFragment::DrawMode drawMode)
{
    m_drawMode = drawMode;
}

void LineFragment::draw()
{
    switch(m_drawMode)
    {
    case List:
        getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::LineList, 0, verts().size() / 2));
        break;

    case Strip:
        getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::LineStrip, 0, verts().size() - 1));
        break;
    }
}
