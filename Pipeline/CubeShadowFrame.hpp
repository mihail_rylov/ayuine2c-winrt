#ifndef CUBESHADOWFRAME_HPP
#define CUBESHADOWFRAME_HPP

#include "Pipeline.hpp"
#include "BaseShadowFrame.hpp"

class CubeRenderTarget;

class PIPELINE_EXPORT CubeShadowFrame : public BaseShadowFrame
{
public:
  Q_INVOKABLE CubeShadowFrame();
  ~CubeShadowFrame();

public:
  bool begin(const ShadowTextureRef& shadowTexture, CubeFace::Enum cubeFace);

public:
  QSharedPointer<BaseRenderTarget> allocRenderTarget(unsigned size);
  QSharedPointer<BaseRenderTarget> cleanRenderTarget();

private:
  QHash<unsigned, QSharedPointer<BaseRenderTarget> > depthTarget;
  QSharedPointer<CubeRenderTarget> cleanTarget;
};

#endif // CUBESHADOWFRAME_HPP
