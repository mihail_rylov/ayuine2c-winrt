#ifndef POINTFRAGMENT_HPP
#define POINTFRAGMENT_HPP

#include "TexturedFragment.hpp"

#include <Math/Vec3.hpp>
#include <QColor>

struct PointVertex
{
  vec3 origin;
  float size;
  unsigned color;

  PointVertex()
    : size(0.0f), color(0)
  {
  }
  PointVertex(const vec3& origin_, float size_ = 0.0f, unsigned color_ = ~0U)
    : origin(origin_), size(size_), color(color_)
  {
  }
  PointVertex(const vec3& origin_, float size_, QColor color_)
    : origin(origin_), size(size_), color(color_.rgba())
  {
  }
};

class PIPELINE_EXPORT PointFragment : public TexturedFragment
{
public:
  PointFragment();
  ~PointFragment();

public:
  const QVector<PointVertex>& verts() const;
  void setVerts(const QVector<PointVertex>& verts);
  void addVerts(const PointVertex& vert);

public:
  VertexBuffer::Types vertexType() const;
  bool bind(const Light* lights);
  void draw();

private:
  QVector<PointVertex> m_verts;
};

class PIPELINE_EXPORT LineFragment : public PointFragment
{
public:
    enum DrawMode
    {
        List,
        Strip
    };

public:
  LineFragment();
  ~LineFragment();

public:
  DrawMode drawMode() const;
  void setDrawMode(DrawMode drawMode);

public:
  void draw();

private:
  DrawMode m_drawMode;
};

#endif // POINTFRAGMENT_HPP
