#ifndef COLORPICKFRAME_HPP
#define COLORPICKFRAME_HPP

#include "SimpleFrame.hpp"

#include <Render/RenderTarget.hpp>

#include <QHash>
#include <QColor>

class PIPELINE_EXPORT ColorPickFrame : public SimpleFrame
{
  Q_OBJECT

public:
  Q_INVOKABLE ColorPickFrame();

public:
  void draw();
  FrameType type() const;

  void clear();
  QColor objectToColor(QObject* object);
  QObject* colorToObject(QColor color);

protected:
  void compile(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);

private:
  QHash<QObject*, QRgb> m_objectToColor;
  QVector<QObject*> m_colorToObject;
};

#endif // COLORPICKFRAME_HPP
