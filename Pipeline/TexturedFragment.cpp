#include "TexturedFragment.hpp"
#include "Frame.hpp"
#include <Core/Resource.hpp>
#include <Render/RenderTexture.hpp>
#include <Render/Texture.hpp>

TexturedFragment::TexturedFragment()
{
  m_fixedState = RenderSystem::FixedTexture;
  m_color = QColor(Qt::white);
  m_blendState = RenderSystem::Opaque;
  m_depthStencilState = RenderSystem::DepthDefault;
  m_rasterizerState = RenderSystem::CullClockwise;
  m_fillMode = RenderSystem::Wire;
  m_alphaTest = RenderSystem::AlphaTestDisabled;
  m_depthBias = true;
  m_transform = mat4Identity;
}

TexturedFragment::~TexturedFragment()
{
}

void TexturedFragment::setTexture(RenderTexture* texture)
{
  m_texture = texture;
}

void TexturedFragment::setTexture(const QSharedPointer<Texture> &texture)
{
  if(texture)
    setTexture(*texture);
  else
    setTexture(NULL);
}

void TexturedFragment::setTexture(const QByteArray& name)
{
  QSharedPointer<Texture> texture(Resource::load<Texture>(name));
  if(texture.isNull())
    m_texture = NULL;
  else
  {
    texture->setInternal(true);
    m_texture = *texture;
  }
}

void TexturedFragment::setColor(const QColor &color)
{
  m_color = color;
}

void TexturedFragment::setTransform(const matrix &transform)
{
  m_transform = transform;
}

void TexturedFragment::setFixedState(RenderSystem::FixedState state)
{
  m_fixedState = state;
}

void TexturedFragment::setBlendState(RenderSystem::BlendState state)
{
  m_blendState = state;
}

void TexturedFragment::setDepthStencilState(RenderSystem::DepthStencilState state)
{
  m_depthStencilState = state;
}

void TexturedFragment::setRasterizerState(RenderSystem::RasterizerState state)
{
  m_rasterizerState = state;
}

void TexturedFragment::setFillMode(RenderSystem::FillMode mode)
{
  m_fillMode = mode;
}

void TexturedFragment::setAlphaTest(RenderSystem::AlphaTest state)
{
  m_alphaTest = state;
}

void TexturedFragment::setDepthBias(bool bias)
{
  m_depthBias = bias;
}

ShaderState * TexturedFragment::materialShaderState() const
{
  return NULL;
}

bool TexturedFragment::prepare()
{
  return true;
}

bool TexturedFragment::bind(const Light* lights)
{
  getRenderSystem().setTransform(m_transform);
  getRenderSystem().setColor(m_color);
  getRenderSystem().setShader(m_fixedState);
  getRenderSystem().setRasterizerState(m_rasterizerState);
  getRenderSystem().setBlendState(m_blendState);
  getRenderSystem().setDepthStencilState(m_depthStencilState, m_depthBias);
  getRenderSystem().setFillMode(m_fillMode);
  getRenderSystem().setAlphaTest(m_alphaTest);
  getRenderSystem().setTexture(m_texture.data(), 0);
  getRenderSystem().setSamplerState(RenderSystem::LinearWrap, 0);
  return true;
}
