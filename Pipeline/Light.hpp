#ifndef LIGHT_HPP
#define LIGHT_HPP

#include "Pipeline.hpp"
#include <QVector>

#include <MaterialSystem/MaterialSystem.hpp>
#include <MaterialSystem/ShaderState.hpp>

class Frame;
class LightShader;
class ShaderState;

struct box;
struct sphere;
class RenderShader;

class PIPELINE_EXPORT Light
{
public:
  enum VisibilityState
  {
    Undefined,
    NotVisible,
    Visible
  };

public:
  Light();
  ~Light();

public:
  virtual bool test(const box& boxBounds, const sphere& sphereBounds) const = 0;

public:
  ShaderState& lightState() const;
  LightShader* lightShader() const;
  void setLightState(const ShaderState& lightState);

  MaterialSystem::Quality lightQuality() const;
  void setLightQuality(MaterialSystem::Quality quality);

public:
  virtual bool set(bool transparent) const = 0;
  virtual void draw(RenderShader* shader, Frame& frame) const;

public:
  Frame* frame() const;
  Light* next() const;
  bool unlink();
  VisibilityState visibilityState() const;

private:
  Frame* m_frame;
  Light* m_next;
protected:
  MaterialSystem::Quality m_quality;
  mutable ShaderState m_lightState;
  mutable VisibilityState m_visibilityState;
  friend class Frame;
};

#endif // LIGHT_HPP
