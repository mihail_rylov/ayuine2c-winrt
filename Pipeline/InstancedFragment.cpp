#include "InstancedFragment.hpp"
#include <Render/RenderSystem.hpp>
#include <Math/Camera.hpp>
#include <Math/Sphere.hpp>
#include <Math/Box.hpp>
#include "Light.hpp"

InstancedFragment::InstancedFragment()
{
}

InstancedFragment::~InstancedFragment()
{
}

const box & InstancedFragment::boxBounds() const
{
  return m_boxBounds;
}

void InstancedFragment::setBoxBounds(const box &bounds)
{
  m_boxBounds = bounds;
}

const sphere & InstancedFragment::sphereBounds() const
{
  return m_sphereBounds;
}

void InstancedFragment::setSphereBounds(const sphere &bounds)
{
  m_sphereBounds = bounds;
}

const QVector<InstanceInfo> & InstancedFragment::instances() const
{
  return m_instances;
}

void InstancedFragment::setInstances(const QVector<InstanceInfo> &instances)
{
  m_instances = instances;
}

bool InstancedFragment::addInstance(const InstanceInfo &instance)
{
  m_instances.append(instance);
  return true;
}

bool InstancedFragment::addInstance(const matrix& objectTransform, const QColor& objectColor, Camera* cameraInfo)
{
  box boxBounds = m_boxBounds.transform(objectTransform);

  if(cameraInfo && !cameraInfo->culler.test(boxBounds))
    return false;

  InstanceInfo info;
  info.boxBounds = boxBounds;
  info.sphereBounds = m_sphereBounds.transform(objectTransform);
  info.transform = objectTransform;
  info.color = objectColor;
  return addInstance(info);
}

bool InstancedFragment::addInstance(const matrix& objectTransform, Camera* cameraInfo)
{
  return addInstance(objectTransform, Qt::white, cameraInfo);
}

void InstancedFragment::render(const Light* lights)
{
  BindState bindState = NoBind;

  // check all instances that are affected by light
  if(lights)
  {
    const unsigned MaxInstances = 256;
    static InstanceInfo instanceList[MaxInstances];
    unsigned instanceCount = 0;

    foreach(InstanceInfo instance, m_instances)
    {
      if(lights->test(instance.boxBounds, instance.sphereBounds))
      {
        // flush instance list
        if(instanceCount == MaxInstances)
        {
          renderInstances(lights, instanceList, instanceCount, bindState);
          instanceCount = 0;
        }

        // add instance to list
        instanceList[instanceCount++] = instance;
      }
    }

    // finalize rendering
    if(instanceCount)
    {
      renderInstances(lights, instanceList, instanceCount, bindState);
    }
  }
  else
  {
    // render all instances
    renderInstances(lights, m_instances.constData(), m_instances.size(), bindState);
  }

  // clean rendering
  switch(bindState)
  {
  case BindInstanced:
    getRenderSystem().unbindInstanceData();
  case BindNonInstanced:
    unbind();
  case NoBind:
    break;
  }
}

void InstancedFragment::renderInstances(const Light* lights, const InstanceInfo *instances, unsigned instanceCount, BindState& bindState)
{
  // draw using instancing
  unsigned maxInstances = getRenderSystem().maxInstances();

  if(usesInstancing())
  {
    // loop through all instances rendering them in groups
    while(instanceCount)
    {
      unsigned count = qMin(instanceCount, maxInstances);

      if(!getRenderSystem().bindInstanceData(VertexBuffer::Instanced, instances, count, sizeof(InstanceInfo)))
        break;

      // bind data
      if(bindState != BindInstanced)
      {
        bind(lights);
        bindState = BindInstanced;
        getRenderSystem().setTransform(mat4Identity);
      }

      // draw all instances
      draw();

      instances += count;
      instanceCount -= count;
    }
  }

  if(instanceCount == 0)
    return;

  // bind data
  if(bindState != BindNonInstanced)
  {
    if(bindState == BindInstanced)
      getRenderSystem().unbindInstanceData();
    bind(lights);
    bindState = BindNonInstanced;
  }

  // render each instance individually
  while(instanceCount--)
  {
    getRenderSystem().setTransform(instances->transform);
    getRenderSystem().setColor(instances->color);
    draw();
    ++instances;
  }
}

VertexBuffer::Types InstancedFragment::vertexType() const
{
  VertexBuffer::Types vertexType = instanceType();

  if(usesInstancing())
  {
    vertexType |= VertexBuffer::Instanced;
  }
  else
  {
    vertexType &= ~VertexBuffer::Instanced;
  }

  return vertexType;
}

bool InstancedFragment::usesInstancing() const
{
  if(getRenderSystem().maxInstances() > 0 && m_instances.size() > 1)
		return true;
	return false;
}
