#ifndef SPHEREFRAGMENT_HPP
#define SPHEREFRAGMENT_HPP

#include "TexturedFragment.hpp"
#include <Math/Sphere.hpp>

class PIPELINE_EXPORT SphereFragment : public TexturedFragment
{
public:
  SphereFragment();
  ~SphereFragment();

public:
  const sphere& bounds() const;
  void setBounds(const sphere& bounds);

public:
  void draw();
  VertexBuffer::Types vertexType() const;

private:
  sphere m_bounds;
};

#endif // SPHEREFRAGMENT_HPP
