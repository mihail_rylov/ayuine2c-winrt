#include "Frame.hpp"
#include "Fragment.hpp"
#include "Fragment_p.hpp"
#include "InstancedFragment.hpp"
#include "Light.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/RenderTarget.hpp>
#include <Render/RenderShader.hpp>

#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/LightShader.hpp>

#include <QImage>

const unsigned MaxTransparentFrags = 16384;
const unsigned MaxOrderedFrags = 16384;

static Fragment* solidFrags[2][MaterialSystem::QualityCount];
static Fragment* alphaTestFrags[2][MaterialSystem::QualityCount];
static Fragment* transparentFrags[MaxTransparentFrags];
static unsigned transparentCount;
static Fragment* orderedFrags[16384];
static unsigned orderedCount;


inline bool renderOpaqueSorter(Fragment *a, Fragment *b)
{
  return a->internalDistance > b->internalDistance;
}

inline bool renderOrderedSorter(Fragment *a, Fragment *b)
{
  return a->order() < b->order();
}

inline Shader* sortMaterials(Fragment* fragment)
{
  static unsigned materialIndex = 0;

  Shader* materials = NULL;

  ++materialIndex;

	for( ; fragment; fragment = fragment->nextMaterial)
  {
    ShaderState* state = fragment->materialShaderState();
    if(!state)
      continue;

    Shader* shader = state->rawMaterialShader();
    if(!shader)
      continue;

    if(state->internalIndex != materialIndex)
    {
      if(shader->internalIndex != materialIndex)
      {
        shader->internalIndex = materialIndex;
        shader->internalNext = materials;
				shader->internalStates = NULL;
        materials = shader;
      }

      state->internalIndex = materialIndex;
      state->internalNext = shader->internalStates;
			state->internalFragments = NULL;
      shader->internalStates = state;
    }

    fragment->internalNext = state->internalFragments;
    state->internalFragments = fragment;
  }

  return materials;
}

Frame::Frame()
{
  m_fragments = NULL;
  m_lights = NULL;
  m_frameTime = 0;
  m_deltaTime = 0;
  m_debugView = NULL;
}

Frame::~Frame()
{
  clear();
}

Fragment * Frame::fragments() const
{
  return m_fragments;
}

bool Frame::addFragment(Fragment *fragment)
{
  if(fragment->m_frame)
    return false;

  fragment->m_frame = this;
  fragment->m_next = m_fragments;
  m_fragments = fragment;
  return false;
}

bool Frame::addFragment(QScopedPointer<Fragment>& fragment)
{
  if(addFragment(fragment.data()))
  {
    fragment.take();
    return true;
  }
  return false;
}

Light * Frame::lights() const
{
  return m_lights;
}

bool Frame::addLight(Light *light)
{
  if(light->m_frame)
    return false;
  light->m_frame = this;
  light->m_next = m_lights;
  m_lights = light;
  return true;
}

bool Frame::addLight(QScopedPointer<Light>& light)
{
  if(addLight(light.data()))
  {
    light.take();
    return true;
  }
  return false;
}

float Frame::frameTime() const
{
  return m_frameTime;
}

void Frame::setFrameTime(float frameTime)
{
  m_frameTime = frameTime;
}

float Frame::deltaTime() const
{
  return m_deltaTime;
}

void Frame::setDeltaTime(float deltaTime)
{
  m_deltaTime = deltaTime;
}

const Camera & Frame::camera() const
{
  return m_camera;
}

void Frame::setCamera(const Camera &camera)
{
  m_camera = camera;
}

void Frame::addIconFragment(const vec3 &iconOrigin, float iconSize, const QColor &iconColor, const QByteArray &iconTexture, bool selected)
{
}

void Frame::addBoxFragment(const box &bbox, const QColor &fillColor, const QColor &outlineColor, bool selected)
{
}

void Frame::addSheetFragment(const vec3 &origin, Axis::Enum axis, const vec2 &size, const QColor &fillColor, const QColor &outlineColor, bool selected)
{
}

void Frame::clear()
{
  for(Fragment* fragment = m_fragments; fragment; )
  {
    Fragment* next = fragment->m_next;
    delete fragment;
    fragment = next;
  }
  m_fragments = NULL;

  for(Light* light = m_lights; light; )
  {
    Light* next = light->m_next;
    delete light;
    light = next;
  }
  m_lights = NULL;
}

void Frame::prepareFrame(bool useTranslucent)
{
  // Wyzeruj ramk�
  for(int i = 0; i < MaterialSystem::QualityCount; ++i)
  {
    solidFrags[0][i] = NULL;
    solidFrags[1][i] = NULL;
    alphaTestFrags[0][i] = NULL;
    alphaTestFrags[1][i] = NULL;
  }
  transparentCount = 0;
  orderedCount = 0;

  vec4 cameraPlane(camera().at, camera().origin);

  // Posortuj wszystkie fragmenty wed�ug materia�u
  for(Fragment* fragment = fragments(); fragment; fragment = fragment->next())
  {
    if(fragment->order() || !fragment->rawMaterialShader())
    {
      if(orderedCount < MaxOrderedFrags)
        orderedFrags[orderedCount++] = fragment;
      continue;
    }

    switch(fragment->opaque())
    {
    default:
    case Material::Opaque:
    {
      MaterialSystem::Quality quality = fragment->materialQuality();
      bool usesInstancing = fragment->usesInstancing();
      fragment->nextMaterial = solidFrags[usesInstancing][quality];
      solidFrags[usesInstancing][quality] = fragment;
    }
    break;

    case Material::Sort:
      if(useTranslucent)
      {
        if(transparentCount < MaxTransparentFrags)
        {
          fragment->internalDistance = fragment->distance(camera().origin, cameraPlane);
          transparentFrags[transparentCount++] = fragment;
        }
        break;
      }

    case Material::AlphaTest:
    {
      MaterialSystem::Quality quality = fragment->materialQuality();
      bool usesInstancing = fragment->usesInstancing();
      fragment->nextMaterial = alphaTestFrags[usesInstancing][quality];
      alphaTestFrags[usesInstancing][quality] = fragment;
    }
    break;
    }
  }

  qSort(transparentFrags, transparentFrags + transparentCount, renderOpaqueSorter);
  qSort(orderedFrags, orderedFrags + orderedCount, renderOrderedSorter);
}

void Frame::renderSolidFrags(const Light* light, bool useAlphaTest)
{
  getRenderSystem().setDepthStencilState(RenderSystem::DepthDefault);

  getRenderSystem().setAlphaTest(RenderSystem::AlphaTestDisabled);

  for(int i = MaterialSystem::QualityCount; i-- > 0; )
  {
    renderSortedFrags(solidFrags[1][i], light, (MaterialSystem::Quality)i, VertexBuffer::Instanced);

    renderSortedFrags(solidFrags[0][i], light, (MaterialSystem::Quality)i, VertexBuffer::Vertex);

    if(alphaTestFrags[0][i] || alphaTestFrags[1][i])
    {
      if(useAlphaTest)
        getRenderSystem().setAlphaTest(RenderSystem::AlphaTestEnabled);

      renderSortedFrags(alphaTestFrags[1][i], light, (MaterialSystem::Quality)i, VertexBuffer::Instanced);

      renderSortedFrags(alphaTestFrags[0][i], light, (MaterialSystem::Quality)i, VertexBuffer::Vertex);

      if(useAlphaTest)
        getRenderSystem().setAlphaTest(RenderSystem::AlphaTestDisabled);
    }
  }
}

void Frame::renderSortedFrags(Fragment* frags, const Light* light, MaterialSystem::Quality quality, VertexBuffer::Types extraType)
{
  if(light)
  {
    quality = (MaterialSystem::Quality)qMax(quality, light->lightQuality());
  }

  for(Shader* shader = sortMaterials(frags); shader; shader = shader->internalNext)
  {
    if(light)
    {
      if(!bindLightShader(qobject_cast<GeneralShader*>(shader), light->lightShader(), quality, shader->vertexType() | extraType))
        continue;
    }
    else
    {
      if(!bindShader(qobject_cast<GeneralShader*>(shader), quality, shader->vertexType() | extraType))
        continue;
    }

    for(ShaderState* state = shader->internalStates; state; state = state->internalNext)
    {
      if(!state->setConsts())
        continue;

      for(Fragment* fragment = state->internalFragments; fragment; fragment = fragment->internalNext)
      {
        fragment->render(light);
      }
    }
  }
}

void Frame::renderOrderedFrags(OrderedRender render)
{
  switch(render)
  {
  case OrderNegative:
    for(unsigned i = 0; i < orderedCount; ++i)
    {
      if(orderedFrags[i]->order() >= 0)
        break;

      Fragment* fragment = orderedFrags[i];

      if(bindShader(fragment->materialShader(), fragment->materialQuality(), fragment->vertexType()))
      {
        fragment->materialShaderState()->setConsts();
      }

      fragment->render(NULL);
    }
    break;

  case OrderPositive:
    for(unsigned i = 0; i < orderedCount; ++i)
    {
      if(orderedFrags[i]->order() < 0)
        continue;

      Fragment* fragment = orderedFrags[i];

      if(bindShader(fragment->materialShader(), fragment->materialQuality(), fragment->vertexType()))
      {
        fragment->materialShaderState()->setConsts();
      }

      fragment->render(NULL);
    }
    break;

  case OrderAll:
    for(unsigned i = 0; i < orderedCount; ++i)
    {
      Fragment* fragment = orderedFrags[i];

      if(bindShader(fragment->materialShader(), fragment->materialQuality(), fragment->vertexType()))
      {
        fragment->materialShaderState()->setConsts();
      }

      fragment->render(NULL);
    }
    break;
  }
}

void Frame::renderTransparentFrags(bool lights)
{
  getRenderSystem().setDepthStencilState(RenderSystem::DepthDefault);
  getRenderSystem().setAlphaTest(RenderSystem::AlphaTestEnabled);

  for(unsigned i = 0; i < transparentCount; ++i)
  {
    if(lights)
    {
      for(Light* light = m_lights; light; light = light->next())
      {
        if(light->set(true))
        {
          if(bindLightShader(transparentFrags[i]->materialShader(), light->lightShader(), (MaterialSystem::Quality)qMax(transparentFrags[i]->materialQuality(), light->lightQuality()), transparentFrags[i]->vertexType()))
          {
            if(transparentFrags[i]->materialShaderState()->setConsts())
            {
              transparentFrags[i]->render(NULL);
            }
          }
        }
      }
    }
    else
    {
      getRenderSystem().setBlendState(RenderSystem::NonPremultipled);

      if(bindShader(transparentFrags[i]->materialShader(), transparentFrags[i]->materialQuality(), transparentFrags[i]->vertexType()))
      {
        if(transparentFrags[i]->materialShaderState()->setConsts())
        {
          transparentFrags[i]->render(NULL);
        }
      }
    }
  }

  getRenderSystem().setAlphaTest(RenderSystem::AlphaTestDisabled);
}

bool Frame::isWireframe() const
{
  return false;
}

const DebugView* Frame::debugView() const
{
  return m_debugView;
}

void Frame::setDebugView(const DebugView* debugView)
{
  m_debugView = debugView;
}

bool Frame::hasLighting() const
{
  return false;
}

unsigned Frame::vertexToIndex(VertexBuffer::Types vertexType, MaterialSystem::Quality quality)
{
  return (type() * VertexBuffer::Max + VertexBuffer::decodeType(vertexType)) * MaterialSystem::QualityCount + quality;
}

QString Frame::shaderFileName(QObject* objA, QObject* objB, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  QString cacheDir = getSettings().value("MaterialSystem/CacheDir").toString();
  if(cacheDir.isEmpty())
    return QString();

  const char* typeText = Frame::staticMetaObject.enumerator(Frame::staticMetaObject.indexOfEnumerator("FrameType")).valueToKey(type());
  const char* qualityText = MaterialSystem::staticMetaObject.enumerator(MaterialSystem::staticMetaObject.indexOfEnumerator("Quality")).valueToKey(quality);
  QString fileName = typeText;
  if(objA)
      fileName.append("_" + objA->objectName());
  if(objB)
      fileName.append("_" + objB->objectName());
  fileName.append(QString("_") + qualityText);
  if(vertexType & VertexBuffer::Instanced)
      fileName.append("_Instanced");
  fileName.append(".fx");
  return Resource::getResourceDir().absoluteFilePath(cacheDir + "/" + fileName);
}

QSharedPointer<RenderShader> Frame::readShader(QObject* objA, QObject* objB, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  if(!getSettings().value("MaterialSystem/ReadFromCache", false).toBool())
    return QSharedPointer<RenderShader>();

  QString fileName = shaderFileName(objA, objB, quality, vertexType);
  if(fileName.isEmpty())
    return QSharedPointer<RenderShader>();

  if(QFile::exists(fileName))
    return QSharedPointer<RenderShader>(new RenderShader(RenderShader::File, fileName));

  return QSharedPointer<RenderShader>();
}

void Frame::writeShader(QString shaderCode, QObject* objA, QObject* objB, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  if(!getSettings().value("MaterialSystem/WriteToCache", true).toBool())
    return;

  QString fileName = shaderFileName(objA, objB, quality, vertexType);
  if(fileName.isEmpty())
    return;

  QFile shaderFile(fileName);
  if(shaderFile.open(QFile::WriteOnly))
  {
    shaderFile.write(shaderCode.toAscii());
    qDebug() << "[FRAME] Wrote shader to " << fileName;
  }
}

bool Frame::isSelected(QObject *object, bool def) const
{
  if(m_debugView && object)
    return m_debugView->isSelected(object);
  return def;
}

bool Frame::isHidden(QObject *object, bool def) const
{
  if(m_debugView && object)
    return m_debugView->isHidden(object);
  return def;
}

const QRect & Frame::viewport() const
{
  return m_viewport;
}

void Frame::setViewport(const QRect &viewport)
{
  m_viewport = viewport;
}

QImage Frame::toImage(const QRect &area)
{
  if(m_viewport.isEmpty() || area.isEmpty())
    return QImage();

  // render scene to buffer
  if(getRenderSystem().beginScene())
  {
    // create render target
    RenderTarget renderTarget(m_viewport.width(), m_viewport.height(), RenderTarget::Rgba);
    renderTarget.setDepthFormat(RenderTarget::Depth24Stencil8);

    // bind render target
    if(begin())
    {
      getRenderSystem().setRenderTarget(&renderTarget);
      getRenderSystem().clear(Qt::black);

      if(m_viewport != area)
      {
        QRect areaEnlarged = area.adjusted(-1, -1, 1, 1);
        getRenderSystem().setScissor(&areaEnlarged);
      }

      draw();

      getRenderSystem().setScissor(NULL);
    }
    getRenderSystem().endScene();

    return renderTarget.toImage();
  }

  return QImage();
}

QImage Frame::toImage()
{
  return toImage(QRect(QPoint(), m_viewport.size()));
}

int Frame::beginOrder()
{
  return -10;
}

int Frame::drawOrder()
{
  return 10;
}

bool Frame::bindShader(GeneralShader *materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  return false;
}

bool Frame::bindLightShader(GeneralShader *materialShader, LightShader *lightShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  return false;
}

int Frame::fragmentsCount(float* instancedRatio) const
{
  unsigned instanceCount = 0, instancedCount = 0;
  int count = 0;
  for(Fragment* fragment = fragments(); fragment; fragment = fragment->next())
  {
    ++count;

    if(InstancedFragment* instanced = dynamic_cast<InstancedFragment*>(fragment))
    {
      ++instancedCount;
      instanceCount += instanced->instances().size();
    }
  }
  if(instancedRatio)
    *instancedRatio = instancedCount ? (float)instanceCount / instancedCount : 0.0f;
  return count;
}

int Frame::lightsCount(int *visibleCount) const
{
  int lightCount = 0, visibleLights = 0;

  for(Light* light = lights(); light; light = light->next())
  {
    ++lightCount;
    visibleLights += light->visibilityState() == Light::Visible;
  }

  if(visibleCount)
    *visibleCount = visibleLights;
  return lightCount;
}

QStringList Frame::toStringList() const
{
  QStringList lines;
  int fragments = 0, lights = 0, visibleLights = 0;
  float instancedRatio = 0.0f;

  lights = lightsCount(&visibleLights);
  fragments = fragmentsCount(&instancedRatio);

  lines.append(QString("Fragments: %1/%2").arg(QString::number(fragments), QString::number(instancedRatio, 'g', 3)));
  lines.append(QString("Lights: %1 of %2").arg(visibleLights).arg(lights));

  return lines;
}
