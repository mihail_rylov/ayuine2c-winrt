#include "BoxFragment.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/VertexBuffer.hpp>
#include <Render/PrimitiveDrawer.hpp>

BoxFragment::BoxFragment()
{
}

BoxFragment::~BoxFragment()
{
}

const box & BoxFragment::bounds() const
{
  return m_bounds;
}

void BoxFragment::setBounds(const box &bounds)
{
  m_bounds = bounds;
}

void BoxFragment::draw()
{
  if(m_fillMode == RenderSystem::Wire)
    getRenderSystem().drawLineBox(m_bounds, m_transform);
  else
    getRenderSystem().drawBox(m_bounds, m_transform);
}

VertexBuffer::Types BoxFragment::vertexType() const
{
  return VertexBuffer::Vertex;
}
