#ifndef WIREFRAME_HPP
#define WIREFRAME_HPP

#include "SimpleFrame.hpp"

class PIPELINE_EXPORT WireFrame : public SimpleFrame
{
  Q_OBJECT

public:
  Q_INVOKABLE WireFrame();

public:
  bool isWireframe() const;

public:
  FrameType type() const;

protected:
  void compile(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);
};

#endif // WIREFRAME_HPP
