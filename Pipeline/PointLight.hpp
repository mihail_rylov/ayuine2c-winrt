#ifndef POINTLIGHT_HPP
#define POINTLIGHT_HPP

#include "Pipeline.hpp"
#include "Light.hpp"
#include <MaterialSystem/ShaderState.hpp>
#include <Math/Vec3.hpp>

class Frame;

class PIPELINE_EXPORT PointLight : public Light
{
public:
  PointLight();

public:
  bool set(bool transparent) const;

  const vec3& origin() const;
  void setOrigin(const vec3& origin);

  float radius() const;
  void setRadius(float radius);

  void draw(RenderShader* shader, Frame& frame) const;

public:
  bool test(const box& boxBounds, const sphere& sphereBounds) const;

private:
  vec3 m_origin;
  float m_radius;
};

#endif // POINTLIGHT_HPP
