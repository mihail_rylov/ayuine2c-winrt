#include "SSAOComponent.hpp"

#include <Render/RenderTarget.hpp>
#include <Render/RenderShader.hpp>

SSAOComponent::SSAOComponent()
{
  // m_shader.reset(new Shader(Shader::File, ":/Shaders/SSAO.fx"));
}

SSAOComponent::~SSAOComponent()
{
}

void SSAOComponent::resizeEvent(const QSize &newSize)
{
  m_mask.reset(new RenderTarget(newSize.width() / 2, newSize.height() / 2, RenderTarget::Rgb));
}

void SSAOComponent::draw()
{
}
