#include "ColorPickFrame.hpp"

#include <MaterialSystem/MaterialShaderCompiler.hpp>

#include <Render/RenderSystem.hpp>

ColorPickFrame::ColorPickFrame()
{
}

Frame::FrameType ColorPickFrame::type() const
{
  return ColorPick;
}

void ColorPickFrame::compile(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output)
{
  compiler.ps("outColor.rgb = %1;", compiler.getp(MaterialShaderCompiler::pObjectColor));
  compiler.ps("outColor.a = 1.0f;");

  if(output)
  {
    if(ShaderBlock::InputLink alphaLink = output->input("Alpha"))
      compiler.ps("outColor.a = %1 > 0.5f;", alphaLink.varName(compiler));
    else if(ShaderBlock::InputLink alphaLink = output->input("Opacity"))
      compiler.ps("outColor.a = %1 > 0.5f;", alphaLink.varName(compiler));
  }
}

void ColorPickFrame::draw()
{
  prepareFrame(false);

  getRenderSystem().setCamera(camera());
  getRenderSystem().setFrame(deltaTime(), frameTime());

  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setBlendState(RenderSystem::Opaque);

  renderSolidFrags(NULL);

  renderOrderedFrags(OrderPositive);
}

void ColorPickFrame::clear()
{
  SimpleFrame::clear();
  m_objectToColor.clear();
  m_colorToObject.clear();
}

QColor ColorPickFrame::objectToColor(QObject *object)
{  
  if(m_objectToColor.contains(object))
    return m_objectToColor[object];

  unsigned index = m_colorToObject.size();
  QRgb newColor = QColor::fromRgb(index+1).rgb();
  m_objectToColor[object] = newColor;
  m_colorToObject.append(object);
  return QColor::fromRgb(newColor);
}

QObject * ColorPickFrame::colorToObject(QColor color)
{
  QRgb rgb = color.rgb() & 0xFFFFFF;
  if(rgb == 0 || rgb > m_colorToObject.size())
    return NULL;
  return m_colorToObject[rgb-1];
}
