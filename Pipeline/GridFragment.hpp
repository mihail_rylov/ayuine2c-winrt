#ifndef GRIDFRAGMENT_HPP
#define GRIDFRAGMENT_HPP

#include "Fragment.hpp"

#include <Math/MathLib.hpp>
#include <Math/Vec3.hpp>
#include <QColor>
#include <QRectF>

class PIPELINE_EXPORT GridFragment : public Fragment
{
public:
  GridFragment();
  ~GridFragment();

public:
  ShaderState *materialShaderState() const;

public:
  bool bind(const Light* lights);
  void draw();

  VertexBuffer::Types vertexType() const;

public:
  bool showText() const;
  void setShowText(bool showText);

  Axis::Enum axis() const;
  void setAxis(Axis::Enum axis);

  float size() const;
  void setSize(float size);

  unsigned blockSize() const;
  void setBlockSize(unsigned blockSize);

  const vec3& offset() const;
  void setOffset(const vec3& offset);

  QColor color() const;
  void setColor(const QColor &color);

  QColor blockColor() const;
  void setBlockColor(const QColor& blockColor);

  QColor axisColor() const;
  void setAxisColor(const QColor& axisColor);

  QColor textColor() const;
  void setTextColor(const QColor& textColor);

  QRectF bounds() const;
  void setBounds(const QRectF& bounds);

private:
  bool m_showText;
  Axis::Enum m_axis;
  float m_size;
  unsigned m_blockSize;
  vec3 m_offset;
  QColor m_color, m_blockColor, m_axisColor, m_textColor;
  QRectF m_bounds;
};

#endif // GRIDFRAGMENT_HPP
