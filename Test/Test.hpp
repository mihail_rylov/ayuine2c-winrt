//------------------------------------//
//
// Test.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Test
// Date: 2011-05-03
//
//------------------------------------//

#pragma once

class Foo {
	TOLUA_STRUCT;

public:
	Foo() {
		logf("Foo:ctor[%08x]", this);
	}
	~Foo() {
		logf("Foo:dtor[%08x]", this);
	}

public:
	virtual void setValue(vec3 v) {
		m_value = v;
	}
	vec3 value() {
		return m_value;
	}

#if 0
	const intrusive_ptr<Foo>& foo() const {
		return m_foo;
	}
	void setFoo(const intrusive_ptr<Foo>& foo) {
		m_foo = foo;
	}

	intrusive_ptr<Foo> newFoo() {
		return new Foo();
	}
#endif

private:
	vec3 m_value;
	// intrusive_ptr<Foo> m_foo;
};
