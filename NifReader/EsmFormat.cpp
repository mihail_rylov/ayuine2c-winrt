#include "EsmFormat.hpp"

#include <Core/qtiocompressor/zlib.h>

bool EsmType::operator == (const char* name) const
{
  return data == *(quint32*)name;
}
bool EsmType::operator != (const char* name) const
{
  return data != *(quint32*)name;
}

QDebug operator << (QDebug dbg, const EsmType& type)
{
  union
  {
    char string[5];
    ulong data;
  } tmp;

  tmp.data = type.data;
  tmp.string[4] = 0;

  return dbg << tmp.string;
}

bool EsmFormId::operator < (const EsmFormId other) const
{
  return data < other.data;
}

bool EsmFormId::operator == (const EsmFormId other) const
{
  return data == other.data;
}

QDebug operator << (QDebug dbg, const EsmFormId formId)
{
  return dbg << QString::number(formId.data, 16);
}

const EsmGroup* EsmGroup::next() const
{
  return (const EsmGroup*)((const char*)this + groupSize);
}

const EsmRecord* EsmGroup::firstRecord() const
{
  return (const EsmRecord*)(this + 1);
}

const EsmRecord * EsmGroup::lastRecord() const
{
  return (const EsmRecord*)next();
}

QDebug operator << (QDebug dbg, const EsmGroup& g)
{
  return dbg << "[GROUP] Name:" << g.type << g.name() << " Size:" << g.groupSize << " Type:" << g.groupType;
}

static const EsmRecord* tempRecord;
static QByteArray tempRecordData;

struct EsmDataCompressed
{
  uint decompSize;
  uchar compData[1];
};

const char* EsmRecord::data() const
{
  if(flags & Compressed)
  {
    if(tempRecord != this)
    {
      const uint* decompSize = (const uint*)(this + 1);

      tempRecordData.clear();
      tempRecordData.resize(*decompSize);

      uLongf destLen = *decompSize;

      if(uncompress((Bytef*)tempRecordData.data(), &destLen, (const Bytef*)(decompSize + 1), dataSize - 4) != Z_OK ||
        destLen != *decompSize)
      {
        qDebug() << *this << ": Invalid compressed data";
        return NULL;
      }

      tempRecord = this;
    }
    return tempRecordData.constData();
  }
  else
  {
    return (char*)(this + 1);
  }
}

const EsmSubRecord* EsmRecord::firstSubRecord() const
{
  return (const EsmSubRecord*)data();
}

const EsmSubRecord * EsmRecord::lastSubRecord() const
{
  if(flags & Compressed)
  {
    uint* decompSize = (uint*)(this + 1);
    return (const EsmSubRecord*)(data() + *decompSize);
  }
  else
    return (const EsmSubRecord*)(data() + dataSize);
}

const EsmRecord* EsmRecord::next() const
{
  if(const EsmGroup* grp = group())
    return grp->lastRecord();
  else
    return (const EsmRecord*)((const uchar*)(this + 1) + dataSize);
}

QDebug operator << (QDebug dbg, const EsmRecord& r)
{
  return dbg << "[RECORD] Type:" << r.type << " DataSize:" << r.dataSize << " Flags:" << r.flags << " FormId:" << r.formId;
}

unsigned EsmSubRecord::realDataSize() const
{
  if(dataSize == 0)
    return *(uint*)(&dataSize + 1);
  return dataSize;
}

const char* EsmSubRecord::data() const
{
  if(dataSize == 0)
    return (const char*)(this + 1) + sizeof(uint);
  return (const char*)(this + 1);
}

const EsmSubRecord* EsmSubRecord::next() const
{
  return (const EsmSubRecord*)(data() + realDataSize());
}

const EsmSubRecord* EsmSubRecord::next(EsmType type) const
{
  const EsmSubRecord* nextRecord = next();
  if(nextRecord && nextRecord->type == type)
    return nextRecord;
  return NULL;
}

QDebug operator << (QDebug dbg, const EsmSubRecord& s)
{
  return dbg << "[SUBRECORD] Type:" << s.type << " Size:" << s.dataSize;
}

const EsmGroup* EsmRecord::group() const
{
  if(type == "GRUP")
  {
    return (const EsmGroup*)this;
  }
  return NULL;
}

const EsmSubRecord * EsmRecord::findRecord(EsmType type, const EsmSubRecord *after) const
{
  for(const EsmSubRecord* record = firstSubRecord(), *last = lastSubRecord(); record < last; record = record->next())
  {
    if(record <= after)
      continue;
    if(record->type == type)
      return record;
  }
  return NULL;
}

EsmFile::EsmFile(const void *data, qint64 size_)
{
  header = (const EsmRecord*)data;
  size = size_;
}

EsmFile::EsmFile(const QString& fileName)
  : file(fileName), header(NULL), size(0)
{
  if(file.open(QFile::ReadOnly))
  {
    size = file.size();
    header = (const EsmRecord*)file.map(0, size);
    theFileName = fileName;
  }
}

EsmFile::EsmFile(QFile& file)
  : header(NULL), size(0)
{
  if(file.isOpen() || file.open(QFile::ReadOnly))
  {
    size = file.size();
    header = (const EsmRecord*)file.map(0, size);
    theFileName = file.objectName();
  }
}

EsmFile::~EsmFile()
{
}

bool EsmFile::isValid() const
{
  if(size < sizeof(EsmRecord))
    return false;
  if(header && header->type == "TES4")
    return true;
  return false;
}

const EsmGroup * EsmFile::firstGroup() const
{
  if(!isValid())
    return NULL;
  return (const EsmGroup*)header->next();
}

const EsmGroup * EsmFile::lastGroup() const
{
  if(!isValid())
    return NULL;
  return (const EsmGroup*)((const char*)header + size);
}

const EsmGroup * EsmFile::findGroup(EsmType type) const
{
  for(const EsmGroup* group = firstGroup(), *last = lastGroup(); group < last; group = group->next())
  {
    if(group->type == "GRUP" && group->groupType == EsmGroup::Top)
    {
      if(group->label == type)
        return group;
    }
  }
  return NULL;
}

static void processGroup(const EsmGroup* group, QHash<EsmFormId, const EsmRecord *>& records, EsmType groupType)
{
  for(const EsmRecord* record = group->firstRecord(), *last = group->lastRecord(); record < last; record = record->next())
  {
    if(const EsmGroup* recordGroup = record->group())
    {
      switch(recordGroup->groupType)
      {
      case EsmGroup::WorldChildren:
      case EsmGroup::CellChildren:
      case EsmGroup::CellPersistentChildren:
      case EsmGroup::CellTemporaryChildren:
      case EsmGroup::CellVisibleDistantChildren:
      case EsmGroup::TopicChildren:
        continue;
      }

      processGroup(recordGroup, records, groupType);
    }
    else
    {
      if(record->type != groupType)
      {
        qDebug() << "[PROCESS] Invalid GroupType found " << record->type << " expected " << groupType;
        continue;
      }

      if(!record->formId)
      {
        qDebug() << "[PROCESS] Ignore Null FormId";
        continue;
      }

      if(records.contains(record->formId))
      {
        qDebug() << "[PROCESS] Record with " << record->formId << " already exists";
        continue;
      }

      records[record->formId] = record;
    }
  }
}

QHash<EsmFormId, const EsmRecord *> EsmFile::enumRecords(EsmType type) const
{
  if(const EsmGroup* group = findGroup(type))
  {
    QHash<EsmFormId, const EsmRecord *> records;
    processGroup(group, records, type);
    return records;
  }
  return QHash<EsmFormId, const EsmRecord*>();
}

QByteArray EsmSubRecord::toByteArray() const
{
  if(!this)
    return QByteArray();

  const char* ptr = data();
  unsigned ptrSize = realDataSize();
  if(ptrSize > 0 && ptr[ptrSize-1] == 0)
    --ptrSize;
  return QByteArray(ptr, ptrSize);
}

QByteArray EsmRecord::toByteArray(EsmType type) const
{
  return findRecord(type)->toByteArray();
}

bool EsmCell::isValid() const
{
  return type == "CELL";
}

const EsmGroup * EsmCell::children() const
{
  if(isValid())
  {
    if(const EsmGroup* group = next()->group())
    {
      if(group->groupType == EsmGroup::CellChildren)
      {
        return group;
      }
    }
  }
  return NULL;
}

bool EsmWorld::isValid() const
{
  return type == "WRLD";
}

const EsmGroup * EsmWorld::children() const
{
  if(isValid())
  {
    if(const EsmGroup* group = next()->group())
    {
      if(group->groupType == EsmGroup::WorldChildren)
      {
        return group;
      }
    }
  }
  return NULL;
}

bool EsmRefr::isValid() const
{
  return type == "REFR";
}

bool EsmRefr::isMarker() const
{
  return findRecord("XMRK") != NULL;
}

float EsmRefr::scale() const
{
  return findRecord("XSCL")->as<float>(1.0f);
}

EsmFormId EsmRefr::refrFormId() const
{
  return findRecord("NAME")->as<EsmFormId>();
}

EsmRefr::MarkerType EsmRefr::markerType() const
{
  return (MarkerType)findRecord("TNAM")->as<MarkerType>(NotFound);
}

struct Coords
{
  float xyz[3];
  float rxyz[3];
};

vec3 EsmRefr::origin() const
{
  Coords coords = findRecord("DATA")->as<Coords>();
  return vec3(coords.xyz) / 64.0f;
}

euler EsmRefr::angles() const
{
  Coords coords = findRecord("DATA")->as<Coords>();
  return -euler(coords.rxyz[2], coords.rxyz[1], coords.rxyz[0]);
}

const EsmRecord* EsmFile::findRecord(EsmType type, EsmFormId formId)
{
  if(!records.contains(type))
    records[type] = enumRecords(type);

  if(!records[type].contains(formId))
    return NULL;
  return records[type][formId];
}

QByteArray EsmRecord::itemName() const
{
  if(const EsmSubRecord* full = findRecord("FULL"))
    return full->toByteArray();
  if(const EsmSubRecord* edid = findRecord("EDID"))
    return edid->toByteArray();
  return QByteArray();
}

struct LightParamsV1
{
  unsigned time;
  unsigned radius;
  QRgb color;
  unsigned flags;
  float value;
  float weight;

  LightParamsV1()
  {
    time = -1;
    radius = 16;
    color = ~0;
    flags = 0;
    value = 0;
    weight = 0;
  }
};

struct LightParams
{
  unsigned time;
  unsigned radius;
  QRgb color;
  unsigned flags;
  float falloff;
  float fov;
  float value;
  float weight;

  LightParams()
  {
    time = -1;
    radius = 16;
    color = ~0;
    flags = 0;
    falloff = 1;
    fov = 90;
    value = 0;
    weight = 0;
  }
  LightParams(const LightParamsV1& v1)
  {
    time = v1.time;
    radius = v1.radius;
    color = v1.color;
    flags = v1.flags;
    falloff = 1;
    fov = 90;
    value = v1.value;
    weight = v1.weight;
  }
};

inline LightParams lightParams(const EsmRecord* record)
{
  if(const EsmSubRecord* lightParams = record->findRecord("DATA"))
  {
    if(lightParams->realDataSize() == sizeof(LightParams))
    {
      return lightParams->as<LightParams>();
    }
    if(lightParams->realDataSize() == sizeof(LightParamsV1))
    {
      return lightParams->as<LightParamsV1>();
    }
  }
  return LightParams();
}

long EsmLight::time() const
{
  return lightParams(this).time;
}

float EsmLight::radius() const
{
  return 2.0f * lightParams(this).radius / 64.0f;
}

QRgb EsmLight::color() const
{
  return lightParams(this).color;
}

EsmLight::LightFlags EsmLight::flags() const
{
  return (LightFlags)lightParams(this).flags;
}

float EsmLight::falloff() const
{
  return lightParams(this).falloff;
}

float EsmLight::fov() const
{
  return lightParams(this).fov;
}

float EsmLight::value() const
{
  return lightParams(this).value;
}

float EsmLight::weight() const
{
  return lightParams(this).weight;
}

QByteArray EsmStat::modelName() const
{
  return findRecord("MODL")->toByteArray();
}

QString EsmFile::fileName() const
{
  return theFileName;
}

box EsmMisc::bounds() const
{
  return findRecord("OBND")->as<box>(bboxEmpty);
}

struct MiscData
{
  unsigned value;
  float weight;

  MiscData()
  {
    value = 0;
    weight = 0;
  }
};

float EsmMisc::weight() const
{
  MiscData data = findRecord("DATA")->as<MiscData>();

  if(qFuzzyIsNull(data.weight))
  {
    box boxBounds = bounds();
    if(boxBounds.empty())
      return 0;
    vec3 size = boxBounds.size();
    return size.X * size.Y * size.Z * 0.01f;
  }

  return data.weight;
}

const QString EsmGroup::name() const
{
  switch(groupType)
  {
  case EsmGroup::Top:
    return QString().sprintf("Top '%4s'", label.data);

  case EsmGroup::WorldChildren:
    return QString().sprintf("WorldChildren 'FormId:%08x'", label.data);

  case EsmGroup::InteriorCellBlock:
    return QString().sprintf("InteriorCellBlock '%i'", (int)label.data);

  case EsmGroup::InteriorCellSubBlock:
    return QString().sprintf("InteriorCellSubBlock '%i'", (int)label.data);

  case EsmGroup::ExteriorCellBlock:
    return QString().sprintf("ExteriorCellBlock '%ix%i'", short(label.data >> 16), short(label.data & 0xFFFF));

  case EsmGroup::ExteriorCellSubBlock:
    return QString().sprintf("ExteriorCellSubBlock '%ix%i'", short(label.data >> 16), short(label.data & 0xFFFF));

  case EsmGroup::CellChildren:
    return QString().sprintf("CellChildren 'FormId:%08x'", label.data);

  case EsmGroup::TopicChildren:
    return QString().sprintf("TopicChildren 'FormId:%08x'", label.data);

  case EsmGroup::CellPersistentChildren:
    return QString().sprintf("CellPersistentChildren 'FormId:%08x'", label.data);

  case EsmGroup::CellTemporaryChildren:
    return QString().sprintf("CellTemporaryChildren 'FormId:%08x'", label.data);

  case EsmGroup::CellVisibleDistantChildren:
    return QString().sprintf("CellVisibleDistantChildren 'FormId:%08x'", label.data);

  default:
    return QString();
  }
}

const EsmVertexNormal * EsmLand::normals() const
{
  if(const EsmSubRecord* record = findRecord("VNML"))
  {
    return (const EsmVertexNormal*)record->data();
  }
  return NULL;
}

unsigned EsmLand::normalCount() const
{
  if(const EsmSubRecord* record = findRecord("VNML"))
  {
    return record->realDataSize() / sizeof(EsmVertexNormal);
  }
  return NULL;
}

#pragma pack(4)
struct EsmVertexGradientV1
{
  float offset;
  qint8 data[33][33];
};

struct EsmVertexGradientV2
{
  float offset;
  qint8 data[65][65];
};
#pragma pack()

float EsmLand::heightOffset() const
{
  if(const EsmSubRecord* record = findRecord("VHGT"))
  {
    if(record->realDataSize() == sizeof(EsmVertexGradientV2))
      return ((EsmVertexGradientV2*)record->data())->offset;
    if(record->realDataSize() == sizeof(EsmVertexGradientV1))
      return ((EsmVertexGradientV1*)record->data())->offset;
  }
  return 0.0f;
}

const qint8 * EsmLand::heights() const
{
  if(const EsmSubRecord* record = findRecord("VHGT"))
  {
    if(record->realDataSize() == sizeof(EsmVertexGradientV2))
      return ((EsmVertexGradientV2*)record->data())->data[0];
    if(record->realDataSize() == sizeof(EsmVertexGradientV1))
      return ((EsmVertexGradientV1*)record->data())->data[0];
  }
  return NULL;
}

unsigned EsmLand::heightCount() const
{
  if(const EsmSubRecord* record = findRecord("VHGT"))
  {
    if(record->realDataSize() == sizeof(EsmVertexGradientV2))
      return 65 * 65;
    if(record->realDataSize() == sizeof(EsmVertexGradientV1))
      return 33 * 33;
  }
  return 0.0f;
}

const EsmTextureRecord * EsmLand::baseTexture(const EsmTextureRecord *after) const
{
  if(const EsmSubRecord* record = findRecord("BTXT", (const EsmSubRecord*)after))
  {
    return (const EsmTextureRecord*)record;
  }
  return NULL;
}

const EsmTextureRecord* EsmLand::alphaTexture(const EsmTextureRecord *after) const
{
  if(const EsmSubRecord* record = findRecord("ATXT", (const EsmSubRecord*)after))
  {
    return (const EsmTextureRecord*)record;
  }
  return NULL;
}

EsmGridInfo EsmCell::gridInfo() const
{  
  if(const EsmSubRecord* record = findRecord("XCLC"))
  {
    if(record->realDataSize() == 8)
      return *(const EsmGridInfo*)record->data();
    if(record->realDataSize() == 12)
      return *(const EsmGridInfo*)record->data();
  }
  return EsmGridInfo();
}
