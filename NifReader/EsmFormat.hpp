#ifndef ESMFORMAT_HPP
#define ESMFORMAT_HPP

#include <QtGlobal>
#include <QDebug>
#include <QFile>
#include <QColor>

#include <Math/Vec3.hpp>
#include <Math/Euler.hpp>
#include <Math/Box.hpp>

struct EsmType;
struct EsmGroup;
struct EsmRecord;
struct EsmSubRecord;

#pragma pack(1)
struct EsmType
{
public:
  quint32 data;

public:
  EsmType()
  {
    data = 0;
  }
  EsmType(quint32 name)
  {
    data = name;
  }
  EsmType(const char* name)
  {
    data = *(quint32*)name;
  }

public:
  bool operator == (const char* name) const;
  bool operator != (const char* name) const;
  operator quint32 () const
  {
    return data;
  }

public:
  friend QDebug operator << (QDebug dbg, const EsmType& type);
};

struct EsmFormId
{
public:
  quint32 data;

public:
  bool operator < (const EsmFormId other) const;
  bool operator == (const EsmFormId other) const;

  operator quint32 () const
  {
    return data;
  }

public:
  friend QDebug operator << (QDebug dbg, const EsmFormId formId);
};

struct EsmGroup
{
public:
  enum Type
  {
    Top = 0,
    WorldChildren = 1,
    InteriorCellBlock = 2,
    InteriorCellSubBlock = 3,
    ExteriorCellBlock = 4,
    ExteriorCellSubBlock = 5,
    CellChildren = 6,
    TopicChildren = 7,
    CellPersistentChildren = 8,
    CellTemporaryChildren = 9,
    CellVisibleDistantChildren = 10
  };

public:
  EsmType type;
  quint32 groupSize;
  EsmType label;
  long groupType;
  ulong stamp;
  ulong unk1;
  const QString name() const;

public:
  const EsmGroup* next() const;
  const EsmRecord* firstRecord() const;
  const EsmRecord* lastRecord() const;

public:
  friend QDebug operator << (QDebug dbg, const EsmGroup& g);
};

struct EsmRecord
{
public:
  enum Flags
  {
    EsmFile = 0x00000001,
    Deleted = 0x00000020,
    CastsShadows = 0x00000200,
    QuestItem = 0x00000400,
    InitiallyDisabled = 0x00000800,
    Ignored = 0x00001000,
    VisibleWhenDistant = 0x00008000,
    Dangerous = 0x00020000,
    Compressed = 0x00040000,
    CantWait = 0x00080000
  };

public:
  EsmType type;
  ulong dataSize;
  ulong flags;
  EsmFormId formId;
  unsigned unk1;
  unsigned unk2;

public:
  const char* data() const;
  const EsmSubRecord* firstSubRecord() const;
  const EsmSubRecord* lastSubRecord() const;
  const EsmRecord* next() const;
  const EsmGroup* group() const;

public:
  const EsmSubRecord* findRecord(EsmType type, const EsmSubRecord* after = NULL) const;
  QByteArray toByteArray(EsmType type) const;

public:
  QByteArray itemName() const;

public:
  friend QDebug operator << (QDebug dbg, const EsmRecord& r);
};

struct EsmSubRecord
{
public:
  EsmType type;
  ushort dataSize;

public:
  unsigned realDataSize() const;
  const char* data() const;
  const EsmSubRecord* next() const;
  const EsmSubRecord* next(EsmType type) const;

public:
  template<typename T>
  T as(T def = T()) const
  {
    if(!this)
      return def;
    if(realDataSize() != sizeof(T))
      return def;
    return *(const T*)data();
  }

public:
  QByteArray toByteArray() const;

public:
  friend QDebug operator << (QDebug dbg, const EsmSubRecord& s);
};

class EsmFile
{
public:
  EsmFile(const void* data, qint64 size);
  EsmFile(const QString& fileName);
  EsmFile(QFile& file);
  ~EsmFile();

public:
  QString fileName() const;

public:
  const EsmGroup* firstGroup() const;
  const EsmGroup* lastGroup() const;
  const EsmGroup* findGroup(EsmType type) const;

public:
  QHash<EsmFormId, const EsmRecord*> enumRecords(EsmType type) const;
  const EsmRecord* findRecord(EsmType type, EsmFormId formId);

public:
  bool isValid() const;

private:
  const EsmRecord* header;
  QString theFileName;
  qint64 size;
  QFile file;
  QHash<EsmType, QHash<EsmFormId, const EsmRecord*> > records;
};

struct EsmGridInfo
{
  long X, Y;

  EsmGridInfo()
  {
    X = 0;
    Y = 0;
  }
};

struct EsmCell : public EsmRecord
{
public:
  bool isValid() const;

public:
  const EsmGroup* children() const;

  EsmGridInfo gridInfo() const;
};

struct EsmWorld : public EsmRecord
{
public:
  bool isValid() const;

public:
  const EsmGroup* children() const;
};

struct EsmRefr : public EsmRecord
{
public:
  enum MarkerType
  {
    NotFound = -1,
    None,
    Camp,
    Cave,
    City,
    ElvenRuin,
    FortRuin,
    Mine,
    Landmark,
    Tavern,
    Settlement,
    DaedricShrine,
    OlivionGate,
    DoorIcon
  };

public:
  bool isValid() const;

public:
  bool isMarker() const;
  MarkerType markerType() const;
  float scale() const;

  EsmFormId refrFormId() const;

  vec3 origin() const;
  euler angles() const;
};

struct EsmLight : public EsmRecord
{
public:
  enum LightFlags
  {
    Dynamic = 0x1,
    Carried = 0x2,
    Negative = 0x4,
    Flicker = 0x8,
    OffByDefault = 0x20,
    FlickerSlow = 0x40,
    Pulse = 0x80,
    PulseSlow = 0x100,
    SpotLight = 0x200,
    SpotShadow = 0x400
  };

public:
  long time() const;
  float radius() const;
  QRgb color() const;
  LightFlags flags() const;
  float falloff() const;
  float fov() const;
  float value() const;
  float weight() const;
};

struct EsmStat : public EsmRecord
{
public:
  QByteArray modelName() const;
};

struct EsmMisc : public EsmStat
{
public:
  box bounds() const;
  float weight() const;
};

struct EsmVertexNormal
{
  qint8 X, Y, Z;
};

struct EsmTextureRecord : public EsmSubRecord
{
  enum Quadrants
  {
    BottomLeft,
    BottomRight,
    UpperLeft,
    UpperRight
  };

  EsmFormId formId;
  quint8 quadrant;
  quint8 unk;
  quint16 layer;
};

struct EsmLand : public EsmRecord
{
public:
  const EsmVertexNormal* normals() const;
  unsigned normalCount() const;

  float heightOffset() const;

  const qint8* heights() const;
  unsigned heightCount() const;

  const EsmTextureRecord* baseTexture(const EsmTextureRecord *after = NULL) const;
  const EsmTextureRecord* alphaTexture(const EsmTextureRecord *after = NULL) const;
};

struct EsmVtxtRecord : public EsmSubRecord
{
  struct Vtxt
  {
    unsigned short position;
    quint8 unk[2];
    float opacity;
  };

  Vtxt records[288];

  unsigned recordCount() const
  {
    return realDataSize() / sizeof(Vtxt);
  }
};

#pragma pack()

#endif // ESMFORMAT_HPP
