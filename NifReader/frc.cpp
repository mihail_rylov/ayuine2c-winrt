#include <SceneGraph/Scene.hpp>
#include <SceneGraph/ActorManager.hpp>
#include <SceneGraph/PointLightActor.hpp>
#include <SceneGraph/StaticMeshActor.hpp>

#include <Core/ProgressStatus.hpp>
#include <Core/Resource.hpp>
#include <Core/ResourceImporter.hpp>

class FrcImporter : public ResourceImporter
{
public:
  FrcImporter()
  {
    registerImporter("frc", &Scene::staticMetaObject);
  }

public:
  QSharedPointer<Resource> load(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {    
    if(!device)
      return QSharedPointer<Resource>();

    if(device->objectName().isEmpty())
      return QSharedPointer<Resource>();

    QSharedPointer<Scene> scene(new Scene());

    if(parseFrc(scene, *device))
      return scene;

    return QSharedPointer<Resource>();
  }

  const QMetaObject* metaObject(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    return &Scene::staticMetaObject;
  }

private:
  bool parseFrc(QSharedPointer<Scene>& scene, QIODevice& stream)
  {
    QTextStream textStream(&stream);

    // Wczytaj nag��wek
    unsigned ambientColor[3];
    unsigned fogColor[3], fogNear, fogFar;
    textStream >> ambientColor[0] >> ambientColor[1] >> ambientColor[2] >>
                  fogColor[0] >> fogColor[1] >> fogColor[2] >>
                  fogNear >> fogFar;
    if(textStream.atEnd())
      return false;

    // Ustaw podstawowe parametry sceny
    // setAmbientColor(color(ambientColor[0], ambientColor[1], ambientColor[2], 255));

    QString type;

    ProgressStatus status(&stream);

    // Wczytaj obiekty z pliku
    while(!textStream.atEnd())
    {
      bool result = false;

      textStream >> type;
      if(type.isEmpty())
        break;

      status.setValue(&stream);

      QString line = textStream.readLine();

      // Wczytaj obiekt
      if(type == "[LIGH]")
        result = readLight(scene, line, stream.objectName());
      else if(type == "[STAT]")
        result = readMesh(scene, line, stream.objectName());
      else if(type == "[FURN]")
        result = readMesh(scene, line, stream.objectName());
      else if(type == "[CONT]")
        result = readMesh(scene, line, stream.objectName());
      else if(type == "[MISC]")
        result = readMesh(scene, line, stream.objectName());

      // Sprawd� wynik
      if(!result)
      {
        qDebug() << "[FRC] Failed to parse: " << type;
      }
    }
    return true;
  }

  bool readMesh(QSharedPointer<Scene>& scene, const QString& line, const QString& sceneName)
  {
    QTextStream stream(line.toLatin1());
    QString meshName;
    vec3 origin(0,0,0);
    euler angles(0,0,0);
    float scale = 1.0f, weight = 0.0f;

    // Wczytaj parametry
    stream >> meshName;
    if(stream.atEnd() || meshName.length() <= 2)
      return false;

    stream >> origin.X >> origin.Y >> origin.Z;
    stream >> angles.Roll >> angles.Pitch >> angles.Yaw;
    stream >> scale >> weight;

    meshName = meshName.toLower();

    // Usu� []
    if(meshName.startsWith('[') && meshName.endsWith(']'))
      meshName = meshName.mid(1, meshName.size()-2);

    // Ignoruj markery
    if(meshName.contains("marker", Qt::CaseInsensitive))
      return true;
    if(meshName.startsWith("Effects", Qt::CaseInsensitive))
      return true;

    // Zamie� rozszerzenie
    if(meshName.endsWith(".nif"))
      meshName = meshName.replace(".nif", ".obj");

    // Utw�rz mesha
    QSharedPointer<StaticMeshActor> meshActor(new StaticMeshActor());
    meshActor->setObjectName(QFileInfo(meshName).baseName());
    meshActor->setTransform(origin / 64.0f, -angles, vec3(scale));
    meshActor->setMesh(Resource::load<Mesh>("meshes/" + meshName, sceneName));
    if(!meshActor->mesh())
      return false;

    meshActor->setCastShadows(true);
    meshActor->setGravity(true);

    // Ustaw parametry fizyki
    if(!meshActor->mesh())
    {
      meshActor->setMovable(false);
      meshActor->setCollidable(false);
    }
    else if(weight <= Epsf)
    {
      meshActor->setMovable(false);
      meshActor->setCollidable(true);
    }
    else
    {
      meshActor->setMovable(true);
      meshActor->setCollidable(true);
      meshActor->setMass(weight);
    }

    scene->rootActor()->addActor(meshActor);
    return true;
  }

  bool readLight(QSharedPointer<Scene>& scene, const QString& line, const QString& sceneName)
  {
    QTextStream stream(line.toLatin1());
    QString lightName;
    vec3 origin(0,0,0);
    euler angles(0,0,0);
    float scale = 1.0f, weight = 0.0f;
    float time = 0.0f;
    unsigned color[3] = {255,255,255};
    QString flags;
    float falloff = 0.0f, fov = 0.0f, fade = 0.0f, radius = 8.0f;

    // Wczytaj parametry
    stream >> lightName;
    if(stream.atEnd() || lightName.length() <= 2)
      return false;

    stream >> origin.X >> origin.Y >> origin.Z;
    stream >> angles.Roll >> angles.Pitch >> angles.Yaw;
    stream >> scale >> weight;
    stream >> time >> color[0] >> color[1] >> color[2];
    stream >> flags >> falloff >> fov >> fade >> radius;

    lightName = lightName.toLower();

    // Usu� []
    if(lightName.startsWith('[') && lightName.endsWith(']'))
      lightName = lightName.mid(1, lightName.size()-2);

    // Utw�rz �wiat�o
    QSharedPointer<PointLightActor> lightActor(new PointLightActor());
    lightActor->setObjectName(lightName);
    lightActor->setTransform(origin / 64.0f, -angles, 1.0f);
    // lightActor->setShadows(true);
    lightActor->setLightShader("AdvancedLightingModel.grcz");
    lightActor->setLightColor(QColor::fromRgb(color[0], color[1], color[2]));
    lightActor->setLightRadius(2.0f * radius / 64.0f);
    scene->rootActor()->addActor(lightActor);
    return true;
  }
};

static FrcImporter frcImporter;
