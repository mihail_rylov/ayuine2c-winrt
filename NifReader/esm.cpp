#include <Core/ResourceImporter.hpp>
#include <Core/ProgressStatus.hpp>

#include <Render/Texture.hpp>

#include <QInputDialog>

#include <SceneGraph/Scene.hpp>
#include <SceneGraph/ActorManager.hpp>
#include <SceneGraph/PointLightActor.hpp>
#include <SceneGraph/StaticMeshActor.hpp>
#include <SceneGraph/TerrainActor.hpp>
#include <SceneGraph/NaiveActorManager.hpp>

#include "EsmFormat.hpp"

const float GridScale = 32;

class EsmImporter : public ResourceImporter
{
public:
  EsmImporter()
  {
    registerImporter("esm", NULL, "Oblivion/Fallout3 Main Game File");
    registerImporter("esp", NULL, "Oblivion/Fallout3 Plugin File");
  }

private:
  QHash<QString, const EsmRecord*> selectable(const EsmFile& esmFile)
  {
    QHash<QString, const EsmRecord*> records;

    foreach(const EsmRecord* record, esmFile.enumRecords("CELL"))
    {
      QByteArray name = record->itemName();
      if(name.isEmpty())
        continue;
      records["Cell - " + name] = record;
    }

    foreach(const EsmRecord* record, esmFile.enumRecords("WRLD"))
    {
      QByteArray name = record->itemName();
      if(name.isEmpty())
        continue;
      records["World - " + name] = record;
    }

    return records;
  }

private:
  void parseLight(EsmFile& esmFile, const EsmLight* light, const vec3& origin, const euler& angles, float scale, const QSharedPointer<ActorManager>& parentActor)
  {
    QSharedPointer<PointLightActor> lightActor(new PointLightActor());
    lightActor->setObjectName(light->itemName());
    lightActor->setTransform(origin, angles, scale);
    if(light->flags() & EsmLight::Dynamic)
      lightActor->setLightShader("SimpleLightingModel.grcz");
    else
      lightActor->setLightShader("AdvancedLightingModel.grcz");
    lightActor->setLightColor(light->color());
    lightActor->setLightRadius(light->radius());
    parentActor->addActor(lightActor);
  }

  void parseStat(EsmFile& esmFile, const EsmStat* stat, const vec3& origin, const euler& angles, float scale, const QSharedPointer<ActorManager>& parentActor)
  {
    QByteArray modelName = stat->modelName();
    if(modelName.isEmpty())
      return;
    modelName = modelName.toLower();

    // Zamie� rozszerzenie
    if(modelName.endsWith(".nif"))
      modelName = modelName.replace(".nif", ".obj");

    // Utw�rz mesha
    QSharedPointer<StaticMeshActor> meshActor(new StaticMeshActor());
    meshActor->setObjectName(stat->itemName());
    meshActor->setTransform(origin, angles, scale);
    meshActor->setMesh(Resource::load<Mesh>("meshes/" + modelName, esmFile.fileName()));
    if(!meshActor->mesh())
      return;
    meshActor->mesh()->setColliderType(Mesh::ColliderExact);
    parentActor->addActor(meshActor);
  }

  void parseMovable(EsmFile& esmFile, const EsmMisc* misc, const vec3& origin, const euler& angles, float scale, const QSharedPointer<ActorManager>& parentActor)
  {
    QByteArray modelName = misc->modelName();
    if(modelName.isEmpty())
      return;
    modelName = modelName.toLower();

    // Zamie� rozszerzenie
    if(modelName.endsWith(".nif"))
      modelName = modelName.replace(".nif", ".obj");

    // Utw�rz mesha
    QSharedPointer<StaticMeshActor> meshActor(new StaticMeshActor());
    meshActor->setObjectName(misc->itemName());
    meshActor->setTransform(origin, angles, scale);
    meshActor->setMesh(Resource::load<Mesh>("meshes/" + modelName, esmFile.fileName()));
    if(!meshActor->mesh())
      return;

    meshActor->mesh()->setColliderType(Mesh::ColliderConvex);
    meshActor->setMass(misc->weight());
    if(!qFuzzyIsNull(meshActor->mass()))
      meshActor->setMovable(true);

    parentActor->addActor(meshActor);
  }

  void parseRefr(EsmFile& esmFile, const EsmRefr* refr, const QSharedPointer<ActorManager>& parentActor)
  {
    if(refr->isMarker())
      return;

    EsmFormId formId = refr->refrFormId();
    if(!formId)
      return;

    if(const EsmRecord* stat = esmFile.findRecord("STAT", formId))
    {
      parseStat(esmFile, (const EsmStat*)stat, refr->origin(), refr->angles(), refr->scale(), parentActor);
    }
    else if(const EsmRecord* light = esmFile.findRecord("LIGH", formId))
    {
      parseLight(esmFile, (const EsmLight*)light, refr->origin(), refr->angles(), refr->scale(), parentActor);
    }
    else if(const EsmRecord* furniture = esmFile.findRecord("FURN", formId))
    {
      parseMovable(esmFile, (const EsmMisc*)furniture, refr->origin(), refr->angles(), refr->scale(), parentActor);
    }
    else if(const EsmRecord* misc = esmFile.findRecord("MISC", formId))
    {
      parseMovable(esmFile, (const EsmMisc*)misc, refr->origin(), refr->angles(), refr->scale(), parentActor);
    }
    else if(const EsmRecord* movable = esmFile.findRecord("MSTT", formId))
    {
      parseMovable(esmFile, (const EsmMisc*)movable, refr->origin(), refr->angles(), refr->scale(), parentActor);
    }
  }

  inline void setTextureSet(ShaderState& state, EsmFile& esmFile, EsmFormId textureSetId, const QByteArray& nameSuffix)
  {
    if(const EsmRecord* texture = esmFile.findRecord("LTEX", textureSetId))
    {
      EsmFormId textureId = texture->findRecord("TNAM")->as<EsmFormId>();

      if(const EsmRecord* textureSet = esmFile.findRecord("TXST", textureId))
      {
        QByteArray diffuseTexture = textureSet->toByteArray("TX00");

        if(diffuseTexture.size())
        {
          state.set("diffuse" + nameSuffix, Resource::load<Texture>("textures/" + diffuseTexture, esmFile.fileName()));
        }

        QByteArray normalTexture = textureSet->toByteArray("TX01");

        if(normalTexture.size())
        {
          state.set("normal" + nameSuffix, Resource::load<Texture>("textures/" + normalTexture, esmFile.fileName()));
        }
      }

      state.set("specularPower", texture->findRecord("SNAM")->as<quint8>(30.0f));
    }
  }

  void parseLand(EsmFile& esmFile, const EsmLand* land, const vec3& origin, const QSharedPointer<ActorManager>& parentActor)
  {
    unsigned heightSize = 33;
    unsigned halfSize = heightSize / 2 + 1;

    QVector<qint16> heights(heightSize * heightSize, 0);
    QVector<vec3> normals(heightSize * heightSize, vec3(0,0,1));
    QVector<vec4> blends[4] = 
    {
      QVector<vec4>(halfSize * halfSize, vec4(0,0,0,0)), 
      QVector<vec4>(halfSize * halfSize, vec4(0,0,0,0)),
      QVector<vec4>(halfSize * halfSize, vec4(0,0,0,0)),
      QVector<vec4>(halfSize * halfSize, vec4(0,0,0,0))
    };

    if(const qint8* diffs = land->heights())
    {
      for(int y = 0; y < heightSize; ++y)
      {
        qint32 height = 0;

        for(int yy = 0; yy < y; ++yy)
        {
          height += diffs[yy * heightSize];
        }

        for(int x = 0; x < heightSize; ++x)
        {
          height += diffs[y * heightSize + x];
          heights[y * heightSize + x] = height;
        }
      }
    }

    if(const EsmVertexNormal* esmNormals = land->normals())
    {
      for(int i = 0; i < land->normalCount(); ++i, ++esmNormals)
      {
        vec3 normal(esmNormals->X / 128.0f, esmNormals->Y / 128.0f, esmNormals->Z / 128.0f);
        normals[i] = normal.normalize();
      }
    }

    QSharedPointer<TerrainActor> terrainActor[4];

    for(int i = 0; i < 4; ++i)
    {
      vec3 terrainOrigin(i & 1 ? 16 : -16, i & 2 ? 16 : -16, land->heightOffset() / 8);
      vec3 terrainScale(2.0f, 2.0f, 1.0f/8.0f);

      terrainActor[i] = QSharedPointer<TerrainActor>(new TerrainActor());
      terrainActor[i]->setTerrainShader("SimpleTerrainMaterial.grcz");
      terrainActor[i]->setObjectName(land->itemName() + QString::number(i&1?1:0) + "x" + QString::number(i&2?1:0));
      terrainActor[i]->setSize(QSize(halfSize, halfSize));
      terrainActor[i]->setTransform(origin + terrainOrigin, euler(), terrainScale);
    }

    for(const EsmTextureRecord* alphaTexture = NULL; alphaTexture = land->alphaTexture(alphaTexture); )
    {
      if(alphaTexture->quadrant < 0 || alphaTexture->quadrant >= 4)
        continue;
      if(alphaTexture->layer < 0 || alphaTexture->layer >= 4)
        continue;

      // set layer textures
      setTextureSet(terrainActor[alphaTexture->quadrant]->terrainState(), esmFile, alphaTexture->formId, "Texture" + QByteArray::number(alphaTexture->layer));

      // check if we have VTXT
      if(const EsmVtxtRecord* vtxt = (const EsmVtxtRecord*)alphaTexture->next("VTXT"))
      {
        for(int i = vtxt->recordCount(); i-- > 0; )
        {
          EsmVtxtRecord::Vtxt record = vtxt->records[i];
          if(record.position < 0 || record.position >= halfSize * halfSize)
            continue;
          blends[alphaTexture->quadrant][record.position][alphaTexture->layer] = record.opacity;
        }
      }
    }

    // set base textures
    for(const EsmTextureRecord* baseTexture = NULL; baseTexture = land->baseTexture(baseTexture); )
    {
      if(baseTexture->quadrant < 0 || baseTexture->quadrant >= 4)
        continue;
      setTextureSet(terrainActor[baseTexture->quadrant]->terrainState(), esmFile, baseTexture->formId, "BaseTexture");
    }

    for(int i = 0; i < 4; ++i)
    {
      QVector<qint16> heightsLocal(halfSize * halfSize, 0);
      QVector<vec3> normalsLocal(halfSize * halfSize, vec3(0,0,1));

      for(int y = 0; y < halfSize; ++y)
      {
        int yy = i&2 ? (y + halfSize - 1) : y;

        for(int x = 0; x < halfSize; ++x)
        {
          int xx = i&1 ? (x + halfSize - 1) : x;
          int xy = y * halfSize + x;
          int xxyy = yy * heightSize + xx;

          heightsLocal[xy] = heights[xxyy];
          normalsLocal[xy] = normals[xxyy];
        }
      }

      terrainActor[i]->setHeights(heightsLocal);
      terrainActor[i]->setNormals(normalsLocal);
      terrainActor[i]->setBlends(blends[i]);
      parentActor->addActor(terrainActor[i]);
    }
  }

  void parseCell(EsmFile& esmFile, const EsmGroup* group, const QSharedPointer<ActorManager>& parentActor)
  {
    ProgressStatus status((uchar*)group->lastRecord() - (uchar*)group->firstRecord());

    for(const EsmRecord *record = group->firstRecord(), *last = group->lastRecord(); record < last; record = record->next())
    {
      status.setValue((uchar*)record - (uchar*)group->firstRecord());

      if(const EsmGroup* recordGroup = record->group())
      {
        switch(recordGroup->groupType)
        {
        case EsmGroup::CellPersistentChildren:
        case EsmGroup::CellTemporaryChildren:
        case EsmGroup::CellVisibleDistantChildren:
          parseCell(esmFile, recordGroup, parentActor);
          break;
        }
      }
      else if(record->type == "REFR")
      {
        parseRefr(esmFile, (const EsmRefr*)record, parentActor);
      }
    }
  }

  QSharedPointer<Scene> parseCell(EsmFile& esmFile, const EsmCell* cell)
  {    
    if(!cell)
      return QSharedPointer<Scene>();

    QSharedPointer<Scene> scene(new Scene());
    scene->setObjectName(cell->itemName());

    if(const EsmGroup* group = cell->children())
    {
      parseCell(esmFile, group, scene->rootActor());
    }

    return scene;
  }

  void parseWorld(EsmFile& esmFile, const EsmGroup* group, const vec3& origin, const QSharedPointer<ActorManager>& parentActor)
  {
    if(!group)
      return;

    ProgressStatus status((uchar*)group->lastRecord() - (uchar*)group->firstRecord());

    for(const EsmRecord *record = group->firstRecord(), *last = group->lastRecord(); record < last; record = record->next())
    {
      status.setValue((uchar*)record - (uchar*)group->firstRecord());

      if(const EsmGroup* recordGroup = record->group())
      {
        switch(recordGroup->groupType)
        {
        case EsmGroup::CellTemporaryChildren:
        case EsmGroup::CellVisibleDistantChildren:
        case EsmGroup::CellPersistentChildren:
        case EsmGroup::InteriorCellBlock:
        case EsmGroup::InteriorCellSubBlock:
          parseWorld(esmFile, recordGroup, origin, parentActor);
          break;

        case EsmGroup::ExteriorCellBlock:
        case EsmGroup::ExteriorCellSubBlock:
        {
          QSharedPointer<ActorManager> manager(new NaiveActorManager());
          manager->setObjectName(recordGroup->name());
          parentActor->addActor(manager);
          parseWorld(esmFile, recordGroup, origin, manager);
          if(manager->actors().isEmpty())
            parentActor->removeActor(manager);
        }
          break;
        }
      }
      else if(record->type == "CELL")
      {
        const EsmCell* cell = (const EsmCell*)record;
        EsmGridInfo cellGrid = cell->gridInfo();

        vec3 newOrigin(vec3((cellGrid.X + 0.5f) * 64, (cellGrid.Y + 0.5f) * 64, 0.0f));

        QSharedPointer<ActorManager> manager(new NaiveActorManager());
        manager->setObjectName(cell->itemName());
        parentActor->addActor(manager);

        parseWorld(esmFile, cell->children(), newOrigin, manager);

        if(manager->actors().isEmpty())
          parentActor->removeActor(manager);
      }
      else if(record->type == "LAND")
      {
        parseLand(esmFile, (const EsmLand*)record, origin, parentActor);
      }
      else if(record->type == "REFR")
      {
        parseRefr(esmFile, (const EsmRefr*)record, parentActor);
      }
      else
      {
        qDebug() << "[ESMWORLD] Ignored record " << record->type;
      }
    }
  }

  QSharedPointer<Scene> parseWorld(EsmFile& esmFile, const EsmWorld* world)
  {
    if(!world)
      return QSharedPointer<Scene>();

    QSharedPointer<Scene> scene(new Scene());
    scene->setObjectName(world->itemName());

    if(const EsmGroup* group = world->children())
    {
      parseWorld(esmFile, group, vec3(0,0,0), scene->rootActor());
    }

    return scene;
  }

public:
  QSharedPointer<Resource> load(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    QFile* file = qobject_cast<QFile*>(device.data());

    if(!file)
    {
      qDebug() << "[ESM] Only QFile handle supported: " << device->objectName();
      return QSharedPointer<Resource>();
    }

    EsmFile esmFile(*file);

    if(!esmFile.isValid())
    {
      qDebug() << "[ESM] File not valid: " << device->objectName();
      return QSharedPointer<Resource>();
    }

    QHash<QString, const EsmRecord*> records = selectable(esmFile);
    QStringList recordNames(records.keys());
    recordNames.sort();

    QString recordName = QInputDialog::getItem(NULL, "Esm Importer", "Select object:", recordNames, 0, false);
    if(recordName.isEmpty() || !records.contains(recordName))
    {
      return QSharedPointer<Resource>();
    }

    const EsmRecord* record = records[recordName];

    ProgressStatus status;
    status.setText(recordName);

    if(record->type == "CELL")
    {
      return parseCell(esmFile, (const EsmCell*)record);
    }
    else if(record->type == "WRLD")
    {
      return parseWorld(esmFile, (const EsmWorld*)record);
    }
    else
    {
      return QSharedPointer<Resource>();
    }
  }
};

static EsmImporter esmImporter;
