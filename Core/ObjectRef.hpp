#ifndef OBJECTREF_HPP
#define OBJECTREF_HPP

#include <QDataStream>
#include <QSharedPointer>

#include "Core.hpp"

#if 0
template<class Type>
class BaseObjectRef
{
public:
  BaseObjectRef()
    : theMetaObject(NULL)
  {
  }

  BaseObjectRef(const QSharedPointer<Type>& sharedObject, const QMetaObject* metaObject)
    : theSharedObject(sharedObject), theMetaObject(metaObject)
  {
  }

  template<typename NewType>
  BaseObjectRef(const QSharedPointer<NewType> &sharedObject)
    : theSharedObject(sharedObject), theMetaObject(&NewType::staticMetaObject)
  {
  }

  ~BaseObjectRef()
  {
  }

public:
  operator const QSharedPointer<Type>& () const
  {
    return theSharedObject;
  }
  operator bool () const
  {
    return theSharedObject.data() != NULL;
  }
  bool operator ! () const
  {
    return theSharedObject.data() == NULL;
  }

public:
  const QMetaObject* metaObject() const
  {
    return theMetaObject;
  }

  template<class NewType>
  QSharedPointer<NewType> toObject() const
  {
    return theSharedObject.template objectCast<NewType>();
  }

public:
  QSharedPointer<Type> theSharedObject;
  const QMetaObject* theMetaObject;
};

template<class NewType, class BaseType>
class ObjectRefT : public BaseObjectRef<BaseType>
{
public:
  ObjectRefT()
  {
  }

  ObjectRefT(const QSharedPointer<NewType>& sharedObject)
    : BaseObjectRef<BaseType>(sharedObject, &NewType::staticMetaObject)
  {
  }

  QSharedPointer<NewType> toObject() const
  {
    return BaseObjectRef<BaseType>::template toObject<NewType>();
  }

  operator QSharedPointer<NewType>() const
  {
    return toObject();
  }

  operator bool() const
  {
    return !toObject().isNull();
  }

  NewType* operator -> () const
  {
    QSharedPointer<NewType> object(toObject());
    if(object.isNull())
      return NULL;
    return object.data();
  }
};

typedef QSharedPointer<QObject> ObjectRef;
#endif

/*
typedef QVector<ObjectRef> ObjectRefList;

Q_DECLARE_METATYPE(ObjectRef)
Q_DECLARE_METATYPE(ObjectRefList)

CORE_EXPORT QDataStream &operator << (QDataStream &ds, const ObjectRef &objectRef);
CORE_EXPORT QDataStream &operator >> (QDataStream &ds, ObjectRef &objectRef);

QT_BEGIN_NAMESPACE
template <typename Type, typename BaseType>
struct QMetaTypeId< typename ObjectRefT<Type, BaseType> >
{
  enum { Defined = QMetaTypeId<BaseObjectRef<BaseType> >::Defined };
  static int qt_metatype_id()
  {
    return QMetaTypeId<BaseObjectRef<BaseType> >::qt_metatype_id();
  }
};
QT_END_NAMESPACE

#define Q_DECLARE_OBJECTREF(Type, BaseType) \
  typedef QSharedPointer<Type> Type##Ref; \
  typedef QSharedPointer<Type> Type##RefList;
*/

#define Q_DECLARE_OBJECTREF(Type) \
  typedef QSharedPointer<Type> Type##Ref; \
  typedef QVector<Type##Ref> Type##RefList; \
  Q_DECLARE_METATYPE(Type##Ref) Q_DECLARE_METATYPE(Type##RefList) \
  inline QDataStream &operator << (QDataStream &ds, const Type##Ref &objectRef) { internalSave(objectRef.data(), ds, objectRef); return ds; } \
  inline QDataStream &operator >> (QDataStream &ds, Type##Ref &objectRef) { internalLoad(objectRef.data(), ds, objectRef); return ds; }

#define Q_REGISTER_METATYPE(Type) \
  static int registerMetaType##Type() { return qRegisterMetaTypeStreamOperators<Type>(); } \
  Q_CONSTRUCTOR_FUNCTION(registerMetaType##Type)

#define Q_IMPLEMENT_OBJECTREF(Type) \
  Q_REGISTER_METATYPE(Type##Ref) Q_REGISTER_METATYPE(Type##RefList)

#define Q_IMPLEMENT_ALIASREF(Type, BaseType) \
  static int registerTypeAlias##Type() { return QMetaType::registerTypedef(#Type, qMetaTypeId<BaseType>()); } \
  Q_CONSTRUCTOR_FUNCTION(registerTypeAlias##Type)

CORE_EXPORT void internalLoadQObject(QDataStream& ds, QSharedPointer<QObject>& object, const QMetaObject* metaObject);
CORE_EXPORT void internalSaveQObject(QDataStream& ds, const QSharedPointer<QObject>& object, const QMetaObject* metaObject);

template<typename T>
inline void internalLoad(const QObject*, QDataStream& ds, QSharedPointer<T>& object)
{
  internalLoadQObject(ds, (QSharedPointer<QObject>&)object, &T::staticMetaObject);
}

template<typename T>
inline void internalSave(const QObject*, QDataStream& ds, const QSharedPointer<T>& object)
{
  internalSaveQObject(ds, (const QSharedPointer<QObject>&)object, &T::staticMetaObject);
}

#endif // OBJECTREF_HPP
