#ifndef SERIALIZER_HPP
#define SERIALIZER_HPP

#include "Core.hpp"
#include <QSharedPointer>
#include <QDataStream>

inline void swapOrder(char&)
{
}

inline void swapOrder(qint8&)
{
}

inline void swapOrder(quint8&)
{
}

inline void swapOrder(short& value)
{
  short otherValue = value;
  char* ptr = (char*)&value;
  char* otherPtr = (char*)&otherValue;
  ptr[0] = otherPtr[1];
  ptr[1] = otherPtr[0];
}

inline void swapOrder(unsigned short& value)
{
  swapOrder((short&)value);
}

inline void swapOrder(int& value)
{
  int otherValue = value;
  char* ptr = (char*)&value;
  char* otherPtr = (char*)&otherValue;
  ptr[0] = otherPtr[3];
  ptr[1] = otherPtr[2];
  ptr[2] = otherPtr[1];
  ptr[3] = otherPtr[0];
}

inline void swapOrder(unsigned& value)
{
  swapOrder((int&)value);
}

inline void swapOrder(float& value)
{
  swapOrder((int&)value);
}

template<typename Type>
inline void swapOrder(QVector<Type>& value)
{
  Type* ptr = value.data();
  Type* end = ptr + value.size();
  while(ptr < end)
    swapOrder(*ptr++);
}

template<typename Type>
inline void swapOrderDataStream(Type& value, QDataStream& ds)
{
  if(ds.byteOrder() != (QDataStream::ByteOrder)QSysInfo::ByteOrder)
  {
    swapOrder(value);
  }
}

#define Q_IMPLEMENT_METATYPE(Type) \
  static int metatype##Type() { \
    qRegisterMetaTypeStreamOperators<Type>(); \
    return 0; \
  } Q_CONSTRUCTOR_FUNCTION(metatype##Type)

#define Q_DECLARE_RAW_SERIALIZER(Type) \
  inline QDataStream& operator << (QDataStream& ds, const Type& data) { ds.writeRawData((const char *)&data, sizeof(Type)); return ds; } \
  inline QDataStream& operator >> (QDataStream& ds, Type& data) { ds.readRawData((char *)&data, sizeof(Type)); swapOrderDataStream(data, ds); return ds; }

#define Q_DECLARE_RAW_VECTOR_SERIALIZER(Type) \
  inline QDataStream& operator << (QDataStream& ds, const QVector<Type>& data) { ds << (quint32)data.size(); ds.writeRawData((const char*)data.constData(), data.size() * sizeof(Type)); return ds; } \
  inline QDataStream& operator >> (QDataStream& ds, QVector<Type>& data) { quint32 n = 0; ds >> n; data.resize(n); ds.readRawData((char*)data.data(), data.size() * sizeof(Type)); swapOrderDataStream(data, ds); return ds; }

#if 0
#define Q_DECLARE_COLLECTION(Coll, Type) \
  typedef Coll<Type> Type##Coll; \
  Q_DECLARE_METATYPE(Type##Coll)

#define Q_DECLARE_RAW_METATYPE(Type) \
  inline QDataStream& operator << (QDataStream& ds, const Type& data) { ds.writeRawData((const char *)&data, Size); return ds; } \
  inline QDataStream& operator >> (QDataStream& ds, Type& data) { ds.readRawData((char *)&data, Size); return ds; }

#define Q_DECLARE_COLLECTION_SERIALIZER(Type) \
  typedef QVector<Type> Type##Vector; \
  Q_DECLARE_METATYPE(Type##Vector)

#define Q_DECLARE_RAW_COLLECTION_SERIALIZER(Type) \
  Q_DECLARE_COLLECTION_SERIALIZER(Type) \
  inline QDataStream& operator << (QDataStream& ds, const Type& data) { ds.writeRawData((const char *)&data, Size); return ds; } \
  inline QDataStream& operator >> (QDataStream& ds, Type& data) { ds.readRawData((char *)&data, Size); return ds; }

#define Q_DECLARE_RAW_SERIALIZER2(Type, Size) \
  Q_DECLARE_METATYPE(Type) \
  Q_DECLARE_RAW_COLLECTION_SERIALIZER(Type)

#define Q_DECLARE_RAW_SERIALIZER(Type) \
  Q_DECLARE_RAW_SERIALIZER2(Type, sizeof(Type))

#define Q_IMPLEMENT_RAW_SERIALIZER(Type) \
  static int Type##RegisterSerializer() { \
    qRegisterMetaTypeStreamOperators<Type>(); \
    qRegisterMetaTypeStreamOperators<Type##Vector>(); \
    return 0; \
  } Q_CONSTRUCTOR_FUNCTION(Type##RegisterSerializer)
#endif

CORE_EXPORT QSharedPointer<QObject> loadFromDevice(QIODevice& device);
CORE_EXPORT QSharedPointer<QObject> loadFromFile(const QString& fileName);

CORE_EXPORT const QMetaObject* peakMetaObjectFromDevice(QIODevice& device);
CORE_EXPORT const QMetaObject* peakMetaObjectFromFile(const QString& fileName);
CORE_EXPORT void saveToDevice(const QSharedPointer<QObject>& object, QIODevice& device);

CORE_EXPORT QSharedPointer<QObject> shallowCopy(const QSharedPointer<QObject>& object);

#endif // SERIALIZER_HPP
