#include "ResourceImporter.hpp"
#include "Serializer.hpp"

#include "qtiocompressor.h"

class GenericResourceZipImporter : public ResourceImporter
{
public:
  GenericResourceZipImporter()
  {
    registerNativeImporter("grcz", NULL, "Compressed Resource");
    registerExporter("grcz", NULL, "Compressed Resource");
  }

  QSharedPointer<Resource> load(const QSharedPointer<QIODevice>& device, const QByteArray& extension)
  {
    if(!device)
      return QSharedPointer<Resource>();

    QtIOCompressor compressor(device.data());

    if(compressor.open(QIODevice::ReadOnly))
    {
      compressor.setObjectName(device->objectName());

      QSharedPointer<QObject> object(::loadFromDevice(compressor));
      QSharedPointer<Resource> resource(object.objectCast<Resource>());

      if(resource)
      {
        resource->setFileName(device->objectName());
        return resource;
      }
    }

    return QSharedPointer<Resource>();
  }

  const QMetaObject* metaObject(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    if(!device)
      return NULL;

    QtIOCompressor compressor(device.data());

    if(compressor.open(QIODevice::ReadOnly))
    {
      return ::peakMetaObjectFromDevice(compressor);
    }

    return NULL;
  }

  bool save(QSharedPointer<Resource> &resource, const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    if(!device)
      return false;

    QtIOCompressor compressor(device.data());

    if(compressor.open(QIODevice::WriteOnly))
    {
      compressor.setObjectName(device->objectName());
      saveToDevice(resource, compressor);
    }
    return true;
  }
};

static GenericResourceZipImporter genericResourceZipImporter;
