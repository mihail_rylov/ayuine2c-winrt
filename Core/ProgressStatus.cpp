#include "ProgressStatus.hpp"

static ProgressStatus* progressStack;

ProgressStatus::ProgressStatus(int count)
{
  if(progressStack)
  {
    connect(this, SIGNAL(valueChanged(ProgressStatus*)), progressStack, SIGNAL(valueChanged(ProgressStatus*)));
    connect(this, SIGNAL(textChanged(ProgressStatus*)), progressStack, SIGNAL(textChanged(ProgressStatus*)));
  }

  setParent(progressStack);
  progressStack = this;
  m_value = 0;
  m_count = count;
}

ProgressStatus::ProgressStatus(QIODevice* device)
{
  if(progressStack)
  {
    connect(this, SIGNAL(valueChanged(ProgressStatus*)), progressStack, SIGNAL(valueChanged(ProgressStatus*)));
    connect(this, SIGNAL(textChanged(ProgressStatus*)), progressStack, SIGNAL(textChanged(ProgressStatus*)));
  }

  setParent(progressStack);
  progressStack = this;
  m_value = 0;
  m_count = 1;

  if(device)
  {
    m_count = device->size();
    setText(QFileInfo(device->objectName()).baseName());
  }
}

ProgressStatus::~ProgressStatus()
{
  if(progressStack == this)
  {
    progressStack = qobject_cast<ProgressStatus*>(parent());

    if(progressStack)
    {
      emit valueChanged(progressStack);
      emit textChanged(progressStack);
    }
  }
}

ProgressStatus * ProgressStatus::last()
{
  return progressStack;
}

void ProgressStatus::setValue(int value)
{
  if(m_value != value)
  {
    m_value = qBound(0, value, m_count);
    emit valueChanged(this);
  }
}

void ProgressStatus::setValue(QIODevice* device)
{
  if(device)
  {
    setValue(device->pos());
  }
}

float ProgressStatus::progress() const
{
  float current = 0.0f;

  for(const ProgressStatus* status = this; status; status = qobject_cast<const ProgressStatus*>(status->parent()))
  {
    current = ((float)status->m_value + current) / (float)status->m_count;
  }

  return current;
}

QString ProgressStatus::text() const
{
  for(const ProgressStatus* status = this; status; status = qobject_cast<const ProgressStatus*>(status->parent()))
  {
    if(!status->m_text.isEmpty())
      return status->m_text;
  }
  return QString();
}

void ProgressStatus::setText(const QString &text)
{
  if(m_text != text)
  {
    m_text = text;
    emit textChanged(this);
  }
}

int ProgressStatus::count() const
{
  return m_count;
}

int ProgressStatus::value() const
{
  return m_value;
}
