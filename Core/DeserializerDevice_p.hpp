#ifndef DESERIALIZERDEVICE_P_HPP
#define DESERIALIZERDEVICE_P_HPP

#include <QIODevice>

#include "ObjectDesc_p.hpp"

class DeserializerDevice : public QIODevice
{
  Q_OBJECT

public:
  explicit DeserializerDevice(QIODevice *device);

public:
  qint64 pos() const;
  qint64 size() const;
  qint64 readData(char *data, qint64 maxlen);
  qint64 writeData(const char *data, qint64 len);

public:
  void add(const ObjectDesc& desc);
  unsigned count();
  const ObjectDesc& operator [] (unsigned index) const;
  ObjectDesc& operator [] (unsigned index);

public:
  const QSharedPointer<QObject> &getObject(unsigned refIndex);
  void deserializeObject(QDataStream& ds, const QSharedPointer<QObject>& object);
  bool deserializeObjects(QDataStream& ds);

private:
  QVector<ObjectDesc> m_objects;
  QIODevice* m_device;
  qint32 m_maxlen;
};

#endif // DESERIALIZERDEVICE_P_HPP
