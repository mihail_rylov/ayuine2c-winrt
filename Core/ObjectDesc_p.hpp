#ifndef OBJECTDESC_P_HPP
#define OBJECTDESC_P_HPP

#include <QByteArray>
#include <QSharedPointer>
#include <QObject>
#include <QDataStream>

const unsigned MagicValue = {0x00FF1122};
const unsigned MagicValueOther = {0x2211FF00};

struct ObjectDesc
{
  QByteArray className;
  unsigned refIndex;
  unsigned dataSize;
  QSharedPointer<QObject> sharedObject;

  ObjectDesc();

  friend QDataStream &operator<<(QDataStream &ds, const ObjectDesc &info);
  friend QDataStream &operator>>(QDataStream &ds, ObjectDesc &info);
};

#endif // OBJECTDESC_P_HPP
