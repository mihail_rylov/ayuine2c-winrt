//------------------------------------//
//
// Logger.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-8-31
//
//------------------------------------//

#pragma once

#include <QDebug>
#include "Core.hpp"

//------------------------------------//
// LogEvent structure
//
//! Struktura przechowująca informacje 
//! o logowanym zdarzeniu
//

struct LogEvent
{
  QtMsgType Level;
  float Time;
  QString Text;
  QString Hint;
};

//------------------------------------//
// log function
//
//! Funkcja logująca do domyślnego Loggera
//! umieszczonego w Core::Logger
//

template<QtMsgType level>
void log(const char* fmt, ...) {
  char buffer[500];
  va_list args;
  va_start(args, fmt);
  vsprintf(buffer, fmt, args);
  qt_message_output(level, buffer);
}

template<QtMsgType level>
void log(const QByteArray& text) {
  qt_message_output(level, text.constData());
}

template<QtMsgType level>
void log(const QString& text) {
  qt_message_output(level, qPrintable(text));
}

//------------------------------------//
// log macros

//! Debug Logger <see>LogLevel::llDebug</see>
#define debugf		::log<QtDebugMsg>

//! Info Logger <see>LogLevel::llInfo</see>
#define infof       ::log<QtSystemMsg>

//! Warning Logger <see>LogLevel::llWarning</see>
#define warningf	::log<QtWarningMsg>

//! Error Logger <see>LogLevel::llError</see>
#define errorf		::log<QtFatalMsg>

