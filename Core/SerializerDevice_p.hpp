#ifndef SERIALIZERDEVICE_P_HPP
#define SERIALIZERDEVICE_P_HPP

#include <QIODevice>

#include "ObjectDesc_p.hpp"

class BaseSerializerDevice : public QIODevice
{
  Q_OBJECT
public:
  virtual unsigned getObject(const QSharedPointer<QObject>& object) = 0;
};

class SerializerDevice : public BaseSerializerDevice
{
  Q_OBJECT
public:
  explicit SerializerDevice(QIODevice *device);  

public:
  qint64 pos() const;
  qint64 size() const;
  qint64 readData(char *data, qint64 maxlen);
  qint64 writeData(const char *data, qint64 len);

public:
  QVector<ObjectDesc> objects() const;
  unsigned count();
  const ObjectDesc& operator [] (unsigned index) const;
  ObjectDesc& operator [] (unsigned index);

public:
  unsigned getObject(const QSharedPointer<QObject>& object);
  void walkObjects(const QSharedPointer<QObject>& object);
  void serializeObjects();
  void serializeObject(QDataStream& ds, QSharedPointer<QObject> object);

private:
  QIODevice* m_device;
  QHash<QObject*, unsigned> m_refs;
  QHash<const QMetaObject*, QMap<QString, QObject*> > m_refsByName;
  QVector<ObjectDesc> m_objects;
  bool m_finished;
  qint64 m_written;
  qint64 m_total;
};

class NullSerializerDevice : public BaseSerializerDevice
{
  Q_OBJECT
public:
  NullSerializerDevice(BaseSerializerDevice* other);

public:
  qint64 readData(char *data, qint64 maxlen);
  qint64 writeData(const char *data, qint64 len);
  qint64 written() const;
  unsigned getObject(const QSharedPointer<QObject>& object);

private:
  BaseSerializerDevice* m_other;
  qint64 m_written;
};

#endif // SERIALIZERDEVICE_P_HPP
