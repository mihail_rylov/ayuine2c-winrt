#ifndef CLASS_HPP
#define CLASS_HPP

#include <QObject>
#include <QMetaType>
#include <QDataStream>
#include "Core.hpp"

class CORE_EXPORT Class : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString objectName READ objectName WRITE setObjectName)
public:
  Q_INVOKABLE explicit Class(QObject *parent = 0);

public:
  virtual void setObjectName(const QString &name);

signals:
  void objectNameChanged(const QString& newName, const QString& oldName);

public:
  static void serializeTree(QDataStream &ds, const Class *obj);
  static Class* deserializeTree(QDataStream &ds);

public:
  friend QDataStream &operator<<(QDataStream &ds, const Class *obj);
  friend QDataStream &operator>>(QDataStream &ds, Class *&obj);
};

Q_DECLARE_METATYPE(Class*)

#endif // CLASS_HPP
