#ifndef RESOURCEIMPORTER_HPP
#define RESOURCEIMPORTER_HPP

#include "Resource.hpp"

class ResourceImporter;

struct ResourceExtension
{
public:
  ResourceExtension()
  {
    importer = NULL;
    metaObject = NULL;
    isNative = false;
  }
  ResourceExtension(ResourceImporter* importer_)
  {
    importer = importer_;
    metaObject = NULL;
    isNative = false;
  }

public:
  operator bool () const
  {
    return importer != NULL;
  }
  bool operator ! () const
  {
    return importer == NULL;
  }

  QByteArray info() const
  {
    if(description.length())
      return description;

    if(metaObject)
      return metaObject->className();

    return "Resource";
  }

public:
  QByteArray extension;
  QByteArray description;
  ResourceImporter* importer;
  const QMetaObject* metaObject;
  bool isNative;
};

class CORE_EXPORT ResourceImporter
{
public:
  typedef QHash<QByteArray, ResourceExtension> ResourceExtensions;
  static ResourceExtensions& resourceImporters();
  static ResourceExtensions& resourceExporters();

public:
  ResourceImporter();
  ~ResourceImporter();

protected:
  bool registerImporter(const QByteArray& extension, const QMetaObject* metaObject = NULL, const QByteArray& description = QByteArray());
  bool registerNativeImporter(const QByteArray& extension, const QMetaObject* metaObject = NULL, const QByteArray& description = QByteArray());
  bool unregisterImporter(const QByteArray& extension);
  bool registerExporter(const QByteArray& extension, const QMetaObject* metaObject = NULL, const QByteArray& description = QByteArray());
  bool unregisterExporter(const QByteArray& extension);

public:
  static QStringList importExtensions(const QMetaObject* metaObject = NULL);
  static QString importFilters(const QMetaObject* metaObject = NULL);
  static QStringList exportExtensions(const QMetaObject* metaObject = NULL);
  static QString exportFilters(const QMetaObject* metaObject = NULL);

public:
  static bool isLoadable(const QString& fileName);
  static bool isLoadable(const QString& fileName, const QByteArray& extension);

public:
  static bool isNative(const QString& fileName);
  static bool isNative(const QString& fileName, const QByteArray& extension);

public:
  static bool isSaveable(const QString& fileName);
  static bool isSaveable(const QString& fileName, const QByteArray& extension);

public:
  static QSharedPointer<Resource> loadFromFile(const QString& fileName);
  static QSharedPointer<Resource> loadFromFile(const QString& fileName, const QByteArray& extension);
  static QSharedPointer<Resource> loadFromStream(const QSharedPointer<QIODevice>& io, const QByteArray& extension);

public:
  static const QMetaObject* metaObjectFromFile(const QString& fileName);
  static const QMetaObject* metaObjectFromFile(const QString& fileName, const QByteArray& extension);
  static const QMetaObject* metaObjectFromStream(const QSharedPointer<QIODevice>& io, const QByteArray& extension);

public:
  static bool saveToFile(QSharedPointer<Resource>& resource, const QString& fileName);
  static bool saveToFile(QSharedPointer<Resource>& resource, const QString& fileName, const QByteArray& extension);
  static bool saveToStream(QSharedPointer<Resource>& resource, const QSharedPointer<QIODevice>& io, const QByteArray& extension);

protected:
  virtual QSharedPointer<Resource> load(const QSharedPointer<QIODevice>& device, const QByteArray& extension) = 0;
  virtual const QMetaObject* metaObject(const QSharedPointer<QIODevice>& device, const QByteArray& extension);
  virtual bool save(QSharedPointer<Resource>& resource, const QSharedPointer<QIODevice>& device, const QByteArray& extension);
};

#endif // RESOURCEIMPORTER_HPP
