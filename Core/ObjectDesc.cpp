#include "ObjectDesc_p.hpp"

ObjectDesc::ObjectDesc()
{
  refIndex = 0;
  dataSize = 0;
}

QDataStream &operator<<(QDataStream &ds, const ObjectDesc &info)
{
  QHash<QByteArray, QByteArray> values;
  values["className"] = info.className;
  values["dataSize"] = QString::number(info.dataSize).toLatin1();
  ds << values;
  return ds;
}

QDataStream &operator>>(QDataStream &ds, ObjectDesc &info)
{
  QHash<QByteArray, QByteArray> values;
  ds >> values;
  info.className = values["className"];
  info.dataSize = values["dataSize"].toUInt();
  return ds;
}
