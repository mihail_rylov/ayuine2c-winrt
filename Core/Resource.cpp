#include "Resource.hpp"
#include "Logger.hpp"
#include "ResourceImporter.hpp"
#include <QHash>
#include <QFileInfo>
#include <QVariant>
#include <QSettings>

// Q_REGISTER_METATYPE(ResourceRef)

static QSettings* globalSettings;

QSettings& getSettings()
{
  if(globalSettings)
    return *globalSettings;

  static QSettings settings("ayufan", "ayuine2c");
  return settings;
}

void setSettings(QSettings* settings)
{
  globalSettings = settings;
}

QHash<QString, QWeakPointer<Resource> >& Resource::resources()
{
  static QHash<QString, QWeakPointer<Resource> > resources;
  return resources;
}

Resource::Resource()
{
  m_isLocal = false;
}

Resource::~Resource()
{
  if(m_fileName.length())
  {
    resources().remove(m_fileName);
  }
}

QString Resource::fileName() const
{
  return m_fileName;
}

void Resource::setFileName(const QString &fileName)
{
  if(m_fileName != fileName)
  {
    resources().remove(m_fileName);

    m_fileName = fileName;

    if(fileName.size())
    {
      resources()[m_fileName] = QWeakPointer<Resource>(this).toStrongRef();
    }

    emit updated(this);
  }
}

bool Resource::isLocal() const
{
  if(m_isLocal || m_fileName.length() == 0)
    return true;
  if(m_fileName.startsWith(':'))
    return false;
  if(m_fileName.startsWith(getResourceDir().absolutePath(), Qt::CaseInsensitive))
    return !ResourceImporter::isNative(m_fileName);
  return true;
}

void Resource::setLocal(bool local)
{
  if(m_isLocal == local)
    return;
  m_isLocal = local;
}

void Resource::finish()
{
}

void Resource::setObjectName(const QString &name)
{
  if(objectName() != name)
  {
    QString oldName(objectName());
    QObject::setObjectName(name);
    emit objectNameChanged(this, oldName);
    emit updated(this);
  }
}

QSharedPointer<Resource> Resource::load(const QString &name)
{
  QString absoluteFileName = absoluteFilePath(name);
  absoluteFileName = QDir::cleanPath(absoluteFileName);

  if(resources().contains(absoluteFileName))
  {
    if(QWeakPointer<Resource> resource = resources()[absoluteFileName])
      return resource.toStrongRef();
    resources().remove(absoluteFileName);
  }

  if(QSharedPointer<Resource> resource = ResourceImporter::loadFromFile(absoluteFileName))
  {
    qCritical() << "[RES] Loaded " << resource->metaObject()->className() << " from " << name;
    if(resource->objectName().isEmpty())
      resource->setObjectName(QFileInfo(absoluteFileName).baseName());
    resource->setFileName(absoluteFileName);
    return resource;
  }

  qWarning() << "[RES] Failed to load " << name;

  return QSharedPointer<Resource>();
}

QSharedPointer<Resource> Resource::load(const QString &name, const QString &otherFileName)
{
  if(name.isEmpty())
    return QSharedPointer<Resource>();

  if(otherFileName.isEmpty())
      return load(name);

  QSharedPointer<Resource> resource;

#if 0
  resource = load(name);

  if(resource)
  {
    return resource;
  }
#endif

  QFileInfo otherFileInfo(absoluteFilePath(otherFileName));
  if(!otherFileInfo.exists())
    return QSharedPointer<Resource>();

  QDir otherDir = otherFileInfo.absoluteDir();

  do
  {
    if(otherDir.exists(name))
    {
      resource = load(otherDir.absoluteFilePath(name));

#if 0
      if(resource && resource->isLocal())
      {
        resource->saveFile(name);
      }
#endif

      return resource;
    }
  }
  while(otherDir.cdUp());

  return QSharedPointer<Resource>();
}

bool Resource::saveFile(const QString& fileName)
{
  if(fileName.isEmpty())
  {
    qWarning() << "[RES] Failed to save " << objectName() << ": FileName is empty";
    return false;
  }

  QString absoluteFileName = absoluteFilePath(fileName);

  QSharedPointer<Resource> resource(QWeakPointer<Resource>(this).toStrongRef());
  if(!resource)
    return false;

  if(ResourceImporter::saveToFile(resource, absoluteFileName))
  {
    qCritical() << "[RES] Saved " << absoluteFileName;
    setFileName(absoluteFileName);
    return true;
  }

  if(isLocal())
  {
    QFileInfo info(absoluteFileName);
    QString newFileName(info.path() + "/" + info.completeBaseName() + ".grcz");
    newFileName = QDir::cleanPath(newFileName);

    if(ResourceImporter::saveToFile(resource, newFileName))
    {
      qCritical() << "[RES] Saved " << absoluteFileName << " as " << newFileName;
      setFileName(newFileName);
      return true;
    }
  }

  qWarning() << "[RES] Write to failed " << fileName;
  return false;
}

bool Resource::saveFile()
{
  return saveFile(fileName());
}

bool Resource::isInternal() const
{
  return !m_internal.isNull();
}

void Resource::setInternal(bool internal)
{
  if(isInternal() != internal)
  {
    if(internal)
    {
      m_internal = QWeakPointer<Resource>(this).toStrongRef();
    }
    else
    {
      m_internal.clear();
    }
  }
}

QDir & Resource::getResourceDir()
{
  static QDir dir = QDir::currentPath();
  return dir;
}

QString Resource::relativeName() const
{
  return getResourceDir().relativeFilePath(fileName());
}

QString Resource::absoluteFilePath(const QString &filePath)
{
  QString absoluteFilePath = QDir::isAbsolutePath(filePath) ? filePath : getResourceDir().absoluteFilePath(filePath);
  absoluteFilePath = QDir::fromNativeSeparators(absoluteFilePath);
  absoluteFilePath = QDir::cleanPath(absoluteFilePath);
  return absoluteFilePath;
}

QString Resource::relativeFilePath(const QString &filePath)
{
  QString relativeFilePath = getResourceDir().relativeFilePath(filePath);
  relativeFilePath = QDir::fromNativeSeparators(relativeFilePath);
  relativeFilePath = QDir::cleanPath(relativeFilePath);
  return relativeFilePath;
}
