#ifndef CORE_HPP
#define CORE_HPP

#include <QtCore>

#ifdef CORE_EXPORTS
#define CORE_EXPORT Q_DECL_EXPORT
#else
#define CORE_EXPORT Q_DECL_IMPORT
#endif

class QSettings;

CORE_EXPORT QSettings& getSettings();
CORE_EXPORT void setSettings(QSettings* settings);

inline QDebug qFatal() { return QDebug(QtFatalMsg); }

#endif // CORE_HPP
