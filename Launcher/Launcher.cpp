#include <Editor/CoreFrames/ResourceBrowser.hpp>
#include <Editor/CoreFrames/ResourceEditor.hpp>
#include <Editor/CoreFrames/LogView.hpp>
#include <Core/Resource.hpp>
#include <Render/RenderSystem.hpp>
#include <GameSystem/Game.hpp>
#include <GameSystem/TimeDemoGame.hpp>
#include <QInputDialog>
#include <QMessageBox>
#include <QApplication>

static bool beginGame(Game& game, QSettings& settings, QSize* size = NULL)
{
  QString sceneFileName = settings.value("Game/File", "TranquilityLane.grcz").toString();
  QSharedPointer<Scene> scene = Scene::load<Scene>(sceneFileName);

  if(!scene)
  {
      qCritical() << "[GAME] Failed to load scene: " << sceneFileName;
	  QMessageBox::critical(NULL, "Launcher", QString("Couldn't load scene %1").arg(sceneFileName), QMessageBox::Ok, QMessageBox::NoButton);
      return false;
  }

  QString fixedQuality = settings.value("Render/FixedQuality").toString();
  if(fixedQuality.length())
    MaterialSystem::setFixedQuality((MaterialSystem::Quality)MaterialSystem::staticMetaObject.enumerator(MaterialSystem::staticMetaObject.indexOfEnumerator("Quality")).keyToValue(fixedQuality.toAscii()));

  if(settings.value("Render/AskForResolution", true).toBool() && !size)
  {
      if(!game.askForResolution())
          return false;
  }
  else
  {
    if(size)
      game.setFixedSize(*size);
    else
      game.setFixedSize(settings.value("Render/Width", 1024).toInt(),
                    settings.value("Render/Height", 768).toInt());

    if(settings.value("Render/Windowed", true).toBool())
        game.show();
    else
        game.showFullScreen();
  }

  return game.beginGame(scene);
}

static void runTimeDemo(QApplication& app, QSettings& settings, QSize* size = NULL)
{
  TimeDemoGame game;

  game.setFrameTime(settings.value("TimeDemo/FrameTime", 20.0f / 1000.0f).toFloat());
  game.setMaxFrames(settings.value("TimeDemo/MaxFrames", 10000).toInt());

  if(beginGame(game, settings, size))
  {
    game.run();
    game.endGame();

    qCritical() << "[TIMEDEMO] Frames: " << game.frameTimes().count();
    qCritical() << "[TIMEDEMO] Min Frame Time:" << game.minFrameTime() * 1000.0f << "ms";
    qCritical() << "[TIMEDEMO] Max Frame Time:" << game.maxFrameTime() * 1000.0f << "ms";
    qCritical() << "[TIMEDEMO] Average Frame Time:" << game.avgFrameTime() * 1000.0f << "ms";
    qCritical() << "[TIMEDEMO] Mean Frame Time:" << game.meanFrameTime() * 1000.0f << "ms";
    qCritical() << "[TIMEDEMO] StdDev Frame Time:" << game.stdDevFrameTime() * 1000.0f << "ms";

    QString dumpFrameTimes = settings.value("TimeDemo/DumpFrameTimes").toString();

    if(dumpFrameTimes.length())
    {
      dumpFrameTimes.replace("%f", game.isFullScreen() ? "fullscreen" : "windowed");
      dumpFrameTimes.replace("%w", QString::number(game.width()));
      dumpFrameTimes.replace("%h", QString::number(game.height()));

      QFile file(dumpFrameTimes);

      if(file.open(QFile::WriteOnly))
      {
        file.write("minFrameTime;maxFrameTime;avgFrameTime;meanFrameTime;stdDevFrameTime;\n");
        file.write(QString("%1;%2;%3;%4;%5;\n")
                   .arg(game.minFrameTime() * 1000.0f)
                   .arg(game.maxFrameTime() * 1000.0f)
                   .arg(game.avgFrameTime() * 1000.0f)
                   .arg(game.meanFrameTime() * 1000.0f)
                   .arg(game.stdDevFrameTime() * 1000.0f).toAscii());
        file.write("\n");

        file.write("frameTime;fragmentCount;lightCount;drawCount;primCount;\n");

        foreach(const TimeDemoStats& stats, game.frameTimes())
        {
          file.write(QString("%1;%2;%3;%4;%5;\n")
                     .arg(stats.frameTime*1000.0)
                     .arg(stats.fragmentCount)
                     .arg(stats.lightCount)
                     .arg(stats.drawCount)
                     .arg(stats.primCount).toAscii());
        }
      }
    }
  }
}

static int runConfig(QApplication& app, QString configName = QString())
{
    if(configName.length())
    {
        if(!QFile::exists(configName))
        {
            QMessageBox::critical(NULL, "Launcher", QString("Couldn't load config %1").arg(configName), QMessageBox::Ok, QMessageBox::NoButton);
            return -1;
        }
    }
    else
    {
        configName = "Launcher.ini";
    }

    QSettings settings(configName, QSettings::IniFormat);
    setSettings(&settings);

    // setup resource dir
    Resource::getResourceDir().setPath(settings.value("Resources/DataPath", "data").toString());
    Resource::getResourceDir().makeAbsolute();

    // force initialize
    getRenderSystem().maxInstances();

    QString launcherMode = settings.value("Launcher/Mode", "Editor").toString();

    // run editor
    if(launcherMode == "Editor")
    {
      ResourceBrowser resourceBrowser;

      settings.beginGroup("EditorPlugins");

      foreach(QString key, settings.allKeys())
      {
          if(settings.value(key, true).toBool())
          {
              QScopedPointer<QLibrary> newLibrary(new QLibrary(key, &resourceBrowser));

              if(newLibrary->load())
              {
                  qDebug() << "[PLUGIN] Successfully loaded:" << key;
                  newLibrary.take();
              }
              else
              {
                  qWarning() << "[PLUGIN] Failed to load:" << key;
              }
          }
      }

      settings.endGroup();

      QString fixedQuality = settings.value("Render/FixedQuality").toString();
      if(fixedQuality.length())
        MaterialSystem::setFixedQuality((MaterialSystem::Quality)MaterialSystem::staticMetaObject.enumerator(MaterialSystem::staticMetaObject.indexOfEnumerator("Quality")).keyToValue(fixedQuality.toAscii()));

      resourceBrowser.show();

      QString openFileName = settings.value("Editor/Open", "").toString();

      if(openFileName.length())
      {
        ResourceEditor* resourceEditor = ResourceEditor::findOrOpenEditor(openFileName);

        if(resourceEditor)
        {
          resourceEditor->show();
          resourceEditor->activateWindow();
        }
      }

      return app.exec();
    }
    else if(launcherMode == "TimeDemoAuto")
    {
      LogView logView;
      logView.show();

      QString resolutions = settings.value("TimeDemo/Resolutions").toString();
      if(resolutions.isEmpty())
        resolutions = "640x480,800x600,1024x768,1280x1024,1680x1050,1920x1080";

      qDebug() << "[TIMEDEMO] Resolutions " << resolutions.split(',');

      if(resolutions.length())
      {
        foreach(QString res, resolutions.split(','))
        {
          QStringList wh = res.split('x');
          if(wh.size() != 2)
            continue;

          QSize size(wh[0].toInt(), wh[1].toInt());
          qCritical() << "[TIMEDEMO] Starting for " << size;
          runTimeDemo(app, settings, &size);
        }
      }
      else
      {
        runTimeDemo(app, settings);
      }

      if(logView.isVisible())
      {
          return app.exec();
      }
    }
    else if(launcherMode == "TimeDemo")
    {
      LogView logView;
      logView.show();

      runTimeDemo(app, settings);

      if(logView.isVisible())
      {
          return app.exec();
      }
    }
    else if(launcherMode == "Game")
    {
        Game game;

        if(beginGame(game, settings))
        {
          game.run();
          game.endGame();
        }
    }
    return 0;
}


int main(int argc, char* argv[])
{
  QApplication app(argc, argv);

  // run selected config
  if(argc == 2)
      return runConfig(app, argv[1]);

  QStringList configFiles = QDir(".").entryList(QDir::nameFiltersFromString("*.ini"), QDir::Files);

  // run just one config
  if(configFiles.length() == 1)
  {
      return runConfig(app, configFiles[0]);
  }

  // select config from list
  else if(configFiles.length() > 1)
  {
      QStringList configNames;

      foreach(QString config, configFiles)
      {
          QSettings settings(config, QSettings::IniFormat);
          QString configName = settings.value("Launcher/Name", QFileInfo(config).baseName()).toString();
          configNames.append(configName);
      }

      bool ok = false;
      QString configName = QInputDialog::getItem(NULL, "Launcher", "Select config:", configNames, 0, false, &ok);
      if(configName.isEmpty() || !ok)
          return 0;

      return runConfig(app, configFiles[configNames.indexOf(configName)]);
  }

  // run default one
  return runConfig(app);
}
