TEMPLATE = lib
TARGET = Sound

DEPLOYFILES += ../lib/dll/ogg.dll \
    ../lib/dll/OpenAL32.dll \
    ../lib/dll/vorbis.dll \
    ../lib/dll/vorbisenc.dll \
    ../lib/dll/vorbisfile.dll \
    ../lib/dll/wrap_oal.dll

include(../ayuine2c.pri)

HEADERS += MusicSource.hpp \
    SoundSource.hpp \
    SoundSample.hpp \
    SoundEngine.hpp \
    Sound.hpp

SOURCES += MusicSource.cpp \
    SoundSource.cpp \
    SoundSample.cpp \
    SoundEngine.cpp \
    Sound.cpp

LIBS += -lalut -logg -lvorbis -lvorbisenc -lvorbisfile -lopenal32 -lCore -lMath
