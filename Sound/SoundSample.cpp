//------------------------------------//
// 
// SoundSample.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
// 
//------------------------------------//

#include "SoundSample.hpp"
#include "SoundEngine.hpp"
#include <Core/Logger.hpp>
#include <al.h>
#include <alut.h>
#include <alc.h>

//------------------------------------//
// SoundSample: Constructor

SoundSample::SoundSample() {
	m_buffer = -1;
}

SoundSample::SoundSample(const QByteArray &data)
{
  m_data = data;
  finish();
}

//------------------------------------//
// SoundSample: Destructor

SoundSample::~SoundSample()
{
    if(m_buffer != ~0U)
    {
		// Znisz bufor
		alDeleteBuffers(1, &m_buffer);
	}
}

//------------------------------------//
// SoundSample: Methods

unsigned SoundSample::frequency() {
	int value;

	// Pobierz i zwr�� cz�stotliwo�� d�wi�ku
	ale(alGetBufferi(m_buffer, AL_FREQUENCY, &value));
	return value;
}

unsigned SoundSample::bits() {
	int value;

	// Pobierz i zwr�� ilo�� bit�w
	ale(alGetBufferi(m_buffer, AL_BITS, &value));
	return value;
}

unsigned SoundSample::channels() {
	int value;

	// Pobierz i zwr�� ilo�� kana��w
	ale(alGetBufferi(m_buffer, AL_CHANNELS, &value));
	return value;
}

unsigned SoundSample::size() {
	int value;

	// Pobierz i zwr�� rozmiar pr�bki
	ale(alGetBufferi(m_buffer, AL_SIZE, &value));
	return value;
}

float SoundSample::length() {
	return float(size() * 8) / float(frequency() * bits() * channels());
}
