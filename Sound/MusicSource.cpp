//------------------------------------//
//
// MusicSource.cpp
//
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
//
//------------------------------------//

#include "MusicSource.hpp"
#include "SoundEngine.hpp"
#include <Core/Resource.hpp>
#include <al.h>
#include <codec.h>
#include <vorbisenc.h>
#include <vorbisfile.h>

//------------------------------------//
// ovCallbacksImpl structure

struct ovCallbacksImpl : ov_callbacks
{
  // Constructor
public:
  ovCallbacksImpl() {
    // Ustaw funkcj�
    reinterpret_cast<void*&>(read_func) = (void*)(read);
    reinterpret_cast<void*&>(seek_func) = (void*)(seek);
    reinterpret_cast<void*&>(close_func) = (void*)(close);
    reinterpret_cast<void*&>(tell_func) = (void*)(tell);
  }

  // Functions
private:
  static size_t read(void* ptr, size_t s, size_t nmemb, QIODevice* file) {
    if(file)
      return -1;
    qint64 size = file->read((char*)ptr, s * nmemb);
    return size / s;
  }
  static int seek(QIODevice* file, ogg_int64_t offset, int whence) {
    if(!file)
      return -1;
    // Ustaw pozycj� w pliku
    switch(whence) {
    case SEEK_CUR:
      file->seek(file->pos() + offset);
      break;

    case SEEK_SET:
      file->seek(offset);
      break;

    case SEEK_END:
      file->seek(file->size());
      break;
    }
    return 0;
  }
  static int close(QIODevice* file) {
    // Plik zamkniemy r�cznie
    return 0;
  }
  static long tell(QIODevice* file) {
    if(!file)
      return -1;
    // Pobierz pozycj� w pliku
    return file->pos();
  }
};

//------------------------------------//
// MusicSource: Consts

const int MusicBufferSize = 32 * 4096;
const int MusicBufferCount = 4;

//------------------------------------//
// MusicSource: Constructor

MusicSource::MusicSource(const QString& name) : m_source(-1), m_info(NULL), m_loop(true)
{
  m_oggFile = new OggVorbis_File;

  // Wczytaj plik
  m_file.setFileName(Resource::absoluteFilePath(name));
  m_file.open(QFile::ReadOnly);

  // Za�aduj nowy plik
  if(ov_open_callbacks(&m_file, m_oggFile, NULL, 0, ovCallbacksImpl()) != 0)
    Q_ASSERT(false && "failed to open file");

  // Pobierz parametry �cie�ki
  m_info = ov_info(m_oggFile, -1);

  // Wygeneruj �r�d�o muzyczne
  ale(alGenSources(1, &m_source));

  // Ustaw parametry �r�d�a
  ale(alSource3f(m_source, AL_POSITION,        0.0, 0.0, 0.0));
  ale(alSource3f(m_source, AL_VELOCITY,        0.0, 0.0, 0.0));
  ale(alSource3f(m_source, AL_DIRECTION,       0.0, 0.0, 0.0));
  ale(alSourcef	(m_source, AL_ROLLOFF_FACTOR,  0.0));
  ale(alSourcei	(m_source, AL_SOURCE_RELATIVE, AL_TRUE));
  ale(alSourcei	(m_source, AL_LOOPING,				 FALSE));

  // Utw�rz bufory
  m_buffers.resize(MusicBufferCount);
  ale(alGenBuffers(m_buffers.size(), &m_buffers[0]));
}

//------------------------------------//
// MusicSource: Destructor

MusicSource::~MusicSource() {
  // Zamknij ogg
  if(m_info) {
    ov_clear(m_oggFile);
    m_info = NULL;
  }

  // Zniszcz �r�d�o
  if(m_source != ~0U)
  {
    // Zatrzymaj odtwarzanie
    ale(alSourceStop(m_source));

    // Usu� �r�d�o
    ale(alDeleteSources(1, &m_source));
  }

  // Usu� bufory
  if(m_buffers.size()) {
    ale(alDeleteBuffers(m_buffers.size(), &m_buffers[0]));
    m_buffers.clear();
  }

  delete m_oggFile;
}

//------------------------------------//
// MusicSource: Methods

bool MusicSource::update(unsigned buffer) {
  Q_ASSERT(m_info);

  char data[MusicBufferSize];
  size_t size = 0;
  int result, section;

  // Wczytuj dop�ki nie wype�nimy ca�ego bufora
  while(size < sizeof(data)) {
    // Wczytaj dane do podr�cznego bufora
    result = ov_read(m_oggFile, data + size, sizeof(data) - (int)size, 0, 2, 1, &section);

    // Sprawd� ile danych wczytano
    if(result > 0)
      size += result;
    else {
      if(result < 0) {
        // TODO : Dok�adna informacja o b��dzie zwracana przez modu� do wczytywania ogg
        return false;
      }
      else {
        // Sprawd�, czy mamy przewin�c bufor do pocz�tku
        if(m_loop)
          ov_raw_seek(m_oggFile, 0);
        else
          break;
      }
    }
  }

  // Je�li nie wczytano �adnych danych zwr�� b�ad
  if(size == 0)
    return false;

  // Uaktualnij dane w buforze
  ale(alBufferData(buffer, m_info->channels == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16, data, (ALsizei)size, m_info->rate));
  return true;
}

void MusicSource::update()
{
  Q_ASSERT(m_info);

  ALint left = 0;
  ALuint buffer = 0;

  // Pobierz ilo�� przetworzonych bufor�w
  ale(alGetSourcei(m_source, AL_BUFFERS_PROCESSED, &left));

  // Za�aduj nowe dane do bufor�w
  while(left-- > 0) {
    // Pobierz numer bufora
    ale(alSourceUnqueueBuffers(m_source, 1, &buffer));

    // Zapisz nowe dane do bufora
    if(update(buffer)) {
      // Dodaj bufor ponownie do listy
      ale(alSourceQueueBuffers(m_source, 1, &buffer));
    }
  }
}

void MusicSource::play() {
  if(stopped()) {
    ALint buffers = 0;
    ALuint buffer;

    // Zdejmij buforki
    ale(alGetSourcei(m_source, AL_BUFFERS_QUEUED, &buffers));
    while(buffers--)
      ale(alSourceUnqueueBuffers(m_source, 1, &buffer));


    // Wystartuj od 0
    ov_raw_seek(m_oggFile, 0);

    // Prze�aduj do nich dane
    foreach(unsigned itor, m_buffers)
      if(!update(itor))
        break;


    // Skojarz je z �r�d�em
    ale(alSourceQueueBuffers(m_source, m_buffers.size(), m_buffers.constData()));
  }

  ale(alSourcePlay(m_source));
}

void MusicSource::pause() {
  ale(alSourcePause(m_source));
}

void MusicSource::stop() {
  ale(alSourceStop(m_source));
}

bool MusicSource::paused() const {
  ALint	sourceState;
  ale(alGetSourcei(m_source, AL_SOURCE_STATE, &sourceState));
  return sourceState == AL_PAUSED;
}

bool MusicSource::stopped() const {
  ALint	sourceState;
  ale(alGetSourcei(m_source, AL_SOURCE_STATE, &sourceState));
  return sourceState == AL_STOPPED || sourceState == AL_INITIAL;
}

float MusicSource::gain() const {
  float gain;
  ale(alGetSourcef(m_source, AL_GAIN, &gain));
  return gain;
}

void MusicSource::setGain(float gain) {
  ale(alSourcef(m_source, AL_GAIN, gain));
}

bool MusicSource::loop() const {
  return m_loop;
}

void MusicSource::setLoop(bool loop) {
  m_loop = loop;
}
