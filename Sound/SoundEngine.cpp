#include "SoundEngine.hpp"
#include <Core/Logger.hpp>
#include <al.h>
#include <alut.h>
#include <alc.h>

SoundEngine& getSoundEngine() {
    static SoundEngine self;
    return self;
}

//------------------------------------//
// SoundEngine: Constructor

SoundEngine::SoundEngine() : m_device(NULL), m_context(NULL) {
}

//------------------------------------//
// SoundEngine: Destructor

SoundEngine::~SoundEngine() {
    release();
}

//------------------------------------//
// SoundEngine: Methods

void SoundEngine::init() {
    release();

    infof("SoundEngine: Initializing...");

    // Otw�rz urz�dzenie
    m_device = alcOpenDevice("DirectSoundEngine3D");
    Q_ASSERT(m_device);

    // Utw�rz kontekst
    m_context = alcCreateContext(m_device, NULL);
    if(!m_context) {
        // Zamknij urz�dzenie
        alcCloseDevice(m_device);
        Q_ASSERT(m_context);
    }

    // Ustaw kontekst
    alcMakeContextCurrent(m_context);

    // Wy�wietl informacje o obs�udze d�wi�ku
    infof("OpenAL: Vender = %s", alGetString(AL_VENDOR));
    infof("OpenAL: Version = %s", alGetString(AL_VERSION));
    infof("OpenAL: Renderer = %s", alGetString(AL_RENDERER));
    infof("OpenAL: Extensions = %s", alGetString(AL_EXTENSIONS));
}

void SoundEngine::release() {
    if(!m_context && !m_device)
        return;

    infof("SoundEngine: Releasing...");

    if(m_context) {
        // Wy��cz kontekst
        alcMakeContextCurrent(NULL);

        // Zwolnij kontekst
        alcDestroyContext(m_context);
        m_context = NULL;
    }

    if(m_device) {
        // Zwolnij urz�dzenie
        alcCloseDevice(m_device);
        m_device = NULL;
    }
}

void SoundEngine::setObserver(const vec3& origin, const vec3& direction, const vec3& velocity) {
    // Ustaw parametry s�uchacza
    ale(alListenerfv(AL_POSITION, &origin.X));
    ale(alListenerfv(AL_VELOCITY, &velocity.X));
    ale(alListenerfv(AL_DIRECTION, &direction.X));
}

void SoundEngine::checkErrorState()
{
    unsigned err;
    if((err = alGetError()) != AL_NO_ERROR)
    {
      errorf("alGetError() = %i", err);
    }
}
