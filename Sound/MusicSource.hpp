//------------------------------------//
// 
// Music.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
// 
//------------------------------------//

#pragma once

#include <QObject>
#include <QString>
#include <QSharedPointer>
#include <QVector>
#include <QIODevice>
#include <QFile>
#include "Sound.hpp"

typedef struct vorbis_info vorbis_info;
typedef struct OggVorbis_File OggVorbis_File;

class SOUND_EXPORT MusicSource : public QObject
{
  Q_OBJECT

  // Fields
private:
  //! Indeks �r�d�a
  unsigned m_source;

  //! Bufory d�wi�kowe
  QVector<unsigned> m_buffers;

  //! Uchwyt do pliku
  QFile m_file;

  //! Opis pliku
  vorbis_info* m_info;
  OggVorbis_File* m_oggFile;

  //! Zap�tlanie
  bool m_loop;

  // Constructor
public:
  MusicSource(const QString& name);

  // Destructor
public:
  virtual ~MusicSource();

  // Methods
protected:
  bool update(unsigned buffer);

public:
  //! W��cza odtwarzanie
  void play();

  //! Pauzuje odtwarzanie
  void pause();

  //! Wy��cza odtwarzanie
  void stop();

  //! Zatrzymane?
  bool paused() const;

  //! Wy��czone
  bool stopped() const;

  //! Zwraca g�osno��
  float gain() const;

  //! Ustawia g�osno��
  void setGain(float gain);

  //! Zwraca zap�tlanie
  bool loop() const;

  //! W�acza zap�tlanie
  void setLoop(bool loop);

  //! Uaktualnia �cie�k�
  void update();
};
