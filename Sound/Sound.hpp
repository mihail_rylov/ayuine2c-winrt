#ifndef SOUND_HPP
#define SOUND_HPP

#include <QtCore>

#ifdef SOUND_EXPORTS
#define SOUND_EXPORT Q_DECL_EXPORT
#else
#define SOUND_EXPORT Q_DECL_IMPORT
#endif

#endif // SOUND_HPP
