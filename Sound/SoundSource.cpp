//------------------------------------//
// 
// SoundSource.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
// 
//------------------------------------//

#include "SoundSource.hpp"
#include "SoundSample.hpp"
#include "SoundEngine.hpp"
#include <Core/Logger.hpp>
#include <al.h>
#include <alc.h>
#include <alut.h>

//------------------------------------//
// SoundSource: Constructor

SoundSource::SoundSource(const QSharedPointer<SoundSample>& sample) : m_source(~0), m_sample(sample)
{
    Q_ASSERT(sample);

	// Utw�rz nowe �r�d�o
	ale(alGenSources(1, &m_source));

	// Ustaw bufor
	ale(alSourcei(m_source, AL_BUFFER, m_sample->buffer()));
}

//------------------------------------//
// SoundSource: Destructor

SoundSource::~SoundSource()
{
    if(m_source != ~0U)
    {
		// Zniszcz �r�d�o
		ale(alDeleteSources(1, &m_source));
	}
}

//------------------------------------//
// SoundSource: Methods

void SoundSource::play() {
	// W��cz �r�d�o
	ale(alSourcePlay(m_source));
}

void SoundSource::pause() {
	// Zatrzymaj �r�d�o
	ale(alSourcePause(m_source));
}

void SoundSource::stop() {
	// Wy��cz �r�d�o
	ale(alSourceStop(m_source));
}

void SoundSource::rewind() {
	// Przewi� �r�d�o
	ale(alSourceRewind(m_source));
}

bool SoundSource::stopped() {
	int value;

	// Pobierz stan �r�d�a
	ale(alGetSourcei(m_source, AL_SOURCE_STATE, &value));
	return value == AL_STOPPED || value == AL_INITIAL;
}

bool SoundSource::paused() {
	int value;

	// Pobierz stan �r�d�a
	ale(alGetSourcei(m_source, AL_SOURCE_STATE, &value));
	return value == AL_PAUSED;
}

float SoundSource::length() {
	// D�ugo�� �r�d�a
	return m_sample ? m_sample->length() : -1.0f;
}

//------------------------------------//
// SoundSource: Source methods

float SoundSource::pitch() const {
	float value;
	ale(alGetSourcef(m_source, AL_PITCH, &value));
	return value;
}

float SoundSource::gain() const {
	float value;
	ale(alGetSourcef(m_source, AL_GAIN, &value));
	return value;
}

float SoundSource::gainMin() const {
	float value;
	ale(alGetSourcef(m_source, AL_MIN_GAIN, &value));
	return value;
}

float SoundSource::gainMax() const {
	float value;
	ale(alGetSourcef(m_source, AL_MAX_GAIN, &value));
	return value;
}

float SoundSource::distance() const {
	float value;
	ale(alGetSourcef(m_source, AL_MAX_DISTANCE, &value));
	return value;
}


float SoundSource::coneOuterGain() const {
	float value;
	ale(alGetSourcef(m_source, AL_CONE_OUTER_GAIN, &value));
	return value;
}

float SoundSource::coneInnerAngle() const {
	float value;
	ale(alGetSourcef(m_source, AL_CONE_INNER_ANGLE, &value));
	return value;
}


float SoundSource::coneOuterAngle() const {
	float value;
	ale(alGetSourcef(m_source, AL_CONE_OUTER_ANGLE, &value));
	return value;
}

const vec3 &SoundSource::origin() const {
	ale(alGetSourcefv(m_source, AL_POSITION, &m_origin.X));
	return m_origin;
}

const vec3 &SoundSource::velocity() const {
	ale(alGetSourcefv(m_source, AL_VELOCITY, &m_velocity.X));
	return m_velocity;
}

const vec3 &SoundSource::direction() const {
	ale(alGetSourcefv(m_source, AL_DIRECTION, &m_direction.X));
	return m_direction;
}

bool SoundSource::looping() const {
	int value;
	ale(alGetSourcei(m_source, AL_LOOPING, &value));
	return value == AL_TRUE;
}

bool SoundSource::relative() const {
	int value;
	ale(alGetSourcei(m_source, AL_SOURCE_RELATIVE, &value));
	return value == AL_TRUE;
}

void SoundSource::setPitch(float value) {
	ale(alSourcef(m_source, AL_PITCH, value));
}

void SoundSource::setGain(float value) {
	ale(alSourcef(m_source, AL_GAIN, value));
}

void SoundSource::setGainMin(float value) {
	ale(alSourcef(m_source, AL_MIN_GAIN, value));
}

void SoundSource::setGainMax(float value) {
	ale(alSourcef(m_source, AL_MAX_GAIN, value));
}

void SoundSource::setDistance(float value) {
	ale(alSourcef(m_source, AL_MAX_DISTANCE, value));
}

void SoundSource::setConeOuterGain(float value) {
	ale(alSourcef(m_source, AL_CONE_OUTER_GAIN, value));
}

void SoundSource::setConeInnerAngle(float value) {
	ale(alSourcef(m_source, AL_CONE_INNER_ANGLE, value));
}

void SoundSource::setConeOuterAngle(float value) {
	ale(alSourcef(m_source, AL_CONE_OUTER_ANGLE, value));
}

void SoundSource::setOrigin(const vec3 &value) {
	ale(alSourcefv(m_source, AL_POSITION, &value.X));
}

void SoundSource::setVelocity(const vec3 &value) {
	ale(alSourcefv(m_source, AL_VELOCITY, &value.X));
}

void SoundSource::setDirection(const vec3 &value) {
	ale(alSourcefv(m_source, AL_DIRECTION, &value.X));
}

void SoundSource::setLooping(bool value) {
	ale(alSourcei(m_source, AL_LOOPING, value ? AL_TRUE : AL_FALSE));
}

void SoundSource::setRelative(bool value) {
	ale(alSourcei(m_source, AL_SOURCE_RELATIVE, value ? AL_TRUE : AL_FALSE));
}

