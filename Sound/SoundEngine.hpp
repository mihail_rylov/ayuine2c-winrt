#ifndef SOUNDENGINE_HPP
#define SOUNDENGINE_HPP

#include <QtGlobal>
#include <Math/Vec3.hpp>
#include "Sound.hpp"

typedef struct ALCdevice_struct ALCdevice;
typedef struct ALCcontext_struct ALCcontext;

class SOUND_EXPORT SoundEngine
{
    // Fields
private:
    ALCdevice* m_device;
    ALCcontext* m_context;

    // Constructor
public:
    SoundEngine();

    // Destructor
public:
    virtual ~SoundEngine();

    // Operators
public:
    operator bool () const {
        return m_device != NULL;
    }

    // Methods
public:
    //! Iniciuj podsystem d�wi�kowy
    void init();

    //! Zwalnia podsystem d�wi�kowy
    void release();

    //! Ustawia parametry obserwatora
    void setObserver(const vec3& origin, const vec3& direction, const vec3& velocity);

    static void checkErrorState();
};

#define ale(x)  (x,SoundEngine::checkErrorState())

Q_DECL_EXPORT SoundEngine& getSoundEngine();

#endif // SOUNDENGINE_HPP
