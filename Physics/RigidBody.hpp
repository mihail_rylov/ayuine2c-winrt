//------------------------------------//
// 
// RigidBody.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Physics
// Date: 2008-08-28
// 
//------------------------------------//

#pragma once

#include <QSharedPointer>
#include <QVector>
#include <Math/Matrix.hpp>
#include "Physics.hpp"
#include "Collider.hpp"

//------------------------------------//
// RigidBodyCollider struct

struct RigidBodyCollider
{
  // Fields
  QSharedPointer<Collider> m_collider;
  matrix m_transform;

  // Constructor
  RigidBodyCollider() : m_transform(mat4Identity) {
  }
  RigidBodyCollider(const QSharedPointer<Collider>& collider) : m_collider(collider), m_transform(mat4Identity) {
  }
  RigidBodyCollider(const QSharedPointer<Collider>& collider, const matrix& transform) : m_collider(collider), m_transform(transform) {
  }
  ~RigidBodyCollider() {
  }
};

//------------------------------------//
// RigidBody class

class PhysicsScene;
class Collider;

class PHYSICS_EXPORT RigidBody : public QObject
{
  Q_OBJECT

  // Fields
protected:
  class NxActor* m_actor;
  matrix m_transform;

  // Constructor
protected:
  RigidBody(PhysicsScene* scene);

  // Destructor
public:
  ~RigidBody();

  // Methods
public:
  const matrix& transform() const;

  bool isDynamic() const;

  bool isValid() const;

  unsigned colliderCount();
  Collider* collider(unsigned index);

private:
  virtual void updateTransform(const class NxMat34& transform);

private:
  friend class PhysicsScene;
};
