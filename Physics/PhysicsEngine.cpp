//------------------------------------//
// 
// PhysicsEngine.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: PhysicsEngine
// Date: 2008-03-06
// 
//------------------------------------//

#include "PhysicsEngine.hpp"
#include <NxUserOutputStream.h>
#include <NxPhysicsSDK.h>
#include <NxPhysics.h>
#include <QDebug>

struct PhysXOutputStream : NxUserOutputStream
{
  void reportError(NxErrorCode code, const char * message, const char *file, int line)
  {
    QDebug(QtCriticalMsg) << "[PHYSX]" << message << " [ERR:" << code << "]";
  }

  NxAssertResponse reportAssertViolation(const char * message, const char *file, int line)
  {
    QDebug(QtWarningMsg) << "[PHYSX]" << message;
    return NX_AR_CONTINUE;
  }

  void print(const char * message)
  {
    QDebug(QtDebugMsg) << "[PHYSX]" << message;
  }
};

//------------------------------------//
// PhysicsEngine: Singleton

PhysicsEngine& getPhysicsEngine()
{
  static PhysicsEngine self;
  return self;
}

//------------------------------------//
// PhysicsEngine: Constructor

PhysicsEngine::PhysicsEngine() : m_outputStream(new PhysXOutputStream)
{
  // Stw�rz engine fizyczny
  m_physics = NxCreatePhysicsSDK(NX_PHYSICS_SDK_VERSION, NULL, m_outputStream.data());
  Q_ASSERT(m_physics);
}

//------------------------------------//
// PhysicsEngine: Destructor

PhysicsEngine::~PhysicsEngine()
{
	// HACK: Crashes with virtual pure call
#if 0
  // Zwolnij engine fizyczny
  m_physics->release();
  m_physics = NULL;
#endif
}
