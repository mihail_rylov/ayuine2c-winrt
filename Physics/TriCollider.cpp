#include "TriCollider.hpp"
#include "PhysicsEngine.hpp"
#include "MemoryStream.hpp"
#include <NxPhysicsSDK.h>
#include <NxTriangleMeshShapeDesc.h>
#include <NxTriangleMeshDesc.h>
#include <NxCooking.h>
#include <Math/Vec3.hpp>

TriCollider::TriCollider() : m_shapeDesc(new NxTriangleMeshShapeDesc)
{
  m_shapeDesc->userData = this;
}

TriCollider::TriCollider(const QByteArray& data)
  : m_shapeDesc(new NxTriangleMeshShapeDesc)
{
  m_shapeDesc->userData = this;
  m_shapeDesc->meshData = getPhysicsEngine()->createTriangleMesh(MemoryNxStream<QByteArray>(data));
}

NxShapeDesc* TriCollider::shapeDesc()
{
  return m_shapeDesc;
}

TriCollider::~TriCollider()
{
  if(m_shapeDesc->meshData) {
    getPhysicsEngine()->releaseTriangleMesh(*m_shapeDesc->meshData);
  }
  delete m_shapeDesc;
}

QByteArray TriCollider::cookTriangleMesh(
    const void* vertices, unsigned vertexCount, unsigned vertexStride,
    const void* indices, unsigned indexCount, unsigned indexStride,
    bool use16indices)
{
  Q_ASSERT(vertices);
  Q_ASSERT(vertexCount);
  Q_ASSERT(vertexStride >= sizeof(vec3));
  Q_ASSERT(indices);
  Q_ASSERT(indexCount >= 3);
  Q_ASSERT((indexCount % 3) == 0);
  Q_ASSERT(use16indices ? indexStride >= 2 : indexStride >= 4);

  // Wype�nij opis
  NxTriangleMeshDesc desc;
  desc.numVertices = vertexCount;
  desc.numTriangles = indexCount / 3;
  desc.pointStrideBytes = vertexStride;
  desc.triangleStrideBytes = indexStride * 3;
  desc.points = vertices;
  desc.triangles = indices;
  desc.flags = use16indices ? NX_MF_16_BIT_INDICES : 0;

  // Wypal TriMesha
  NxInitCooking();
  MemoryNxStream<QByteArray> stream;
  bool result = NxCookTriangleMesh(desc, stream);
  NxCloseCooking();

  if(result)
    return stream;
  return QByteArray();
}
