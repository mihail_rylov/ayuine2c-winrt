//------------------------------------//
// 
// Controller.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Engine
// Date: 2008-08-29
// 
//------------------------------------//

#pragma once

#include <QObject>
#include "Physics.hpp"

#include <Math/Vec3.hpp>

struct vec3;

//------------------------------------//
// Controller class

class PhysicsScene;
class RigidBody;
class PhysicsController;
class PhysicsControllerPrivate;

struct ControllerBodyHit
{
  RigidBody* body;
  PhysicsController* self;
  vec3 worldOrigin;
  vec3 worldNormal;
  vec3 localNormal;
  float length;

  ControllerBodyHit()
  {
    body = NULL;
    self = NULL;
    length = 0.0f;
  }
};

struct ControllerToControllerHit
{
  PhysicsController* other;
  PhysicsController* self;

  ControllerToControllerHit()
  {
    other = NULL;
    self = NULL;
  }
};

class PHYSICS_EXPORT PhysicsController : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(PhysicsController)

  // Fields
protected:
  QScopedPointer<PhysicsControllerPrivate> d_ptr;

  // Constructor
public:
  PhysicsController(PhysicsScene* scene);

  // Destructor
public:
  virtual ~PhysicsController();

signals:
  void bodyHit(const ControllerBodyHit& hit);
  void controllerHit(const ControllerToControllerHit& hit);

public:
  bool isOnFloor() const;
  bool isHittingCeil() const;
  bool isHittingSides() const;

  vec3 origin() const;
  void setOrigin(const vec3& origin);

  float height() const;
  void setHeight(float height);

  float radius() const;
  void setRadius(float radius);

  void setStepOffset(float stepOffset);

  void move(const vec3& offset);
};
