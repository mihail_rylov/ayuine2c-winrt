#include "SphereCollider.hpp"
#include <Math/Sphere.hpp>
#include <NxSphereShapeDesc.h>

SphereCollider::SphereCollider() : m_shapeDesc(new NxSphereShapeDesc) {
  m_shapeDesc->userData = this;
}

SphereCollider::SphereCollider(const sphere& sph)  : m_shapeDesc(new NxSphereShapeDesc) {
  m_shapeDesc->userData = this;
  setOrigin(sph.Center);
  setRadius(sph.Radius);
}

NxShapeDesc* SphereCollider::shapeDesc() {
  return m_shapeDesc;
}

//! Zwraca rozmiar
float SphereCollider::radius() const {
  return m_shapeDesc->radius;
}

//! Ustawia rozmiar
void SphereCollider::setRadius(float radius) {
  m_shapeDesc->radius = radius;
}

SphereCollider::~SphereCollider() {
  delete m_shapeDesc;
}
