#include "ConvexCollider.hpp"
#include "MemoryStream.hpp"
#include "PhysicsEngine.hpp"
#include <NxPhysicsSDK.h>
#include <NxConvexShapeDesc.h>
#include <NxConvexMeshDesc.h>
#include <NxCooking.h>
#include <Math/Vec3.hpp>

ConvexCollider::ConvexCollider() : m_shapeDesc(new NxConvexShapeDesc) {
  m_shapeDesc->userData = this;
}

ConvexCollider::ConvexCollider(const QByteArray& data) : m_shapeDesc(new NxConvexShapeDesc)
{
  m_shapeDesc->userData = this;
  m_shapeDesc->meshData = getPhysicsEngine()->createConvexMesh(MemoryNxStream<QByteArray>(data));
}

NxShapeDesc* ConvexCollider::shapeDesc()
{
  return m_shapeDesc;
}

ConvexCollider::~ConvexCollider() {
  if(m_shapeDesc->meshData) {
    getPhysicsEngine()->releaseConvexMesh(*m_shapeDesc->meshData);
  }
  delete m_shapeDesc;
}

QByteArray ConvexCollider::cookConvexMesh(const void* vertices, unsigned vertexCount, unsigned vertexStride)
{
  Q_ASSERT(vertices);
  Q_ASSERT(vertexCount);
  Q_ASSERT(vertexStride >= sizeof(vec3));

  // Wype�nij opis
  NxConvexMeshDesc desc;
  desc.numVertices = vertexCount;
  desc.pointStrideBytes = vertexStride;
  desc.points = vertices;
  desc.flags = NX_CF_COMPUTE_CONVEX | NX_CF_INFLATE_CONVEX;

  // Wypal Convexa
  NxInitCooking();
  MemoryNxStream<QByteArray> stream;
  bool result = NxCookConvexMesh(desc, stream);
  NxCloseCooking();

  if(result)
     return stream;
  return QByteArray();
}

QByteArray ConvexCollider::cookConvexMesh(const void *vertices, unsigned vertexCount, unsigned vertexStride, const void *indices, unsigned indexCount, unsigned indexStride, bool use16indices)
{
  Q_ASSERT(vertices);
  Q_ASSERT(vertexCount);
  Q_ASSERT(vertexStride >= sizeof(vec3));
  Q_ASSERT(indices);
  Q_ASSERT(indexCount >= 3);
  Q_ASSERT((indexCount % 3) == 0);
  Q_ASSERT(use16indices ? indexStride >= 2 : indexStride >= 4);

  // Wype�nij opis
  NxConvexMeshDesc desc;
  desc.numVertices = vertexCount;
  desc.numTriangles = indexCount / 3;
  desc.pointStrideBytes = vertexStride;
  desc.triangleStrideBytes = indexStride * 3;
  desc.points = vertices;
  desc.triangles = indices;
  desc.flags =  0;

  // Wypal Convexa
  NxInitCooking();
  MemoryNxStream<QByteArray> stream;
  bool result = NxCookConvexMesh(desc, stream);
  NxCloseCooking();

  if(result)
     return stream;
  return QByteArray();
}
