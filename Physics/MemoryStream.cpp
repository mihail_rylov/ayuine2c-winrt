//------------------------------------//
// 
// Stream.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Physics
// Date: 2008-03-06
// 
//------------------------------------//

#include "NxStream.hpp"

//------------------------------------//
// MemoryNxStream: Constructor

MemoryNxStream::MemoryNxStream(QByteArray& data, bool clear)
  : m_data(data), m_offset(0)
{
  if(clear)
    m_data.clear();
}

//------------------------------------//
// MemoryNxStream: Methods

void MemoryNxStream::read(void* buffer, NxU32 size) const {
  if(m_offset + size > m_data.size())
    throw std::overflow_error("eof");
  memcpy(buffer, &m_data[m_offset], size);
  m_offset += size;
}

void MemoryNxStream::store(const void* buffer, NxU32 size) {
  m_data.append((const char*)buffer, size);
}

NxU8 MemoryNxStream::readByte() const {
  return read<NxU8>();
}

NxU16 MemoryNxStream::readWord() const {
  return read<NxU16>();
}

NxU32 MemoryNxStream::readDword() const {
  return read<NxU32>();
}

float MemoryNxStream::readFloat() const {
  return read<float>();
}

double MemoryNxStream::readDouble() const {
  return read<double>();
}

void MemoryNxStream::readBuffer(void* buffer, NxU32 size) const {
  if(m_offset + size > m_data.size())
    throw std::overflow_error("eof");
  memcpy(buffer, &m_data[m_offset], size);
  m_offset += size;
}

NxStream& MemoryNxStream::storeByte(NxU8 b) {
  store(b);
  return *this;
}

NxStream& MemoryNxStream::storeWord(NxU16 w) {
  store(w);
  return *this;
}

NxStream& MemoryNxStream::storeDword(NxU32 d) {
  store(d);
  return *this;
}

NxStream& MemoryNxStream::storeFloat(NxReal f) {
  store(f);
  return *this;
}

NxStream& MemoryNxStream::storeDouble(NxF64 f) {
  store(f);
  return *this;
}

NxStream& MemoryNxStream::storeBuffer(const void* buffer, NxU32 size) {
  store(buffer, size);
  return *this;
}
