//------------------------------------//
// 
// Physics.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Physics
// Date: 2008-03-06
// 
//------------------------------------//

#pragma once

#include <QObject>

#include "Physics.hpp"

class PhysXOutputStream;
class NxPhysicsSDK;

class PHYSICS_EXPORT PhysicsEngine : public QObject
{
  Q_OBJECT

  // Fields
private:
  //! Engine fizyczny
  NxPhysicsSDK* m_physics;
  QScopedPointer<PhysXOutputStream> m_outputStream;

  // Constructor
public:
  explicit PhysicsEngine();

  // Destructor
public:
  ~PhysicsEngine();

  // Operators
public:
  class NxPhysicsSDK* operator -> () const
  {
    return m_physics;
  }
};

//! Pobiera uchwyt do fizyki
PHYSICS_EXPORT PhysicsEngine& getPhysicsEngine();
