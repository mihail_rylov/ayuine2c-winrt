#ifndef COLLIDEINFO_HPP
#define COLLIDEINFO_HPP

#include <QObject>
#include <QSharedPointer>
#include <Math/Vec3.hpp>
#include <Math/Ray.hpp>

class CollideInfo
{
  Q_GADGET
  Q_ENUMS(Flag)
  Q_FLAGS(Flags)

public:
  enum Flag
  {
    None = 0,
    Picking = 1,
    Fast = 2,
    Edges = 4
  };
  Q_DECLARE_FLAGS(Flags, Flag)

  QSharedPointer<QObject> owner, collider, material;
  vec3 hit;
  float hitTime;
  Flags flags;

  CollideInfo()
  {
    hitTime = FLT_MAX;
    flags = None;
  }
};

#endif // COLLIDEINFO_HPP
