#include "BoxCollider.hpp"
#include <Math/Box.hpp>
#include <NxBoxShapeDesc.h>

BoxCollider::BoxCollider() : m_shapeDesc(new NxBoxShapeDesc) {
  m_shapeDesc->userData = this;
}

BoxCollider::BoxCollider(const box& boxx) : m_shapeDesc(new NxBoxShapeDesc) {
  m_shapeDesc->userData = this;
  setOrigin(boxx.center());
  setExtent(boxx.size() * 0.5f);
}

NxShapeDesc* BoxCollider::shapeDesc() {
  return m_shapeDesc;
}

vec3 BoxCollider::extent() const {
  vec3 extent;
  m_shapeDesc->dimensions.get(&extent.X);
  return extent;
}

void BoxCollider::setExtent(const vec3& extent) {
  m_shapeDesc->dimensions.set(&extent.X);
}

BoxCollider::~BoxCollider() {
  delete m_shapeDesc;
}
