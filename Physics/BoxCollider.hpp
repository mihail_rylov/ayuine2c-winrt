#ifndef BOXCOLLIDER_HPP
#define BOXCOLLIDER_HPP

#include "Collider.hpp"

class NxBoxShapeDesc;
struct box;

class PHYSICS_EXPORT BoxCollider : public Collider
{
  Q_OBJECT

private:
  NxBoxShapeDesc* m_shapeDesc;

  // Constructor
public:
  BoxCollider();
  BoxCollider(const box& b);
  BoxCollider(const vec3& extent);
  BoxCollider(const vec3& origin, const vec3& extent);

  // Destructor
public:
  ~BoxCollider();

  // Methods
public:
  NxShapeDesc* shapeDesc();
  vec3 extent() const;
  void setExtent(const vec3& extent);
};

#endif // BOXCOLLIDER_HPP
