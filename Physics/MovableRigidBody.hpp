#ifndef MOVABLERIGIDBODY_HPP
#define MOVABLERIGIDBODY_HPP

#include "RigidBody.hpp"

class PHYSICS_EXPORT MovableRigidBody : public RigidBody
{
  Q_OBJECT

public:
  enum ForceMode
  {
    Force,
    Impulse,
    VelocityChange,
    SmoothImpulse,
    SmoothVelocityChange,
    Acceleration
  };

public:
  explicit MovableRigidBody(Collider* collider, const matrix& transform, PhysicsScene* scene);

signals:
  void bodyMoved(const matrix& transform);

public:
  bool setTransform(const matrix& transform);

public:
  bool collidable() const;
  void setCollidable(bool flag);

  bool gravity() const;
  void setGravity(bool flag);

  float density() const;
  void setDensity(float density);

  float mass() const;
  void setMass(float mass);

  bool isSleeping() const;
  void wakeUp();

public:
  vec3 angularVelocity() const;
  bool setAngularVelocity(const vec3& value);
  vec3 linearVelocity() const;
  bool setLinearVelocity(const vec3& value);
  float maxAngularVelocity() const;
  bool setMaxAngularVelocity(float value);

public:
  float angularDamping() const;
  bool setAngularDamping(float value);
  float linearDamping() const;
  bool setLinearDamping(float value);

public:
  vec3 angularMomentum() const;
  bool setAngularMomentum(const vec3& value);
  vec3 linearMomentum() const;
  bool setLinearMomemtum(const vec3& value);

public:
  bool addForceAtPos(const vec3& force, const vec3& pos, ForceMode mode = Force);
  bool addForceAtLocalPos(const vec3& force, const vec3& pos, ForceMode mode = Force);
  bool addLocalForceAtPos(const vec3& force, const vec3& pos, ForceMode mode = Force);
  bool addLocalForceAtLocalPos(const vec3& force, const vec3& pos, ForceMode mode = Force);
  bool addForce(const vec3& force, ForceMode mode = Force);
  bool addLocalForce(const vec3& force, ForceMode mode = Force);
  bool addTorque(const vec3& torque, ForceMode mode = Force);
  bool addLocalTorque(const vec3& torque, ForceMode mode = Force);

private:
  void updateTransform(const class NxMat34& transform);
};

#endif // MOVABLERIGIDBODY_HPP
