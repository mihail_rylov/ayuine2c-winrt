#ifndef HEIGHTFIELDCOLLIDER_HPP
#define HEIGHTFIELDCOLLIDER_HPP

#include "Collider.hpp"

#include <QVector>

class NxHeightFieldShapeDesc;
class NxHeightField;

class PHYSICS_EXPORT HeightFieldCollider : public Collider
{
  Q_OBJECT

private:
  NxHeightFieldShapeDesc* m_shapeDesc;
  NxHeightField* m_heightField;

  // Constructor
public:
  HeightFieldCollider(const QVector<qint16>& heights, unsigned columns, unsigned rows);

  // Destructor
public:
  ~HeightFieldCollider();

  // Methods
public:
  NxShapeDesc* shapeDesc();

public:
  float rowScale() const;
  void setRowScale(float rowScale);

  float columnScale() const;
  void setColumnScale(float columnScale);

  float heightScale() const;
  void setHeightScale(float heightScale);
};

#endif // HEIGHTFIELDCOLLIDER_HPP
