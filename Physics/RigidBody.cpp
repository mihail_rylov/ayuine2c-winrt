//------------------------------------//
// 
// RigidBody.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Physics
// Date: 2008-08-28
// 
//------------------------------------//

#include "RigidBody.hpp"
#include <Math/Matrix.hpp>
#include <Nx.h>
#include <NxActor.h>
#include <NxScene.h>

RigidBody::RigidBody(PhysicsScene* scene) : m_actor(NULL)
{
  Q_ASSERT(scene);
  m_transform = mat4Identity;
}

RigidBody::~RigidBody()
{
  if(m_actor)
  {
	m_actor->getScene().releaseActor(*m_actor);
    m_actor = NULL;
  }
}

unsigned RigidBody::colliderCount()
{
  if(m_actor)
  {
    return m_actor->getNbShapes();
  }
  return 0;
}

Collider *RigidBody::collider(unsigned index)
{
  if(m_actor)
  {
    if(index >= m_actor->getNbShapes())
      return NULL;

    NxShape* shape = m_actor->getShapes()[index];
    return (Collider*)shape->userData;
  }
  return NULL;
}

bool RigidBody::isDynamic() const
{
  if(m_actor)
    return m_actor->isDynamic();
  return false;
}

bool RigidBody::isValid() const
{
  return m_actor != NULL;
}

const matrix & RigidBody::transform() const
{
  return m_transform;
}

void RigidBody::updateTransform(const NxMat34 &transform)
{
}
