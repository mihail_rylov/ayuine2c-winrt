#ifndef PHYSICSSCENE_HPP
#define PHYSICSSCENE_HPP

#include <QObject>

#include "Physics.hpp"

struct vec3;

class PHYSICS_EXPORT PhysicsScene : public QObject
{
  Q_OBJECT

public:
  PhysicsScene();

public:
  ~PhysicsScene();

public:
  void simulate(float dt);

  vec3 gravity() const;
  void setGravity(const vec3& v) const;

private:
  class NxScene* m_scene;
  class NxControllerManager* m_controller;

  friend class PhysicsController;
  friend class StaticRigidBody;
  friend class MovableRigidBody;
};

#endif // PHYSICSSCENE_HPP
