//------------------------------------//
// 
// Controller.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Engine
// Date: 2008-08-29
// 
//------------------------------------//

#include "PhysicsController.hpp"
#include "PhysicsEngine.hpp"
#include "PhysicsScene.hpp"
#include <Math/Vec3.hpp>
#include <NxPhysicsSDK.h>
#include <NxController.h>
#include <NxCapsuleController.h>
#include <NxControllerManager.h>
#include <NxActor.h>
#include <NxShape.h>

class PhysicsControllerPrivate : public NxUserControllerHitReport
{
public:
  NxCapsuleController* m_controller;
  unsigned m_collisionFlags;
  PhysicsController* q_ptr;

public:
  PhysicsControllerPrivate(PhysicsController* ptr) : q_ptr(ptr)
  {
  }

  // Methods
public:
  NxControllerAction onShapeHit(const NxControllerShapeHit& hit);
  NxControllerAction onControllerHit(const NxControllersHit& hit);
};

PhysicsController::PhysicsController(PhysicsScene* scene) : d_ptr(new PhysicsControllerPrivate(this))
{
  Q_ASSERT(scene);

  if(scene && scene->m_controller)
  {
    // Wype�nij opis
    NxCapsuleControllerDesc desc;
    desc.upDirection = NX_Z;
    desc.stepOffset = 1;
    desc.callback = d_ptr.data();
    desc.userData = scene->m_controller;
    desc.radius = 1.0;
    desc.height = 1.0;

    // Utw�rz controllera
    d_ptr->m_controller = (NxCapsuleController*)scene->m_controller->createController(scene->m_scene, desc);
    Q_ASSERT(d_ptr->m_controller);
  }
}

PhysicsController::~PhysicsController()
{
  if(d_ptr->m_controller)
  {
    ((NxControllerManager*)d_ptr->m_controller->getUserData())->releaseController(*d_ptr->m_controller);
    d_ptr->m_controller = NULL;
  }
}

NxControllerAction PhysicsControllerPrivate::onShapeHit(const NxControllerShapeHit& hit)
{
  NxActor& actor = hit.shape->getActor();

  ControllerBodyHit newHit;
  newHit.body = (RigidBody*)actor.userData;
  newHit.self = q_ptr;
  newHit.worldOrigin = vec3(hit.worldPos.x, hit.worldPos.y, hit.worldPos.z);
  newHit.worldNormal = vec3(hit.worldNormal.x, hit.worldNormal.y, hit.worldNormal.z);
  newHit.localNormal = vec3(hit.dir.x, hit.dir.y, hit.dir.z);
  newHit.length = hit.length;
  emit q_ptr->bodyHit(newHit);

  return NX_ACTION_NONE;
}

NxControllerAction PhysicsControllerPrivate::onControllerHit(const NxControllersHit& hit)
{
  ControllerToControllerHit newHit;
  newHit.self = q_ptr;
  newHit.other = (PhysicsController*)hit.other->getUserData();
  emit q_ptr->controllerHit(newHit);

  return NX_ACTION_NONE;
}

bool PhysicsController::isOnFloor() const
{
  return (d_ptr->m_collisionFlags & NXCC_COLLISION_DOWN) != 0;
}

bool PhysicsController::isHittingCeil() const
{
  return (d_ptr->m_collisionFlags & NXCC_COLLISION_UP) != 0;
}

bool PhysicsController::isHittingSides() const
{
  return (d_ptr->m_collisionFlags & NXCC_COLLISION_SIDES) != 0;
}

void PhysicsController::setOrigin(const vec3& origin)
{
  d_ptr->m_controller->setPosition(NxExtendedVec3(origin.X, origin.Y, origin.Z));
}

void PhysicsController::setHeight(float height)
{
  d_ptr->m_controller->setHeight(height);
}

void PhysicsController::setRadius(float radius)
{
  d_ptr->m_controller->setRadius(radius);
}

void PhysicsController::setStepOffset(float stepOffset)
{
  d_ptr->m_controller->setStepOffset(stepOffset);
}

void PhysicsController::move(const vec3& offset)
{
  NxVec3 offsetNx;
  offsetNx.set(&offset.X);
  d_ptr->m_controller->move(offsetNx, -1, offset.length() / 16.0f, d_ptr->m_collisionFlags);
}

vec3 PhysicsController::origin() const
{
  if(d_ptr && d_ptr->m_controller)
  {
    NxExtendedVec3 origin = d_ptr->m_controller->getPosition();
    return vec3(origin.x, origin.y, origin.z);
  }
  return vec3Zero;
}

float PhysicsController::height() const
{
  if(d_ptr && d_ptr->m_controller)
    return d_ptr->m_controller->getHeight();
  return 0;
}

float PhysicsController::radius() const
{
  if(d_ptr && d_ptr->m_controller)
    return d_ptr->m_controller->getRadius();
  return 0;
}

