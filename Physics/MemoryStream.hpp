//------------------------------------//
// 
// Stream.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Physics
// Date: 2008-03-06
// 
//------------------------------------//

#pragma once

#include <QByteArray>
#include <NxPhysicsSDK.h>
#include <NxStream.h>

template<typename T>
class Q_DECL_EXPORT MemoryNxStream : public NxStream
{
private:
  mutable unsigned m_offset;

public:
  T data;

public:
  MemoryNxStream(const T& d)
    : m_offset(0), data(d)
  {
  }
  MemoryNxStream()
    : m_offset(0)
  {
  }

public:
  operator T& () {
    return data;
  }
  operator const T& () const {
    return data;
  }

  // Methods
public:
  template<typename Type>
  Type read() const {
    Type data;
    readBuffer(&data, sizeof(Type));
    return data;
  }

  template<typename Type>
  void store(const Type& value) {
    storeBuffer(&value, sizeof(Type));
  }

  NxU8 readByte() const {
    return read<NxU8>();
  }

  NxU16 readWord() const {
    return read<NxU16>();
  }

  NxU32 readDword() const {
    return read<NxU32>();
  }

  float readFloat() const {
    return read<float>();
  }

  double readDouble() const {
    return read<double>();
  }

  void readBuffer(void* buffer, NxU32 size) const {
    unsigned maxSize = qMin(data.size() - m_offset, size);
    memcpy(buffer, data.data() + m_offset, maxSize);
    m_offset += maxSize;
  }

  NxStream& storeByte(NxU8 b) {
    store(b);
    return *this;
  }

  NxStream& storeWord(NxU16 w) {
    store(w);
    return *this;
  }

  NxStream& storeDword(NxU32 d) {
    store(d);
    return *this;
  }

  NxStream& storeFloat(NxReal f) {
    store(f);
    return *this;
  }

  NxStream& storeDouble(NxF64 f) {
    store(f);
    return *this;
  }

  NxStream& storeBuffer(const void* buffer, NxU32 size) {
    data.append((const char*)buffer, size);
    return *this;
  }
};
