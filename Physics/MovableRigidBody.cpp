#include "MovableRigidBody.hpp"
#include "PhysicsScene.hpp"
#include "PhysicsEngine_p.hpp"
#include <NxScene.h>
#include <NxActor.h>
#include <NxBodyDesc.h>

inline NxForceMode fromForceMode(MovableRigidBody::ForceMode mode)
{
  return (NxForceMode)mode;
}

MovableRigidBody::MovableRigidBody(Collider* collider, const matrix& transform, PhysicsScene* scene) :
  RigidBody(scene)
{
  if(scene)
  {
    NxActorDesc actorDesc;
    NxBodyDesc bodyDesc;
    actorDesc.globalPose = fromMatrix(transform);
    actorDesc.userData = this;
    actorDesc.body = &bodyDesc;
    actorDesc.density = 0;
    bodyDesc.mass = 1;

    if(collider)
      actorDesc.shapes.push_back(collider->shapeDesc());

    // Utw�rz aktora
    m_actor = scene->m_scene->createActor(actorDesc);
    m_transform = transform;
    Q_ASSERT(m_actor);
  }
}

bool MovableRigidBody::gravity() const
{
  return m_actor && !m_actor->readBodyFlag(NX_BF_DISABLE_GRAVITY);
}

void MovableRigidBody::setGravity(bool gravity)
{
  if(m_actor)
  {
    if(gravity)
      m_actor->clearBodyFlag(NX_BF_DISABLE_GRAVITY);
    else
      m_actor->raiseBodyFlag(NX_BF_DISABLE_GRAVITY);
  }
}

bool MovableRigidBody::collidable() const
{
  return m_actor && !m_actor->readActorFlag(NX_AF_DISABLE_COLLISION);
}

void MovableRigidBody::setCollidable(bool collidable)
{
  if(m_actor)
  {
    if(collidable)
      m_actor->clearActorFlag(NX_AF_DISABLE_COLLISION);
    else
      m_actor->raiseActorFlag(NX_AF_DISABLE_COLLISION);
  }
}

float MovableRigidBody::mass() const
{
  if(m_actor)
  {
    return m_actor->getMass();
  }
  return 0;
}

void MovableRigidBody::setMass(float mass)
{
  if(m_actor)
  {
    m_actor->setMass(mass);
  }
}

void MovableRigidBody::wakeUp()
{
  if(m_actor)
  {
    m_actor->wakeUp();
  }
}

bool MovableRigidBody::addForceAtPos(const vec3 &force, const vec3 &pos, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addForceAtPos(fromVec3(force), fromVec3(pos), fromForceMode(mode));
    return true;
  }
  return false;
}

bool MovableRigidBody::addForceAtLocalPos(const vec3 &force, const vec3 &pos, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addForceAtLocalPos(fromVec3(force), fromVec3(pos), fromForceMode(mode));
    return true;
  }
  return false;
}

bool MovableRigidBody::addLocalForceAtPos(const vec3 &force, const vec3 &pos, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addLocalForceAtPos(fromVec3(force), fromVec3(pos), fromForceMode(mode));
    return true;
  }
  return false;
}

bool MovableRigidBody::addLocalForceAtLocalPos(const vec3 &force, const vec3 &pos, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addLocalForceAtLocalPos(fromVec3(force), fromVec3(pos), fromForceMode(mode));
    return true;
  }
  return false;
}

bool MovableRigidBody::addForce(const vec3 &force, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addForce(fromVec3(force), fromForceMode(mode));
    return true;
  }
  return false;
}

bool MovableRigidBody::addLocalForce(const vec3 &force, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addLocalForce(fromVec3(force), fromForceMode(mode));
    return true;
  }
  return false;
}

bool MovableRigidBody::addTorque(const vec3 &torque, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addTorque(fromVec3(torque), fromForceMode(mode));
    return true;
  }
  return false;
}

bool MovableRigidBody::addLocalTorque(const vec3 &torque, MovableRigidBody::ForceMode mode)
{
  if(m_actor)
  {
    m_actor->addLocalTorque(fromVec3(torque), fromForceMode(mode));
    return true;
  }
  return false;
}

vec3 MovableRigidBody::angularMomentum() const
{
  if(m_actor)
    return toVec3(m_actor->getAngularMomentum());
  return vec3();
}

bool MovableRigidBody::setAngularMomentum(const vec3 &value)
{
  if(m_actor)
  {
    m_actor->setAngularMomentum(fromVec3(value));
    return true;
  }
  return false;
}

vec3 MovableRigidBody::linearMomentum() const
{
  if(m_actor)
    return toVec3(m_actor->getLinearMomentum());
  return vec3();
}

bool MovableRigidBody::setLinearMomemtum(const vec3 &value)
{
  if(m_actor)
  {
    m_actor->setLinearMomentum(fromVec3(value));
    return true;
  }
  return false;
}

vec3 MovableRigidBody::angularVelocity() const
{
  if(m_actor)
    return toVec3(m_actor->getAngularVelocity());
  return vec3();
}

bool MovableRigidBody::setAngularVelocity(const vec3 &value)
{
  if(m_actor)
  {
    m_actor->setAngularVelocity(fromVec3(value));
    return true;
  }
  return false;
}

vec3 MovableRigidBody::linearVelocity() const
{
  if(m_actor)
    return toVec3(m_actor->getLinearVelocity());
  return vec3();
}

bool MovableRigidBody::setLinearVelocity(const vec3 &value)
{
  if(m_actor)
  {
    m_actor->setLinearVelocity(fromVec3(value));
    return true;
  }
  return false;
}

float MovableRigidBody::maxAngularVelocity() const
{
  if(m_actor)
    return m_actor->getMaxAngularVelocity();
  return 0.0f;
}

bool MovableRigidBody::setMaxAngularVelocity(float value)
{
  if(m_actor)
  {
    m_actor->setMaxAngularVelocity(value);
    return true;
  }
  return false;
}

float MovableRigidBody::angularDamping() const
{
  if(m_actor)
    return m_actor->getAngularDamping();
  return 0.0f;
}

bool MovableRigidBody::setAngularDamping(float value)
{
  if(m_actor)
  {
    m_actor->setAngularDamping(value);
    return true;
  }
  return false;
}

float MovableRigidBody::linearDamping() const
{
  if(m_actor)
    return m_actor->getLinearDamping();
  return 0.0f;
}

bool MovableRigidBody::setLinearDamping(float value)
{
  if(m_actor)
  {
    m_actor->setLinearDamping(value);
    return true;
  }
  return false;
}

bool MovableRigidBody::setTransform(const matrix& transform)
{
  if(m_actor)
  {
    m_actor->setGlobalPose(fromMatrix(transform));
    m_transform = transform;
    return true;
  }
  return false;
}

void MovableRigidBody::updateTransform(const NxMat34& transform)
{
  m_transform = toMatrix(transform);
  emit bodyMoved(m_transform);
}
