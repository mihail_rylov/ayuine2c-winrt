#include "BlockWidget.hpp"
#include "ui_BlockWidget.h"
#include "BlockInputWidget.hpp"
#include "BlockOutputWidget.hpp"
#include "BlockArea.hpp"
#include <CoreFrames/ResourceEditor.hpp>
#include <MaterialSystem/ShaderBlock.hpp>
#include <MaterialSystem/ShaderBlockVariable.hpp>
#include <QMouseEvent>

BlockWidget::BlockWidget(const QSharedPointer<ShaderBlock>& block, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::BlockWidget),
  m_block(block)
{
  ui->setupUi(this);

  if(parent)
    updateUi();

  if(block)
  {
    connect(block.data(), SIGNAL(updated()), SLOT(updateContent()));
    connect(block.data(), SIGNAL(updated()), SLOT(updateUi()));
    connect(block.data(), SIGNAL(linksUpdated()), SLOT(updateInputs()));

#if 0
    if(ShaderBlockVariable* variable = qobject_cast<ShaderBlockVariable*>(m_block.data()))
    {
    }
#endif

    move(block->position());
  }
}

BlockWidget::~BlockWidget()
{
  delete ui;
}

void BlockWidget::updateInputs()
{
  if(!m_block)
  {
    foreach(BlockInputWidget* input, m_inputs)
      delete input;
    m_inputs.clear();
    return;
  }

  bool changed = false;

  while(m_inputs.size() > m_block->inputCount())
  {
    delete m_inputs.last();
    m_inputs.pop_back();
    changed = true;
  }

  m_inputs.resize(m_block->inputCount());

  for(int i = 0; i < m_inputs.size(); ++i)
  {
    if(!m_inputs[i])
    {
      m_inputs[i] = new BlockInputWidget(ui->contentFrame);
      m_inputs[i]->show();
      changed = true;

      ui->inputLayout->insertWidget(i, m_inputs[i]);
    }
    m_inputs[i]->setLink(m_block->input(i));
  }

  if(changed)
  {
    ui->contentFrame->adjustSize();
    adjustSize();
  }
}

void BlockWidget::updateOutputs()
{
  if(!m_block)
  {
    foreach(BlockOutputWidget* output, m_outputs)
      delete output;
    m_outputs.clear();
    return;
  }

  bool changed = false;

  while(m_outputs.size() > m_block->outputCount())
  {
    delete m_outputs.last();
    m_outputs.pop_back();
  }

  if(changed)
  {
    ui->outputLayout->update();
  }

  m_outputs.resize(m_block->outputCount());

  for(int i = 0; i < m_outputs.size(); ++i)
  {
    if(!m_outputs[i])
    {
      m_outputs[i] = new BlockOutputWidget(ui->contentFrame);
      m_outputs[i]->show();
      changed = true;

      ui->outputLayout->insertWidget(i, m_outputs[i]);
    }
    m_outputs[i]->setLink(m_block->output(i));
  }

  if(changed)
  {
    ui->contentFrame->adjustSize();
    adjustSize();
  }
}

void BlockWidget::updateCaption()
{
  if(m_block)
  {
    if(m_block->objectName().isNull())
    {
      QString group = m_block->metaObject()->classInfo(m_block->metaObject()->indexOfClassInfo("BlockGroup")).value();
      if(group.isEmpty())
        group = "Other";

      QString name = m_block->metaObject()->classInfo(m_block->metaObject()->indexOfClassInfo("BlockName")).value();
      if(name.isEmpty())
        name = m_block->metaObject()->className();

      ui->caption->setText(QString("%1::%2").arg(group, name));
    }
    else
    {
      ui->caption->setText(m_block->objectName());
    }
  }
  else
  {
    ui->caption->setText("(none)");
  }
}

void BlockWidget::updateContent()
{
  QPixmap pixmap = m_block->constValuePixmap();

  if(!pixmap.isNull())
  {
    ui->contentWidget->setPixmap(pixmap);
    ui->contentWidget->show();
    ui->contentFrame->adjustSize();
    adjustSize();
  }
  else
  {
    ui->contentWidget->hide();
    ui->contentFrame->adjustSize();
    adjustSize();
  }
}

void BlockWidget::updateUi()
{  
  updateInputs();
  updateOutputs();
  updateCaption();
  updateContent();

  adjustSize();
}

QSharedPointer<ShaderBlock> BlockWidget::block() const
{
  return m_block;
}

BlockCaption::BlockCaption(QWidget *parent)
  : QPushButton(parent)
{
  setMinimumWidth(100);
}

void BlockCaption::mousePressEvent(QMouseEvent *event)
{
  if(event->button() == Qt::LeftButton)
  {
    grabMouse();
    offset = event->globalPos();
    parentWidget()->setFocus();
  }
}

void BlockCaption::mouseMoveEvent(QMouseEvent *event)
{
  if(QWidget::mouseGrabber() == this)
  {
    event->accept();
    parentWidget()->move(parentWidget()->mapToParent(event->globalPos() - offset));
    offset = event->globalPos();
  }
}

void BlockCaption::mouseReleaseEvent(QMouseEvent *event)
{
  if(event->button() == Qt::LeftButton)
  {
    releaseMouse();
    event->accept();
    offset = QPoint();
  }
}

void BlockWidget::focusInEvent(QFocusEvent *)
{
  if(blockArea())
  {
    foreach(BlockWidget* widget, blockArea()->selected())
      widget->setSelected(false);
  }

  if(ResourceEditor * resEditor = resourceEditor())
  {
    if(block())
    {
      resEditor->setPropertyObject(block().data());
      setSelected(true);
    }
  }
}

BlockInputWidget * BlockWidget::input(unsigned index) const
{
  if(m_inputs.size() <= index)
    return NULL;
  return m_inputs[index];
}

unsigned BlockWidget::inputCount() const
{
  return m_inputs.size();
}

BlockOutputWidget * BlockWidget::output(unsigned index) const
{
  if(m_outputs.size() <= index)
    return NULL;
  return m_outputs[index];
}

unsigned BlockWidget::outputCount() const
{
  return m_outputs.size();
}

ResourceEditor * BlockWidget::resourceEditor() const
{
  for(QWidget* parent = parentWidget(); parent; parent = parent->parentWidget())
  {
    ResourceEditor* resourceEditor = qobject_cast<ResourceEditor*>(parent);
    if(!resourceEditor)
      continue;
    return resourceEditor;
  }
  return NULL;
}

BlockArea * BlockWidget::blockArea() const
{
  for(QWidget* parent = parentWidget(); parent; parent = parent->parentWidget())
  {
    BlockArea* blockArea = qobject_cast<BlockArea*>(parent);
    if(!blockArea)
      continue;
    return blockArea;
  }
  return NULL;
}

void BlockWidget::moveEvent(QMoveEvent *moveEvent)
{
  if(m_block)
  {
    m_block->setPosition(pos());
  }
}

bool BlockWidget::selected() const
{
  return ui->caption->isChecked();
}

void BlockWidget::setSelected(bool selected)
{
  ui->caption->setChecked(selected);
}
