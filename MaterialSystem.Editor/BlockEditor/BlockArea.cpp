#include "BlockArea.hpp"
#include "MaterialShaderEditor.hpp"
#include "BlockWidget.hpp"
#include "BlockInputWidget.hpp"
#include "BlockOutputWidget.hpp"

#include <Core/ClassInfo.hpp>
#include <MaterialSystem/Shader.hpp>

#include <QDragEnterEvent>
#include <QDebug>
#include <QUrl>
#include <QStandardItemModel>
#include <QPainter>
#include <QRubberBand>

BlockArea::BlockArea(QWidget *parent) :
  QFrame(parent)
{
  m_rubberBand = NULL;
  setStyleSheet("background-color: rgb(220, 220, 220);");
  setAcceptDrops(true);
  setAutoFillBackground(false);
  setFocusPolicy(Qt::ClickFocus);
}

BlockArea::~BlockArea()
{
}

void BlockArea::dragEnterEvent(QDragEnterEvent *dragEvent)
{
  MaterialShaderEditor* editor = qobject_cast<MaterialShaderEditor*>(parent());
  if(!editor)
    return;
  if(!editor->materialShader())
    return;

  QStandardItemModel itemModel;
  if(!itemModel.dropMimeData(dragEvent->mimeData(), dragEvent->dropAction(), 0, 0, QModelIndex()))
    return;

  QModelIndex model = itemModel.index(0,0);
  if(ClassInfo::getMetaObject(itemModel.data(model, Qt::UserRole).toByteArray(), &ShaderBlock::staticMetaObject))
  {
    dragEvent->acceptProposedAction();
  }
}

void BlockArea::dropEvent(QDropEvent *dragEvent)
{
  MaterialShaderEditor* editor = qobject_cast<MaterialShaderEditor*>(parent());
  if(!editor)
    return;
  if(!editor->materialShader())
    return;

  QStandardItemModel itemModel;
  if(!itemModel.dropMimeData(dragEvent->mimeData(), dragEvent->dropAction(), 0, 0, QModelIndex()))
    return;

  QModelIndex model = itemModel.index(0,0);
  if(const QMetaObject* metaObject = ClassInfo::getMetaObject(itemModel.data(model, Qt::UserRole).toByteArray(), &ShaderBlock::staticMetaObject))
  {
    QSharedPointer<ShaderBlock> newBlock(static_cast<ShaderBlock*>(metaObject->newInstance()));

    if(newBlock)
    {
      newBlock->setPosition(dragEvent->pos());
      editor->materialShader()->addBlock(newBlock);
    }
  }
}

void BlockArea::clear()
{
  foreach(BlockWidget* blockWidget, m_blocks)
  {
    delete blockWidget;
  }
  m_blocks.clear();
}

void BlockArea::addBlock(const QSharedPointer<ShaderBlock> &block)
{
  if(!block)
    return;
  if(m_blocks.contains(block))
    return;
  BlockWidget* widget = new BlockWidget(block, this);
  widget->show();
  m_blocks[block] = widget;

  connect(block.data(), SIGNAL(updated()), SLOT(update()));
}

void BlockArea::removeBlock(const QSharedPointer<ShaderBlock> &block)
{
  if(!block)
    return;
  if(!m_blocks.contains(block))
    return;

  BlockWidget* widget = m_blocks[block];
  m_blocks.remove(block);
  delete widget;

  block->disconnect(this);
}

void BlockArea::focusInEvent(QFocusEvent *)
{
  selectSelf();
}

ResourceEditor * BlockArea::resourceEditor() const
{
  for(QWidget* parent = parentWidget(); parent; parent = parent->parentWidget())
  {
    ResourceEditor* resourceEditor = qobject_cast<ResourceEditor*>(parent);
    if(!resourceEditor)
      continue;
    return resourceEditor;
  }
  return NULL;
}

BlockWidget * BlockArea::blockWidget(const QSharedPointer<ShaderBlock> &block)
{
  if(m_blocks.contains(block))
    return m_blocks[block];
  return NULL;
}

void BlockArea::updateUi()
{
  foreach(BlockWidget* blockWidget, m_blocks)
  {
    blockWidget->updateUi();
  }
}

void BlockArea::mousePressEvent(QMouseEvent *mouseEvent)
{
  if(mouseEvent->button() == Qt::LeftButton)
  {
    m_rubberOrigin = mouseEvent->pos();
    if (!m_rubberBand)
        m_rubberBand = new QRubberBand(QRubberBand::Rectangle, this);
    m_rubberBand->setGeometry(QRect(m_rubberOrigin, QSize()));
    m_rubberBand->show();
  }
  else if(mouseEvent->button() == Qt::RightButton)
  {
    m_scrollOrigin = mouseEvent->pos();
  }
}

void BlockArea::mouseReleaseEvent(QMouseEvent *mouseEvent)
{
  if(mouseEvent->button() == Qt::LeftButton)
  {
    if(!m_rubberBand)
      return;

    if(m_rubberBand)
      m_rubberBand->hide();

    QObject* lastSelected = NULL;

    foreach(BlockWidget* widget, m_blocks)
    {
      if(m_rubberBand->geometry().intersects(widget->geometry()))
      {
        widget->setSelected(true);
        lastSelected = widget->block().data();
      }
      else
      {
        widget->setSelected(false);
      }
    }

    if(ResourceEditor *resEditor = resourceEditor())
    {
      resEditor->setPropertyObject(lastSelected ? lastSelected : resEditor->resource().data());
    }
  }
}

void BlockArea::mouseMoveEvent(QMouseEvent *mouseEvent)
{
  if(mouseEvent->buttons() & Qt::LeftButton)
  {
    if(m_rubberBand)
     m_rubberBand->setGeometry(QRect(m_rubberOrigin, mouseEvent->pos()).normalized());
  }
  if(mouseEvent->buttons() & Qt::RightButton)
  {
    QPoint delta = mouseEvent->pos() - m_scrollOrigin;
    scroll(delta.x(), delta.y());
    QCursor::setPos(mapToGlobal(m_scrollOrigin));
  }
}

QList<BlockWidget *> BlockArea::selected() const
{
  QList<BlockWidget*> widgets;
  foreach(BlockWidget* widget, m_blocks)
    if(widget->selected())
      widgets.append(widget);
  return widgets;
}

void BlockArea::setBlocks(const QVector<QSharedPointer<ShaderBlock> > &blocks)
{
  clear();

  foreach(QSharedPointer<ShaderBlock> block, blocks)
  {
    if(m_blocks.contains(block))
      continue;

    BlockWidget* widget = new BlockWidget(block);
    widget->setParent(this);
    widget->show();
    widget->updateOutputs();
    m_blocks[block] = widget;

    connect(block.data(), SIGNAL(updated()), SLOT(update()));
  }

  updateUi();
}

void BlockArea::selectSelf()
{
  foreach(BlockWidget* widget, selected())
  {
    widget->setSelected(false);
  }

  if(ResourceEditor * resEditor = resourceEditor())
  {
    if(resEditor->resource())
    {
      resEditor->setPropertyObject(resEditor->resource().data());
    }
  }
}
