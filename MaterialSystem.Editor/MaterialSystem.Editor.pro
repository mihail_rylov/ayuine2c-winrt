TARGET = MaterialSystem.Editor
TEMPLATE = lib
CONFIG -= precompile_header
CONFIG += editor

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lPipeline -lMaterialSystem -lMeshSystem -lEditor

HEADERS += \
    MaterialShaderEditor.hpp \
    BlockEditor/BlockWidget.hpp \
    BlockEditor/BlockOutputWidget.hpp \
    BlockEditor/BlockInputWidget.hpp \
    BlockEditor/BlockConnWidget.hpp \
    BlockEditor/BlockArea.hpp \
    MaterialShaderValuesProperty.hpp

SOURCES += \
    MaterialShaderEditor.cpp \
    BlockEditor/BlockWidget.cpp \
    BlockEditor/BlockOutputWidget.cpp \
    BlockEditor/BlockInputWidget.cpp \
    BlockEditor/BlockConnWidget.cpp \
    BlockEditor/BlockArea.cpp \
    MaterialShaderValuesProperty.cpp

FORMS += \
    MaterialShaderEditor.ui \
    BlockEditor/BlockWidget.ui

INCLUDEPATH += BlockEditor
