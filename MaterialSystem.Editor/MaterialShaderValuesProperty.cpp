#include "MaterialShaderValuesProperty.hpp"

#include <MaterialSystem/ShaderState.hpp>

Q_IMPLEMENT_PROPERTYTYPE(ShaderValues, MaterialShaderValuesProperty)

MaterialShaderValuesProperty::MaterialShaderValuesProperty(const QString& name, QObject* propertyObject, QObject* parent)
  : Property(name, propertyObject, parent)
{
  m_propertiesInitialized = false;

  if(parent->parent() && !parent->parent()->parent())
  {
    setParent(parent->parent());
  }
}

bool MaterialShaderValuesProperty::isReadOnly()
{
  return false;
}

ShaderValues MaterialShaderValuesProperty::values() const
{
  return value().value<ShaderValues>();
}

ShaderValues MaterialShaderValuesProperty::refValues() const
{
  if (m_propertyObject)
    return m_propertyObject->property(qPrintable("__ref" + objectName())).value<ShaderValues>();
  else
    return ShaderValues();
}

void MaterialShaderValuesProperty::setValues(const ShaderValues &values)
{
  setValue(QVariant::fromValue(values));
}

bool MaterialShaderValuesProperty::event(QEvent *event)
{
  switch(event->type())
  {
  case QEvent::DynamicPropertyChange:
  {
    QDynamicPropertyChangeEvent* changeEvent = (QDynamicPropertyChangeEvent*)event;

    ShaderValues newValues = values();
    newValues[changeEvent->propertyName()] = property(changeEvent->propertyName().constData());
    setValues(newValues);
  }
    return true;
  }
  return false;
}

void MaterialShaderValuesProperty::populateChildren()
{
  if(m_propertiesInitialized)
    return;

  m_propertiesInitialized = true;

  ShaderValues ref = refValues();
  ShaderValues cur = values();

  foreach(QByteArray name, ref.keys())
  {
    // skip existing properties
    if(metaObject()->indexOfProperty(name.constData()) >= 0)
      continue;

    QVariant value = ref[name];

    // get either current or new value
    if(cur.contains(name))
    {
      QVariant curValue = cur[name];
      if(curValue.userType() == value.userType())
      {
        value = curValue;
      }
    }

    // set dynamic property
    setProperty(name.constData(), value);

    // create editor
    if(Property* property = EditableProperty::createPropertyType(ref[name].typeName(), name, this, this))
    {
      m_properties.append(property);
    }
  }
}

bool MaterialShaderValuesProperty::hasChildren() const
{
  if(m_propertiesInitialized)
    return Property::hasChildren();
  return refValues().size() != 0;
}
