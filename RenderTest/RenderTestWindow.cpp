#include "RenderTestWindow.hpp"
#include "ui_RenderTestWindow.h"

#include <QFileDialog>
#include <Core/FileSystem.hpp>
#include <Core/Serializer.hpp>
#include <QDebug>

#include <windows.h>

RenderTestWindow::RenderTestWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::RenderTestWindow)
{
  ui->setupUi(this);
  ui->centralwidget->showFullScreen();

  texture = Resource::load<Texture>(":/Render/DefaultTexture.dds");
  ui->propertyGridContents->setObject(texture.data());

  qDebug() << (void*)LoadLibraryA("MeshSystem.dll");
}

RenderTestWindow::~RenderTestWindow()
{
  delete ui;
}

int main(int argc, char** argv)
{
  QApplication app(argc, argv);
#if 0
  QRenderWidget window;
  window.showFullScreen();
#else
  RenderTestWindow window;
  window.show();
#endif
  return app.exec();
}

void RenderTestWindow::saveToFile()
{
  QString fileName = QFileDialog::getSaveFileName(this, "Save file", QString(), ResourceImporter::filter());
  if(fileName.length())
  {
    QSharedPointer<QIODevice> device = getFileSystem().writeFile(fileName);
    if(device.data())
    {
      QDataStream ds(device.data());
      saveToDataStream(texture, ds);
    }
  }
}

void RenderTestWindow::loadFile()
{
  QString fileName = QFileDialog::getOpenFileName(this, "Load file", QString(), ResourceImporter::filter());
  if(fileName.length())
  {
    QSharedPointer<QIODevice> device = getFileSystem().readFile(fileName);
    if(device.data())
    {
      QDataStream ds(device.data());
      QSharedPointer<QObject> object = loadFromDataStream(ds);
      qDebug() << object.data();
    }
  }
}
