#-------------------------------------------------
#
# Project created by QtCreator 2011-08-09T20:26:29
#
#-------------------------------------------------

QT       += core gui
CONFIG  -= precompile_header

include(E:/Sources/qtpropertybrowser/qtpropertybrowser.pri)

TARGET = RenderTest
TEMPLATE = app

include(../ayuine2c.pri)

SOURCES += RenderTestWindow.cpp \
    ../Test/objectcontroller.cpp
HEADERS += RenderTestWindow.hpp \
    ../Test/objectcontroller.h
FORMS   += RenderTestWindow.ui

INCLUDEPATH += ../Test

LIBS += -lRender -lCore -lMath -lMeshSystem -lMaterialSystem
