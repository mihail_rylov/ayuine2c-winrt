//------------------------------------//
// 
// Frustum.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "Frustum.hpp"
#include "Rect.hpp"

//------------------------------------//
// frustum: Constructor

frustum::frustum()
  : m_hasInvObject(false)
{
  m_hasInvObject = false;
}

frustum::frustum(const matrix &object)
  : m_object(object), m_hasInvObject(false)
{
  m_planes[0] = vec4(object.m41 - object.m11, object.m42 - object.m12, object.m43 - object.m13, object.m44 - object.m14).normalizeNormal();
  m_planes[1] = vec4(object.m41 + object.m11, object.m42 + object.m12, object.m43 + object.m13, object.m44 + object.m14).normalizeNormal();
  m_planes[2] = vec4(object.m41 - object.m21, object.m42 - object.m22, object.m43 - object.m23, object.m44 - object.m24).normalizeNormal();
  m_planes[3] = vec4(object.m41 + object.m21, object.m42 + object.m22, object.m43 + object.m23, object.m44 + object.m24).normalizeNormal();
  m_planes[4] = vec4(object.m41 - object.m31, object.m42 - object.m32, object.m43 - object.m33, object.m44 - object.m34).normalizeNormal();
  m_planes[5] = vec4(object.m41 + object.m31, object.m42 + object.m32, object.m43 + object.m33, object.m44 + object.m34).normalizeNormal();
}

frustum::frustum(const matrix &projection, const matrix &view)
{
  matrix transform;
  matrix::multiply(transform, projection, view);
  new(this) frustum(transform);
}

frustum::frustum(const frustum &culler, const matrix &object)
{
  matrix transform;
  matrix::multiply(transform, culler.object(), object);
  new(this) frustum(transform);
}

//------------------------------------//
// frustum: Methods

bool frustum::test(const vec3 &point) const {
  for(unsigned i = 0; i < _Count; i++) {
    if((m_planes[i] ^ point) < -Epsf)
      return false;
  }
  return true;
}

bool frustum::test(const vec4 &plane) const {
  // TODO !
  return true;
}

bool frustum::test(const vec3 *verts, unsigned count, unsigned stride) const {
  unsigned map = 0;

  // Sprawd� ka�dy wierzcho�ek
  for(unsigned i = 0; i < count; i++, (char*&)verts += stride) {
    for(unsigned j = 0; j < _Count; j++) {
      if((m_planes[j] ^ *verts) < -Epsf) {
        map |= 1 << j;
        goto next;
      }
    }
    return true;
next:;
  }
  if((map & 3) == 3 || (map & 12) == 12 || (map & 48) == 48)
    return true;
  return false;
}

bool frustum::test(const QVector<vec3> &verts) const {
  if(verts.empty())
    return false;
  return test(verts.constData(), verts.size(), sizeof(vec3));
}

bool frustum::test(const box &box) const {
  vec3 verts[8];

  // Je�li box jest pusty to nic nie r�b
  if(box.empty())
    return false;

  // Pobierz wszystkie wierzcho�ki bboxa
  box.vertexes(verts);
  return test(verts, 8, sizeof(vec3));
}

bool frustum::test(const sphere &sphere) const {
  for(unsigned j = 0; j < _Count; j++) {
    if((m_planes[j] ^ sphere.Center) < -(Epsf + sphere.Radius))
      return false;
  }
  return true;
}

vec3 frustum::screen(const vec3 &point) const {
  return m_object.transformCoord(point);
}

vec3 frustum::world(const vec3 &point) const {
  return invObject().transformCoord(point);
}

QRectF frustum::screenRect(const vec3 *verts, unsigned count) const
{
  if(count == 0)
    return QRectF();

  vec2 min;
  vec2 max;

  // Przetransformuj wierzcho�ki
  for(unsigned i = 0; i < 8; i++)
  {
    vec4 v = m_object.transform(verts[i]);
    vec2 vv;

    // Sprawd� W, je�li < 0 tzn. �e wierzcho�ek znajduje si� z ty�u frustuma
    if(v.W < Epsf)
    {
      vv.X = (v.X >= 0 ? 1.0f : -1.0f);
      vv.Y = (v.Y >= 0 ? 1.0f : -1.0f);
    }
    else
    {
      vv = (vec2&)v / v.W;
    }

    if(i == 0)
    {
      min = max = vv;
    }
    else
    {
      min = vec2::minimize(min, vv);
      max = vec2::maximize(max, vv);
    }
  }

  return QRectF(QPointF(min.X, min.Y), QPointF(max.X, max.Y));
}

QRectF frustum::screenRect(const box &box) const
{
  vec3 verts[8];
  box.vertexes(verts);
  return screenRect(verts, 8);
}

const matrix & frustum::object() const
{
  return m_object;
}

const matrix & frustum::invObject() const
{
  if(!m_hasInvObject)
  {
    matrix::invert(m_invObject, m_object);
    m_hasInvObject = true;
  }
  return m_invObject;
}

const vec4 & frustum::plane(Planes plane) const
{
  if(plane < 0 || plane >= _Count)
    return vec4Zero;
  return m_planes[plane];
}
