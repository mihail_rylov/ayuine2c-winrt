#ifndef VEC4_HPP
#define VEC4_HPP

#include <QDataStream>
#include "Math.hpp"
#include "MathLib.hpp"
#include "Vec2.hpp"
#include "Vec3.hpp"

#include <Core/Serializer.hpp>

struct sphere;
struct box;
struct ray;

//------------------------------------//
// vec4 class

struct MATH_EXPORT vec4
{
  // Fields
public:
    float X, Y, Z, W;

    // Constructors
public:
    vec4() {
        X = 0;
        Y = 0;
        Z = 0;
        W = 0;
    }
    vec4(float v[])	{
        X = v[0];
        Y = v[1];
        Z = v[2];
        W = v[3];
    }
    vec4(float _x, float _y, float _z, float _w) {
        X = _x;
        Y = _y;
        Z = _z;
        W = _w;
    }
    vec4(float _w) {
        X = _w;
        Y = _w;
        Z = _w;
        W = _w;
    }
    vec4(const vec3 &a, const vec3 &b, const vec3 &c) {
        normal() = ((b - a) % (c - a)).normalize();
        W = -(normal() ^ a);
    }
    vec4(const vec3 &n, const vec3 &origin) {
        normal() = n;
        W = -(normal() ^ origin);
    }
    vec4(const vec2 &v, float _z, float _w) {
        X = v.X;
        Y = v.Y;
        Z = _z;
        W = _w;
    }
    vec4(const vec3 &v, float _w) {
        X = v.X;
        Y = v.Y;
        Z = v.Z;
        W = _w;
    }
    vec4(const vec4 &plane, const vec3 &a, const vec3 &b) {
        normal() = ((a - b) % plane.normal()).normalize();
        W = -(normal() ^ a);
    }

    // Operators
public:

#if 0
    operator bool () const {
        return !empty();
    }
#endif

    bool operator ! () const {
        return empty();
    }

    float &operator [] (unsigned index)	{
        return (&X)[index];
    }

    float operator [] (unsigned index) const	{
        return (&X)[index];
    }


    // Properties
public:
    vec3 &normal() {
        return *(vec3*)&X;
    }

    const vec3 &normal() const {
        return *(const vec3*)&X;
    }

    float lengthSq() const {
        return X * X + Y * Y + Z * Z + W * W;
    }

    float length() const {
        return sqrt(X * X + Y * Y + Z * Z + W * W);
    }

    bool empty() const {
        return X == 0.0f && Y == 0.0f && Z == 0.0f && W == 0.0f;
    }

    // Methods
public:
    vec4 normalize() const {
        float temp;
        return normalize(temp);
    }
    vec4 normalizeNormal() const {
        float temp;
        return normalizeNormal(temp);
    }
    vec4 normalize(float &l) const;
    vec4 normalizeNormal(float &l) const;
    Side::Enum test(const vec3 &v, float &distance) const;
    Side::Enum test(const vec3 &v) const;
    Side::Enum test(const vec3 *v, unsigned count) const;
    Side::Enum test(const sphere &v) const;
    Side::Enum test(const box &v) const;
    float collide(const ray& v) const;
    vec3 reflect(const vec3 &v) const;
    vec4 prependicular(const vec3 &a, const vec3 &b);
    void copy(float dest[4]) const;

    // Functions
public:
    static vec4 lerp(const vec4 &v1, const vec4 &v2, float amount = 0.5f);

    static vec4 lerp3(const vec4 &v1, const vec4 &v2, const vec4 &v3, float amount = 0.5f);

    static unsigned compress(const vec4 &value);

    static vec4 uncompress(unsigned value);

    static vec4 minimize(const vec4 &v1, const vec4 &v2) {
        return vec4(qMin(v1.X, v2.X), qMin(v1.Y, v2.Y), qMin(v1.Z, v2.Z), qMin(v1.W, v2.W));
    }

    static vec4 maximize(const vec4 &v1, const vec4 &v2) {
        return vec4(qMax(v1.X, v2.X), qMax(v1.Y, v2.Y), qMax(v1.Z, v2.Z), qMax(v1.W, v2.W));
    }

    // Friends

    vec4 &operator += (const vec4 &v) {
        X += v.X;
        Y += v.Y;
        Z += v.Z;
        W += v.W;
        return *this;
    }

    vec4 &operator += (const float v)	{
        X += v;
        Y += v;
        Z += v;
        W += v;
        return *this;
    }

    vec4 &operator -= (const vec4 &v) {
        X -= v.X;
        Y -= v.Y;
        Z -= v.Z;
        W -= v.W;
        return *this;
    }

    vec4 &operator -= (const float v)	{
        X -= v;
        Y -= v;
        Z -= v;
        W -= v;
        return *this;
    }

    vec4 &operator *= (const vec4 &v)	{
        X *= v.X;
        Y *= v.Y;
        Z *= v.Z;
        W *= v.W;
        return *this;
    }

    vec4 &operator *= (const float v)	{
        X *= v;
        Y *= v;
        Z *= v;
        W *= v;
        return *this;
    }

    vec4 &operator /= (const vec4 &v) {
        X /= v.X;
        Y /= v.Y;
        Z /= v.Z;
        W /= v.W;
        return *this;
    }

    vec4 &operator /= (const float v)	{
        X /= v;
        Y /= v;
        Z /= v;
        W /= v;
        return *this;
    }



    vec4 operator + (const vec4 &b) const {
        return vec4(X + b.X, Y + b.Y, Z + b.Z, W + b.W);
    }

    vec4 operator + (const float b) const {
        return vec4(X + b, Y + b, Z + b, W + b);
    }

    vec4 operator - (const vec4 &b) const {
        return vec4(X - b.X, Y - b.Y, Z - b.Z, W - b.W);
    }

    vec4 operator - (const float b) const {
        return vec4(X - b, Y - b, Z - b, W - b);
    }

    vec4 operator * (const vec4 &b) const {
        return vec4(X * b.X, Y * b.Y, Z * b.Z, W * b.W);
    }

    vec4 operator * (const float b) const {
        return vec4(X * b, Y * b, Z * b, W * b);
    }

    vec4 operator / (const vec4 &b) const {
        return vec4(X / b.X, Y / b.Y, Z / b.Z, W / b.W);
    }

    vec4 operator / (const float b) const {
        return vec4(X / b, Y / b, Z / b, W / b);
    }

    float operator ^ (const vec4 &b) const {
        return X * b.X + Y * b.Y + Z * b.Z + W * b.W;
    }

    float operator ^ (const vec3 &b) const {
        return X * b.X + Y * b.Y + Z * b.Z + W * 1.0f;
    }

    float operator % (const vec3 &b) const {
        return X * b.X + Y * b.Y + Z * b.Z + W * 0.0f;
    }

    bool operator == (const vec4 &b) const {
        return fabs(X - b.X) < Epsf && fabs(Y - b.Y) < Epsf && fabs(Z - b.Z) < Epsf && fabs(W - b.W) < Epsf;
    }

    bool operator != (const vec4 &b) const {
        return X != b.X || Y != b.Y || Z != b.Z || W != b.W;
    }

    bool operator < (const vec4 &b) const {
        return X - b.X < -Epsf && Y - b.Y < -Epsf && Z - b.Z < -Epsf && W - b.W < -Epsf;
    }

    //// Serializer
    //aDEFINE_SERIALIZE(vec4) {
    //	return ar & self.X & self.Y & self.Z & self.W;
    //}

public:
    friend QDataStream &operator<<(QDataStream &ds, const vec4 &rhs)
    {
      return ds << rhs.X << rhs.Y << rhs.Z << rhs.W;
    }

    friend QDataStream &operator>>(QDataStream &ds, vec4 &rhs)
    {
      return ds >> rhs.X >> rhs.Y >> rhs.Z >> rhs.W;
    }
};

typedef vec4 plane;

MATH_EXPORT extern const vec4 vec4Zero;
MATH_EXPORT extern const vec4 vec4Identity;
MATH_EXPORT extern const vec4 vec4One;

inline vec4 operator + (const float a, const vec4 &b) {
    return vec4(a + b.X, a + b.Y, a + b.Z, a + b.W);
}

inline vec4 operator - (const float a, const vec4 &b) {
    return vec4(a - b.X, a - b.Y, a - b.Z, a - b.W);
}

inline vec4 operator * (const float a, const vec4 &b) {
    return vec4(a * b.X, a * b.Y, a * b.Z, a * b.W);
}

inline vec4 operator / (const float a, const vec4 &b) {
    return vec4(a / b.X, a / b.Y, a / b.Z, a / b.W);
}

inline vec4 operator + (const vec4 &v) {
    return vec4(+v.X, +v.Y, +v.Z, +v.W);
}

inline vec4 operator - (const vec4 &v) {
    return vec4(-v.X, -v.Y, -v.Z, -v.W);
}

typedef QVector<vec4> Vec4List;

inline void swapOrder(vec4& value)
{
  swapOrder(value.X);
  swapOrder(value.Y);
  swapOrder(value.Z);
  swapOrder(value.W);
}

Q_DECLARE_RAW_VECTOR_SERIALIZER(vec4)

Q_DECLARE_METATYPE(vec4)
Q_DECLARE_METATYPE(Vec4List)

#endif // VEC4_HPP
