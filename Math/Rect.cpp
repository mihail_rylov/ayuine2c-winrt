//------------------------------------//
// 
// Rect.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "Rect.hpp"

//------------------------------------//
// rect: Methods

rect rect::intersect(const rect &r) const {
	rect o;

    o.Left = qMax(Left, r.Left);
    o.Top = qMax(Top, r.Top);
    o.Right = qMin(Right, r.Right);
    o.Bottom = qMin(Bottom, r.Bottom);
	return o;
}

rect rect::merge(const rect &r) const {
	rect o;

    o.Left = qMin(Left, r.Left);
    o.Top = qMin(Top, r.Top);
    o.Right = qMax(Right, r.Right);
    o.Bottom = qMax(Bottom, r.Bottom);
	return o;
}

