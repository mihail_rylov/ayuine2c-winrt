//------------------------------------//
//
// Consts.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Math
// Date: 2011-05-03
//
//------------------------------------//

#include "Vec2.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"
#include "Euler.hpp"
#include "Matrix.hpp"
#include "Box.hpp"
#include "Rect.hpp"

#include <climits>

//------------------------------------//

const vec2 vec2Zero = vec2(0.0f, 0.0f);

//------------------------------------//

const vec3 vec3Zero = vec3(0.0f, 0.0f, 0.0f);

//------------------------------------//

const vec4 vec4Zero = vec4(0.0f, 0.0f, 0.0f, 0.0f);
const vec4 vec4Identity = vec4(0.0f, 0.0f, 0.0f, 1.0f);
const vec4 vec4One = vec4(1.0f, 1.0f, 1.0f, 1.0f);

//------------------------------------//

const euler angleZero = euler(0.0f, 0.0f, 0.0f);

//------------------------------------//

const matrix mat4Zero = matrix(	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0);

const matrix mat4Identity = matrix(	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1);

//------------------------------------//

const box bboxEmpty = box(vec3(FLT_MAX, FLT_MAX, FLT_MAX), vec3(FLT_MIN, FLT_MIN, FLT_MIN));

//------------------------------------//

const rect rectEmpty = rect(vec2(FLT_MAX, FLT_MAX), vec2(FLT_MIN, FLT_MIN));
const rect rectZero = rect(0, 0);
const rect rectClip = rect(vec2(-1, -1), vec2(1, 1));
