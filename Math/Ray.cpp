//------------------------------------//
// 
// Ray.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "Ray.hpp"

//------------------------------------//
// ray: Constructor

ray::ray(const vec3 &origin, const vec3 &at) {
	Origin = origin;
	At = at;
}

//------------------------------------//
// ray: Methods

void ray::assign(const vec3 &origin, const vec3 &at) {
	Origin = origin;
	At = at;
}

ray ray::transform(const matrix &m) const {
	return ray(m.transformCoord(Origin), m.transformNormal(At));
}

vec3 ray::hit(float time) const {
	return Origin + At * time;
}

