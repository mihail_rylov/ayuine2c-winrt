#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Math.hpp"
#include "MathLib.hpp"
#include "Frustum.hpp"
#include "Matrix.hpp"
#include "Vec3.hpp"

struct euler;

struct MATH_EXPORT Camera
{
  Q_GADGET

public:
  bool ortho;
  frustum culler;
  matrix projection;
  matrix view;
  vec3 origin;
  vec3 at, up, right;
  float distance, scaleFactor;

public:
  Camera();

public:
  static void lookAt(Camera& info, const vec3& origin, const vec3& eye, float fovy = Deg2Rad * 60.0f, float aspect = 1.0f, float distance = 0);
  static void fppCamera(Camera& info, const vec3& origin, const euler& angles, float fovy = Deg2Rad * 60.0f, float aspect = 1.0f, float distance = 0);
  static void modelViewCamera(Camera& info, const vec3& origin, const euler& angles, float radius, float fovy = Deg2Rad * 60.0f, float aspect = 1.0f, float distance = 0);
  static void cubeCamera(Camera& info, const vec3& origin, CubeFace::Enum face, float distance = 0);
  static void orthoCamera(Camera& info, const vec3& origin, Axis::Enum axis, float zoom, unsigned width, unsigned height);
};

Q_DECLARE_METATYPE(Camera)

struct MATH_EXPORT LocalCamera
{
  Q_GADGET

public:
  frustum culler;
  matrix view;
  vec3 viewOrigin;
  matrix object;
  vec3 objectOrigin;

public:
  LocalCamera(const Camera& camera);
  LocalCamera(const Camera& camera, const matrix& object);
  LocalCamera(const LocalCamera& camera, const matrix& object);
};

#endif // CAMERA_HPP
