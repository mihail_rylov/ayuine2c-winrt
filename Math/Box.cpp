//------------------------------------//
// 
// Box.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "Box.hpp"
#include "Matrix.hpp"
#include "Ray.hpp"

//------------------------------------//
// box: Static Fields

const unsigned box::Quads[6][4] = {
	{3, 6, 7, 5},
	{0, 2, 4, 1},
	{1, 4, 7, 6},
	{0, 3, 5, 2},
	{2, 5, 7, 4},
	{0, 1, 6, 3}
};

const unsigned short box::Indices[2 * 3 * 6] = {
  7, 6, 3,  5, 7, 3,
  4, 2, 0,  1, 4, 0,
  7, 4, 1,  6, 7, 1,
  5, 3, 0,  2, 5, 0,
  7, 5, 2,  4, 7, 2,
  6, 1, 0,  3, 6, 0,
};

const unsigned box::Edges[12][2] = {
	{0, 1}, {0, 2}, {0, 3},
	{1, 4}, {1, 6},
	{2, 4}, {2, 5},
	{3, 5}, {3, 6},
	{4, 7},
	{5, 7},
	{6, 7},
};

//------------------------------------//
// box: Constructor

box::box(const vec3 &mins, const vec3 &maxs) {
	Mins = mins;
	Maxs = maxs;
}

box::box(const vec3 &origin, float radius) {
	Mins = origin - radius;
	Maxs = origin + radius;
}

box::box(const vec3 *v, unsigned count, unsigned stride) {
	if(v && count >= 1) {
		// Pierwszy wierzchołek zapisz jako granice bboxa
		Mins = Maxs = *v;

		// UNDONE(Stats): Dla kolejnych testuj
		for(unsigned i = 1; i < count; i++) {
			vec3 &vv = (vec3&)*((char*)v + i * stride);
			Mins = vec3::minimize(Mins, vv);
			Maxs = vec3::maximize(Maxs, vv);
		}
	}
	else {
		new(this) box();
	}
}


//------------------------------------//
// box: Methods

void box::add(const vec3 &v) {
	Mins = vec3::minimize(Mins, v);
	Maxs = vec3::maximize(Maxs, v);
}

void box::add(const vec3 *v, size_t count) {
	while(count-- > 0)
		add(*v++);
}

void box::vertexes(vec3 vertexes[8]) const {
	vertexes[0] = vec3(Mins.X, Mins.Y, Mins.Z);
	vertexes[1] = vec3(Mins.X, Mins.Y, Maxs.Z);
	vertexes[2] = vec3(Mins.X, Maxs.Y, Mins.Z);
	vertexes[3] = vec3(Maxs.X, Mins.Y, Mins.Z);
	vertexes[4] = vec3(Mins.X, Maxs.Y, Maxs.Z);
	vertexes[5] = vec3(Maxs.X, Maxs.Y, Mins.Z);
	vertexes[6] = vec3(Maxs.X, Mins.Y, Maxs.Z);
	vertexes[7] = vec3(Maxs.X, Maxs.Y, Maxs.Z);
}

void box::planes(vec4 planes[6]) const {
	planes[0] = vec4( 1, 0, 0, -Maxs.X);
	planes[1] = vec4(-1, 0, 0,  Mins.X);
	planes[2] = vec4(0,  1, 0, -Maxs.Y);
	planes[3] = vec4(0, -1, 0,  Mins.Y);
	planes[4] = vec4(0, 0,  1, -Maxs.Z);
	planes[5] = vec4(0, 0, -1,  Mins.Z);
}

box box::transform(const matrix &m) const {
	vec3 v[8];

	// Pobierz wierzchołki boxa
	vertexes(v);

	// Przetransformuj wierzchołki
    m.transformCoord(v, v, 8);

	// Utwórz nowego boxa
    return box(v, 8);
}

bool box::test(const vec3 &v) const {
	return	Mins.X <= v.X && v.X <= Maxs.X &&
					Mins.Y <= v.Y && v.Y <= Maxs.Y &&
					Mins.Z <= v.Z && v.Z <= Maxs.Z;
}

bool box::test(const box &box) const {
	// TODO : Do sprawdzenia !
	return
        ((Mins.X <= box.Mins.X && box.Mins.X <= Maxs.X) || (Mins.X <= box.Maxs.X && box.Maxs.X <= Maxs.X) || (box.Mins.X <= Mins.X && Mins.X <= box.Maxs.X) || (box.Mins.X <= Maxs.X && Maxs.X <= box.Maxs.X)) &&
         ((Mins.Y <= box.Mins.Y && box.Mins.Y <= Maxs.Y) ||(Mins.Y <= box.Maxs.Y && box.Maxs.Y <= Maxs.Y) || (box.Mins.Y <= Mins.Y && Mins.Y <= box.Maxs.Y) || (box.Mins.Y <= Maxs.Y && Maxs.Y <= box.Maxs.Y)) &&
        ((Mins.Z <= box.Mins.Z && box.Mins.Z <= Maxs.Z) || (Mins.Z <= box.Maxs.Z && box.Maxs.Z <= Maxs.Z) || (box.Mins.Z <= Mins.Z && Mins.Z <= box.Maxs.Z) || (box.Mins.Z <= Maxs.Z && Maxs.Z <= box.Maxs.Z));
}

bool box::test(const sphere &sphere) const {
	return	Mins.X <= sphere.Center.X + sphere.Radius && sphere.Center.X - sphere.Radius <= Maxs.X &&
					Mins.Y <= sphere.Center.Y + sphere.Radius && sphere.Center.Y - sphere.Radius <= Maxs.Y &&
					Mins.Z <= sphere.Center.Z + sphere.Radius && sphere.Center.Z - sphere.Radius <= Maxs.Z;
}


box box::intersect(const box &a, const box &b) {
	if(!a || !b)
		return box();

	return box(vec3::maximize(a.Mins, b.Mins), vec3::minimize(a.Maxs, b.Maxs));
}

box box::merge(const box &a, const box &b) {
	if(!a)
		return b;
	if(!b)
		return a;

	return box(vec3::minimize(a.Mins, b.Mins), vec3::maximize(a.Maxs, b.Maxs));
}

bool box::isOnEdge(const vec3& v, float diff) const {
	bool x = fabs(v.X - Mins.X) < diff || fabs(v.X - Maxs.X) < diff;
	bool y = fabs(v.Y - Mins.Y) < diff || fabs(v.Y - Maxs.Y) < diff;
	bool z = fabs(v.Z - Mins.Z) < diff || fabs(v.Z - Maxs.Z) < diff;
	return (x + y + z) >= 2 && test(v);
}

float box::collide(const ray &r) const {
	float vx[3] = {0, 0, 0};
	float maxT[3] = {0, 0, 0};
	int vxp[3] = {0, 0, 0};

	for (int i = 0; i < 3; i++)
	{
		if (r.Origin[i] <= Mins[i])
		{
			vx[i] = Mins[i];
			vxp[i] = -1;
		}
		else if (Maxs[i] <= r.Origin[i])
		{
			vx[i] = Maxs[i];
			vxp[i] = -1;
		}
		else
			vx[i] = 0;
	}

	if (vxp[0] == 0 && vxp[1] == 0 && vxp[2] == 0)
	{
		return 0; // Mean's inside
	}

	for (int i = 0; i < 3; i++)
	{
		if (vxp[i] != 0 && fabs(r.At[i]) > Epsf)
		{
			maxT[i] = (vx[i] - r.Origin[i]) / r.At[i];
			vxp[i] = 1;
		}
		else
		{
			maxT[i] = 0;
			vxp[i] = 0;
		}
	}

	int whichPlane = 0;

	if (vxp[1] != 0 && (vxp[whichPlane] == 0 || fabs(maxT[whichPlane]) < fabs(maxT[1])))
		whichPlane = 1;
	if (vxp[2] != 0 && (vxp[whichPlane] == 0 || fabs(maxT[whichPlane]) < fabs(maxT[2])))
		whichPlane = 2;

	if (maxT[whichPlane] < 0)
		return -1;

	for (int i = 0; i < 3; i++)
	{
		if (whichPlane == i)
			continue;

		vx[i] = r.Origin[i] + maxT[whichPlane] * r.At[i];
		if (vx[i] < Mins[i] || Maxs[i] < vx[i])
			return -1;
	}
	return maxT[whichPlane];
}

matrix box::toTransform() const
{
  vec3 o = center();
  vec3 s = size() * 0.5f;
  return matrix(
      s.X, 0, 0, 0,
      0, s.Y, 0, 0,
      0, 0, s.Z, 0,
      o.X, o.Y, o.Z, 1.0f);
}
