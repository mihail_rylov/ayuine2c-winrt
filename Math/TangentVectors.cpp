//------------------------------------//
// 
// TangentVectors.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "TangentVectors.hpp"

//------------------------------------//
// TangentVectors: Constructor

TangentVectors::TangentVectors(const vec3 &v1, const vec3 &v2, const vec3 &v3, const vec2 &st1, const vec2 &st2, const vec2 &st3) {
	vec3 p = v2 - v1;
	vec3 q = v3 - v1;
	vec2 puv = st2 - st1;
	vec2 quv = st3 - st1;

	// Oblicz wektory
	Tangent = (q * puv.Y - p * quv.Y).normalize();
	Binormal = (q * puv.X - p * quv.X).normalize();
	Normal = (p % q).normalize();
}

//------------------------------------//
// TangentVectors: Methods

vec3 TangentVectors::calculateTangent(const vec3 &normal) {
	return Tangent.orthogonalize(normal).normalize();
}

vec3 TangentVectors::calculateBinormal(const vec3 &normal) {
	return Binormal.orthogonalize(normal).normalize();
}

vec4 TangentVectors::calculateTangentFlag(const vec3 &normal) {
	vec3 newTangent, newBinormal;
	float result;

	// Oblicz wektory
	newTangent = calculateTangent(normal);
	newBinormal = calculateBinormal(normal);

	// Oblicz flag� binormala
	result = newBinormal ^ (normal % newTangent);
	return vec4(newTangent, (result >= 0.0f ? 1.0f : -1.0f));
}
