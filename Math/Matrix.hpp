//------------------------------------//
// 
// Matrix.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QDataStream>
#include "Math.hpp"

struct vec2;
struct vec3;
struct vec4;
struct euler;
struct rect;

//------------------------------------//
// matrix structure

struct MATH_EXPORT matrix
{
	// Fields
	float m11, m12, m13, m14;
	float m21, m22, m23, m24;
	float m31, m32, m33, m34;
	float m41, m42, m43, m44;

	// Constructor
	matrix();

	matrix(
		float a11, float a21, float a31, float a41,
		float a12, float a22, float a32, float a42,
		float a13, float a23, float a33, float a43,
		float a14, float a24, float a34, float a44);

	matrix(
		float a11, float a21, float a31,
		float a12, float a22, float a32,
		float a13, float a23, float a33);

	// Operators

#if 0
	operator bool () const;
#endif

	bool operator ! () const;

	float& operator()(size_t n) {
		return (&m11)[n];
	}

	float operator()(size_t n) const {
		return (&m11)[n];
	}

	float& operator()(size_t x, size_t y) {
		return (&m11)[y * 4 + x];
	}

	float operator()(size_t x, size_t y) const {
		return (&m11)[y * 4 + x];
	}
	
	vec4 &operator [] (size_t i);

	const vec4 &operator [] (size_t i) const;

	
	// Properties
	vec4 column(unsigned index) const;
	vec4 row(unsigned index) const;
	void column(unsigned index, const vec4 &value);
	void row(unsigned index, const vec4 &value);
	vec3 origin() const;
	vec3 origin1() const;
	euler angles() const;
	vec3 axis(unsigned i) const;
	vec3 scale() const;
	void scale(const vec3 &scale, bool origin = true);
	rect frustumBounds() const;

	bool empty() const;

	matrix inner() const;

	// Methods
	vec4 transform(const vec4 &i) const;
	vec4 transform(const vec3 &i) const;
	vec3 transformCoord(const vec3 &i) const;
	vec3 transformNormal(const vec3 &i) const;
	vec4 transformPlane(const vec4 &i) const;

	// vector Methods
	void transform(vec4 *o, const vec4 *i, size_t count) const;
	void transform(vec4 *o, const vec3 *i, size_t count) const;
	void transformCoord(vec3 *o, const vec3 *i, size_t count) const;
	void transformNormal(vec3 *o, const vec3 *i, size_t count) const;
	void transformPlane(vec4 *o, const vec4 *i, size_t count) const;

	// Functions
	static void lookAt(matrix &m, const vec3 &eye, const vec3 &at, const vec3 &up);
	static void frustum(matrix &m, const rect &bounds, float _near, float _far);
	static void perspective(matrix &m, float fovy, float aspect, float _near, float _far);
	static void perspective(matrix &m, float fovy, float aspect, float _near);
	static void clip(matrix &m, const vec4 &plane);
	static void ortho(matrix &m, const vec2& leftTop, const vec2& rightBottom, float _near, float _far);
	static void ortho(matrix &m, const rect &bounds, float _near, float _far);
	static void reflection(matrix &m, const vec4 &plane);
	static void multiply(matrix &m, const matrix &a, const matrix &b);
	static void firstPersonCamera(matrix &m, const vec3 &o, const euler &a, float z = 1.0f);
	static void modelViewCamera(matrix &m, const vec3 &o, const euler &a, float z = 1.0f);
	static void fromCols(matrix &m, const vec4 &col1, const vec4 &col2, const vec4 &col3, const vec4 &col4);
	static void fromRows(matrix &m, const vec4 &row1, const vec4 &row2, const vec4 &row3, const vec4 &row4);
	static void scaling(matrix &m, const vec3 &scale);
	static void translation(matrix &m, const vec3 &value);
	static void rotation(matrix &m, const euler &a);
	static void invert(matrix &m, const matrix &i);
	static void transpose(matrix &m, const matrix &i);
	static void transpose(matrix &m);
	static void transformation(matrix &m, const vec3 &origin, const euler &angles);
    static void transformation(matrix &m, const vec3 &origin, const euler &angles, const vec3& scale);
	static void invert(matrix &m);
    void copyTo4x4(float dest[4][4]) const;
    void copyTo3x4(float dest[3][4]) const;

	// Friends
	bool operator == (const matrix &b) const;

	bool operator != (const matrix &b) const;

	matrix operator * (const matrix &b) const;

	//// Serializer
	//aDEFINE_SERIALIZE(matrix) {
	//	return ar & self.m[0] & self.m[1] & self.m[2] & self.m[3];
	//}

public:
    friend QDataStream &operator<<(QDataStream &ds, const matrix &rhs)
    {
      ds.writeRawData((const char*)&rhs.m11, sizeof(float) * 4 * 4);
      return ds;
    }

    friend QDataStream &operator>>(QDataStream &ds, matrix &rhs)
    {
      ds.readRawData((char*)&rhs.m11, sizeof(float) * 4 * 4);
      return ds;
    }
};

MATH_EXPORT extern const matrix mat4Zero;
MATH_EXPORT extern const matrix mat4Identity;

Q_DECLARE_METATYPE(matrix)
