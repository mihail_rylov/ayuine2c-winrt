//------------------------------------//
// 
// Sphere.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QDataStream>
#include <QVector>
#include "Math.hpp"
#include "Vec3.hpp"

struct matrix;
struct ray;
struct box;

//------------------------------------//
// sphere structure

struct MATH_EXPORT sphere
{
	// Fields
	vec3 Center;
	float Radius;

	// Operators

#if 0
	operator bool () const {
		return !empty();
	}
#endif
	bool operator ! () const {
		return empty();
	}
	

	// Properties
	bool empty() const {
		return Radius < Epsf;
	}
	vec3 mins() const {
		return Center - vec3(Radius);
	}
	vec3 maxs() const {
		return Center + vec3(Radius);
	}

	// Constructor
	sphere() : Radius(0) {
	}

	sphere(const vec3 &center, float radius);

	sphere(const vec3 *v, unsigned count, unsigned stride = sizeof(vec3));

	template<typename Type>
    sphere(const QVector<Type> &verts) {
        new(this) sphere(verts.constData(), verts.size(), sizeof(vec3));
    }

	// Methods
	sphere transform(const matrix &m) const;
	bool test(const vec3 &v) const;
	bool test(const sphere &v) const;
	float collide(const ray &v) const;

    matrix toTransform() const;

	//! Rozszerza sfer�
	void inflate(float size) {
		Radius += size;
	}

	//! Przesuwa sfer�
	sphere offset(const vec3& offset) const {
		sphere self(*this);
		self.Center += offset;
		return self;
	}

	// Friends
	bool operator == (const sphere &b) const {
		return Center == b.Center && Radius == b.Radius;
	}

	bool operator != (const sphere &b) const {
		return Center != b.Center || Radius != b.Radius;
    }

public:
    friend QDataStream &operator<<(QDataStream &ds, const sphere &rhs)
    {
      return ds << rhs.Center << rhs.Radius;
    }

    friend QDataStream &operator>>(QDataStream &ds, sphere &rhs)
    {
      return ds >> rhs.Center >> rhs.Radius;
    }
};

Q_DECLARE_METATYPE(sphere)
