//------------------------------------//
// 
// Matrix.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "Matrix.hpp"
#include "Euler.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"
#include "Rect.hpp"

//------------------------------------//
// matrix: Constructors

//NEW ORDER: matrix(	float a11, float a12, float a13, float a14,
//			float a21, float a22, float a23, float a24,
//			float a31, float a32, float a33, float a34,
//			float a41, float a42, float a43, float a44)

matrix::matrix(
  float a11, float a21, float a31, float a41,
  float a12, float a22, float a32, float a42,
  float a13, float a23, float a33, float a43,
  float a14, float a24, float a34, float a44)
{
  m11 = a11; m21 = a21; m31 = a31; m41 = a41;
  m12 = a12; m22 = a22;	m32 = a32; m42 = a42;
  m13 = a13; m23 = a23; m33 = a33; m43 = a43;
  m14 = a14; m24 = a24; m34 = a34; m44 = a44;
}

matrix::matrix(
  float a11, float a21, float a31,
  float a12, float a22, float a32,
  float a13, float a23, float a33)
{
  m11 = a11; m21 = a21; m31 = a31; m41 = 0;
  m12 = a12; m22 = a22;	m32 = a32; m42 = 0;
  m13 = a13; m23 = a23; m33 = a33; m43 = 0;
  m14 = 0;   m24 = 0;   m34 = 0;   m44 = 1;
}

matrix::matrix()
{
  m11 = 0; m21 = 0; m31 = 0; m41 = 0;
  m12 = 0; m22 = 0; m32 = 0; m42 = 0;
  m13 = 0; m23 = 0; m33 = 0; m43 = 0;
  m14 = 0; m24 = 0; m34 = 0; m44 = 0;
}

//------------------------------------//
// matrix: Properties

vec4 matrix::column(unsigned index) const {
  switch (index) {
  default:	return vec4();
  case 0:		return vec4(m11, m21, m31, m41);
  case 1:		return vec4(m12, m22, m32, m42);
  case 2:		return vec4(m13, m23, m33, m43);
  case 3:		return vec4(m14, m24, m34, m44);
  }
}

vec4 matrix::row(unsigned index) const {
  switch (index) {
  default:	return vec4();
  case 0:		return vec4(m11, m12, m13, m14);
  case 1:		return vec4(m21, m22, m23, m24);
  case 2:		return vec4(m31, m32, m33, m34);
  case 3:		return vec4(m41, m42, m43, m44);
  }
}

void matrix::column(unsigned index, const vec4 &value) {
  switch(index) {
  case 0:		m11 = value.X; m21 = value.Y; m31 = value.Z; m41 = value.W; break;
  case 1:		m12 = value.X; m22 = value.Y; m32 = value.Z; m42 = value.W; break;
  case 2:		m13 = value.X; m23 = value.Y; m33 = value.Z; m43 = value.W; break;
  case 3:		m14 = value.X; m24 = value.Y; m34 = value.Z; m44 = value.W; break;
  }
}

void matrix::row(unsigned index, const vec4 &value) {
  switch(index) {
  case 0:		m11 = value.X; m12 = value.Y; m13 = value.Z; m14 = value.W; break;
  case 1:		m21 = value.X; m22 = value.Y; m23 = value.Z; m24 = value.W; break;
  case 2:		m31 = value.X; m32 = value.Y; m33 = value.Z; m34 = value.W; break;
  case 3:		m41 = value.X; m42 = value.Y; m43 = value.Z; m44 = value.W; break;
  }
}

rect matrix::frustumBounds() const {
  float inv1 = -1 / m11;
  float inv2 = -1 / m22;
  rect r;
  r.min() = vec2((m14 + 1) * inv1, (m24 - 1) * inv2);
  r.max() = vec2((m14 - 1) * inv1, (m24 + 1) * inv2);
  return r;
}

vec3 matrix::origin() const {
  return vec3(m14, m24, m34);
}

vec3 matrix::origin1() const {
  return vec3(
		-(m14 * m11 + m24 * m21 + m34 * m31),
		-(m14 * m12 + m24 * m22 + m34 * m32),
		-(m14 * m13 + m24 * m23 + m34 * m33));
}

::euler matrix::angles() const {
  float xy = sqrtf(m11 * m11 + m12 * m12);

  if(xy > Epsf) {
    return ::euler(
          -atan2f(m12, m11),
          -atan2f(-m13, xy),
          -atan2f(m23, m33));
  }
  else {
    return ::euler(
          -atan2f(-m21, m22),
          -atan2f(-m13, xy),
          0.0f);
  }
}

vec3 matrix::axis(unsigned i) const {
  return (vec3&)(&m11)[i*4];
}

vec3 matrix::scale() const {
  // Oblicz skal� macierzy
  return vec3(
		((vec3&)m11).length(),
		((vec3&)m21).length(),
		((vec3&)m31).length());
}

void matrix::scale(const vec3 &scale, bool origin) {
  // Przeskaluj macierz
  m11 *= scale.X;
  m12 *= scale.X;
  m13 *= scale.X;
  m21 *= scale.Y;
  m22 *= scale.Y;
  m23 *= scale.Y;
  m31 *= scale.Z;
  m32 *= scale.Z;
  m33 *= scale.Z;

  if(origin) {
    m14 *= scale.X;
    m24 *= scale.Y;
    m34 *= scale.Z;
  }
}

//------------------------------------//
// matrix: Methods

vec4 matrix::transform(const vec4 &i) const {
  return vec4(
		m11 * i.X + m12 * i.Y + m13 * i.Z + m14 * i.W,
		m21 * i.X + m22 * i.Y + m23 * i.Z + m24 * i.W,
		m31 * i.X + m32 * i.Y + m33 * i.Z + m34 * i.W,
		m41 * i.X + m42 * i.Y + m43 * i.Z + m44 * i.W);
}

vec4 matrix::transform(const vec3 &i) const {
  return vec4(
		m11 * i.X + m12 * i.Y + m13 * i.Z + m14 * 1.0f,
		m21 * i.X + m22 * i.Y + m23 * i.Z + m24 * 1.0f,
		m31 * i.X + m32 * i.Y + m33 * i.Z + m34 * 1.0f,
		m41 * i.X + m42 * i.Y + m43 * i.Z + m44 * 1.0f);
}

void matrix::transform( vec4 *o, const vec4 *i, size_t count ) const
{
  while(count-- > 0)
    *o++ = transform(*i++);
}

void matrix::transform( vec4 *o, const vec3 *i, size_t count ) const
{
  while(count-- > 0)
    *o++ = transform(*i++);
}

vec3 matrix::transformCoord(const vec3 &i) const {
  float invW = 1.0f / (m41 * i.X + m42 * i.Y + m43 * i.Z + m44 * 1.0f);
  return vec3(
		(m11 * i.X + m12 * i.Y + m13 * i.Z + m14 * 1.0f) * invW,
		(m21 * i.X + m22 * i.Y + m23 * i.Z + m24 * 1.0f) * invW,
		(m31 * i.X + m32 * i.Y + m33 * i.Z + m34 * 1.0f) * invW);
}

void matrix::transformCoord( vec3 *o, const vec3 *i, size_t count ) const
{
  while(count-- > 0)
    *o++ = transformCoord(*i++);
}

vec3 matrix::transformNormal(const vec3 &i) const {
  return vec3(
		m11 * i.X + m12 * i.Y + m13 * i.Z,
		m21 * i.X + m22 * i.Y + m23 * i.Z,
		m31 * i.X + m32 * i.Y + m33 * i.Z);
}

void matrix::transformNormal( vec3 *o, const vec3 *i, size_t count ) const
{
  while(count-- > 0)
    *o++ = transformNormal(*i++);
}

vec4 matrix::transformPlane(const vec4 &i) const {
  return vec4(transformNormal(i.normal()).normalize(), transformCoord(i.normal() * (-i.W)));
}

void matrix::transformPlane( vec4 *o, const vec4 *i, size_t count ) const
{
  while(count-- > 0)
    *o++ = transformPlane(*i++);
}

//------------------------------------//
// matrix: Functions

void matrix::lookAt(matrix &m, const vec3 &eye, const vec3 &at, const vec3 &up) {
  vec3 x, y, z;

#if 0
  z = (eye - at).normalize();
  x = (up % z).normalize();
  y = (z % x).normalize();
#else
  z = -(eye - at).normalize();
  x = -(up % z).normalize();
  y = -(z % x).normalize();
#endif

  m.m11 = x.X;	m.m12 = x.Y;	m.m13 = x.Z;	m.m14 = -(x ^ eye);
  m.m21 = y.X;	m.m22 = y.Y;	m.m23 = y.Z;	m.m24 = -(y ^ eye);
  m.m31 = z.X;	m.m32 = z.Y;	m.m33 = z.Z;	m.m34 = -(z ^ eye);
  m.m41 = 0;		m.m42 = 0;		m.m43 = 0;		m.m44 = 1;
}

void matrix::frustum(matrix &m, const rect &bounds, float _near, float _far) {
  // Col 1
  m.m11 = (2 * _near) / (bounds.Right - bounds.Left);
  m.m12 = 0;
  m.m13 = (bounds.Left + bounds.Right) / (bounds.Left - bounds.Right);
  m.m14 = 0;

  // Col 2
  m.m21 = 0;
  m.m22 = (2 * _near) / (bounds.Top - bounds.Bottom);
  m.m23 = (bounds.Top + bounds.Bottom) / (bounds.Bottom - bounds.Top);
  m.m24 = 0;

  // Col 3
  m.m31 = 0;
  m.m32 = 0;
  m.m33 = _far / (_far - _near);
  m.m34 = (_near * _far) / (_near - _far);

  // Col 4
  m.m41 = 0;
  m.m42 = 0;
  m.m43 = 1;
  m.m44 = 0;
}

void matrix::perspective(matrix &m, float fovy, float aspect, float _near, float _far) {
  float y = 1.0f / tan(fovy / 2.0f);
  float x = y / aspect;

  // Col 1
  m.m11 = x;
  m.m12 = 0;
  m.m13 = 0;
  m.m14 = 0;

  // Col 2
  m.m21 = 0;
  m.m22 = y;
  m.m23 = 0;
  m.m24 = 0;

  // Col 3
  m.m31 = 0;
  m.m32 = 0;
  m.m33 = _far / (_far - _near);
  m.m34 = (_near * _far) / (_near - _far);

  // Col 4
  m.m41 = 0;
  m.m42 = 0;
  m.m43 = 1;
  m.m44 = 0;
}

void matrix::perspective(matrix &m, float fovy, float aspect, float _near) {
  float y = 1.0f / tan(fovy / 2.0f);
  float x = y / aspect;

  // Col 1
  m.m11 = x;
  m.m12 = 0;
  m.m13 = 0;
  m.m14 = 0;

  // Col 2
  m.m21 = 0;
  m.m22 = y;
  m.m23 = 0;
  m.m24 = 0;

  // Col 3
  m.m31 = 0;
  m.m32 = 0;
  m.m33 = 1.0f;
  m.m34 = -_near;

  // Col 4
  m.m41 = 0;
  m.m42 = 0;
  m.m43 = 1;
  m.m44 = 0;
}

void matrix::clip(matrix &m, const vec4 &plane) {
  // Oblicz wsp�czynniki
  float z = ((vec3&)m.m31).length();
  float a = -m.m34 / z;

  // Ustaw przedni� p�aszczyzn�
  m.m31 = plane.X * z;
  m.m32 = plane.Y * z;
  m.m33 = plane.Z * z;
  m.m34 = (plane.W - a) * z;
}

void matrix::ortho(matrix &m, const vec2& leftTop, const vec2& rightBottom, float _near, float _far) {
  // Col 1
  m.m11 = 2 / (rightBottom.X - leftTop.X);
  m.m12 = 0;
  m.m13 = 0;
  m.m14 = (leftTop.X + rightBottom.X) / (leftTop.X - rightBottom.X);

  // Col 2
  m.m21 = 0;
  m.m22 = 2 / (leftTop.Y - rightBottom.Y);
  m.m23 = 0;
  m.m24 = (leftTop.Y + rightBottom.Y) / (rightBottom.Y - leftTop.Y);

  // Col 3
  m.m31 = 0;
  m.m32 = 0;
  m.m33 = 1 / (_far - _near);
  m.m34 = _near / (_near - _far);

  // Col 4
  m.m41 = 0;
  m.m42 = 0;
  m.m43 = 0;
  m.m44 = 1;
}

void matrix::ortho(matrix &m, const rect &bounds, float _near, float _far)
{
  ortho(m, bounds.min(), bounds.max(), _near, _far);
}

void matrix::reflection(matrix &m, const vec4 &plane)
{
  // Row 1
  m.m11 = -2 * plane.X * plane.X + 1;
  m.m21 = -2 * plane.Y * plane.X;
  m.m31 = -2 * plane.Z * plane.X;
  m.m41 = 0;

  // Row 2
  m.m12 = -2 * plane.X * plane.Y;
  m.m22 = -2 * plane.Y * plane.Y + 1;
  m.m32 = -2 * plane.Z * plane.Y;
  m.m42 = 0;

  // Row 3
  m.m13 = -2 * plane.X * plane.Z;
  m.m23 = -2 * plane.Y * plane.Z;
  m.m33 = -2 * plane.Z * plane.Z + 1;
  m.m43 = 0;

  // Row 4
  m.m14 = -2 * plane.X * plane.W;
  m.m24 = -2 * plane.Y * plane.W;
  m.m34 = -2 * plane.Z * plane.W;
  m.m44 = 1;
}

void matrix::multiply(matrix &m, const matrix &a, const matrix &b)
{
  // Sprawdz wskazniki
  if(&m == &a)
    return multiply(m, matrix(a), b);
  if(&m == &b)
    return multiply(m, a, matrix(b));

  // Row 1
  m.m11 = a.m11 * b.m11 + a.m12 * b.m21 + a.m13 * b.m31 + a.m14 * b.m41;
  m.m21 = a.m21 * b.m11 + a.m22 * b.m21 + a.m23 * b.m31 + a.m24 * b.m41;
  m.m31 = a.m31 * b.m11 + a.m32 * b.m21 + a.m33 * b.m31 + a.m34 * b.m41;
  m.m41 = a.m41 * b.m11 + a.m42 * b.m21 + a.m43 * b.m31 + a.m44 * b.m41;

  // Row 2
  m.m12 = a.m11 * b.m12 + a.m12 * b.m22 + a.m13 * b.m32 + a.m14 * b.m42;
  m.m22 = a.m21 * b.m12 + a.m22 * b.m22 + a.m23 * b.m32 + a.m24 * b.m42;
  m.m32 = a.m31 * b.m12 + a.m32 * b.m22 + a.m33 * b.m32 + a.m34 * b.m42;
  m.m42 = a.m41 * b.m12 + a.m42 * b.m22 + a.m43 * b.m32 + a.m44 * b.m42;

  // Row 3
  m.m13 = a.m11 * b.m13 + a.m12 * b.m23 + a.m13 * b.m33 + a.m14 * b.m43;
  m.m23 = a.m21 * b.m13 + a.m22 * b.m23 + a.m23 * b.m33 + a.m24 * b.m43;
  m.m33 = a.m31 * b.m13 + a.m32 * b.m23 + a.m33 * b.m33 + a.m34 * b.m43;
  m.m43 = a.m41 * b.m13 + a.m42 * b.m23 + a.m43 * b.m33 + a.m44 * b.m43;

  // Row 4
  m.m14 = a.m11 * b.m14 + a.m12 * b.m24 + a.m13 * b.m34 + a.m14 * b.m44;
  m.m24 = a.m21 * b.m14 + a.m22 * b.m24 + a.m23 * b.m34 + a.m24 * b.m44;
  m.m34 = a.m31 * b.m14 + a.m32 * b.m24 + a.m33 * b.m34 + a.m34 * b.m44;
  m.m44 = a.m41 * b.m14 + a.m42 * b.m24 + a.m43 * b.m34 + a.m44 * b.m44;
}

void matrix::firstPersonCamera(matrix &m, const vec3 &o, const ::euler &a, float z) {
  lookAt(m, o, o + a.at() * z, a.up());
}

void matrix::modelViewCamera(matrix &m, const vec3 &o, const ::euler &a, float z) {
  lookAt(m, o + a.at() * z, o, a.up());
}

void matrix::transformation(matrix &m, const vec3 &origin, const ::euler &angles) {
  matrix temp;
  matrix::translation(temp, origin);
  matrix::rotation(m, angles);
  matrix::multiply(m, temp, m);
}

void matrix::transformation(matrix &m, const vec3 &origin, const euler &angles, const vec3& scale) {
  matrix temp;
  matrix::translation(temp, origin);
  matrix::rotation(m, angles);
  matrix::multiply(m, temp, m);
  matrix::scaling(temp, scale);
  matrix::multiply(m, m, temp);
}

void matrix::fromCols(matrix &m, const vec4 &col1, const vec4 &col2, const vec4 &col3, const vec4 &col4)
{
  m = matrix(
		col1.X, col2.X, col3.X, col4.X,
		col1.Y, col2.Y, col3.Y, col4.Y,
		col1.Z, col2.Z, col3.Z, col4.Z,
		col1.W, col2.W, col3.W, col4.W);
}

void matrix::fromRows(matrix &m, const vec4 &row1, const vec4 &row2, const vec4 &row3, const vec4 &row4)
{
  m = matrix(
		row1.X, row1.Y, row1.Z, row1.W,
		row2.X, row2.Y, row2.Z, row2.W,
		row3.X, row3.Y, row3.Z, row3.W,
		row4.X, row4.Y, row4.Z, row4.W);
}

void matrix::scaling(matrix &m, const vec3 &scale) {
  // Row 1
  m.m11 = scale.X;
  m.m21 = 0;
  m.m31 = 0;
  m.m41 = 0;

  // Row 2
  m.m12 = 0;
  m.m22 = scale.Y;
  m.m32 = 0;
  m.m42 = 0;

  // Row 3
  m.m13 = 0;
  m.m23 = 0;
  m.m33 = scale.Z;
  m.m43 = 0;

  // Row 4
  m.m14 = 0;
  m.m24 = 0;
  m.m34 = 0;
  m.m44 = 1.0f;
}

void matrix::translation(matrix &m, const vec3 &value) {
  // Row 1
  m.m11 = 1;
  m.m21 = 0;
  m.m31 = 0;
  m.m41 = 0;

  // Row 2
  m.m12 = 0;
  m.m22 = 1;
  m.m32 = 0;
  m.m42 = 0;

  // Row 3
  m.m13 = 0;
  m.m23 = 0;
  m.m33 = 1;
  m.m43 = 0;

  // Row 4
  m.m14 = value.X;
  m.m24 = value.Y;
  m.m34 = value.Z;
  m.m44 = 1;
}

void matrix::rotation(matrix &m, const euler &a) {
  vec3 tmp;

  // Col 1
  tmp = a.at();
  m.m11 = tmp.X;
  m.m12 = tmp.Y;
  m.m13 = tmp.Z;
  m.m14 = 0;

  // Col 2
  tmp = -a.right();
  m.m21 = tmp.X;
  m.m22 = tmp.Y;
  m.m23 = tmp.Z;
  m.m24 = 0;

  // Col 3
  tmp = a.up();
  m.m31 = tmp.X;
  m.m32 = tmp.Y;
  m.m33 = tmp.Z;
  m.m34 = 0;

  // Col 4
  m.m41 = 0;
  m.m42 = 0;
  m.m43 = 0;
  m.m44 = 1;
}

void matrix::invert(matrix &m, const matrix &i) {
  if(&m == &i) {
    matrix temp(i);
    invert(m, temp);
    return;
  }

  m.m11 = (i.m22 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m23 * (i.m32 * i.m44 - i.m34 * i.m42) + i.m24 * (i.m32 * i.m43 - i.m33 * i.m42));
  m.m21 = -(i.m21 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m23 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m24 * (i.m31 * i.m43 - i.m33 * i.m41));
  m.m31 = (i.m21 * (i.m32 * i.m44 - i.m34 * i.m42) - i.m22 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m24 * (i.m31 * i.m42 - i.m32 * i.m41));
  m.m41 = -(i.m21 * (i.m32 * i.m43 - i.m33 * i.m42) - i.m22 * (i.m31 * i.m43 - i.m33 * i.m41) + i.m23 * (i.m31 * i.m42 - i.m32 * i.m41));

  m.m12 = -(i.m12 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m13 * (i.m32 * i.m44 - i.m34 * i.m42) + i.m14 * (i.m32 * i.m43 - i.m33 * i.m42));
  m.m22 = (i.m11 * (i.m33 * i.m44 - i.m34 * i.m43) - i.m13 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m14 * (i.m31 * i.m43 - i.m33 * i.m41));
  m.m32 = -(i.m11 * (i.m32 * i.m44 - i.m34 * i.m42) - i.m12 * (i.m31 * i.m44 - i.m34 * i.m41) + i.m14 * (i.m31 * i.m42 - i.m32 * i.m41));
  m.m42 = (i.m11 * (i.m32 * i.m43 - i.m33 * i.m42) - i.m12 * (i.m31 * i.m43 - i.m33 * i.m41) + i.m13 * (i.m31 * i.m42 - i.m32 * i.m41));

  m.m13 = (i.m12 * (i.m23 * i.m44 - i.m24 * i.m43) - i.m13 * (i.m22 * i.m44 - i.m24 * i.m42) + i.m14 * (i.m22 * i.m43 - i.m23 * i.m42));
  m.m23 = -(i.m11 * (i.m23 * i.m44 - i.m24 * i.m43) - i.m13 * (i.m21 * i.m44 - i.m24 * i.m41) + i.m14 * (i.m21 * i.m43 - i.m23 * i.m41));
  m.m33 = (i.m11 * (i.m22 * i.m44 - i.m24 * i.m42) - i.m12 * (i.m21 * i.m44 - i.m24 * i.m41) + i.m14 * (i.m21 * i.m42 - i.m22 * i.m41));
  m.m43 = -(i.m11 * (i.m22 * i.m43 - i.m23 * i.m42) - i.m12 * (i.m21 * i.m43 - i.m23 * i.m41) + i.m13 * (i.m21 * i.m42 - i.m22 * i.m41));

  m.m14 = -(i.m12 * (i.m23 * i.m34 - i.m24 * i.m33) - i.m13 * (i.m22 * i.m34 - i.m24 * i.m32) + i.m14 * (i.m22 * i.m33 - i.m23 * i.m32));
  m.m24 = (i.m11 * (i.m23 * i.m34 - i.m24 * i.m33) - i.m13 * (i.m21 * i.m34 - i.m24 * i.m31) + i.m14 * (i.m21 * i.m33 - i.m23 * i.m31));
  m.m34 = -(i.m11 * (i.m22 * i.m34 - i.m24 * i.m32) - i.m12 * (i.m21 * i.m34 - i.m24 * i.m31) + i.m14 * (i.m21 * i.m32 - i.m22 * i.m31));
  m.m44 = (i.m11 * (i.m22 * i.m33 - i.m23 * i.m32) - i.m12 * (i.m21 * i.m33 - i.m23 * i.m31) + i.m13 * (i.m21 * i.m32 - i.m22 * i.m31));

  float invDet = 1.0f / ((i.m11 * m.m11) + (i.m12 * m.m21) + (i.m13 * m.m31) + (i.m14 * m.m41));

  m.m11 *= invDet; m.m21 *= invDet; m.m31 *= invDet; m.m41 *= invDet;
  m.m12 *= invDet; m.m22 *= invDet; m.m32 *= invDet; m.m42 *= invDet;
  m.m13 *= invDet; m.m23 *= invDet; m.m33 *= invDet; m.m43 *= invDet;
  m.m14 *= invDet; m.m24 *= invDet; m.m34 *= invDet; m.m44 *= invDet;
}

void matrix::invert( matrix &m )
{
  matrix temp(m);
  invert(m, temp);
}

void matrix::transpose(matrix &m, const matrix &i) {
  // Row 1
  m.m11 = i.m11;
  m.m21 = i.m12;
  m.m31 = i.m13;
  m.m41 = i.m14;

  // Row 2
  m.m12 = i.m21;
  m.m22 = i.m22;
  m.m32 = i.m23;
  m.m42 = i.m24;

  // Row 3
  m.m13 = i.m31;
  m.m23 = i.m32;
  m.m33 = i.m33;
  m.m43 = i.m34;

  // Row 4
  m.m14 = i.m41;
  m.m24 = i.m42;
  m.m34 = i.m43;
  m.m44 = i.m44;
}

void matrix::transpose(matrix &m)	{
  float tmp;

  tmp = m.m12; m.m12 = m.m21; m.m21 = tmp;
  tmp = m.m13; m.m13 = m.m31; m.m31 = tmp;
  tmp = m.m14; m.m14 = m.m41; m.m41 = tmp;
  tmp = m.m23; m.m23 = m.m32; m.m32 = tmp;
  tmp = m.m24; m.m24 = m.m42; m.m42 = tmp;
  tmp = m.m34; m.m34 = m.m43; m.m43 = tmp;
}

#if 0
matrix::operator bool() const
{
  return !empty();
}
#endif

bool matrix::operator!() const
{
  return empty();
}

vec4 & matrix::operator[]( size_t i )
{
  return (vec4&)(&m11)[i * 4];
}

const vec4 & matrix::operator[]( size_t i ) const
{
  const float* v = &m11 + i*4;
  return *(const vec4*)v;
}

bool matrix::empty() const
{
  for(unsigned i = 0; i < 16; i++) {
    if((&m11)[i] != 0.0f)
      return false;
  }
  return true;
}

matrix matrix::inner() const
{
  return matrix(
		m11, m21, m31, 0,
		m12, m22, m32, 0,
		m13, m23, m33, 0,
		0,   0,   0,   1);
}

bool matrix::operator==( const matrix &b ) const
{
  if(fabs(m11-b.m11)>Epsf || fabs(m12-b.m12)>Epsf || fabs(m13-b.m13)>Epsf || fabs(m14-b.m14)>Epsf)
    return false;
  if(fabs(m21-b.m21)>Epsf || fabs(m22-b.m22)>Epsf || fabs(m23-b.m23)>Epsf || fabs(m24-b.m24)>Epsf)
    return false;
  if(fabs(m31-b.m31)>Epsf || fabs(m32-b.m32)>Epsf || fabs(m33-b.m33)>Epsf || fabs(m34-b.m34)>Epsf)
    return false;
  if(fabs(m41-b.m41)>Epsf || fabs(m42-b.m42)>Epsf || fabs(m43-b.m43)>Epsf || fabs(m44-b.m44)>Epsf)
    return false;
  return true;
}

bool matrix::operator!=( const matrix &b ) const
{
  return !(*this == b);
}

matrix matrix::operator*( const matrix &b ) const
{
  matrix o;
  matrix::multiply(o, *this, b);
  return o;
}

void matrix::copyTo4x4(float dest[4][4]) const
{
  dest[0][0] = m11; dest[0][1] = m12; dest[0][2] = m13; dest[0][3] = m14;
  dest[1][0] = m21; dest[1][1] = m22; dest[1][2] = m23; dest[1][3] = m24;
  dest[2][0] = m31; dest[2][1] = m32; dest[2][2] = m33; dest[2][3] = m34;
  dest[3][0] = m41; dest[3][1] = m42; dest[3][2] = m43; dest[3][3] = m44;
}

void matrix::copyTo3x4(float dest[3][4]) const
{
  dest[0][0] = m11; dest[0][1] = m12; dest[0][2] = m13; dest[0][3] = m14;
  dest[1][0] = m21; dest[1][1] = m22; dest[1][2] = m23; dest[1][3] = m24;
  dest[2][0] = m31; dest[2][1] = m32; dest[2][2] = m33; dest[2][3] = m34;
}
