//------------------------------------//
// 
// SphericalHarmonics.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "SphericalHarmonics.hpp"
#include "Vec4.hpp"

#include <ctime>

//------------------------------------//
// SphericalHarmonics: Static Fields

const unsigned SphericalHarmonics::_quality = 16;
bool SphericalHarmonics::_initialized = false;
float SphericalHarmonics::_weight = 2.0f * (float)Pi;
SphericalHarmonics::Sample	SphericalHarmonics::_samples[_quality * _quality];

//------------------------------------//
// SphericalHarmonics: Functions

static double factorial(unsigned x) {
	static double F[33] = {0.0};

	// Sprawd� czy mamy tyle warto�ci
    if(x >= 33)
		return 0;

	// Oblicz warto�ci
	if(!F[0]) {
		F[0] = 1;
        for(unsigned i = 1; i < 33; i++)
			F[i] = F[i - 1] * i;
	}

	// Zwr�� warto��
	return F[x];
}

static double P(int l, int m, double x) {
	double pmm = 1.0;
	if (m > 0) {
		double h = ::sqrt((1.0-x)*(1.0+x)),
					 f = 1.0;
		for (int i = 1; i <= m; i++) {
			pmm *= -f * h;
			f += 2.0;
		}
	}
	if (l == m)
		return pmm;
	else {
		double pmmp1 = x * (2 * m + 1) * pmm;
		if (l == (m+1))
			return pmmp1;
		else {
			double pll = 0.0;
			for (int ll = m+2; ll <= l; ll++) {
				pll = (x * (2 * ll - 1) * pmmp1 - (ll + m - 1) * pmm) / (ll - m);
				pmm = pmmp1;
				pmmp1 = pll;
			}
			return pll;
		}
	}
}

static double K(int l, int m) {
	return sqrt((2 * l + 1) / (Pi * 4) * factorial(l - m) / factorial(l + m));
}

static double sphericalHarmonicY(int l, int m, double theta, double phi) {
	double a;
	int sm;

	// Warto�� absolutna
	sm = abs(m);

	// Wyznacz k�t
	if(m > 0)
		a = cos(sm * phi) * sqrt(2.0);
	else if(m < 0)
		a = sin(sm * phi) * sqrt(2.0);
	else
		a = 1.0f;

	// Wynik
	return P(l, sm, cos(theta)) * K(l, sm) * a;
}

vec3 SphericalHarmonics::fromSphere(const vec2 &v) {
	float stheta, ctheta;
	float sphi, cphi;

	// Oblicz k�ty
	stheta = sinf(v.X);
	ctheta = cosf(v.X);
	sphi = sinf(v.Y);
	cphi = cosf(v.Y);

	// Zwr�� wektor
	return vec3(cphi * stheta, sphi * stheta, ctheta);
}

vec2 SphericalHarmonics::toSphere(const vec3 &v) {
	// Oblicz k�ty
	return vec2(acosf(v.Z), atan2f(v.Y, v.X));
}

void SphericalHarmonics::computeSamples() {
    // double coeffs[16] = {0};

	// Obliczyli�my ju� �rodowisko
	if(_initialized)
		return;
	_initialized = true;

	// Oblicz skal�
	double invQuality = 1.0 / _quality;
    // double invQuality2 = 2.0 * invQuality;

	// Zainiciuj losowanie
    srand((unsigned)time(NULL));

	// Wype�nij �rodowisko
	for(unsigned i = 0; i < _quality; i++) {
		for(unsigned j = 0; j < _quality; j++) {
			// Oblicz parametry
			double r1 = (double)rand() / (double)RAND_MAX;
			double r2 = (double)rand() / (double)RAND_MAX;
			double theta = 2.0 * ::acos(::sqrt(1 - (j + r2) * invQuality));		// polar: 0 to PI
			double phi = 2.0 * Pi * (i + r1) * invQuality;											// azimuth: 0 to 2PI

			// Pobierz sampla
			Sample &sample = _samples[i * _quality + j];

			// Skonfiguruj sampla
			sample.Normal = fromSphere(vec2((float)theta, (float)phi));
			sample.sphere = toSphere(sample.Normal);
			sample.Normal = sample.Normal.normalize();

			// Oblicz wsp�czynniki
			for(int l = 0; l < 4; l++) {
				for(int m = -l; m <= l; m++) {
					sample.Coeffs[l * (l + 1) + m] = sphericalHarmonicY(l, m, theta, phi);
				}
			}
		}
	}
}

void SphericalHarmonics::simpleLight(const vec3 &dir, float intensity, matrix& out) {
	vec2 s = toSphere(dir);

	// Oblicz �rodowisko
	computeSamples();

	// Oblicz parametry �wiat�a
	for(int l = 0; l < 4; l++) {
		for(int m = -l; m <= l; m++) {
			out(l * (l + 1) + m) = intensity * (float)sphericalHarmonicY(l, m, s.X, s.Y);
		}
	}
}

void SphericalHarmonics::directionalLight(const vec3 &dir, float intensity, matrix& out) {
	double coeffs[16];

	// Oblicz �rodowisko
	computeSamples();

	// Wyzeruj wsp�czynniki
	memset(coeffs, 0, sizeof(coeffs));

	// Uwzgl�dnij ka�dy sampel
    for(unsigned i = 0; i < _quality * _quality; i++) {
		Sample& s = _samples[i];
        double scalar = intensity * qBound(-(dir ^ s.Normal), 0.0f, 1.0f);

		// Dodaj wsp�czynniki
		for(unsigned j = 0; j < 16; j++)
			coeffs[j] += scalar * s.Coeffs[j];
	}

	// Znormalizuj wsp�czynniki
    double scalar = _weight / (double)(_quality * _quality);
	for(unsigned j = 0; j < 16; j++)
		out(j) = (float)(coeffs[j] * scalar);
}

void SphericalHarmonics::sphericalLight(const vec3 &origin, float radius, float intensity, matrix& out) {
	vec3 normal;
	float length;
	double coeffs[16];

	// Wyzeruj wsp�czynniki
	memset(coeffs, 0, sizeof(coeffs));

	// Znormalizuj pozycje
	normal = origin.normalize(length);

	// Sprawd� czy �wiat�o znajduje si� w zasi�gu
	if(length > radius) {
		out[0] = vec4();
		out[1] = vec4();
		out[2] = vec4();
		out[3] = vec4();
		return;
	}

	// Oblicz sta��
	double constant = (intensity * 1) / (length * ::sqrt(2.0 * Pi));

	// Oblicz ka�dy sampel
	//ArrayEach(Sample, i, _samples) {
    for(unsigned ii = 0; ii < _quality * _quality; ii++) { Sample *i = _samples + ii;
		double h = i->Normal ^ normal;
		double x = ::acos(h);
		double scalar = constant * ::exp(-(x * x * x) / (2 * length * length)) * h;

		// Dodaj wsp�czynniki
		for(unsigned j = 0; j < 16; j++)
			coeffs[j] += scalar * i->Coeffs[j];
	}

	// Znormalizuj wsp�czynniki
    double scalar = _weight / (double)(_quality * _quality);
	for(unsigned j = 0; j < 16; j++)
		out(j) = float(coeffs[j] * scalar);
}

void SphericalHarmonics::coneLight(const vec3 &origin, const vec3 &dir, float radius, float intensity, matrix& out) {
	double coeffs[16];

	// Wyzeruj wsp�czynniki
	memset(coeffs, 0, sizeof(coeffs));

	// Znormalizuj pozycje
	float length = origin.length();

	// Sprawd� czy �wiat�o znajduje si� w zasi�gu
	if(length > radius)
		return;

	// Oblicz sta��
	float constant = (intensity * 1) / (length * sqrt(2.0f * (float)Pi));

	// Oblicz ka�dy sampel
	//ArrayEach(Sample, i, _samples) {
    for(unsigned ii = 0; ii < _quality * _quality; ii++) { Sample *i = _samples + ii;
		double h = i->Normal ^ dir;

		if(h > 0.0f) {
			double x = ::acos(h);
			double scalar = constant * ::exp(-(x * x * x) / (2 * length * length)) * h;

			// Dodaj wsp�czynniki
			for(unsigned j = 0; j < 4; j++)
				coeffs[j] += scalar * i->Coeffs[j];
		}
	}

	// Znormalizuj wsp�czynniki
    double scalar = _weight / (double)(_quality * _quality);
	for(unsigned j = 0; j < 16; j++)
		out(j) = float(coeffs[j] * scalar);
}

static double clearSkyLight(const vec2 &sample, const vec2 &sun, float zenithIntensity) {
	if(sample.X > Pi / 2.0f || fabs(::cos(sample.X)) < 0.01f)
		return zenithIntensity * (1.0 + 2.0 * ::cos(sample.X)) / 3.0;

	double Az = fabs(sample.Y - sun.Y);
	double xi = ::acos(::cos(sun.X) * ::cos(sample.X) + ::sin(sun.X) * ::sin(sample.X) * ::cos(Az));
	double top = 1.0 - ::exp(-0.32 / ::cos(sample.X)) * (0.91 + 10 * ::exp(-3 * xi) + 0.45 * ::pow(::cos(xi), 2));
	double bottom = (0.91 + 10 * ::exp(-3 * sun.X) + 0.45 * ::pow(::cos(sun.X), 2)) * 0.274;
	return zenithIntensity * top / bottom;
}

void SphericalHarmonics::clearSkyLight(const vec2 &sun, float zenithIntensity, matrix& out) {
	double coeffs[16];

	// Oblicz �rodowisko
	computeSamples();

	// Wyzeruj wsp�czynniki
	memset(coeffs, 0, sizeof(coeffs));

	// Oblicz ka�dy sampel
    for(unsigned i = 0; i < _quality * _quality; i++) {
		Sample &s = _samples[i];
		double scalar = ::clearSkyLight(s.sphere, sun, zenithIntensity);

		// Dodaj wsp�czynniki
		for(unsigned j = 0; j < 16; j++)
			coeffs[j] += scalar * s.Coeffs[j];
	}

	// Znormalizuj wsp�czynniki
    double scalar = _weight / (double)(_quality * _quality);
	for(unsigned j = 0; j < 16; j++)
		out(j) = float(coeffs[j] * scalar);
}

static double overcastLight(const vec2 &sample, float zenithIntensity) {
	return zenithIntensity * (1.0 + 2.0 * ::cos(sample.X)) / 3.0;
}

void SphericalHarmonics::overcastLight(float zenithIntensity, matrix& out) {
	double coeffs[16];

	// Oblicz �rodowisko
	computeSamples();

	// Wyzeruj wsp�czynniki
	memset(coeffs, 0, sizeof(coeffs));

	// Oblicz ka�dy sampel
    for(unsigned i = 0; i < _quality * _quality; i++) {
		Sample &s = _samples[i];
		double scalar = ::overcastLight(s.sphere, zenithIntensity);

		// Dodaj wsp�czynniki
		for(unsigned j = 0; j < 16; j++)
			coeffs[j] += scalar * s.Coeffs[j];
	}

	// Znormalizuj wsp�czynniki
    double scalar = _weight / (double)(_quality * _quality);
	for(unsigned j = 0; j < 16; j++)
		out(j) = float(coeffs[j] * scalar);
}
