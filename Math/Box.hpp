//------------------------------------//
// 
// Box.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QDataStream>
#include <QVector>
#include "Math.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"
#include "Sphere.hpp"

struct sphere;
struct matrix;

//------------------------------------//
// box structure

struct MATH_EXPORT box
{
    Q_GADGET
    Q_PROPERTY(vec3 mins READ mins WRITE setMins)
    Q_PROPERTY(vec3 maxs READ maxs WRITE setMaxs)
    Q_PROPERTY(bool empty READ empty)
    Q_PROPERTY(vec3 center READ center)
    Q_PROPERTY(float radius READ radius)
    Q_PROPERTY(sphere range READ range)
    Q_PROPERTY(vec3 size READ size)

public:
  // Static Fields
  static const unsigned Quads[6][4];
  static const unsigned short Indices[2 * 3 * 6];
  static const unsigned Edges[12][2];

public:
  // Fields
  vec3 Mins, Maxs;

public:
  vec3 mins() const { return Mins; }
  void setMins(const vec3 &v) { Mins = v; }
  vec3 maxs() const { return Maxs; }
  void setMaxs(const vec3 &v) { Maxs = v; }

public:
  // Constructor
  Q_INVOKABLE box() {
  }

  Q_INVOKABLE box(const vec3 &mins, const vec3 &maxs);

  Q_INVOKABLE box(const vec3 &origin, float radius);

  box(const vec3 *v, unsigned count, unsigned stride = sizeof(vec3));


  template<typename Type>
  box(const QVector<Type> &verts) {
    new(this) box(verts.constData(), verts.size(), sizeof(Type));
  }


  // Operators
public:
  bool operator ! () const {
    return empty();
  }


  // Properties
  bool empty() const {
    return Maxs.X < Mins.X || Maxs.Y < Mins.Y || Maxs.Z < Mins.Z;
  }

  vec3 center() const {
    return (Mins + Maxs) * 0.5f;
  }

  float radius() const {
    return (Maxs - Mins).length() * 0.5f;
  }

  sphere range() const {
    return sphere(center(), radius());
  }

  vec3 size() const {
    return Maxs - Mins;
  }

  // Methods
  Q_INVOKABLE void clear() {
    Mins = FLT_MAX;
    Maxs = -FLT_MAX;
  }

  //! Dodaje wierzcho�ek do boxa
  Q_INVOKABLE void add(const vec3 &v);

  //! Dodaje list� wierzcho�k�w do boxa
  void add(const vec3 *v, size_t count);

  //! Rozszerza boxa
  Q_INVOKABLE void inflate(const vec3& size) {
    Mins -= size;
    Maxs += size;
  }

  //! Rozszerza boxa
  Q_INVOKABLE void inflate(float size) {
    Mins -= size;
    Maxs += size;
  }

  //! Wyznacza wierzcho�ki boxa
  void vertexes(vec3 vertexes[8]) const;

  //! Wyznacza p�aszczyzny boxa
  void planes(vec4 planes[6]) const;

  //! Transformuje boxa
  Q_INVOKABLE box transform(const matrix &m) const;

  //! Wyznacza kolizj� promienia z boxem
  Q_INVOKABLE float collide(const ray &v) const;

  //! Sprawdza czy punkt znajduje si� w boxie
  Q_INVOKABLE bool test(const vec3 &v) const;

  //! Sprawdza czy boxy nachodz� na siebie
  Q_INVOKABLE bool test(const box &a) const;

  //! Sprawdza czy box i sfera nachodz� na siebie
  Q_INVOKABLE bool test(const sphere &s) const;

  //! Sprawdza czy punkt le�y na kraw�dzi
  Q_INVOKABLE bool isOnEdge(const vec3& v, float diff = 0.01f) const;

  matrix toTransform() const;

  // Functions
public:
  //! Wyznacza cz�� wsp�ln� z dw�ch box�w
  static box intersect(const box& a, const box &b);

  //! ��czy dwa boxy
  static box merge(const box& a, const box &b);

  // Friends
public:
  box& operator += (const vec3 &v) {
    add(v);
    return *this;
  }

  box& operator += (const box &other) {
    return *this = merge(*this, other);
  }

  box& operator *= (const box &other) {
    return *this = intersect(*this, other);
  }

  box operator + (const vec3 &v) const {
    box bb(*this);
    bb.add(v);
    return bb;
  }

  box operator + (const box &other) const {
    return merge(*this, other);
  }

  box operator * (const box &other) const {
    return intersect(*this, other);
  }

  bool operator == (const box &b) const {
    return Mins == b.Mins && Maxs == b.Maxs;
  }

  bool operator != (const box &b) const {
    return Mins != b.Mins || Maxs != b.Maxs;
  }

public:
  friend QDataStream &operator<<(QDataStream &ds, const box &rhs)
  {
    return ds << rhs.Mins << rhs.Maxs;
  }

  friend QDataStream &operator>>(QDataStream &ds, box &rhs)
  {
    return ds >> rhs.Mins >> rhs.Maxs;
  }
};

MATH_EXPORT extern const box bboxEmpty;

Q_DECLARE_METATYPE(box)

