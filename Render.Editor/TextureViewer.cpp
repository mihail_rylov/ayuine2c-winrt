#include "TextureViewer.hpp"
#include "ui_TextureViewer.h"

#include <Render/Texture.hpp>
#include <QPicture>
#include <QImage>

Q_REGISTER_RESOURCE_EDITOR(Texture, TextureViewer)

TextureViewer::TextureViewer(QWidget *parent) :
    ResourceEditor(parent),
    ui(new Ui::TextureViewer)
{
    ui->setupUi(this);
}

TextureViewer::~TextureViewer()
{
  delete ui;
}

const QMetaObject * TextureViewer::resourceType() const
{
  return &Texture::staticMetaObject;
}

QSharedPointer<Resource> TextureViewer::resource() const
{
  return m_texture;
}

bool TextureViewer::setResource(const QSharedPointer<Resource> &newResource)
{
  if(newResource.isNull())
  {
    ui->imageView->setPixmap(QPixmap());
    m_texture.clear();
    setPropertyObject(NULL);
    updateUi();
    return true;
  }

  if(newResource.objectCast<Texture>().isNull())
    return false;

  m_texture = newResource.objectCast<Texture>();
  connect(m_texture.data(), SIGNAL(objectNameChanged()), SLOT(updateUi()));

  ui->imageView->setPixmap(QPixmap::fromImage(m_texture->toImage()));

  setPropertyObject(m_texture.data());
  updateUi();
  return true;
}

void TextureViewer::updateScaledContents(bool checked)
{
  ui->imageView->setScaledContents(checked);
  ui->imageView->resize(ui->scrollArea->size());
}

