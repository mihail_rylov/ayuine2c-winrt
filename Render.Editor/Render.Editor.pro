TARGET = Render.Editor
TEMPLATE = lib
CONFIG -= precompile_header
CONFIG += editor

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lPipeline -lEditor

HEADERS += \
    TextureViewer.hpp

SOURCES += \
    TextureViewer.cpp \
    TexturePreviewGenerator.cpp

FORMS += \
    TextureViewer.ui

