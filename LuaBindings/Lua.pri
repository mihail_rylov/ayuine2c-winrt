LUAHEADER = $$PWD/../$$TARGET/$${TARGET}Lua.hpp

exists($$LUAHEADER) {
  INCLUDEPATH += $$PWD/common \
             $$PWD/include

  HEADERS += $$PWD/common/lqt_common.hpp \
            $$PWD/common/lqt_qt.hpp \
            $$PWD/include/lua.h \
            $$PWD/include/lualib.h \
            $$PWD/include/lauxlib.h

  SOURCES += $$PWD/common/lqt_common.cpp \
            $$PWD/common/lqt_qt.cpp

	LIBS += -L$$PWD/lib
#-llua5.1

  LUAXML = $$OUT_PWD/$${TARGET}Lua.xml

  luacpptoxml.name = cpptoxml
  luacpptoxml.input = LUAHEADER
  luacpptoxml.output = $$LUAXML
  luacpptoxml.commands = $$PWD/bin/cpptoxml -C $$PWD/pp-configuration -Q $$PWD/../ ${QMAKE_FILE_IN} -o ${QMAKE_FILE_OUT}
  luacpptoxml.variable_out = OTHER_FILES

  luagenerator.name = lua generator
  luagenerator.input = $$LUAXML
  luagenerator.dependency_type = TYPE_C
  luagenerator.output = $$OUT_PWD/$${TARGET}Lua.cpp
	luagenerator.commands = $$PWD/bin/lua5.1.exe $$PWD/generator/generator.lua ${QMAKE_FILE_IN} -o ${QMAKE_FILE_OUT} -i $${TARGET}.hpp -t $$PWD/generator/types.lua -n $${TARGET}
  luagenerator.variable_out = SOURCES

  QMAKE_EXTRA_COMPILERS += luacpptoxml luagenerator
}
