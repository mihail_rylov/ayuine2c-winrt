@echo off

call :gen Math
goto eof

:gen
echo Math binding
mkdir %1_src 2>nul
bin\cpptoxml.exe -C pp-configuration -Q ..\ ..\%1\%1.hpp -o %1_src\%1.xml
bin\lua5.1.exe generator\generator.lua %1_src\%1.xml -i ..\%1\%1.hpp -t generator\types.lua -n %1
:eof
